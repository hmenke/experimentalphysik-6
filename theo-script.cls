\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{theo-script}

\LoadClassWithOptions{scrbook}

% % %
% % % KOMA Options
% % %

\KOMAoption{open}{right}
\KOMAoption{twoside}{true}
\KOMAoption{listof}{totoc}
\KOMAoption{index}{totoc}
\KOMAoption{numbers}{noenddot}
\KOMAoption{parskip}{half-}
\KOMAoption{bibliography}{totoc}
\KOMAoption{cleardoublepage}{empty}
\KOMAoption{captions}{nooneline}

% % %
% % % Common
% % %

\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenx}
\RequirePackage[T1]{fontenc}
\RequirePackage{currfile}
\RequirePackage{geometry}

% % %
% % % Color
% % %

\RequirePackage[dvipsnames,svgnames,x11names]{xcolor}

% % %
% % % Mathematics
% % %

\RequirePackage{mathtools}
\RequirePackage{amssymb}
\RequirePackage{braket}
\RequirePackage{chemformula,chemfig}
\RequirePackage{siunitx}
\RequirePackage{textcomp}
\sisetup{
  mode = math,
  exponent-product = \cdot,
  number-math-rm = \ensuremath
} % \ensuremath is an ugly hack to get lining figures in siunitx
\allowdisplaybreaks[3]

% % %
% % % Index and Table of contents
% % %

\RequirePackage{imakeidx}

\RequirePackage{tocstyle}
\usetocstyle{nopagecolumn}
\settocstylefeature[0]{entryhook}{\usekomafont{chapterentry}}
\settocstylefeature{pagenumberhook}{\normalfont\color{gray}}

% % %
% % % hyperref
% % %

\RequirePackage{hyperref}
\hypersetup{
  unicode,
  breaklinks=true,
  colorlinks=false,
  pdfborder={0 0 0},
  bookmarksdepth=3,
  pdfdisplaydoctitle=true,
  bookmarksnumbered=true,
}

\numberwithin{equation}{chapter}
\setcounter{secnumdepth}{3}

% % %
% % % Headers
% % %

\RequirePackage{scrlayer-scrpage}

\def\myHeaderRule{|}
\setkomafont{pageheadfoot}{\normalfont\small\scshape\color{gray}}
\setkomafont{pagenumber}{\normalfont\color{gray}}
\renewcommand{\chaptermark}[1]{\markboth{\llap{\thechapter~\myHeaderRule}~\textls{#1}}{\textls{#1}~\rlap{\myHeaderRule~\thechapter}}}
\renewcommand{\sectionmark}[1]{\markleft{\llap{\thesection~\myHeaderRule}~\textls{#1}}}
\clearscrheadfoot
\rohead[]{\rightmark}
\lehead[]{\leftmark}
\ofoot[\pagemark]{\pagemark}
\ifoot[\tiny\currfilebase]{\tiny\currfilebase}
\pagestyle{scrheadings}
\renewcommand{\chapterpagestyle}{scrheadings}
\renewcommand{\indexpagestyle}{scrheadings}

% % %
% % % Graphic
% % %

\RequirePackage{etex}
\RequirePackage{tikz}
\RequirePackage{pgfplots}
\RequirePackage{booktabs}

% % %
% % % Fonts
% % %

\setkomafont{chapter}{\color{MidnightBlue}\Huge\bfseries\rmfamily\raggedright}
\setkomafont{chapterentry}{\color{MidnightBlue}\bfseries\rmfamily}
\addtokomafont{sectioning}{\bfseries\rmfamily}
\setkomafont{descriptionlabel}{\bfseries\rmfamily}
\setkomafont{minisec}{\bfseries\rmfamily}
\setkomafont{paragraph}{\bfseries\rmfamily}
\setkomafont{subparagraph}{\bfseries\rmfamily}

\setkomafont{caption}{\bfseries\small\color{gray}}
\setkomafont{captionlabel}{\normalfont\small\color{gray}}

\renewcommand*{\chapterformat}{%
  \sbox0{\fontsize{100pt}{120pt}\selectfont\thechapter}%
  \dp0=0pt\box0\quad}
\renewcommand*\chapterheadstartvskip{\vskip5pt plus 2pt minus 2pt}
\renewcommand*\chapterheadendvskip{\vskip15pt plus 5pt minus 5pt}

\RequirePackage{remreset}
\@removefromreset{figure}{chapter}
\@removefromreset{table}{chapter}
\renewcommand*\thefigure{\arabic{figure}}
\renewcommand*\thetable{\arabic{table}}
\renewcommand*\figureformat{$\blacktriangleright$ \thefigure}
\renewcommand*\tableformat{$\blacktriangleright$ \thetable}
\renewcommand*\captionformat{\quad}
\setcapindent{0pt}
\textfloatsep=20.0pt plus 5.0pt minus 5.0pt

% % %
% % % Bibliography
% % %

\RequirePackage{csquotes}
\RequirePackage[
backend=biber,firstinits=true,sorting=none,
doi=false,url=false,
]{biblatex}
\renewbibmacro{in:}{}
\DefineBibliographyStrings{ngerman}{%
  andothers = {et\addabbrvspace al\adddot}
}
