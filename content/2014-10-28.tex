\chapter{Halbleiter}

Halbleiter sind von überragender Bedeutung für die Elektronik,
Optoelektronik und Photonik. Sie besitzen ein hohes Potential für
Quantentechnologien. Es folgen einige Beispiele.

In der Elektronik finden Halbleiter ihren Einsatz in der
Silizium-Technologie zur Herstellung von Prozessoren, Speicherchips
und Detektoren.

In den Bereichen Optoelektronik und Photonik, sowie
Quantentechnologien kommen Halbleiter vor allem in Form von
Lichtquellen zum Einsatz.  Ein sehr bekanntes Beispiel ist der
Halbleiter-Laser, bei welchen das aktive Material, welches den
Laserübergang macht, ein Halbleiter ist. In der optischem
Nachrichtentechnik kommen Halbleiter-Laser der Wellenlängen
\SI{1.3}{\micro\m} und \SI{1.5}{\micro\m} zum Einsatz.  Das Schreiben
und Lesen von optischen Datenträgern, wie CD, DVD oder BluRay erfolgt
ebenfalls mittels eines Halbleiter-Lasers.  Ohne explizite Beispiele
kommen Halbleiter-Laser ebenfalls zum Einsatz in der
Laser-Display-Technolgie, der Sensortechnik und der Medizintechnik.

Halbleiter können benutzt werden um Einzel-Photonen-Lichtquellen zu
realisieren.  Auf diesen bauen viele Verfahren der
Quantenkryptographie auf, sowie Elemente des Quantencomputers im
Allgemeinen.

Kürzlich erst mit dem Nobelpreis geehrt und mittlerweile sehr
verbreitet sind Halbleiter in Form von LEDs (Leuchtdioden, von engl.:
\textsl{light-emitting diode}).  Die mannigfaltigen Einsatzgebiete
erstrecken sich über die Beleuchtung von Anzeigeinstrumenten, Ampeln
und Scheinwerfern bis hin zur Raumbeleuchtung.  Der Nobelpreis in
Physik 2014 wurde vergeben für die Entwicklung der blauen und weißen
LED.

\minisec{Besondere Eigenschaften}

Die elektrische Leitfähigkeit $\sigma$ ist stark temperaturabhängig
(exponetionell) und kann durch geringfügige Materialzusätze um viele
Größenordnungen variiert werden.  Besonders charakteristisch und
wichtig für viele Anwendungen ist die Bandlücke zwischen dem vollen
Valenzband (Abk.:~VB) und dem leeren Leitungsband (Abk.:~LB) (volles
Valenzband und leeres Leitungsband gilt nur bei $T=\SI{0}{\kelvin}$).

\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[
    gfx,scale=1.5,
    electron/.style = {
      circle,
      fill=black,
      inner sep=2pt
    },
    defect/.style = {
      circle,
      draw=black,
      fill=white,
      inner sep=2pt
    }
    ]
    \begin{scope}
      \draw[->] (0,0) -- node[below] {Metall} (2,0) node[below] {$x$};
      \draw[->] (0,0) -- (0,2) node[left] {$E$};
      \draw[opacity=0.5,fill=MidnightBlue] (0,0.2) rectangle (2,0.7);
      \draw[opacity=0.5,fill=DarkOrange3] (0,0.5) rectangle (2,1);
      \draw[dashed] (-0.1,0.6) node[left] {$E_F$} -- (2,0.6);
      \foreach \p in {(0.2,0.4), (0.6,0.5), (1,0.4), (1.4,0.5), (1.8,0.4), (1.6,0.8)} {
        \node[electron] at \p {};
      }
    \end{scope}
    
    \begin{scope}[xshift=2.8cm]
      \draw[->] (0,0) -- node[below] {Halbleiter} (2,0) node[below] {$x$};
      \draw[->] (0,0) -- (0,2) node[left] {$E$};
      \draw[opacity=0.5,fill=MidnightBlue] (0,0.2) rectangle (2,0.7);
      \draw[opacity=0.5,fill=DarkOrange3] (0,0.9) rectangle (2,1.4);
      \draw[dashed] (-0.1,0.8) node[left] {$E_F$} -- (2,0.8);
      \foreach \p in {(0.2,0.4), (1,0.4), (1.4,0.5), (1.8,0.4)} {
        \node[electron] at \p {};
      }
      \node[defect] (p) at (0.6,0.5) {};
      \node[electron] (n) at (0.7,1.1) {};
      \draw[->] (p) edge[bend left] (n);
    \end{scope}
    
    \begin{scope}[xshift=5.6cm]
      \draw[->] (0,0) -- node[below] {Isolator} (2,0) node[below] {$x$};
      \draw[->] (0,0) -- (0,2) node[left] {$E$};
      \draw[opacity=0.5,fill=MidnightBlue] (0,0.2) rectangle (2,0.7);
      \draw[opacity=0.5,fill=DarkOrange3] (0,1.3) rectangle (2,1.8);
      \draw[dashed] (-0.1,1) node[left] {$E_F$} -- (2,1);
      \foreach \p in {(0.2,0.4), (0.6,0.5), (1,0.4), (1.4,0.5), (1.8,0.4)} {
        \node[electron] at \p {};
      }
    \end{scope}
  \end{tikzpicture}
  \caption{Das Bändermodell. Das Leitungsband ist orange, das
    Valenzband blau dargestellt. Schwarz ausgefüllte Kreise sind
    Elektronen, weiß ausgefüllte Kreise sind Defektstellen.}
  \label{fig:5.1}
\end{figure}

\section{Daten einiger wichtiger Halbleiter}

Im Folgenden betrachten wir zwei Arten von Halbleitern:
Element-Halbleiter und Verbindungs-Halbleiter.

Typische \acct{Element-Halbleiter} sind Silizium (\ch{Si}) oder
Germanium (\ch{Ge}).  Eine Mischung der $s$- und $p$-Wellenfunktion
führt zu einem tetraedrischen Bindungsorbital ($sp^3$). Im
Gleichgewichtsabstand spalten sie auf in ein bindendes und
antibindendes Orbital.  In bindenden Oribtalen ist das Valenzband
voll, in antibindenden ist das Leitungsband leer. Die Energielücke ist
temperaturabhängig, da sich mit wachsender Temperatur der
Gitterabstand auf Grund der thermischen Ausdehnung vergrößert.

Die beiden bereits genannten Element-Halbleiter, Silizium und
Germanium, haben anisotrope Energiebänder, d.h.\ die Energieflächen
der Leitungselektronen sind nicht in alle Richtungen gleich, sondern
es gilt
\begin{equation}
  \label{eq:9.1}
  E(\bm k) = \hbar^2 \left(\frac{k_x^2+k_y^2}{2m_t^*} + \frac{k_z^2}{2 m_\ell^*}\right) = \const
\end{equation}
mit der transversalen Masse $m_t^*$ und der longitudinalen Masse
$m_\ell^*$.

Bei Silizium und Germanium handelt es sich um indirekte Halbleiter,
d.h.\ die Extrema der Bänder, also das Minimum des Leitungsbandes und
das Maximum des Valenzbandes liegen am $\Gamma$-Punkt nicht
übereinander, vgl.\ Abbildung~\ref{fig:5.2}.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-2,0) -- (2,0) node[below] {$k$};
    \draw[->] (0,-.2) node[below] {$\Gamma$} -- (0,2) node[left] {$E$};
    \draw[MidnightBlue] plot[domain=-2:2] (\x,{-.1*\x*\x});
    \draw[DarkOrange3] plot[domain=-2:2] (\x,{.1*(\x+1)^2+1.5});
    \node[MidnightBlue,dot] at (0,0) {};
    \node[DarkOrange3,dot] at (-1,1.5) {};
  \end{tikzpicture}
  \caption{Indirekte Halbleiter zeichnen sich dadurch aus, dass das
    Minimum des Leitungsbandes nicht über dem Maximum des Valenzbandes
    liegt.}
  \label{fig:5.2}
\end{figure}

\begin{table}[tb]
  \centering
  \begin{tabular}{ccc}
    \toprule
    & $T=\SI{0}{\kelvin}$ & $T=\SI{300}{\kelvin}$ \\
    \midrule
    $E_g(\ch{Si})$ & \SI{1.17}{\eV} & \SI{1.12}{\eV} \\
    $E_g(\ch{Ge})$ & \SI{0.75}{\eV} & \SI{0.67}{\eV} \\
    \bottomrule
  \end{tabular}
  \caption{Energien der Bandlücken von Silizium (\ch{Si}) und
    Germanium (\ch{Ge}) bei $T=\SI{0}{\K}$ und Raumtemperatur
    $T=\SI{300}{\K}$.}
  \label{tab:5.1}
\end{table}

Aus \eqref{eq:8.20} wissen wir, dass die effektive Masse eines
Teilchen reziprok zur Krümmung des Bandes ist.  In Figur~\ref{fig:5.3}
ist angedeutet, dass bei $k=0$ zwei Bänder zusammen fallen.  Da sie
jedoch außer an diesem Punkt mit unterschiedlicher Krümmung verlaufen
lassen sich den Löchern unterschiedliche effektive Massen zuordnen.
Das Band mit der geringeren Krümmung beherbergt die schweren Löcher
(heavy holes, \textsl{hh}), das andere Band mit der größeren Krümmung
die leichten Löcher (light holes, \textsl{lh}).  Die effektiven Massen
der beiden Lochtypen können durch Zyklotronresonanz bestimmt werden.
Weiterhin gibt es das Band, dessen Energie durch Spin-Bahn-Kopplung um
$\Delta$ abgesenkt ist.  Die Ladungsträger dieses Bandes bezeichnet
man als abgespaltene Löcher (split-off holes, \textsl{soh}).

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-2,0) -- (2,0) node[below] {$k$};
    \draw[->] (0,-2) -- (0,1) node[left] {$E$};
    \draw (2pt,-1) -- (-2pt,-1) node[above left] {$\Delta$};
    \draw[MidnightBlue] plot[domain=-2:2] (\x,{-.1*\x*\x}) node[right] {Schwere Löcher};
    \draw[MidnightBlue] plot[domain=-1.5:1.5] (\x,{-.5*\x*\x}) node[right] {Leichte Löcher};
    \draw[MidnightBlue] plot[domain=-2:2] (\x,{-.3*\x*\x-1})  node[right] {Abgespaltene Löcher};
  \end{tikzpicture}
  \caption{Vereinfachtes Bild der Valenzbandstruktur and der
    Valenzbandkante.}
  \label{fig:5.3}
\end{figure}

In der Nähe der Valenzbandkante können die Bänder als kugelförmig
angenommen werden, weshalb eine effektive Masse pro Band zur
Beschreibung ausreicht.  Die Bänder sind also gegeben durch
\begin{equation*}
  E_V(\textsl{hh}) \simeq -\frac{\hbar^2 k^2}{2 m^*_{\textsl{hh}}} \;,\quad
  E_V(\textsl{lh}) \simeq -\frac{\hbar^2 k^2}{2 m^*_{\textsl{lh}}} \;,\quad
  E_V(\textsl{soh}) \simeq -\Delta-\frac{\hbar^2 k^2}{2 m^*_{\textsl{soh}}} \; .
\end{equation*}

Als \acct{Verbindungs-Halbleiter} bezeichnen wir Halbleiter, die durch
Verbindung mehrerer Elemente zum Halbleiter werden.  Verbindungen von
Elementen der III.\ und V.\ Hauptgruppe, wie GaAs, InP,
Al$_x$G$_{1-x}$As, InSb, oder InAs haben gemischt ionisch-kovalente
Bindungen.  Verbindungen von Elementen der III.\ und IV.\ Hauptgruppe,
wie ZnSe, ZnS, CdTe, oder Zn$_x$S$_{1-x}$Se haben einen stärkeren
ionischen Anteil als III-V-Halbleiter.

Weiter oben wurden bereits indirekte Halbleiter besprochen.  Kommen
wir nun zu direkten Halbleitern.  Hier liegt das Minimum des
Leitungsbandes direkt überhalb des Maximums des Valenzbandes, wie in
Abbildung~\ref{fig:5.4}.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-2,0) -- (2,0) node[below] {$k$};
    \draw[->] (0,-2) -- (0,1) node[left] {$E$};
    \draw[DarkOrange3] plot[domain=-2:2] (\x,{.1*\x*\x+.5});
    \clip (-2,0) rectangle (2,-2);
    \draw[MidnightBlue] plot[domain=-2:2] (\x,{-.1*\x*\x});
    \draw[MidnightBlue] plot[domain=-1.5:1.5] (\x,{-.5*\x*\x});
    \draw[MidnightBlue] plot[domain=-2:2] (\x,{-.3*\x*\x-1});
  \end{tikzpicture}
  \caption{Bei direkten Halbleiter liegt das Minimum des
    Leitungsbandes direkt überhalb des Maximums des Valenzbandes.}
  \label{fig:5.4}
\end{figure}

\todo[inline]{Was ist hiermit gemeint?}
Nehmen wir an GeP und AlSb besitzen indirekte Bandlücken.  Die
Elektronen haben eine sphärische Energiefläche, die Löcher sind
ähnlich wie bei Ge, Si.

\section{Vergleich: Direkte und indirekte Halbleiter}

Als \acct[Absorption!direkt]{direkte Absorption} bezeichnet man den
Prozess, bei dem ein Photon ein Elektron direkt in das Leitungsband
anheben kann, was sich im Bänderdiagramm als senkrechter Übergang
auszeichnet.  Direkte Übergange finden bei direkten Halbleitern
begünstigt statt, da die Extrema der Bänder direkt übereinander
liegen.  Auch bei indirekten Halbleitern kann direkte Absorption statt
finden.  Allerdings muss hier die Energie des Photons weitaus größer
sein als die Bandlücke.  Mit \acct[Absorption!indirekt]{indirekter
  Absorption} bezeichnet man eine durch Phononen unterstützte
Absorption.  Um zum Beispiel bei indirekten Halbleitern einen Übergang
vom Valenzband-Maximum zum Leitungsband-Minimum zu erhalten muss der
Pfeil im Bänderdiagramm \enquote{quer} gehen.  Dieser Querpfeil ist
zusammengesetzt aus einem vertikalen und einem horizontalen Pfeil.
Der vertikale Anteil ist wie bei direkten Übergängen das Photon, der
horizontale Anteil wird durch ein Phonon beigetragen.  Das Phonon sei
nun gegeben durch seine Frequenz $\Omega$ und seinen Impuls $\bm Q$,
das Photon wird beschrieben durch die Frequenz $\omega$ und den Impuls
$\bm k$.  Das Minimum des Leitungsband liege bei $\bm k_m$ gegenüber
dem Valenzband-Maximum bei $0$.  Damit indirekte Absorption statt
finden kann müssen die beiden Bedingungen erfüllt sein
\begin{align*}
  \hbar\omega \pm \hbar\Omega &= E_g \; , \\
  \hbar\bm k \pm \hbar\bm Q &= \hbar\bm k_m \; .
\end{align*}
Die Wahrscheinlichkeit für den indirekten Prozess ist weitaus geringer
als für den direkten, da das Elektron an das Phonon koppeln muss.

\begin{figure}[tb]
  \leavevmode\null\hfill
  \begin{tikzpicture}[gfx]
    \draw[->] (-2,0) -- (2,0) node[right] {$k$};
    \draw[->] (0,-.5) -- (0,2) node[left] {$E$};
    \draw[DarkOrange3] plot[domain=-1.5:1.5] (\x,{.1*\x*\x+1.5});
    \draw[MidnightBlue] plot[domain=-1.5:1.5] (\x,{-.1*\x*\x});
    \draw (-1.7,2pt) -- (-1.7,-2pt) node[below] {$-\pi/a$};
    \draw (1.7,2pt) -- (1.7,-2pt) node[below] {$\pi/a$};
    \draw[Purple,->] (0,0) -- node[right] {$\hbar\omega$} (0,1.5);
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}[gfx]
    \draw[->] (-2,0) -- (2,0) node[right] {$k$};
    \draw[->] (0,-.5) -- (0,2) node[left] {$E$};
    \draw[DarkOrange3] plot[domain=-.5:2.5] (\x,{.1*(\x-1)^2+1.5});
    \draw[MidnightBlue] plot[domain=-1.5:1.5] (\x,{-.1*\x*\x});
    \draw (-1.7,2pt) -- (-1.7,-2pt) node[below] {$-\pi/a$};
    \draw (1.7,2pt) -- (1.7,-2pt) node[below] {$\pi/a$};
    \draw[Purple,->,dashed] (0,0) -- (1,1.5);
    \draw[Purple,->] (0,0) -- node[left] {$\hbar\omega$} (0,1.5);
    \draw[Purple,->] (0,1.5) -- node[below] {$\hbar\Omega$} (1,1.5);
  \end{tikzpicture}
  \hfill\null
  \caption{Bei direkten Halbleitern kann der Interband-Übergang
    einfach durch Absorption eines Photons der richtigen Energie
    stattfinden (links).  Bei indirekten Halbleitern ist dies nicht
    möglich.  Der Übergang muss durch ein Phonon unterstützt werden
    (rechts).}
  \label{fig:5.5}
\end{figure}

Für optische Übergänge müssen die Energie- und Impulserhaltung erfüllt
sein, das heißt es muss gelten
\begin{equation*}
  k_e^{\text{VB}} + k_{\text{Photon}} = k_e^{\text{LB}} .
\end{equation*}
Abschätzung:
\begin{align*}
  k_{\text{Photon}} &= \frac{2\pi}{\lambda} = \frac{2\pi}{\SI{500}{\nano\m}} \\
  k_{\text{Rand d.\ BZ}} &= \frac{\pi}{a} = \frac{\pi}{\SI{0.5}{\nano\m}} \\
  \frac{k_{\text{Photon}}}{k_{\text{Rand d.\ BZ}}} &= \num{e-3}
\end{align*}
Im $E(\bm k)$-Diagramm zeigt sich dies als senkrechter Übergang.

Damit überhaupt ein optischer Übergang stattfinden kann muss die
Energie des absorbierten Photons mindestens die Energie der Bandlücke
überwinden, also $\hbar\omega \geq E_L - E_V = E_g$.  Daher sind viele
Halbleiter im nahen Infraroten transparent, da die Energie der
Photonen nicht ausreicht um ein Elektron über die Bandlücke zu heben.

In indirekten Halbleitern sind Übergänge nur mit Phononenbeteiligung
möglich (Impulserhaltung). Wegen der sehr vieler kleineren
Übergangswahrscheinlichkeiten sind indirekte Halbleiter nicht geeignet
für effiziente Lichtemitter (z.B.\ Halbleiter-Laser oder EPQ).

\section{Undotierte Halbleiter}

Beim Halbleiter tragen Elektronen und Löcher zum Stromtransport
bei. Nach (7.24) und (7.25) gilt
\begin{equation}
  \label{eq:9.2}
  \boxed{\sigma = |e| (n \mu_n + p \mu_p)}
\end{equation}
mit den Beweglichkeiten der Elektronen und Löcher $\mu_n$ und $\mu_p$
und den Volumenkonzentrationen der Elektronen und Löcher $n$ und $p$.

Intrinsische Halbleiter besitzen freie Elektronen und Löcher durch
thermische Anregung über die Energielücke.  Die
Ladungsträgerkonzentrationen in Leitungs- und Valenzband erhalten wir
durch Integration der Zustandsdichte mal der Verteilungsfunktion über
die Energie.  Die Verteilungsfunktion $f(E,T)$ ist natürlich durch die
Fermi-Dirac-Statistik gegeben.  Da diese exponentiell gegen Null
abfällt können wir, statt die Integration bis zur
Leitungsbandoberkante auszuführen, die obere Integrationsgrenze auch
unendlich setzen.  Gleiches gilt für die Löcher.  Es gilt also
\begin{align}
  \label{eq:9.3}
  n &= \int_{E_L}^{\infty} D_L(E)\ f(E,T) \diff E \; , \\
  \label{eq:9.4}
  p &= \int_{-\infty}^{E_V} D_V(E)\ (1-f(E,T)) \diff E
\end{align}
mit den Zustandsdichten $D_V$ des Valenzbandes und $D_L$ des
Leitungsbandes.  Die Verteilung der Löcher ist durch $1-f(E,T)$
gegeben, da diese durch unbesetzte Elektronenzustände entstehen. Die
Zustandsdichten sind bekannt als
\begin{alignat}{3}
  \label{eq:9.5}
  D_L(E) &= \frac{(2 m_n^*)^{3/2}}{2 \pi^2 \hbar^3} \sqrt{E-E_L} \qquad&&(E > E_L) \; , \\
  \label{eq:9.6}
  D_V(E) &= \frac{(2 m_p^*)^{3/2}}{2 \pi^2 \hbar^3} \sqrt{E_V-E} \qquad&&(E < E_V) \; .
\end{alignat}
Die beiden Bänder sind durch die Energielücke getrennt, das heißt für
$E_V < E < E_L$ existieren gar keine Zustände.

\begin{notice}[Anmerkung:]
  In der Halbleiterphysik nennt man das chemische Potential $\mu$ oft
  Fermi-Niveau $E_F$.
\end{notice}

Da die \enquote{Aufweichungszone} der Fermi-Funktion ($\approx 2\kB T
\approx \SI{50}{\milli\eV}$) bei üblichen Temperaturen klein ist gegen
den Bandabstand ($\approx \SI{1}{\eV}$) lässt sich innerhalb der
Bänder die Fermi-Funktion durch die
Boltzmann-Besetzungswahrscheinlichkeit annähern.
\begin{equation*}
  f(E,T) = \frac{1}{\ee^{(E-E_F)/(\kB T)} + 1} \approx \ee^{-(E-E_F)/(\kB T)} \ll 1
  \quad\text{für } E-E_F \gg 2 \kB T \; .
\end{equation*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: