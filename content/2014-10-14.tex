\setcounter{chapter}{8}
\setcounter{section}{3}
\setcounter{equation}{14}
\chaptermark{Elektronische Transporteigenschaften}
\addchaptertocentry{8}{Elektronische Transporteigenschaften}

% Wiederholung zu Blockfunktionen
% Hunklinger Bild 8.14

\section{Die effektive Massennäherung}

Wir nehmen im Folgenden die Elektronen im Festkörper als
Materiewellenpakete mit Wellenvektoren aus der nächsten Umgebung von
$\bm k$ an.  Wir entwickeln die Wellenfunktion nach Blochwellen.  Zur
Erinnerung: Blochwellen treten in einem periodischen Potential auf.
Sie setzen sich zusammen aus einer ebenen Welle multipliziert mit
einer periodischen Funktion $u_{\bm k}(r)$, also $u_{\bm k}(r) =
u_{\bm k}(r+\ell)$.  Eine Blochwelle hat also die Form
\begin{equation*}
  \psi_{\bm k}(\bm r) = u_{\bm k}(r) \ee^{\ii\bm k\cdot\bm r} .
\end{equation*}
Um eine Lokalisierung der Welle zu erzwingen wird sie mit einem
Gauß-förmigen Wellenpaket $\sim\ee^{E_n(k) t/\hbar}$ mit $E_n(k) =
\hbar^2 k^2 / (2 m)$ überlagert.  Die Wellenfunktion des Wellenpakets
lautet dann
\begin{equation*}
  \psi_n(\bm r,t) = \sum_{k-\Delta k/2}^{k+\Delta k/2} c(k) u_{\bm k}(\bm r) \ee^{\ii(\bm k\cdot\bm r-E_n(\bm k) t/\hbar)},
\end{equation*}
mit der Gruppengeschwindigkeit
\begin{equation}
  \bm v_g(\bm k)=\frac{\diff\omega(\bm k)}{\diff\bm k}=\frac1\hbar\frac{\diff E(\bm k)}{\diff\bm k}.
  \label{eq:8.15}
\end{equation}
Diese spiegelt den Einfluss des Kristalls über die Dispersionsrelation
$E(\bm k)$, d.h.\ die Bandstruktur wieder.  Für freie Elektronen mit
parabelförmiger Dispersionsrelation ergibt sich die
Gruppengeschwindigkeit $\bm v_g = \hbar \bm k / m$.

Andererseits ist der Energieübertrag $\diff E(\bm k)$ in einem Feld
gegeben durch
\begin{equation}
  \label{eq:8.16}
  \diff E(\bm k) = \bm F \cdot \diff \bm r = \bm F \cdot \bm v \diff t
\end{equation}
mit \eqref{eq:8.15} folgt
\begin{equation}
  \label{eq:8.17}
  \boxed{\hbar \frac{\diff \bm k}{\diff t} = \bm F}
\end{equation}
Nun lässt sich die Bewegungsgleichung für $\bm k$-Vektoren im
Impulsraum aus \eqref{eq:8.15} ableiten:
\begin{align}
  \frac{\diff \bm v_g}{\diff t} &= \left(\frac{1}{\hbar^2}\frac{\diff^2 E}{\diff k^2}\right) \bm F \notag \\
  \implies \bm F &= \frac{\hbar^2}{\frac{\diff^2 E}{\diff k^2}} \cdot \frac{\diff \bm v_g}{\diff t} \label{eq:8.18}
\end{align}
Die Gleichung \eqref{eq:8.18} hat die Form des zweiten Newtonschen
Gesetzes. Damit definieren wir die \acct{effektive Masse} $m^*$:
\begin{equation}
  \boxed{m^* =  \frac{\hbar^2}{\frac{\diff^2 E}{\diff k^2}}}
  \label{eq:8.19}
\end{equation}
Die effektive Masse ist proportional zur Krümmung des Energiebandes
(zweite Ableitung der Energie). Die Definition lässt sich erweitern
auf anisotrope Energieflächen:
\begin{equation}
  \boxed{\frac{1}{m_{ij}^*} =  \frac{1}{\hbar^2}\frac{\partial^2 E}{\partial k_i \partial k_j}}
  \label{eq:8.20}
\end{equation}
Falls die Energieflächen Paraboloide sind folgt, dass $m^*$ konstant
ist. Paraboloide erhält man auch bei der Lösung der simplifizierten
Schrödingergleichung, das heißt es ist kein Potential vorhanden:
\begin{equation*}
  \left( \frac{\hbar^2}{2 m_i^*} \nabla^2_i + E \right) \psi = 0 \; .
\end{equation*}
In $m_i^*$ ist die Wirkung des periodischen Potentials bereits
enthalten, da
\begin{equation}
  \label{eq:8.21}
  E_{n,i} = \frac{\hbar^2 k_i^2}{2 m_i^*} \; .
\end{equation}

Die effektive Masse kann dann eingeführt werden, wenn sich Elektronen
in einem periodischen Kristallfeld unter dem Einfluss sich langsam
ändernder Felder bewegen.  Wichtig ist, dass die Felder sich langsam
ändern, denn man kann dann ihr Potential über eine Gitterzelle als
konstant annehmen und so die oben erwähnte Entwicklung nach
Blochwellen rechtfertigen.  Für eine ausführliche Herleitung und den
Beweis siehe \textcite[133]{haken-qft}.

\begin{table}[tb]
  \centering
  \begin{tabular}{rl}
    \toprule
    \multicolumn{1}{c}{Material} &
    \multicolumn{1}{c}{$m^*/m$} \\
    \midrule
    Cu, Au, Ag & $\approx$ \num{1} \\
    Alkalimetalle & $\approx$ \num{0.85}\ldots\num{1.1} \\
    Pt, Ni, Fe, Co & $\approx$ \num{10} \\
    Ge, Si & $\approx$ \num{0.1}\ldots\num{0.4} \\
    InSb & $\approx$ \num{0.014} \\
    \bottomrule
  \end{tabular}
  \caption{Einige Zahlenwerte für $m^*/m$.}
  \label{tab:1.1}
\end{table}

\begin{notice}
  Für große Wellenvektoren $\bm k$ sind Abweichungen zu erwarten.
\end{notice}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-3,0) -- (3,0) node[below] {$\bm k$};
    \draw[->] (-3,2) -- (3,2) node[below] {$\bm k$};
    \draw[->] (0,-2) -- (0,1.5) node[left] {$m^*$};
    \draw[->] (0,1.8) -- (0,4) node[left] {$E$};
    \foreach \i in {-2,-1,1,2} {
      \draw[dashed] (\i,-2) -- (\i,4);
    }
    \node[below right] at (2,0) {$\pi/a$};
    \draw[DarkOrange3] (-2,3) to[out=0,in=180] (0,2) to[out=0,in=180] (2,3);
    \draw[MidnightBlue] (-2,-1) to[out=0,in=90] (-1,-2);
    \draw[MidnightBlue] (-1,1) to[out=-90,in=180] (0,0) to[out=0,in=-90] (1,1);
    \draw[MidnightBlue] (1,-2) to[out=90,in=180] (2,-1);
  \end{tikzpicture}    
  \caption{Zusammenhang von Dispersion (oben) und
    effektiver Masse (unten).  Für abnehmende Bandkrümmung nimmt die
    effektive Masse zu.  Am Wendepunkt ist die Bandkrümmung null,
    somit divergiert die effektive Masse.  Da die Krümmung danach
    negativ wird (es handelt sich schließlich um einen Wendepunkt,
    bei dem die Krümmung das Vorzeichen wechselt) wird auch die
    effektive Masse negativ.}
  \label{fig:1.1}
\end{figure}

Aus der Defintion folgt, dass $m^*$ groß ist, wenn die Krümmung klein
ist.  Die Begründung ist einfach: Ist die Krümmung klein, dann ist das
Band schmal und somit die Austauschwechselwirkung klein, also die
Elektronen sind nahe am Atom.  Dies führt zu großer Trägheit, also ist
$m^*$ groß.

\begin{notice}[Diskussion:]
  Eine Kraft (z.B.\ ein elektrisches Feld) führt zu einer Zunahme von
  $\bm k$
  \begin{equation*}
    \diff k = \frac1\hbar e \bm E \diff t.
  \end{equation*}
  Dies treibt die Elektronen näher in den Bereich von $\pi/a$ wo
  Braggreflexion stattfindet, z.B.\ mit $\bm k = \frac12 \bm G_{100}$.
  Daraus resultiert eine Impulszunahme in Gegenrichtung und $m^*$ wird
  negativ.  In einem konstanten äußeren Feld $\bm E$ würde ein
  Festkörperelektron somit eine permanente Pendelbewegung ausführen.
  Die Erfahrung zeigt, dass ein Strom fließt, der gegeben ist durch
  die mittlere Bewegung der Elektronen in Feldrichtung.  Die Ursache dafür
  ist die Streuung der Elektronen an Phononen und Gitterfehlern.
  \begin{itemize}
  \item $k$-Zuwachs auf kleinen Wert $\bm k_{\text{stationär}}$ beschränkt.
  \item $e^-$ bei kleinen $k$ (teilgefüllte Bänder)
  \item Volle Bänder kein Strom
  \end{itemize}
\end{notice}

Die zuvor erwähnte Pendelbewegung heißt \acct{Blochoszillation}
(Abbildung~\ref{fig:1.2}).
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-3,0) -- (3,0) node[below] {$\bm k$};
    \draw[->] (0,-.5) -- (0,1.5) node[left] {$E$};
    \foreach \i in {-2,2} {
      \draw[dashed] (\i,-.2) -- (\i,1.5);
    }
    \node[below] at (-2,-.2) {$-\pi/a$};
    \node[below] at (2,-.2) {$\pi/a$};
    \draw[DarkOrange3,arrow inside] (-2,1) to[out=0,in=180] (0,0);
    \draw[DarkOrange3,arrow inside] (0,0) to[out=0,in=180] (2,1);
    \draw[MidnightBlue,<-] (-2,1) -- node[above,pos=.25] {$\bm G$} (2,1);
  \end{tikzpicture}
  \caption{Im reduzierten Zonenschema werden die Elektronen von
    $\pi/a$ nach $-\pi/a$ zurück gefaltet, da am Zonenrand
    Braggreflexion statt findet und sich ihre Bewegungsrichtung
    umkehrt.  Es stellt sich also eine Oszillation zwischen den
    Zonenrändern ein.}
  \label{fig:1.2}
\end{figure}

Folgen die Elektronen einem äußeren Feld $E$ so wird ihre Bewegung
beschrieben durch die Bewegungsgleichung
\begin{equation*}
  \hbar \frac{\diff \bm k}{\diff t} = e E.
\end{equation*}
Die Lösung ist trivial und somit erfolgt die Bewegung des Elektrons
durch die Brillouin"=Zone mit konstanter Geschwindigkeit
\begin{equation*}
  |\dot{\bm k}| = \frac{eE}{\hbar} = \const .
\end{equation*}
Für die Pendelbewegung ergibt sich die Periode
\begin{equation*}
  T_B = \frac{2\pi/a}{eE/\hbar} = \frac{h}{a e E}
\end{equation*}
und die daraus abgeleitete \acct{Blochfrequenz}
\begin{equation}
  \label{eq:8.22}
  \boxed{\omega_B = \frac{2\pi}{T_B} = \frac{eEa}{\hbar}} \; .
\end{equation}
Dies entspricht einer periodischen Geschwindigkeit im $k$-Raum, als
auch im reellen Raum.

Die Voraussetzung für eine stabile Pendelbewegung wäre, dass keine
Streuprozesse innerhalb von $T_B$ statt finden, also
\begin{equation}
  \label{eq:8.23}
  \omega_B T_2 \le 1
\end{equation}
mit der Streuzeit $T_2$ mit Phononen und Gitterelektronen. Diese ist
winzig verglichen mit der Periodendauer der Blochoszillationen und
liegt im Bereich von Picosekunden.  Es ist daher völlig unmöglich,
dass die Elektronen in realen Kristallen die Zonenränder ohne
Streuprozesse erreichen können.  Das folgende Beispiel verdeutlicht
dies.

\begin{example}
  Sei das äußere Feld $E = \SI{1}{\kilo\volt\per\m}$ und die
  Gitterkonstante $a = \SI{2}{\angstrom}$.  Dann ergibt sich eine
  Periodendauer der Blochoszillationen von $T_B = \SI{20}{\nano\s}$,
  also eine Frequenz von $\nu_B = \SI{50}{\mega\Hz}$.  Setze man für
  die mittlere Geschwindigkeit die Fermi"=Geschwindigkeit $\bar v
  \approx \SI{e6}{\m\per\s}$ ein, ergibt sich eine Auslenkung der
  Pendelbewegung von $\delta x = \SI{5}{\milli\m}$.
\end{example}

Jedoch ist es in Halbleiter-Übergittern möglich, dass die Elektronen
die Zonenränder stoßfrei erreichen.  Diese Übergitter haben
Gitterkonstanten von circa \SI{100}{\angstrom}.  Die Frequenzen der
Blochoszillationen liegen also im Bereich von Terahertz
(Abbildung~\ref{fig:1.3}).
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \draw (0,1) -| (1,0) -| (2,1) -| (3,0) -| (4,1) -- (5,1);
      \draw (1,.3) -- (2,.3);
      \draw (3,.3) -- (4,.3);
      \begin{scope}[shift={(1.5,.2)}]
        \draw plot[domain=-.6:.6,smooth] (\x,{.5*exp(-(5*\x)^2)});
      \end{scope}
      \begin{scope}[shift={(3.5,.2)}]
        \draw plot[domain=-.6:.6,smooth] (\x,{.5*exp(-(5*\x)^2)});
      \end{scope}
    \end{scope}
    \begin{scope}[shift={(0,-1.5)}]
      \draw[DarkOrange3,fill=DarkOrange3!20] (-.75,.25) rectangle (5.75,.75);
      \draw (0,1) -| (1,0) -| (2,1) -| (3,0) -| (4,1) -- (5,1);
      \node[DarkOrange3,left] at (5.75,.5) {Übergitter};	
    \end{scope}
  \end{tikzpicture}   
  \caption{Halbleiter-Übergitter ermöglichen die Beobachtung von
    Blochoszillationen.}
  \label{fig:1.3}
\end{figure}


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End:
