Mit dieser Form der Verteilungsfunktion und der Zustandsdichte aus
\eqref{eq:9.5} können wir das Integral aus \eqref{eq:9.3} berechnen.
\begin{equation*}
  n = \frac{(2 m^*)^{3/2}}{2 \pi^2 \hbar^3} \ee^{E_F/\kB T}
  \int_{E_L}^{\infty} \sqrt{E-E_L} \, \ee^{-E/\kB T} \diff E \; .
\end{equation*}
Mit der Substitution $X_L = (E-E_L)/\kB T$ ergibt sich
\begin{equation*}
  n = \frac{(2 m^*)^{3/2}}{2 \pi^2 \hbar^3} (\kB T)^{3/2} \ee^{-(E_L-E_F)/\kB T}
  \int_{0}^{\infty} X_L^{1/2} \ee^{-X_L} \diff X_L \; .
\end{equation*}
Das Integral lässt sich leicht analytisch lösen, siehe dazu
\textcite{gross}, und wir erhalten die Ladungsträgerkonzentrationen
\begin{align}
  \label{eq:9.7}
  n &= N_{\text{eff}}^L \ee^{-(E_L-E_F)/\kB T}
  \quad\text{mit}\quad
  N_{\text{eff}}^L = 2 \left( \frac{2 \pi m_n^* \kB T}{h^2} \right)^{3/2} \; , \\
  \label{eq:9.8}
  p &= N_{\text{eff}}^V \ee^{(E_V-E_F)/\kB T}
  \quad\text{mit}\quad
  N_{\text{eff}}^V = 2 \left( \frac{2 \pi m_p^* \kB T}{h^2} \right)^{3/2}
\end{align}
wobei hier die effektiven Zustandsdichten $N_{\text{eff}}$ eingeführt
wurden.

\begin{notice}[Anmerkung:]
  Bei hohen Ladungsträgerdichten (Dotierung) kann diese Näherung nicht
  mehr verwendet werden. Man spricht dann von entarteten Halbleitern.
\end{notice}

Bilden wir das Produkt von $n$ und $p$ aus \eqref{eq:9.7} und
\eqref{eq:9.8}, so fällt die Abhängigkeit von der Fermi-Energie
weg.  Mit der Defintion der Bandlücke $E_g = E_L - E_V$ gilt
\begin{equation}
  \label{eq:9.9}
  \boxed{
    n p = N_{\text{eff}}^L N_{\text{eff}}^V \ee^{-E_g/\kB T}
    = 4 \left( \frac{\kB T}{2 \pi \hbar^2} \right)^3 (m_n^* m_p^*)^{3/2} \ee^{-E_g/\kB T}
  } \; .
\end{equation}
Das Produkt von $n$ und $p$ ist also vollständig charakterisiert durch
die effektiven Massen der Ladungsträger und die Energielücke des
Halbleiters.  Die Bedingung $np=\const$ wird in Anlehnung an die
Thermodynamik als \acct{Massenwirkungsgesetz} bezeichnet.

Da bei einem intrinsischen Halbleiter die Elektronen im Leitungsband
ausschließlich aus dem Valenzband stammen gilt die
Neutralitätsbedingung
\begin{equation}
  \label{eq:9.10}
  n = p = \sqrt{N_{\text{eff}}^L N_{\text{eff}}^V} \ee^{-E_g/2 \kB T}
\end{equation}
Für $T = \SI{300}{\kelvin}$ sind einige intrinsische
Ladungsträgerdichten in Tabelle~\ref{tab:6.1} aufgetragen.

\begin{table}[tb]
  \centering
  \begin{tabular}{cSS}
    \toprule
    & {$E_g$ [\si{\eV}]} & {$n$ [\si{\per\cubic\centi\m}]} \\
    \midrule
    Ge   & 0.67 & 2.4e13 \\
    Si   & 1.1  & 1.5e10 \\
    GaAs & 1.43 & 5e7    \\
    \bottomrule
  \end{tabular}
  \caption{Intrinsische Ladungsträgerdichten bei $T=\SI{300}{\kelvin}$.}
  \label{tab:6.1}
\end{table}

Mit Ladungsneutralität $n=p$ kann man die Lage des Fermi-Niveaus
selbstkonsistent bestimmen.  Dazu setzt man \eqref{eq:9.7} und
\eqref{eq:9.8} gleich und löst nach $E_F$ auf.
\begin{equation}
  \label{eq:9.11}
  E_F = \frac{E_L+E_V}{2} + \frac{\kB T}{2} \ln\left(\frac{N_{\text{eff}}^L}{N_{\text{eff}}^V}\right)
  = \frac{E_L+E_V}{2} + \frac{3}{4} \kB T \ln\left(\frac{m_p^*}{m_n^*}\right) \; .
\end{equation}

\section{Dotierte Halbleiter}

Dotierte Halbleiter sind technologisch wichtig. Praktisch alle
Bauelemente sind dotiert.

\begin{figure}[tb]
  \centering
  \leavevmode\null\hfill
  \begin{tikzpicture}[gfx]
    \shade[shading=radial,inner color=DimGray,outer color=DimGray!10] (0,0) circle (2);
    \node[draw,DarkOrange3,fill=DarkOrange3!20,circle,inner sep=.1em] at (-135:2) {$-$};
    \node[draw=MidnightBlue,fill=MidnightBlue!20,circle,inner sep=.1em] at (0,0) {As$^+$};
    \foreach \x in {-2,...,2} {
      \foreach \y in {-2,...,2} {
        \ifnum\x=0\ifnum\y=0\else
          \node[draw,circle,inner sep=.1em] at (\x,\y) {Si};
        \fi
        \else
          \node[draw,circle,inner sep=.1em] at (\x,\y) {Si};
        \fi
      }
    }
    \foreach \x in {-2,...,2} {
      \foreach \y in {-2.5,-1.5,-.5,.5,1.5,2.5} {
        \node at (\x,\y) {$\|$};
      }
    }
    \foreach \x in {-2.5,-1.5,-.5,.5,1.5,2.5} {
      \foreach \y in {-2,...,2} {
        \node at (\x,\y) {$=$};
      }
    }
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}[gfx]
    \shade[shading=radial,inner color=DimGray,outer color=DimGray!10] (0,0) circle (2);
    \node[draw,DarkOrange3,fill=DarkOrange3!20,circle,inner sep=.1em] at (-45:2) {$+$};
    \node[draw=MidnightBlue,fill=MidnightBlue!20,circle,inner sep=.1em] at (0,0) {B$^-$};
    \foreach \x in {-2,...,2} {
      \foreach \y in {-2,...,2} {
        \ifnum\x=0\ifnum\y=0\else
          \node[draw,circle,inner sep=.1em] at (\x,\y) {Si};
        \fi
        \else
          \node[draw,circle,inner sep=.1em] at (\x,\y) {Si};
        \fi
      }
    }
    \foreach \x in {-2,...,2} {
      \foreach \y in {-2.5,-1.5,-.5,.5,1.5,2.5} {
        \node at (\x,\y) {$\|$};
      }
    }
    \foreach \x in {-2.5,-1.5,-.5,.5,1.5,2.5} {
      \foreach \y in {-2,...,2} {
        \node at (\x,\y) {$=$};
      }
    }
  \end{tikzpicture}
  \hfill\null
  \caption{Zwei dotierte Silizium-Gitter.  Im linken Bild ist das
    Gitter mit einem \ch{As^+} dotiert.  Dieses bringt ein
    zusätzliches Elektron ein und wird daher Donator genannt.  Im
    rechten Bild wird ein \ch{B^-} eingebracht, welches ein Elektron
    bindet und daher eine Fehlstelle erzeugt, weshalb es Akzeptor
    genannt wird.}
  \label{fig:6.1}
\end{figure}

Man setzt einen subtituionellen Donator, z.B.\ Phosphor (5-wertig) auf
einen Gitterplatz im Silizium-Gitter (4-wertig). Das fünfte nicht für
die Bindung zum Silizium benötigte Elektron bewegt sich praktisch
wasserstoffartig um das positive Phosphor-Zentrum. Wir lösen das
Wasserstoffproblem unter Berücksichtigung des anwesenden
Festkörpers. Damit ergibt sich die Donator-Rydberg-Energie
\begin{equation*}
  E_n = \frac{m_e^*/m_0}{\varepsilon^2} E_n^H
\end{equation*}
wobei $E_n^H$ die Energie-Niveaus des Wasserstoff-Atoms sind.  Für $n
= 1$ ergibt sich das Grundzustandsniveaus des Donator-/Akzeptoratoms.
In Abbildung~\ref{fig:6.2} ist dies skizziert.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node at (1.5,3.5) {n-Halbleiter};
      \draw[<->] (0,3) node[left] {$E$} |- (3,0) node[below] {$x$};
      \fill[MidnightBlue!40] (.5,0) rectangle (2.5,.5);
      \fill[MidnightBlue!20] (.5,2.5) rectangle (2.5,3);
      \draw (.5,.5) -- (2.5,.5);
      \draw (.5,2.5) -- (2.5,2.5);
      \draw (.5,0) rectangle (2.5,3);
      \draw[MidnightBlue] (.5,2) -- (2.5,2) node[right] {Donatorniveau};
      \draw[MidnightBlue,<->] (1,2) -- node[right] {$E_D^B$} (1,2.5);
    \end{scope}
    \begin{scope}[shift={(6,0)}]
      \node at (1.5,3.5) {p-Halbleiter};
      \draw[<->] (0,3) node[left] {$E$} |- (3,0) node[below] {$x$};
      \fill[MidnightBlue!40] (.5,0) rectangle (2.5,.5);
      \fill[MidnightBlue!20] (.5,2.5) rectangle (2.5,3);
      \draw (.5,.5) -- (2.5,.5);
      \draw (.5,2.5) -- (2.5,2.5);
      \draw (.5,0) rectangle (2.5,3);
      \draw[DarkOrange3] (.5,1) -- (2.5,1) node[right] {Akzeptorniveau};
      \draw[DarkOrange3,<->] (1,.5) -- node[right] {$E_A^B$} (1,1);
    \end{scope}
  \end{tikzpicture}
  \caption{Störstellenniveaus in Halbleitern.  Bei einem
    n-Halbleiter liegt die Grundzustandsenergie der Donatoren direkt
    unter der Leitungsbandkante.  Die Ionisierungsenergie ist $E_D^B$.
    Der Grundzustand der Akzeptoren liegt überhalb der
    Valenzbandkante.  Die Ionisierungsenergie der Löcher beträgt
    $E_A^B$.}
  \label{fig:6.2}
\end{figure}

Die Bindungsenergie der zusätzlichen Ladungsträger an die Rümpfe der
Fremdatome ist relativ gering, sodass bereits Raumtemperatur ausreicht
um die Coulomb-Wechselwirkung zu überwinden und den Ladungsträger vom
Rumpf zu lösen.  Dieser zusätzliche freie Ladungsträger kann dann zur
Leitfähigkeit beitragen.  Handelt es sich bei dem Ladungsträger um ein
zusätzliches Elektron so bezeichnet man das Dotier-Atom als
\acct{Donator}, handelt es sich hingegen um ein zusätzliches Loch
spricht man von einem \acct{Akzeptor}.

\begin{notice}[Abschätzung:]
  \begin{align*}
    m_e^* &\approx \num{0.01}\ldots\num{0.5} m_0 \\
    \varepsilon &\approx \num{7}\ldots\num{12} \\
    E_D^B &\approx \num{7}\ldots\SI{50}{\milli\eV}
  \end{align*}
\end{notice}

Mit der obigen Definition von Donator und Akzeptor kann eine Elektron
im Leitungsband entweder aus dem Valenzband oder aus einem
Donatorniveau stammen.  Demnach entspricht eine Loch im Valenzband
entweder einem Elektron im Leitungsband oder einem ionisierten
Akzeptorniveau.

Für intrinsische Halbleiter wurde die Neutralitätsbedingung
\eqref{eq:9.10} abgeleitet, die die Ladungsträgerdichte im thermischen
Gleichgewicht angibt.  Da nun aber dotierte Halbleiter vorliegen
müssen die durch die Dotierung zusätzlich eingebrachten Akzeptoren und
Donatoren berücksichtigt werden.  Da die eingebrachten Akzeptoren und
Donatoren jeweils mit einem Elektron oder Loch besetzt werden können
spalten wir die Dichte in einen neutralen und einen geladenen Anteil
\begin{equation*}
  N_D = N_D^0 + N_D^+
  \quad\text{und}\quad
  N_A = N_A^0 + N_A^- \; .
\end{equation*}
Die Neutralitätsbedingung \eqref{eq:9.10} muss daher modifiziert
werden, damit die zusätzlichen Akzeptoren und Donatoren berücksichtigt
werden.
\begin{equation}
  \label{eq:9.12}
  n + N_A^- = p + N_D^+ \; .
\end{equation}
Bereits zuvor haben wir aus dieser Neutralitätsbedingung die
Fermi-Energie $E_F$ berechnet.  Dies wollen wir nun auch für dotierte
Halbleiter tun.

Wie oben angegeben ist die Gesamtkonzentration der Donatoren und
Akzeptoren durch $N_D$ und $N_A$ gegeben. Die Konzentrationen der
ionisierten Donatoren und Akzeptoren sind $N_D^+$ und $N_A^-$.

Für die Besetzung der Donatoren mit Elektronen gilt nach
\eqref{eq:9.3}
\begin{align}
  \label{eq:9.13}
  N_D^0 &= \int D_D(E)\,f_D^0(E) \diff E \\
  \label{eq:9.14}
  D_D(E) &= N_D\,\delta(E-E_D) \\
  \label{eq:9.15}
  f_D^0(E) &= g_i \frac{1}{\ee^{(E_D-E_F)/\kB T} + 1}
\end{align}
mit $g_i$ dem Entartungsgrad der $i$-ten Störstelle. Der
Entartungsfaktor berücksichtigt die Entartung der
Störstellenniveaus. Bei einfachen Donatoren kann ein Elektron mit Spin
nach oben oder nach unten eingebaut werden. Das heißt, wir haben
doppeltes statistisches Gewicht $g_i = 2$. Dieser Faktor wird im
Folgenden vernachlässigt.  Gleiches gilt für die Besetzung der
Akzeptoren mit Löchern.

Das heißt, wir können die Wahrscheinlichkeit dafür, dass eine
Störstelle nicht ionisert ist, also neutral ist durch die
Fermi-Dirac-Statistik ausdrücken.
\begin{align}
  \label{eq:9.16}
  N_D^0 &= N_D \left[ 1 + \ee^{(E_D-E_F)/\kB T} \right]^{-1} \\ 
  \label{eq:9.17}
  N_A^0 &= N_A \left[ 1 + \ee^{(E_F-E_A)/\kB T} \right]^{-1}
\end{align}

Um eine Aussage über den Einfluss der Störstellen auf die
Leitfähigkeit zu machen berechnen wir die Zahl der freien Elektronen
als Funktion der Temperatur.  Da dies im Falle von Donatoren und
Akzeptoren nur numerisch möglich ist beschränken wir uns auf
n-dotierte Halbleiter wo fast keine Akzeptoren vorliegen und diese
daher vernachlässigt werden können ($N_D \gg N_A$).  Weiter oben
wurden einige relevante Beziehungen aufgezählt, die der Übersicht
halber hier nochmals zusammengefasst sind.
\begin{align*}
  \tag{\ref{eq:9.7}}
  n &= N_{\text{eff}}^L \ee^{-(E_L-E_F)/\kB T} \; , \\
  \notag
  N_D &= N_D^0 + N_D^+ \; , \\
  \tag{\ref{eq:9.16}}
  N_D^0 &= N_D \left[ 1 + \ee^{(E_D-E_F)/\kB T} \right]^{-1} \; .
\end{align*}
Freie Elektronen können nur von Donatoren oder aus dem Valenzband
stammen, d.h.
\begin{equation}
  \label{eq:9.18}
  n = N_D^+ + p \; .
\end{equation}
Zur Vereinfachung sagen wir, dass der Hauptteil von den Donatoren
kommt, d.h.\ $N_D^+ \gg p$, also
\begin{equation*}
  n \approx N_D^+ = N_D - N_D^0 \; .
\end{equation*}
Damit folgt mit \eqref{eq:9.16}
\begin{equation}
  \label{eq:9.19}
  n \approx N_D \left( 1 - \frac{1}{1+ \ee^{(E_D-E_F)/\kB T}} \right) \; .
\end{equation}

Wir stellen die Gleichung \eqref{eq:9.7} nach der Fermi-Energie $E_F$
um und erhalten
\begin{equation*}
  \left( \frac{n}{N_{\text{eff}}^L} \right) \ee^{E_D/\kB T} = \ee^{E_F/\kB T} \; ,
\end{equation*}
setzt man dies nun im Nenner von \eqref{eq:9.19} ein so erhält man
\begin{equation*}
  n = \frac{N_D}{1 + \frac{n}{N_{\text{eff}}^L} \ee^{E_d/\kB T}}
  \quad\text{mit}\quad
  E_d = E_L - E_D \; .
\end{equation*}
weiterhin multiplizieren wir mit dem Nenner der rechten Seite durch
und bringen $N_D$ ebenfalls auf die linke Seite, sodass
\begin{equation*}
  \frac{n^2}{N_{\text{eff}}^L} \ee^{E_d/\kB T} + n - N_D = 0 \; .
\end{equation*}
Dies ist offensichtlich eine quadratische Gleichung in $n$, die man
lösen kann.  Die Lösung lautet
\begin{equation}
  \label{eq:9.20}
  n \approx 2 N_D \left( 1 + \sqrt{ 1 + 4 \frac{N_D}{N_{\text{eff}}^L} \ee^{E_d/\kB T}} \right)^{-1} \; .
\end{equation}

Nun werden wir drei Grenzfälle von \eqref{eq:9.20} diskutieren.
\begin{enumerate}
\item Für kleine Temperaturen $\kB T \ll E_d$ kommt es zur sogenannten
  \acct{Störstellenreserve}.  Dabei ist $T$ so klein, dass
  \begin{equation*}
    4 \frac{N_D}{N_{\text{eff}}^L} \ee^{E_d/\kB T} \gg 1
  \end{equation*}
  und es ergibt sich
  \begin{equation}
    \label{eq:9.21}
    n = \sqrt{N_D N_{\text{eff}}^L} \ee^{- E_d/2 \kB T} \; .
  \end{equation}
  Das heißt, dass im Temperaturbereich der Störstellenreserve die
  thermische Energie nicht ausreicht um alle Donatoren zu ionisieren.
  Wie beim intrinsischen Halbleiter hängt die Elektronenkonzentration
  exponentiell von der Temperatur ab, vgl.\ \eqref{eq:9.10}.
  Allerdings geht hier statt der verhältnismäßig riesigen Bandlücke
  $E_g$ die Donator-Ionisationsenergie $E_d$ ein.

\item Für mittlere Temperaturen findet die sogenannte
  \acct{Störstellenerschöpfung} statt.  Da die Temperatur nun höher
  ist als bei der Störstellenreserve erhalten wir
  \begin{equation*}
    4 \frac{N_D}{N_{\text{eff}}^L} \ee^{E_d/\kB T} \ll 1
  \end{equation*}
  und es ergibt sich
  \begin{equation}
    \label{eq:9.22}
    n = N_D = \const \; .
  \end{equation}
  Das bedeutet, dass alle Donatoren ionisiert sind und die
  Elektronendichte um Leitungsband maximal ist.  Weil keine weiteren
  Donatoren mehr ionisiert werden können spricht man von Erschöpfung.
  Wir haben weiterhin angenommen, dass die Temperatur noch so niedrig
  ist, dass die Anregung von Elektronen aus dem Valenzband
  vernachlässigt werden kann.

\item Für hohe Temperaturen erreichen wir den \acct[Intrinsischer
  Bereich]{intrinsischen Bereich}.  Nun können Elektronen aus dem
  Valenzband die Bandlücke überwinden und bald ist die
  Elektronendichte im Leitungsband durch die angeregten Elektronen
  dominiert.  Da sich die Störstellen jetzt nicht mehr bemerkbar
  machen sieht der n-Halbleiter aus, als wäre er intrinsisch.
  \begin{gather}
    \label{eq:9.23}
    n \sim \ee^{-E_g/2 \kB T}
  \end{gather}
\end{enumerate}

Die verschiedenen Bereiche sind zusammen mit der Fermi-Energie in
Abbildung~\ref{fig:6.3} dargestellt.  Die Elektronenkonzentration kann
durch Hall-Messungen bestimmt werden.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,4) node[left] {$\log n$} |- (5,0) node[right] {$1/T$};
    \draw[MidnightBlue,rounded corners]
    (.2,4) -- (1,1.5) -- (2.5,1.5) -- (4.5,.2) node[right] {$N_D$};
    \draw[MidnightBlue,dashed,rounded corners]
    (.2,4) -- (.85,2) -- (2.3,2) -- (4.5,.6) node[above right] {$N_D'>N_D$};
    \path (.2,4) -- node[pin={above right:Steigung $-E_g/2 \kB$}] {} (1,1.5);
    \path (2.3,2) -- node[pin={above right:Steigung $-E_d/2 \kB$}] {} (4.5,.6);

    \node[rotate=90] at (.5,.7) {\scriptsize intrinsisch};
    \node[rotate=90] at (1.8,.7) {\scriptsize Erschöpfung};
    \node[rotate=90] at (2.7,.7) {\scriptsize Reserve};

    \begin{scope}[shift={(0,-3.5)}]
      \fill[MidnightBlue!20] (0,2.5) rectangle (5,2.7);
      \fill[MidnightBlue!40] (0,.5) rectangle (5,.3);
      \draw[<->] (0,3) node[left] {$E$} |- (5,0) node[right] {$1/T$};
      \draw (0,2.5) node[left] {$E_L$} -- (5,2.5);
      \draw (0,.5) node[left] {$E_V$} -- (5,.5);
      \draw[dashed] (0,2.2) node[left] {$E_D$} -- (5,2.2);
      \draw[dotted] (0,1.5) node[left] {$E_i$} -- (5,1.5);
      \draw[MidnightBlue] (0,1.5) to[out=0,in=180]
      node[pos=.35,pin={below right:Fermi-Energie $E_F(T)$}] {} (3,2.4) -- (5,2.4);
      \draw[DarkOrange3,<->] (5.3,.5) -- node[right] {$E_g$} (5.3,2.5);
      \draw[DarkOrange3,->] (.9,3) -- (.9,2.5);
      \draw[DarkOrange3,->] (.9,1.7) -- node[left] {$E_d$} (.9,2.2);
    \end{scope}
    \draw[help lines] (1,3) -- (1,-3.3);
    \draw[help lines] (2.5,3) -- (2.5,-3.3);
  \end{tikzpicture}
  \caption{Oben ist die Abhängigkeit der Elektronenkonzentration im
    Leitungsband des n-Halbleiters von der Temperatur für verschiedene
    Donatorkonzentrationen $N_D$ und $N_D'$ dargestellt, wobei
    $N_D'>N_D$.  $E_g$ ist die Breite der Bandlücke und $E_d$ die
    Ionisierungsenergie der Donatoren.  Unten ist die Lage des
    Fermi-Niveaus in Abhängigkeit von der Temperatur dargestellt.
    Ganz klar sind $E_L$ und $E_V$ die Leitungs- und Valenzbandkanten,
    $E_D$ die Lage des Donatorniveaus und $E_i$ die Fermi-Energie des
    intrinsischen Halbleiters.}
  \label{fig:6.3}
\end{figure}

\section{Leitfähigkeit und Beweglichkeit}

Weiter oben haben wir bereits den Begriff der Leitfähigkeit für
Halbleiter verallgemeinert
\begin{equation}
  \label{eq:9.24}
  \sigma = |e| (n\mu_n + p\mu_p) \; .
\end{equation}
Aus der Elektrodynamik ist bekannt, dass für die Stromdichte
\begin{equation}
\label{eq:9.25}
  \bm j = \sigma \bm E
\end{equation}
gilt.

Um ein besseres Verständnis der Eigenschaften des Halbleiters zu
bekommen werden wir die wichtigen Größen stets mit denen eines Metalls
vergleichen.

Beginnen wir mit den Beweglichkeiten der Ladungsträger.  Wie bereits
bekannt tragen im Metall nur diejenigen Elektronen zur Leitfähigkeit
bei, die sich nahe der Fermi-Kante befinden, daher galt dort
\begin{equation*}
 \mu = \frac{e}{m^*} \tau(E_F)
\end{equation*}
Die Zahl der Stöße pro Zeiteinheit
ist proportional zum Streuquerschnitt $\Sigma$ und der Geschwindigkeit
der Elektronen, die im Metall durch die Fermi-Geschwindigkeit gegeben ist.
\begin{equation*}
 \frac{1}{\tau} \sim \Sigma\cdot v(E_F) \implies \Sigma \sim \frac1\tau \; .
\end{equation*}
Im Halbleiter tragen dahingegen alle freien Ladungsträger im Valenz-
und Leitungsband bei, weshalb $\mu_n$ und $\mu_p$ als Mittelwerte über
die von den Elektronen und Löchern besetzten Zustände im unteren
Leitungsband und oberen Valenzband berücksichtigt werden müssen.  Dies
erfordert eigentlich eine Behandlung mit der Boltzmann-Gleichung der
kinetischen Gas-Theorie.  Wir beschränken uns hier aber nur auf eine
qualitative Diskussion der Streuprozesse.  Im Gegensatz zum Metall
müssen wir nun den Mittelwert der Geschwindigkeit betrachten.
\begin{equation}
  \label{eq:9.26}
  \frac1\tau \sim \Sigma\cdot\braket{v} \; .
\end{equation}
Dabei ist $\braket{v}$ im Gegensatz zu Metallen als thermischer
Mittelwert über alle Elektronen- und Löcher-Geschwindigkeiten zu
betrachten.  Nach dem Gleichverteilungssatz der kinetischen
Gas-Theorie gilt
\begin{equation*}
  \braket{v} = \sqrt{\frac{3 \kB T}{m^*}}
\end{equation*}
womit offensichtlich
\begin{equation}
  \label{eq:9.27}
  \braket{v} \sim \sqrt{T} \; .
\end{equation}
Kommen wir nun auf den Streuquerschnitt $\Sigma$ zurück.  Wir
betrachten nun zwei mögliche Streuprozesse der Ladungsträger und zwar
mit Phononen und Störstellen.  Die Streuung der Ladungsträger
untereinander wird auf Grund ihrer geringen Dichte vernachlässigt.  In
Metallen ist der Streuquerschnitt proportional zur Stoßzeit welche
sich auf Grund von $v(E_F) = \const(T)$ ganz einfach zusammensetzt
\begin{equation*}
  \frac1\tau = \frac1{\tau_{\text{Phonon}}} + \frac1{\tau_{\text{Stör}}} \; .
\end{equation*}
Für die Streuung an Phononen gilt im Metall
\begin{equation*}
  \tau_{\text{Phonon}} \sim \frac1T (T\gg\Theta)
  \implies
  \Sigma_{\text{Phonon}} \sim T
\end{equation*}
und damit
\begin{equation}
  \label{eq:9.28}
  \boxed{\mu \sim \frac1T}
  \quad\text{sowie}\quad
  \sigma\sim\frac1T.
\end{equation}
Im Halbleiter gilt für die Streuung an Phononen ebenfalls
\begin{equation*}
  \Sigma_{\text{Phonon}} \sim T \; .
\end{equation*}
Allerdings folgt mit \eqref{eq:9.26} und dem Gleichverteilungssatz
\begin{equation*}
  \frac1{\tau_{\text{Phonon}}} \sim \Sigma_{\text{Phonon}} \cdot \sqrt{T} \sim T^{3/2}
\end{equation*}
und es folgt für die Beweglichkeit
\begin{equation}
  \label{eq:9.29}
  \mu_{\text{Phonon}} \sim \frac1{T^{3/2}} \; .
\end{equation}
Zusätzlich möchten wir noch die Streuung an ionisierten Donatoren und
Akzeptoren, also geladenen Störstellen betrachten.  Dazu benutzen wir
die Formel für die Rutherford-Streuung
\begin{equation*}
  \Sigma_{\text{Stör}} \sim \braket{v}^{-4} \; .
\end{equation*}
Mit dem Gleichverteilungssatz \eqref{eq:9.27} folgt für die Stoßzeit
\eqref{eq:9.26}
\begin{equation*}
  \frac{1}{\tau_{\text{Stör}}} \sim \frac{N_{\text{Stör}}}{T^{3/2}} \; .
\end{equation*}
Damit ist auch die Mobilität gegeben mit
\begin{equation}
  \label{eq:9.30}
  \boxed{\mu \sim T^{3/2}} \; .
\end{equation}

% Störstellen in Metall
% \begin{equation*}
%   \frac{1}{\tau_{\text{Stör}}} \sim N_{\text{Stör}} \cdot \const(T)
% \end{equation*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,note/.style={text width=2cm,align=right}]
    \draw[<->] (0,2) node[left] {$\log\mu$} |- (3,0) node[below] {$\log T$};
    \draw[MidnightBlue,text=DimGray] (.2,.2) .. controls (1,1.5) and (2,1.5) ..
    node[pos=.1,pin={[note]left:ionisierte Störstellen}] (A) {}
    node[pos=.3,pin={100:$T^{3/2}$}] (B) {}
    node[pos=.7,pin={80:$T^{-3/2}$}] (C) {}
    node[pos=.9,pin={right:Phononen}] (D) {}
    (2.8,.2);
  \end{tikzpicture}
  \caption{Qualitativer Temperaturverlauf der Beweglichkeit der
    Ladungsträger in einem Halbleiter.  Für kleine Temperaturen ist
    die Beweglichkeit von der Streuung an den Störstellen, für große
    Temperaturen durch die Streuung an Phononen dominiert.}
  \label{fig:6.4}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: