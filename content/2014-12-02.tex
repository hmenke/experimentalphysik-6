Die Magnetisierung ist damit gegeben durch
\begin{equation}
  \label{eq:11.20}
  M = n \bar\mu_z = n g \mu_B J \mathcal B(x) \; .
\end{equation}
Der Überschuss der parallelen gegenüber den antiparallelen
magnetischen Momenten ist verantwortlich für den Paramagnetismus.

Für sehr tiefe Temperaturen ist $\kB T \ll g \mu_B J B$ und damit geht
$x \sim 1/\kB T \to \infty$.  Geht $x$ in \eqref{eq:11.19} gegen
unendlich, so geht $\mathcal B(x) \to 1$.  Das heißt, dass alle
Momente in Feldrichtung ausgerichtet sind, also die
Sättigungsmagnetisierung tritt ein.  Ist jedoch $g \mu_B J B \ll \kB
T$, so tritt der entgegengesetzte Fall mit $x \ll 1$ ein.  Entwickelt
man die Brillouin-Funktion mit Hilfe von
\begin{equation*}
  \coth x \approx \frac1x + \frac x3 + \mathcal O(x^2)
\end{equation*}
dann ergibt sich für das magnetische Moment
\begin{equation*}
  \bar\mu_z = \frac{g^2 \mu_B J(J+1) B}{3 \kB T}
\end{equation*}
und damit das \acct{Curie-Gesetz} für die Suszeptibilität
\begin{equation}
  \label{eq:11.21}
  \chi = \frac{n \bar\mu_z \mu_0}{B}
  = \frac{n \mu_0 g^2 J(J+1) \mu_B^2}{3 \kB T}
  = \frac{n \mu_0 p^2 \mu_B^2}{3 \kB T}
  = \frac{C}{T}
\end{equation}
mit der effektiven Magnetonenzahl $p = g \sqrt{J(J+1)}$.

\minisec{Diskussion}

Im Fall von reinem Spinmagnetismus liefert nur der Spin einen Beitrag
zum magnetischen Moment und der Bahndrehimpuls verschwindet.
\begin{equation*}
  L=0\;,\quad J=S=\frac N2\;,\quad g_S=2
\end{equation*}
wobei $N$ die Anzahl der Spins ist. Führt man das Material in
Sättigungsmagnetisierung über, so gilt
\begin{equation*}
  M = n g \mu_B J = n 2 \mu_B \frac N2 = N n \mu_B
\end{equation*}
woraus sich die Zahl der Spins $N$ ermittelt lässt.

In seltenen Erden werden die magnetischen Momente von den Elektronen
der $4f$-Schale verursacht, wie in
$[K,L,M,4s^24p^64d^{10}4f^*,5s^2,5p^6]$.  Die $4f$-Elektronen werden
jedoch von den weiter außen liegenden $5s$- und $5p$-Elektronen
abgeschirmt.  Daher passen die experimentellen Werte für die effektive
Magnetonenzahl $p_{\text{exp}}$ gut zu den theoretischen Werten
$p_{\text{theo}}$, vergleiche Tabelle~\ref{tab:15.1}.

\begin{table}
  \centering
  \begin{tabular}{r@{\quad}*{4}{>{$}c<{$}}SS}
    \toprule
    Ionen $3^+$, \SI{300}{\K} & x & L & S & J & {$p_{\text{exp}}$} & {$p_{\text{theo}}$} \\
    \midrule
    Cenium \ch{Ce^3+} & 1 & 3 & 1/2 & 5/2 & 2.4 & 2.54 \\
    Holonium \ch{Ho^3+} & 10 & 6 & 2 & 8 & 10.4 & 10.6 \\
    \bottomrule
  \end{tabular}
  \caption{Drehimpulse und effektive Magnetonenzahlen einiger Ionen
    seltener Erden.}
  \label{tab:15.1}
\end{table}

In Übergangsmetallen wie Salzen der Eisengruppe $[K,L,3s^2p^6d^x]$
werden die $d$-Elektronen nicht abgeschirmt, weshalb der Beitrag des
Bahndrehimpulses ausgelöscht wird.  Als Beispiel berechne man die
effektive Magnetonenzahl von \ch{Cr^3+}:
\begin{gather*}
  \ch{Cr^3+} \quad x=3 \quad L=3 \quad S=\frac32 \quad J=\frac32 \\
  p_{\text{theo}} = \num{0.77} \quad p_{\text{exp}} = \num{3.8}
\end{gather*}
Falls aber nur der Spinmagnetismus berücksichtigt wird ergibt sich
$p_{\text{theo}} = \num{3.87}$, was sehr nahe am experimentellen Wert
von $p_{\text{exp}} = \num{3.8}$ liegt.  Hier ist also allein der Spin
verantwortlich für den Paramagnetismus, obwohl die Atome einen
Bahndrehimpuls $L\ne0$ haben.  Der Grund sind stark inhomogene
elektrische Kristallfelder.  Diese kommen bei seltenen Erden nicht zur
Wirkung, da die $f$-Schale sehr gut durch die $5s^2,5p^6$-Elektronen
abgeschirmt ist.  Im Kristall wirkt ein elektrisches Feld auf die
Elektronenkonfiguration und auf den Bahndrehimpuls, nicht jedoch auf
den Spin.  Die Wechselwirkung mit den Kristallfeld kann sogar die
Energie der $LS$-Kopplung übertreffen und diese aufbrechen.  $L_z$ ist
dann keine konstante der Bewegung mehr
\begin{equation*}
\bar L_z = 0 \to \mu_L = 0
\end{equation*}
und allein der Spinmagnetismus bewirkt Paramagnetismus.

Noch komplizierter wird die Lage für magnetische Ionen mit $d$- bzw.\
$f$-Elektronen in Metallen.  Die Legierung \ch{CuMn} ist metallisches
Kupfer mit einer geringen Konzentration an Mangan.  Mangan hat die
Konfiguration $3d^54s^2$, der Grundzustand ist $^6S_{5/2}$. In diesem
Fall sind die paramagnetischen Eigenschaften wie erwartet.  Verwendet
man anstatt Kupfer Aluminium für die Legierung, also \ch{AlMn}, dann
verschwinden die magnetischen Eigenschaften vollständig.  Die Ursache
ist die starke Wechselwirkung der $d$-Elektronen mit den
delokalisierten $s$-Elektronen von Aluminium.  Dies führt zu einer
Hybridisierung der Zustände und der Unterdrückung der lokalen
magnetischen Momente.

\minisec{Anwendungen}

Die Messung der Magnetisierung, bzw.\ der Suszeptibilität wird in der
Tieftemperaturphysik zur Temperaturmessung herangezogen.  Typische
Materialien sind Cer-Magnesium-Nitrat (CMN), welches Temperaturmessung
bis auf wenige Millikelvin erlaubt.  Die Antwort dieses Materials auf
Temperaturänderung ist jedoch sehr langsam.  Metalle wie \ch{PdFe}
oder \ch{AuEr} eignen sich besser, da sie eine bessere
Wärmeleitfähigkeit besitzen und sich das Temperaturgleichgewicht
schneller einstellen kann.

SQUID-Magnetometer erlauben Suszeptibilitätsmessungen mit hoher
Empfindlichkeit und Genauigkeit.

Für Temperaturen kleiner als \SI{10}{\milli\K} trägt der Kern zum
magnetischen Moment bei, obwohl
\begin{equation*}
  \mu_K \sim \frac{1}{1000} \mu_e \; .
\end{equation*}


\section{Paulische Spinsuszeptibilität}

Wir möchten die temperaturabhängige paramagnetische Suszeptibilität
des freien Elektronen-Gases erhalten. Das Elektron im Magnetfeld hat
zwei mögliche Einstellungen. Die Energieaufspaltung der beiden Niveaus
ist
\begin{equation}
\label{eq:11.22}
  \delta E = g \mu_B B \; .
\end{equation}
Mit $g = 2$ gilt $\delta E = 2 \mu_B B$.  Aus dem Curiegesetz
\eqref{eq:11.21} folgt
\begin{equation}
  \label{eq:11.23}
  \chi_p' = \frac{n \mu_0 \mu_B^2}{\kB T}
\end{equation}
Setzt man Zahlenwerte für Natrium bei Zimmertemperatur ein, so ergibt
sich $\chi'_{\ch{Na}} = \num{6.9e-4}$.  Der experimentelle Wert ist
mit $\chi_{\ch{Na}}' = \num{8.6e-6}$ jedoch um Größenordnungen zu
klein.  Dies liegt daran, dass die Fermi-Statistik eine klassische
Besetzung der beiden Zustände verhindert.  Ähnlich wie bei der
spezifischen Wärme trägt nur ein Bruchteil der Spins zur
Magnetisierung bei.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[no marks,domain=-2:2,axis on top,ymin=0,ymax=4,enlargelimits=false,
      xtick=\empty,ytick=\empty,extra y ticks={3},extra y tick labels={$E_F$},
      xlabel={Zustandsdichte ${}^{1}\!/{}_{\!2}\, D(E)$},ylabel={Energie $E$}]
      \fill[MidnightBlue!20] plot[smooth,domain=-sqrt(2):0] (axis cs: \x,{(\x)^2+1}) -- (axis cs:0,3) -- cycle;
      \fill[MidnightBlue!20] plot[smooth,domain=0:sqrt(2)] (axis cs: \x,{(\x)^2}) -- (axis cs:0,2) -- cycle;
      \fill[MidnightBlue!40] plot[smooth,domain=sqrt(2):sqrt(3)] (axis cs: \x,{(\x)^2}) -- (axis cs:0,3) -- (axis cs:0,2) -- cycle;
      \draw (axis cs:0,0) -- (axis cs:0,4);
      \node at (axis cs:.8,2.5) {$\delta n$};
      \draw[very thick,->] (axis cs:.3,1.2) -- (axis cs:.3,1.8);
      \draw[very thick,->] (axis cs:-.3,1.8) -- (axis cs:-.3,1.2);
      \addplot[name path=A,MidnightBlue,domain=-2:0] {x^2+1};
      \addplot[dashed] {x^2+.5};
      \addplot[name path=B,MidnightBlue,domain=0:2] {x^2};
      \addplot[name path=Ef] {3};
      \draw[DarkOrange3,decorate,decoration=brace] (axis cs:0,0) -- node[left] {$2\mu_B B$} (axis cs:0,1);
      \draw[DarkOrange3,decorate,decoration=brace] (axis cs:0,2) -- node[left] {$2\mu_B B$} (axis cs:0,3);
    \end{axis}
  \end{tikzpicture}
  \caption{Es findet eine Verschiebung der Zustandsdichte mit
    unterschiedlicher Spinrichtung im Magnetfeld statt.  Auf der
    linken Seite ist das magnetische Moment antiparallel auf der
    rechten Seite parallel zum Magnetfeld.  Die Elektronen in der
    dunkelblau eingefärbten Fläche bewirken die beobachtete
    Magnetisierung.  Nach \textcite{hunklinger}.}
  \label{fig:15.1}
\end{figure}

Da $\mu_B B \ll E_F$ ist die Verschiebung der Zustände klein.  Die
Zustandsdichte kann im Bereich $E_F \pm \mu_B B$ als konstant
angenommen werden.
\begin{equation}
  \label{eq:11.24}
  \delta n = \frac12 D(E_F) \mu_B B \cdot 2
\end{equation}
Wegen der kleinen Verschiebung sind auch die Magnetisierung und die
Suszeptibilität klein.
\begin{align}
  \notag
  M &= \delta n \mu_B = D(E_F) \mu_B^2 B = \frac{3 n \mu_B^2 B}{2 \kB T_F} \\
  \label{eq:11.25}
  \chi_p &= \mu_0 D(E_F) \mu_B^2
\end{align}
Aus dem Vergleich von \eqref{eq:11.23} und \eqref{eq:11.25} folgt
\begin{equation}
  \label{eq:11.26}
  \frac{\chi_p}{\chi_p'} = \frac32 \frac{T}{T_F} \; .
\end{equation}
Die Reduktion gegenüber der klassischen Rechnung ist also ein Faktor
$\sim T/T_F$ (siehe spezifische Wärme).  Leitungselektronen besitzen
gleichzeitig auch diamagnetische Eigenschaften, die auf die
Bahnbewegung zurückgehen.  Falls $m_{\text{eff}} = m_0$ und
\eqref{eq:11.9}
\begin{equation}
  \label{eq:11.27}
  \chi = \chi_p - \chi_d = \boxed{ \frac23 \mu_0 D(E_F) \mu_B^2 } \; .
\end{equation}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: