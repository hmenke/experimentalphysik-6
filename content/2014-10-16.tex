\section{Löcher}

In der Halbleiterphysik und der Festkörperphysik spielen Eigenschaften
von unbesetzten Zuständen in einem sonst gefüllten Band eine wichtige
Rolle.

Der Formalismus der zweiten Quantisierung erlaubt es uns den Begriff
der Defektelektronen oder Löcher elegant einzuführen.  Wir gehen dazu
von einem gefüllten Valenzband, beschrieben durch den Index~$V$, aus.
Die Wellenfunktion $\Phi$ des gefüllten Valenzbandes mit $N$~Moden
setzt sich zusammen aus
\begin{equation*}
  \Phi_V \equiv \hat a_{k_1,V}^\dagger \hat a_{k_2,V}^\dagger \cdots \hat a_{k_N,V}^\dagger \Phi_0
\end{equation*}
wobei $\Phi_0$ den Vakuumzustand darstellt.  Entfernen wir ein
Elektron im Zustand $k$ aus dem Band, so schreiben wir
\begin{equation*}
  \Phi_k = \hat a_{k,V} \Phi_V .
\end{equation*}
Die dabei entstehende Fehlstelle können wir wiederum als Quasiteilchen
sehen und ordnen diesem die Bezeichnung \enquote{Loch} (engl.:
\textsl{hole}) zu.  Offensichtlich entspricht die Vernichtung eines
Elektrons der Erzeugung eines Lochs und wir können die Transformation
\begin{equation*}
  \hat a_{k,V} = \hat d_k^\dagger
  \;,\quad
  \hat a_{k,V}^\dagger = \hat d_k
\end{equation*}
einführen, wobei $\hat d_k^\dagger$ der Erzeuger und $\hat d_k$ der
Vernichter eines Lochs im Zustand $k$ ist.

Elektronen im Festkörper werden durch den Hamiltonoperator des
Hubbard"=Modells beschrieben.
\begin{equation*}
  H = \sum_{lm} E(l | m)\, \hat a_l^\dagger \hat a_m
  + \frac12 \sum_{lml'm'} V(lm | m'l')\,
  \hat a_l^\dagger \hat a_m^\dagger \hat a_{m'} \hat a_{l'} .
\end{equation*}
Dabei vereint $E(l | m)$ die kinetische Energie des
Elektrons und die Energie durch ein externes Potential, während
$V(lm | m'l')$ die Coulomb"=Wechselwirkung beinhaltet.  Nun lassen
wir die Indizes nur über die Quantenzahlen des Valenzbandes laufen,
wenden die obige Transformation auf Defektelektronen an und sortieren
die Operatoren um.  Nach einiger Rechnung ergibt sich mit der
Ersetzung der Quantenzahlen des Valenzbandes durch Wellenvektoren der
Blochwellen
\begin{equation*}
  H = E_V - \sum_{\bm k} E_{\bm k,V}\, \hat d_{\bm k}^\dagger \hat d_{\bm k}
  + \frac12 \sum_{\bm k_1,\bm k_2,\bm k_3,\bm k_4} W(\bm k_3 \bm k_4 | \bm k_1 \bm k_2)\,
  \hat d_{\bm k_1}^\dagger \hat d_{\bm k_2}^\dagger \hat d_{\bm k_3} \hat d_{\bm k_4} .
\end{equation*}
Dabei ist $E_V$ die Energie des vollen Valenzbandes, der zweite Term
gibt die Energie der Defektelektronen ohne Paarwechselwirkung an und
der dritte Term ist die Coulomb"=Wechselwirkung der Löcher.  Es
scheint, als ob die Defektelektronen eine negative und somit
unphysikalische Energie hätten.  Entwickelt man jedoch $E_{\bm k,V}$
um $k=0$ erhält man
\begin{equation*}
  E_{\bm k,V} = E_{0,V} - \frac{\hbar^2 k^2}{2 m^*} .
\end{equation*}
Lässt man die Wechselwirkung der Defektelektronen außer acht und
vernachlässigt die konstante Energieverschiebung $E_V$ erhält man den
Hamiltonoperator der Defektelektronen
\begin{equation*}
  H = \sum_{\bm k} \hat d_{\bm k}^\dagger \hat d_{\bm k}
  \left( \frac{\hbar^2 k^2}{2 m^*} - E_{0,V} \right) .
\end{equation*}
Defektelektronen verhalten sich also wie massive Teilchen mit
effektiver Masse $m^*$.  Um zu verstehen, warum sich die
Defektelektronen verhalten als hätten sie positive Elementarladung,
berechnen wir ihre Ladungsdichte.  Dazu schreiben wir die
Feldoperatoren $\hat\psi(\bm x)$ als Entwicklung nach den
Blochfunktionen.
\begin{align*}
  \hat\varrho(\bm x)
  &= e \hat\psi^\dagger(\bm x) \hat\psi(\bm x)
  = e \sum_{\bm k \bm k'} \varphi_{\bm k}^*(\bm x) \varphi_{\bm k'}(\bm x)
  \hat a_{\bm k}^\dagger \hat a_{\bm k'}
\intertext{wir setzen die Transformation auf Defektelektronen ein,
  nutzen die Antikommutatorrelationen um die Operatoren zu vertauschen,
  tauschen die Indizes und erhalten so}
  \hat\varrho(\bm x)
  &= e \sum_{\bm k} |\varphi(\bm k)|^2
  - e \sum_{\bm k \bm k'} \varphi_{\bm k'}^*(\bm x) \varphi_{\bm k}(\bm x)
  \hat d_{\bm k}^\dagger \hat d_{\bm k'} .
\end{align*}
Der erste Term ist offensichtlich die Ladungsdichte des vollen
Valenzbandes und muss von den positiven Kernen der Gitterbausteine
kompensiert werden, damit der gesamte Kristall neutral ist.  Der
zweite Term ist vollkommen analog zur Ladungsdichte der Elektronen in
der Zeile darüber, jedoch mit einem negativen Vorzeichen für die
Ladung.  Es bliebe noch zu zeigen, dass der Erwartungswert eine
positive Ladungsdichte ergibt.  Dazu und für die ausführliche Rechnung
verweisen wir auf \textcite[147--152]{haken-qft}.

Es ist interessant zu sehen, dass die Ladungsumkehr der
Defektelektronen durch die mathematischen Vertauschungsrelationen
ihrer Operatoren folgt.  Die Ladungsumkehr ist jedoch nicht nur ein
mathematischer Trick, sondern kann im Rahmen des anomalen Hall"=Effekts
in Halbleitern nachgewiesen werden.

\minisec{Eigenschaften}

Im Folgenden werden Löcher den Index~$h$ erhalten (engl.:
\textsl{hole}) und Elektronen den Index~$e$.
\begin{enumerate}
\item Wellenvektor:
  \begin{equation}
    \label{eq:8.24}
    \bm k_h = - \bm k_e    
  \end{equation}
  Der Gesamtwellenvektor in einem gefüllten Band ist $\sum \bm k =
  0$. Fehlt ein Elektron mit $\bm k_e$ ist der Gesamtwellenvektor des
  System $-\bm k_e$.  Unser Arbeitsbegriff ist
  \begin{quote}
    Das Loch ist eine andere Beschreibung für ein Band mit fehlenden
    Elektronen.
  \end{quote}
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx]
      \draw[->] (-2,0) -- (2,0) node[below] {$k$};
      \draw[->] (0,-.2) -- (0,2) node[left] {$E$};
      \draw[DarkOrange3]
      (-2,-.5) to[out=20,in=180] node[MidnightBlue,dot,label={[MidnightBlue]below:$\bm k_e$}] (ke1) {}
      (0,0) to[out=0,in=160] node[MidnightBlue,dot,label={[MidnightBlue]below:$\bm k_h$}] (kh) {}
      (2,-.5);
      \draw[DarkOrange3] (-2,2) to[out=-20,in=180] node[MidnightBlue,dot] (ke2) {} (0,1.5) to[out=0,in=200] (2,2);
      \draw[MidnightBlue,->] (ke1) -- (ke2);
    \end{tikzpicture}
    \caption{Wird ein Elektron mit Wellenvektor $\bm k_e$ aus dem
      Valenzband ins Leitungsband geschickt entsteht eine Fehlstelle
      mit Wellenvektor $-\bm k_e$.}
    \label{fig:2.1}
  \end{figure}
\item Energie:
  \begin{equation}
    \label{eq:8.25}
    \varepsilon_h(\bm k_h) = - \varepsilon_e(\bm k_e)
  \end{equation}
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx]
      \draw[->] (-2,0) -- (2,0) node[below] {$k$};
      \draw[->] (0,-.2) -- (0,2) node[left] {$E$};
      \draw[DarkOrange3]
      (-2,-.5) to[out=20,in=180] node[dot,label={below:$\bm k_e$}] (ke1) {} (0,0) to[out=0,in=160] (2,-.5) node[below right] {Inversion des Bandes};
      \draw[MidnightBlue] (-2,.5) to[out=-20,in=180] (0,0) to[out=0,in=200] node[dot,label={above:$\bm k_h$}] (kh) {} (2,.5) node[above right] {Lochband};
    \end{tikzpicture}
    \caption{Anstelle von Löchern im Band kann man sich auch ein
      zusätzliches Lochband definieren, das aus der Inversion des
      eigentlichen Bandes entsteht.}
    \label{fig:2.2}
  \end{figure}
  Je tiefer im Band das fehlende Elektron sitzt, desto größer ist die
  Energie des Systems.  Die Energie des Lochs hat das entgegengesetzte
  Vorzeichen zur Energie des fehlenden Elektrons
  \begin{equation*}
    \varepsilon_e(\bm k_e) = \varepsilon_e(-\bm k_e) = -\varepsilon_h(\bm k_e) = - \varepsilon_h(\bm k_h) \; .
  \end{equation*}
  Dies folgt aus der Symmetrie des Bandes.
\item Geschwindigkeit:
  \begin{equation}
    \label{eq:8.26}
    \bm v_h(\bm k_h) = \bm v_e(\bm k_e)
  \end{equation}
  Die Ableitung ist offensichtlich
  \begin{align*}
    \nabla_k \varepsilon_h(\bm k_h) &= \nabla_k \varepsilon_e(\bm k_e) \\
    \bm v_h &= \bm v_e
  \end{align*}
\item Masse
  \begin{equation}
    \label{eq:8.27}
    m_h^* = - m_e^*
  \end{equation}
  Wie auch im Falle von Elektronen ist die effektive Masse umgekehrt
  proportional zur Krümmung des Bandes, vgl.\ \eqref{eq:8.20}.
\item Kraft:
  \begin{equation}
    \label{eq:8.28}
    \hbar \frac{\diff \bm k_h}{\diff t} = e (\bm E + \bm v_h \times \bm B)
  \end{equation}
  Diese Beziehung folgt aus der Bewegungsgleichung \eqref{eq:8.17}:
  \begin{equation*}
    \hbar \frac{\diff \bm k_e}{\diff t} = - e (\bm E + \bm v_e \times \bm B)
  \end{equation*}
  mit $\bm k_e = - \bm k_h$ und $\bm v_e = \bm v_h$ folgt unmittelbar \eqref{eq:8.28}.
  Die Bewegungsgleichung eines Lochs ist die eines Teilchens mit positiver Ladung $e$.
\end{enumerate}

\begin{notice}[Ausblick:]
  Beim Stromtransport bewegen sich Elektronen und Löcher in
  entgegengesetzte Richtungen, da sich Elektronen an der Unterkante des
  Leitungsbandes und die Löcher sich an der Oberkante des Valenzbandes
  aufhalten. Ihr Strombeitrag addiert sich.

  \begin{figure}[tb]
    \centering
      \begin{minipage}{.5\linewidth}
    \centering
    \begin{tikzpicture}[gfx]
      \draw[->] (-2,0) -- (2,0) node[below] {$k$};
      \draw[->] (0,-.2) -- (0,2) node[left] {$E$};
      \draw[DarkOrange3] (-2,-.5) to[out=20,in=180] (0,0) to[out=0,in=160] (2,-.5) node[below right] {Valenzband};
      \draw[MidnightBlue] (-2,2) to[out=-20,in=180] (0,1.5) to[out=0,in=200] (2,2) node[above right] {Lochband};
    \end{tikzpicture}    
  \end{minipage}%
  \begin{minipage}{.5\linewidth}
    \centering
    \begin{tikzpicture}[gfx]
      \node (n+) at (0,0) {$+$};
      \node (n-) at (2,0) {$-$};
      \draw[->] (n+) -- (n-); \node[below of= n+] (ve) {$\bm v_e^{\text{Drift}}$};
      \node[below of=n-] (vh) {$\bm v_h^{\text{Drift}}$}; \draw[->] (ve) -- +(-1,0);
      \draw[->] (vh) -- +(+1,0);
    \end{tikzpicture}
  \end{minipage}
  \caption{Legt man an einen Kristall ein elektrisches Feld an, so
    driften die Elektronen und Löcher in entgegengesetzte Richtungen.
    Beide Arten von Ladungsträgern tragen also zum Stromtransport bei
    und ihr Beitrag addiert sich.}
  \label{fig:2.4}
  \end{figure}
\end{notice}

\section{Boltzmann"=Gleichung und Relaxationszeit"=Näherung}

In unserer bisherigen makroskopischen Beschreibung galt $\sigma = e
\mu n$ und damit für die Mobilität der Ladungsträger
\begin{equation*}
  \mu = \frac{e}{m} \bar\tau = \frac{v_D}{E}
\end{equation*}
mit der gemittelten Relaxationszeit $\bar\tau$ und der
Driftgeschwindigkeit $v_D$.  Für eine mikroskopische Beschreibung
betrachten wir nun die Verteilungsfunktion $f(\bm r,\bm k,t)$. Diese
beschreibt die Ladungsträger am Ort $\bm r$ zur Zeit $t$ in den
Zuständen $\bm k$.

Im thermodynamischen Gleichgewicht wird die Verteilungsfunktion der
Elektronen durch die \acct{Fermi"=Dirac"=Statistik} beschrieben.
\begin{equation*}
  f_0(E(\bm k)) = \frac{1}{\ee^{(E(\bm k)-\mu)/\kB T}+1}.
\end{equation*}

\minisec{Nichtgleichgewichtsverhalten (Einschalten einer Störung)}

Betrachten wir eine kleine Störung der Gleichgewichtsverteilung.  Sei
$\delta f$ dazu eine kleine Auslenkung aus der
Gleichgewichtsverteilung $f_0$:
\begin{equation*}
  f(\bm r,\bm k,t) = f_0 + \delta f.
\end{equation*}
Nach Abschalten der Störung relaxiert die Verteilung exponentiell
zurück ins Gleichgewicht mit einer Proportionalität $\delta f \sim
\exp(-t/\tau)$.  Damit schreiben wir den Stoßzahloperator als
\begin{equation}
  \label{eq:8.29}
  \left.\frac{\diff \delta f}{\diff \tau}\right|_{\text{Stöße}} = - \frac{\delta f}{\tau}.
\end{equation}
Diese Näherung heißt \acct{Relaxationszeitnäherung} mit der
Relaxationszeit $\tau$.

Im stationären Gleichgewicht ist die Verteilung $f$ lokal zeitunabhängig, also
\begin{equation*}
    \frac{\partial f}{\partial t} = 0 .
\end{equation*}
Außerdem ist die Gleichgewichtsverteilung $f_0$ sowieso
zeitunabhängig, sonst wäre sie ja nicht im Gleichgewicht
\begin{equation*}
  \frac{\diff f}{\diff t}
  = \underbrace{\frac{\diff f_0}{\diff t}}_{=0} + \frac{\diff \delta f}{\diff t}
  = \frac{\diff \delta f}{\diff t} .
\end{equation*}
Daraus ergibt sich in Relaxationszeitnäherung
\begin{equation*}
  \left.\frac{\diff}{\diff t} f(\bm r,\bm k,t)\right|_{\text{Stöße}}
  = \left.\frac{\diff}{\diff t} \delta f\right|_{\text{Stöße}}
  = - \frac{\delta f}{\tau} .
\end{equation*}

Setzen wir nun diese Annahmen in die Boltzmann Transportgleichung ein
so ergibt sich
\begin{equation*}
  \left. \frac{\diff f}{\diff t} \right|_{\text{Stöße}} =
  \underbrace{\frac{\partial f}{\partial t}}_{=0}
  + \underbrace{\frac{\partial f}{\partial \bm r}}_{\text{(a)}} \dot{\bm r}
  + \underbrace{\frac{\partial f}{\partial \bm k}}_{\text{(b)}} \dot{\bm k} .
\end{equation*}
Der Term (a) heißt Diffusionsterm, da er den Populationsgradient mit
der Geschwindigkeit $\dot{\bm r}$ transportiert.  Ähnlich dazu heißt
(b) Kraftterm.  Da hier die Kraft durch ein angelegtes Feld bestimmt
wird nennt man (b) manchmal auch Feldterm.

Schreiben wir die Gradienten aus und setzen die Lorentzkraft des
elektromagnetischen Feldes ein erhalten wir die
\acct{Boltzmanngleichung in Relaxationszeitnäherung}.
\begin{align}
  \label{eq:8.30}
  - \frac{\delta f}{\tau} &= \bm v \grad_{\bm r} f + \dot{\bm k} \grad_{\bm k} f \; , \\
  \label{eq:8.31}
  \Aboxed{- \frac{\delta f}{\tau} &= \bm v \grad_{\bm r} f - \frac{e}{\hbar} (\bm E + \bm v \times \bm B) \grad_{\bm k} f}\; .
\end{align}

Sei die Störung $\delta f$ nun infinitesimal, so kann man die
Auslenkung aus dem Gleichgewicht vernachlässigen
\begin{equation*}
  f = f_0 + \delta f \approx f_0 .
\end{equation*}
Damit wurde die Boltzmanngleichung in Relaxationszeitnäherung
linearisiert.\index{Boltzmanngleichung in
  Relaxationszeitnäherung!linearisiert}.
\begin{equation}
  \label{eq:8.32}
  \boxed{\delta f = - \tau \left[ \bm v \grad_{\bm r} f_0
      - \frac{e}{\hbar} (\bm E + \bm v \times \bm B) \grad_{\bm k} f_0 \right]}
\end{equation}

Gesucht ist die Nichtgleichgewichtsverteilung unter Einfluss eines
homogenen elektrischen Feldes $\bm E$.  Wir nehmen einen stationären
isotropen Anfangszustand im Gleichgewicht an. Für diesen stationären
Zustand gilt $\partial_t f=0$. Außerdem hängt $f$ dann nicht vom Ort
ab, d.h.\ $\grad_{\bm r} f = 0$.  Setzen wir diese Annahmen in die
linearisierte Boltzmanngleichung ein
\begin{align}
  \notag
  \delta f &= \tau \frac{e}{\hbar} \bm E \grad_{\bm k} f_0 \; , \\
  \label{eq:8.33}
  f(\bm k) &= f_0(\bm k) + \tau \frac{e}{\hbar} \bm E \grad_{\bm k} f_0 \; .
\end{align}
Gleichung \eqref{eq:8.33} hat Ähnlichkeit mit einer
Taylorentwicklung von $f_0(\bm k + \Delta \bm k)$ um den Punkt
$\Delta \bm k = 0$ (in einem linearisierten Problem), also
\begin{equation*}
  f_0(\bm k + \Delta k) \approx f_0(\bm k) + \Delta k \grad_{\bm k} f_0(\bm k).
\end{equation*}
Dies benutzen wir um die Nichtgleichgewichtsverteilung durch die
Gleichgewichtsverteilung anzunähern.
\begin{equation}
  \label{eq:8.34}
  f(\bm k) \approx f_0 \left( \bm k + \frac{e}{\hbar} \tau \bm E \right).
\end{equation}
Die sich unter dem Einfluss eines äußeren Feldes $\bm E$ und der
Wirkung von Stößen einstellende stationäre Verteilung lässt sich somit
als eine um $e \tau \bm E/\hbar$ verschobene
Gleichgewichts"=Fermi"=Verteilung darstellen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,scale=.7,dist/.style={out=0,in=180,looseness=0.5}]
    \begin{scope}
      \draw[->] (-2,0) -- (3,0) node[below] {$k_x$};
      \draw[->] (0,-2) -- (0,2) node[left] {$k_y$};
      \draw[dashed] (0,0) circle (1.5);
      \draw[MidnightBlue,->] (0,0) -- node[below] {$\delta k_x$} (1,0);
      \draw[DarkOrange3] (1,0) circle (1.5);
      \draw[Purple,->] (0,0) -- (135:1.5) node[above left] {$\bm k_F$};
    \end{scope}
    \begin{scope}[shift={(6,0)}]
      \draw[->] (-2,0) -- (3,0) node[below] {$k$};
      \draw[->] (0,-2) -- (0,2) node[left] {$f$};
      \draw[dashed] (-1.5,0) to[dist] (-1,1) node[above left] {$f_0$} -- (1,1) to[dist] (1.5,0);
      \draw[MidnightBlue,->] (0,0) -- node[below] {$\delta k_F$} (1,0);
      \begin{scope}[shift={(1,0)}]
        \draw[DarkOrange3] (-1.5,0) to[dist] (-1,1) -- (1,1) node[above right] {$f$} to[dist] (1.5,0);
        \draw[dotted] (0,1.2) -- (0,-.2);
      \end{scope}
    \end{scope}
    \begin{scope}[shift={(12,0)}]
      \draw[->] (-2,0) -- (3,0) node[below] {$k$};
      \draw[->] (0,-2) -- (0,2) node[left] {$f-f_0$};
      \begin{scope}[shift={(-1,0)}]
        \draw[DarkOrange3] plot[domain=-.6:.6,smooth] (\x,{-exp(-(5*\x)^2)});
      \end{scope}
      \begin{scope}[shift={(1,0)}]
        \draw[DarkOrange3] plot[domain=-.6:.6,smooth] (\x,{exp(-(5*\x)^2)});
      \end{scope}
      \draw[Purple,->] (0,0) -- node[below] {$\delta k_F$} (1,0);
    \end{scope}
  \end{tikzpicture} 
  \caption{Im Text wurde die Nichtgleichgewichtsverteilung, die durch
    ein elektrisches Feld erzeugt wird durch eine verschobene
    Gleichgewichtsverteilung angenähert.  Graphisch ergibt sich dann
    natürlich eine Verschiebung der Fermi"=Kugel und der Verteilungen
    im $k$-Raum.  Die Abweichung vom Gleichgewicht ist nur im Überhang
    von $f$ und $f_0$ von Null verschieden und für kleine $\delta k_F$
    scharf lokalisiert.}
  \label{fig:2.3}
\end{figure}

Die Änderung der Fermi"=Verteilung $f(E(\bm k))$ gegenüber der
Gleichgewichtsverteilung $f_0$ ist nur in der Umgebung der
Fermi"=Energie bzw.\ des Fermi"=Radius $k_F$ merklich von Null
verscheiden (Abbildung~\ref{fig:2.3}).

\begin{notice}[Merke:]
  Nur Elektronen in der Nähe der Fermi"=Energie sind in Metallen für
  den Stromtransport relevant.
\end{notice}

Vergleich mit dem Drude"=Modell:
\begin{equation}
  \label{eq:8.35}
  \begin{array}{>{\displaystyle}c>{\displaystyle}c}
    \toprule
    \textbf{Drude} & \textbf{parabolisches Band} \\
    \midrule
    \sigma = \frac{e^2 \tau}{m} n & \sigma = \frac{e^2 \tau(E_F)}{m^*} n \\
    \mu = \frac{e \tau}{m} n & \mu = \frac{e \tau(E_F)}{m^*} \\
    \bottomrule
  \end{array}
\end{equation}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End:
