Wir können die Summe auf ein Integral über alle Zustände erweitern,
müssen dann aber die mittleren Besetzungen der Phononen abhängig von
der Temperatur und deren Zustandsdichte berücksichtigen.
\begin{equation}
  \label{eq:14.16}
  J = \frac{1}{L} \sum_i \int_0^\infty D_i^{\text{1D}} \hbar\omega_i v_{g,i} \left[
    \braket{n_W(\omega,T)} - \braket{n_K(\omega,T)} \right] \diff k
\end{equation}
Der Index $i$ steht für den Phononenzweig.  $D^{\text{1D}}$ ist die
eindimensionale Zustandsdichte und $v_{g,i}$ die
Gruppengeschwindigkeit der Phononen.  Die Verteilungsfunktion ist
gegeben durch die Bose-Einstein-Statistik
\begin{equation*}
  \braket{n(\omega,T)} = \frac{1}{\ee^{\hbar\omega/\kB T} - 1} \; .
\end{equation*}
Wir machen nun den Übergang von $\bm k$ nach $\omega$. Dabei wird nur
eine Mode betrachtet.
\begin{equation}
  \label{eq:14.17}
  J = \frac{1}{\cancel{L}} \frac{\cancel{L}}{2\pi} \int_0^\infty \hbar\omega
  \cancel{v_g} \underbrace{\frac{\partial n}{\partial T} \Delta T}_{(\ast)}
  \underbrace{\frac{\diff k}{\diff \omega}}_{\cancel{v_g}}
  \diff \omega \; ,
\end{equation}
wobei $(\ast)$ der erste Term der Taylorentwicklung ist.  Um das
Integral berechnen zu können substituieren wir $x=\beta\hbar\omega$.
\begin{equation}
  \label{eq:14.18}
  G = \frac{J}{\Delta T} = \frac{\kB^2 T}{h} \sum_i \int_0^\infty
  \frac{x^2 \ee^x}{(\ee^x-1)^2} \diff x
  = \sum_i \frac{\pi^2}{3} \frac{\kB^2 T}{h}
  = N_i G_0 \; .
\end{equation}
Da der Verbindungssteg vier verschiedene Schwingungen ausführen kann,
nämlich eine Dilatations-, eine Torsions- und zwei
Biegeschwingungen ist $N_i = 4$.  Jede Mode liefert denselben
Leitwert.
\begin{equation}
  \label{eq:14.19}
  \boxed{
    G_0 = \frac{\pi^2 \kB^2 T}{3h}
    = \left( \SI{9.46e-13}{\watt\per\square\K} \right) T
  }
\end{equation}

\begin{notice}
  Im allgemeinen Fall muss man noch den Transmissionskoeffizienten
  $\tau$, der die Kopplung zwischen Verbindungssteg und Wärmebad
  charakterisiert berücksichtigen
  \begin{equation}
    \label{eq:14.20}
    G_0 = \frac{\pi^2 \kB^2 T}{3h} \tau
  \end{equation}
\end{notice}
Die Eindimensionalität ist beobachtbar:
\begin{equation*}
  \lambda_{\text{therm}} \geq \text{Querdimension der Probe} \; .
\end{equation*}

\begin{notice}[Nebenrechnung:]
Für die Energie von Schwingungen gilt
\begin{equation*}
  E = h\nu = h \frac{v_{\text{Ph}}}{\lambda} \; .
\end{equation*}
Die thermische Energie ist gegeben durch
\begin{equation*}
  E_{\text{therm}} = \kB T \; .
\end{equation*}
Gleichsetzen der beiden und Umstellen nach $T$ liefert
\begin{equation*}
  \implies T = \frac{h v_{\text{Ph}}}{\lambda \kB} \; .
\end{equation*}
Angenommen der Steg über den die Wärme fließt ist schmäler als
$\lambda/2$, dann folgt für die kritische Temperatur
\begin{equation*}
  w < \frac{\lambda}{2} \text{ Stegbreite}
  \implies T_c \leq \frac{h v_{\text{Ph}}}{2 \lambda \kB} \; .
\end{equation*}
Für einen Steg der Breite $w = \SI{200}{\nm}$ und eine
Phasengeschwindigkeit der Phononen $v_{\text{Ph}} =
\SI{6000}{\m\per\s}$ folgt für die kritische Temperatur $T_c \approx
\SI{0.8}{\K}$.
\end{notice}

\section{Elektronische Zustandsdichten}

Für freie Elektronen gilt
\begin{equation*}
  E = \hbar\omega = \frac{\hbar^2 k^2}{2 m} \;,\quad
  v_g = \frac{\diff\omega}{\diff k} = \frac{\hbar k}{m}
\end{equation*}
mit \eqref{eq:14.12} wäre die dreidimensionale Zustandsdichte
\begin{align*}
  D^{\text{3D}}(E) \diff E &= \frac{V}{2\pi^2\hbar} \frac{k^2}{v_g} \diff E \\
  D^{\text{3D}}(E) &= \frac{(2m)^{3/2} \sqrt{E}}{4\pi^2\hbar^3} V \; .
\end{align*}
Für die Zustandsdichte pro Volumen unter Beachtung des Spins gilt also
\begin{equation}
  \label{eq:14.21}
  D^{\text{3D}}(E) = \frac{(2m)^{3/2} \sqrt{E}}{2\pi^2\hbar^3} \; .
\end{equation}
Die analoge Überlegung gilt für zwei, eine und null Dimensionen.  Mit
\eqref{eq:14.13} und \eqref{eq:14.14}:
\begin{align}
  \label{eq:14.22}
  D^{\text{2D}}(E) &= \frac{m}{\pi\hbar^2} \\
  \label{eq:14.23}
  D^{\text{1D}}(E) &= \frac{1}{\pi\hbar} \sqrt{\frac{2m}{E}} \\
  \label{eq:14.24}
  D^{\text{0D}}(E) &= 2\delta(E-E_0)
\end{align}

Ein Beispiel für ein zweidimensionales System wäre ein Metallfilm der
Dicke $d$ in $z$-Richtung.  Für ein Elektron in diesem System wählt
man für die Lösung der Schrödingergleichung den Ansatz
\begin{equation*}
  \psi(x,y,z) = \frac{1}{\sqrt{V}}
  \underbrace{\sin\left(\frac{j\pi z}{d}\right)}_{\text{stehende Welle}}
  \underbrace{\ee^{\ii k_x x} \ee^{\ii k_y y}}_{\text{ebene Welle}} \; .
\end{equation*}
Aus den Randbedingungen folgt, dass sich in $z$-Richtung eine stehende
Welle mit Wellenlänge $\lambda_z = 2d/j$ bilden muss, wobei
$j\in\mathbb N$ als Subbandindex bezeichnet wird.

Die Energieeigenwerte sind
\begin{equation}
  \label{eq:14.25}
  E = \frac{j^2 h^2}{8 md^2} + \frac{\hbar^2 k^2}{2m} = E_j + \frac{\hbar^2 k^2}{2m}
\end{equation}
mit $\bm k$ in der $x$-$y$-Ebene, also $k^2 = k_x^2 + k_y^2$.

\section{Elektrischer Transport in einer Dimension --
  Leitfähigkeitsquantisierung}

Auf Grund von zunehmender Miniaturisierung von Bauelementen ist dieser
Effekt auch von technologischer Bedeutung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \draw (0,1) -- (1,1) -- (2,.5) -- (4,.5) -- (5,1) -- (6,1);
      \draw (0,-1) -- (1,-1) -- (2,-.5) -- (4,-.5) -- (5,-1) -- (6,-1);
      \draw[<->] (2,1) -- node[above] {$L$} (4,1);
      \draw[<->] (2,-1) -- node[below] (U) {$U$} (4,-1);
      \node[below] at (U) {Spannung};
      \node at (0,0) {Metallkontakt};
      \node at (6,0) {Metallkontakt};
      \node[below] (mq) at (.5,-1) {$\mu_{\text{Quelle}}$};
      \node[below] (ms) at (5.5,-1) {$\mu_{\text{Senke}}$};
    \end{scope}
    \begin{scope}[shift={(0,-3)}]
      \draw[MidnightBlue,out=0,in=180] (1.5,1) -- (3.5,1) to node[pin={right:Quasi-Ferminiveaus}] {} (4.5,0) -- (6,0);
      \draw[MidnightBlue,dashed,out=0,in=180] (0,1) -- (1.5,1) to (2.5,0) -- (6,0);
      \draw[MidnightBlue,<->] (2.5,1) -- node[right] {$\Delta\mu=e U$} (2.5,0);
      \draw[->] (mq) -- (.5,1);
      \draw[->] (ms) -- (5.5,0);
    \end{scope}
  \end{tikzpicture}
  \caption{Schematische Darstellung eines eindimensionalen Leiters.
    Der Verlauf des Fermi-Niveaus für die Elektronen der Quelle ist
    durckgezogen, für die Elektronen der Senke gestrichelt
    dargestellt.}
  \label{fig:30.1}
\end{figure}

Ein Metallleiter hat eindimensionale Eigenschaften, wenn die
Fermi-Wellenlänge $\lambda_F$ in der Größenordnung des Durchmessers
des Drahtes liegt, dabei ist bei Metallen typischerweise $\lambda_F =
\SI{5}{\angstrom}$.  Eine weitere Annahme ist, dass die mittlere freie
Weglänge größer als die Länge der Probe ist, sodass es keine
Streuprozesse gibt.  Der Transport ist dann ballistisch.  Es findet
kein Spannungsabfall entlang des Leiters statt, jedoch beim Übergang
zu den Elektroden.

Die Berechnung des Stromtransportes erfolgt analog zur thermischen
Leitfähigkeit in einer Dimension.  Wir gehen zunächst davon aus, dass
der Kanal nur einen transversalen Energieeigenwert $E_{i,j}$ zulässt.
Für den Stromtransport entlang des Kanals gilt analog zu
\eqref{eq:14.15}
\begin{equation}
  \label{eq:14.26}
  I = \frac{1}{L} \sum_k e v_k
  = \frac{1}{L} \int D_k^{\text{1D}} e v(\bm k) \left[
    f\left(E+\frac{eU}{2}\right) - f\left(E-\frac{eU}{2}\right) \right] \diff k \; .
\end{equation}
Mit der Beziehung 
\begin{equation*}
  v = \frac{1}{\hbar}\frac{\partial E}{\partial k}
\end{equation*}
kann man das Integral von $k$ auf $E$ umschreiben.  Wir setzen
außerdem $D^{\text{1D}}(k) = L/\pi$ ein
\begin{align*}
  I &= \frac{1}{L} \frac{L}{\pi} \int_0^\infty e \frac{\diff E}{\diff k} \diff k
  \frac{1}{\hbar}  \left[
    f\left(E+\frac{eU}{2}\right) - f\left(E-\frac{eU}{2}\right) \right] \\
  &= \frac{2\pi}{L} \int_0^\infty \underbrace{\left[ f\left(E+\frac{eU}{2}\right)
      - f\left(E-\frac{eU}{2}\right) \right]}_{eU} \diff E \\
  &= \frac{2 e^2}{h} U
\end{align*}
Die Klammer entspricht nur dann $eU$, wenn der Übergang zu den
Anschlusselektroden kurz gegenüber $L$ ist, vergleiche dazu am besten
Abbildung~\ref{fig:30.1}.

Damit gilt für den elektrischen Leitwert
\begin{equation*}
  G_Q = \frac{I}{U} = \frac{2 e^2}{h}
\end{equation*}
und für den elektrischen Widerstand
\begin{equation*}
  R_Q = \frac{h}{2 e^2} \; .
\end{equation*}
Daran sehen wir, dass ein perfekt durchlässiger eindimensionaler
Leiter einen endlichen Leitwert hat, der sich durch fundamentale
Konstanten ausdrücken lässt.  Wir bezeichnen diese beiden Werte als
\acct{Leitwertsquantum}, bzw.\ \acct{Widerstandsquantum}.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: