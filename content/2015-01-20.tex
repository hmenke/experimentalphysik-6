\section{Elektrische Polarisation in Isolatoren}

\subsection{Elektrische Polarisierbarkeit}

Die optischen Eigenschaften von Festkörpern werden durch
Interbandübergänge und Intrabandübergänge bestimmt.  Ein einfaches
klassisches Oszillatormodell ergibt schon eine gute
Beschreibung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \fill[MidnightBlue!20] (0,0) circle (1);
    \node[dot,pin={above right:Kern}] at (.2,.3) {};
    \node[MidnightBlue] at (-40:.5) {$e^-$};
    \draw[DarkOrange3,->] (-1,-1.5) -- node[below] {$E_{\text{lok}}$} (1,-1.5);
  \end{tikzpicture}
  \caption{Durch das lokale Feld $E_{\text{lok}}$ wird die
    Ladungswolke verschoben.}
  \label{eq:24.1}
\end{figure}

Nehmen wir dazu an, dass das lokale Feld mit
\begin{equation*}
  E_{\text{lok}}(t) = E_{\text{lok}}^0 \ee^{-\ii\omega t}
\end{equation*}
oszilliert.  Die Ladungswolke wird durch die einfallende
elektromagnetische Welle zu einer harmonischen Schwingungen angeregt.
Die Bewegungsgleichung ist der eines getriebenen harmonischen
Oszillators ähnlich
\begin{equation}
  \label{eq:13.21}
  m \ddot x
  + \underbrace{m \gamma \dot x}_{\mathclap{\text{Dämpfung durch Energieabstrahlung}}}
  + m \omega_0^2 x = - e E_{\text{lok}}
\end{equation}
mit der Resonanzfrequenz des ungedämpften Oszillators $\omega_0$ und
der Dämpfungskonstante $\gamma$.  Die stationäre Lösung ist
\begin{equation}
  \label{eq:13.22}
  x(t) = - \frac{e}{m} \frac{1}{\omega_0^2 - \omega^2 - \ii \gamma \omega}
  E_{\text{lok}} \; .
\end{equation}
Mit der Bewegung ist das Dipolmoment $p=-ex$ verknüpft, aus dem wir
mit $p=\varepsilon_0 \alpha E_{\text{lok}}$ \eqref{eq:13.13} die
Polarisierbarkeit erhalten können
\begin{equation}
  \label{eq:13.23}
  \alpha = \frac{e^2}{\varepsilon_0 m}
  \frac{1}{\omega_0^2 - \omega^2 - \ii \gamma \omega} \; .
\end{equation}

Weiterhin ist zu beachten, dass das mittlere makroskopische Feld $E$
sich von $E_{\text{lok}}$ unterscheidet durch
\begin{equation}
  \label{eq:13.24}
  E_{\text{lok}} = E + \frac{P}{3\varepsilon_0} = E - \frac{n e x}{3 \varepsilon_0} \; .
\end{equation}
Setzen wir diese Form des lokalen Feldes \eqref{eq:13.24} in die
Bewegungsgleichung \eqref{eq:13.21} ein, so ändert sich diese zu
\begin{equation}
  \label{eq:13.25}
  m \ddot x + m \gamma \dot x + \left( m \omega_0^2
    - \frac{n e^2}{3 \varepsilon_0} \right) x = -e E \; .
\end{equation}
Daraus ergibt sich mit $\varepsilon(\omega) = 1 + \chi(\omega) = 1 +
P(\omega)/\varepsilon E$ der Zusammenhang
\begin{equation}
  \label{eq:13.26}
  \varepsilon(\omega) = 1 + \frac{n e^2}{\varepsilon_0 m} \frac{1}{\underbrace{\omega_0^2 - \frac{n e^2}{3 \varepsilon_0 m}}_{\omega_1^2} - \omega^2 - \ii \gamma \omega} \; .
\end{equation}
Das lokale Feld, das von den Nachbarn hervorgerufen wird führt also zu
einer Verschiebung der Resonanzfrequenz
\begin{equation}
  \label{eq:13.27}
  \boxed{
    \varepsilon(\omega) = 1 + \frac{n e^2}{\varepsilon_0 m} \frac{1}{\omega_1^2 - \omega^2 - \ii\gamma\omega}
  } \; .
\end{equation}
Nehmen wir eine Zerlegung der komplexen dielektrischen Funktion
$\varepsilon = \varepsilon' + \ii\varepsilon''$ in Real- und
Imaginärteil vor, so lauten diese
\begin{align}
  \label{eq:13.28}
  \varepsilon'(\omega) &= 1 + \frac{n e^2}{\varepsilon_0 m} \frac{\omega_1^2 - \omega^2}{(\omega_1^2 - \omega^2)^2 + \gamma^2 \omega^2} \; , \\
  \label{eq:13.29}
  \varepsilon''(\omega) &= \frac{n e^2}{\varepsilon_0 m} \frac{\gamma \omega}{(\omega_1^2 - \omega^2)^2 + \gamma^2 \omega^2} \; .
\end{align}
Der schematische Verlauf der beiden ist exemplarisch in
Abbildung~\ref{fig:24.2} zu sehen.  Bei resonanter Anregung ist
$\varepsilon''(\omega)$ nur in der Umgebung von $\omega_1$ ungleich
Null.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \def\gamma{.04}
    \def\om{1}
    \begin{axis}[axis on top,domain=.5:2,enlargelimits=false,ymin=-15,ymax=30,width=6cm,
      xlabel={Frequenz $\omega$},ylabel={Amplitude},
      extra y ticks={0},extra y tick labels={$0$},
      extra x ticks={\om},extra x tick labels={$\omega_1$},
      xtick=\empty,ytick=\empty,samples=100,smooth]
      \draw (axis cs:.5,0) -- (axis cs:2,0);
      \draw[dashed] (axis cs:\om,-15) -- (axis cs:\om,30);
      \addplot[MidnightBlue] {1 + (\om^2-x^2)/((\om^2-x^2)^2 + \gamma^2*x^2};
      \addplot[DarkOrange3] {\gamma*x/((\om^2-x^2)^2 + \gamma^2*x^2};
      \legend{$\varepsilon'(\omega)$,$\varepsilon''(\omega)$}
    \end{axis}
  \end{tikzpicture}
  \caption{Schematischer Verlauf der dielektrischen Funktion.}
  \label{fig:24.2}
\end{figure}

In Atomen, bzw.\ Festkörpern existieren mehrere Resonanzenfrequenzen,
die sich unterschiedlich stark auf die Polarisation auswirken.  Daher
müssen wir unsere dielektrische Funktion abändern zu
\begin{equation}
  \label{eq:13.30}
  \varepsilon(\omega) = 1 + \frac{n e^2}{\varepsilon_0 m} \sum_k \frac{f_k}{\omega_k^2 - \omega^2 - \ii \gamma_k \omega}
\end{equation}
mit der Oszillatorstärke $f_k$. Diese kann experimentell angepasst
werden und ist durch das Matrixelement des jeweiligen Übergangs
bestimmt.  Das gleiche Frequenzverhalten findet man auch in einer
quantenmechanischen Rechnung.  

In Festkörpern hängt die Wahrscheinlichkeit für einen bestimmten
Übergang ab vom Matrixelement des Übergangs, von der elektronische
Zustandsdichte der Anfangs- und Endzustände (kombinierte
Zustandsdichte).  Außerdem können auch Phononen-assistierte Prozesse
beitragen.  Daher erhält man ein komplexes Spektrum, bestimmt durch
die Energielücken, also kritische Punkte der Zustandsdichte, d.h.\
durch die Bandstruktur des Festkörpers.

\subsection{Ionenpolarisation}

Im Rahmen der mikroskopischen Beschreibung kann die Wechselwirkung von
Infrarotstrahlung mit den Ionen eines Gitters als Stoß zwischen
Photonen und optischen Phononen beschrieben werden.  Da Photonen keine
Ruhemasse besitzen ist ihr Quasiimpuls klein.  Beim Stoß gilt die
Quasiimpulserhaltung, deshalb können am Stoß nur Phononen mit kleinen
Wellenzahlen teilnehmen.  Da die Dispersionskurve im Bereich $k\approx
0$ nahezu konstant verläuft (siehe Abbildung~\ref{fig:24.3}) haben
alle Phononen etwa die gleiche Frequenz.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,3) node[above] {$\omega$} |- (4,0) node[right] {$k$};
    \draw[MidnightBlue] (0,2.5) to[out=0,in=180] node[above] {TO} (3,2);
    \draw[MidnightBlue] (0,0) to[in=180] (3,1.5);
    \draw[dashed] (3,-2pt) node[below] {$\pi/a$} -- (3,3);
    \draw[DarkOrange3] (0,2.5) circle (.2);
    \node[DarkOrange3,below left,text width=1.9cm,align=right] at (-.2,2.5)
    {relevante Phononen mit ungefähr konstanter Energie};
  \end{tikzpicture}
  \caption{Beim Stoß mit Photonen können nur optische Phononen mit
    kleinen Wellenvektoren teilnehmen (oranger Kreis), da der
    Quasiimpuls von Photonen durch ihre fehlende Ruhemasse sehr klein
    ist.  Im eingekreisten Bereich ist die Dispersionsrelation nahezu
    konstant.}
  \label{fig:24.3}
\end{figure}

Die einzige Schwingung, bei der keine Propagation stattfindet aber
dennoch $\omega\neq0$ hat ist diejenige bei der die verschiedenen
Ladungsträger gegeneinander schwingen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[dashed] plot[domain=0:2*pi] (\x,{sin(\x r)}) [postaction=decorate,decoration={markings,mark=between positions 0.05 and 0.95 step 9mm with \node[MidnightBlue] {$\oplus$};}];
    \draw[dashed] plot[domain=0:2*pi] (\x,{-sin(\x r)}) [postaction=decorate,decoration={markings,mark=between positions 0.05 and 0.95 step 9mm with \node[DarkOrange3] {$\ominus$};}];
  \end{tikzpicture}
  \caption{Die Ionen $\oplus$ sind in Gegenphase zu den Ionen
    $\ominus$.  Für eine makroskopische Beschreibung reicht es ein
    Ionenpaar herauszugreifen, da sich alle Ionenpaare gleich
    verhalten.}
  \label{fig:24.4}
\end{figure}

\minisec{Eigenschwingungen von Ionenkristallen}

Es wirken elastische und elektrische Kräfte (vgl.\ Kapitel
Gitterschwingungen) auf die Ionen der beiden Untergitters.  Es sind
$u_1$, $u_2$ die Auslenkungen der Massen $M_1$ und $M_2$.  Im lokalen
elektrischen Feld folgen die Auslenkungen den Bewegungsgleichungen
\begin{equation}
  \label{eq:13.31}
  \begin{aligned}
    M_1 \ddot u_1 + 2 C u_1 -2 C u_2 &= q E_{\text{lok}} \; , \\
    M_2 \ddot u_2 + 2 C u_2 -2 C u_1 &= -q E_{\text{lok}} \; .
  \end{aligned}
\end{equation}
Wir interessieren uns für die Relativbewegung der beiden Untergitter
zueinander und führen dazu neue Variablen ein und zwar die Relativkoordinate $u$
\begin{equation*}
  u = u_2 - u_1
\end{equation*}
und die reduzierte Masse $\mu$
\begin{equation*}
  \frac{1}{\mu} = \frac{1}{M_1} + \frac{1}{M_2} \; .
\end{equation*}
Damit reduzieren sich unsere ursprünglich zwei Bewegungsgleichungen zu
einer
\begin{equation}
  \label{eq:13.32}
  \mu \ddot u + \mu \omega_0^2 u = q E_{\text{lok}}
\end{equation}
mit der Resonanzfrequenz $\omega_0^2 = 2C/\mu$, falls nur elastische
Kräfte wirksam sind.  Dies ist wieder die Bewegungsgleichung eines
getriebenen linearen harmonischen Oszillators.  Wir fügen noch einen
Dämpfungsterm mit der Dämpfungskonstante $\gamma$ ein.  Dieser
berücksichtigt die Dämpfung durch Energieabstrahlung und die Kopplung
der Phononen untereinander (Anregung höherer Moden).  Damit wird
\eqref{eq:13.32} zu
\begin{equation}
  \label{eq:13.33}
  \mu \ddot u + \mu \gamma \dot u + \mu \omega_0^2 u = q E_{\text{lok}} \; .
\end{equation}
Für das lokale Feld setzen wir eine harmonische Schwingung
$E_{\text{lok}}(t) = E_{\text{lok}}^0 \ee^{-\ii\omega t}$ an und
erhalten damit die stationäre Lösung
\begin{equation}
  \label{eq:13.34}
  u(t) = \frac{q}{\mu} \frac{1}{\omega_0^2 - \omega^2 - \ii\gamma\omega} E_{\text{lok}}(t) \; .
\end{equation}

\subsection{Optische Phononen}

Vor der Diskussion von \eqref{eq:13.34} betrachten wir die Natur der
Gitterschwingungen, um den Einfluss der lokalen elektrischen Felder
auf das Schwingungsspektrum zu studieren.  Die Bewegung der Ionen
erzeugt ein oszillierendes Dipolmoment $p(t) = q\, u(t)$ und eine
oszillierende Polarisation $p(t) = n q\, u(t)$. Die Gesamtpolarisation
der Probe ist damit gegeben durch
\begin{equation}
  \label{eq:13.35}
  p(t) = \underbrace{n q\, u(t)}_{\mathclap{\text{ionischer Anteil}}}
  + \overbrace{n\varepsilon_0 \alpha E_{\text{lok}}(t)}^{\mathclap{\text{elektronischer Anteil}}}
\end{equation}
Die Bewegung der Ionen unterscheidet sich maßgeblich in transversaler
und longitudinaler Richtung.  Dies hat natürlich auch Auswirkungen auf
die Propagation transversaler und longitudinaler Wellen.  Falls kein
äußeres Feld anliegt
% 22.01.2015
ist die atomare Auslenkung bei longitudinal optischen Phononen
parallel zum Wellenvektor.  $\bm E$ und $\bm P$ sind senkrecht zur
Knotenebene.  In diesem Fall ist der Depolarisationsfaktor $f=1$ und es folgt
\begin{equation}
  \label{eq:13.36}
  E_{\text{lok}}^\ell = E_D + E_L
  = - \frac{P_\ell}{\varepsilon_0} + \frac{P_\ell}{3 \varepsilon_0}
  = -\frac{2}{3}\frac{P_\ell}{\varepsilon_0}
\end{equation}
Das lokale Feld wirkt der relativen Auslenkung $u=u_1-u_2$ der
Untergitter entgegen und versucht die Auslenkung zu reduzieren.  Es
entspricht also einer zusätzlichen rücktreibenden Kraft.

Bei transversal optischen Phononen liegt kein Entelektrisierungsfeld
an, da $E_{\text{lok}}$ und $\bm P$ parallel zur Scheibenoberfläche
sind
\begin{equation}
  \label{eq:13.37}
  E_{\text{lok}}^t = E_D + E_L
  = 0 + \frac{P_t}{3 \varepsilon_0}
  = \frac{P_t}{3 \varepsilon_0}
\end{equation}
Das lokale Feld hat gegenüber longitudinal optischen Wellen ein
entgegengesetztes Vorzeichen und wirkt den rücktreibenden Kräften
entgegen.  Das Material wird \enquote{weicher}.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: