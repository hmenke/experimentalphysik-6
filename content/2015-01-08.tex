Die über die Bindungselektronen vermittelte Wechselwirkung zwischen
zwei Protonen kann auch noch über mehrere Bindungen hinweg messbar
sein.  Einige Zahlenwerte sind in Tabelle~\ref{tab:21.1} gegeben.

\begin{table}[tb]
  \centering
  \begin{tabular}{llr@{}l}
    \toprule
    Kerne & Molekül & \multicolumn{2}{l}{$J$ [\si{\per\s}]} \\
    \midrule
    \ch{H-H} & \ch{H2} & \num{276} & \\
    \ch{H-C} & \ch{CH4} & \num{125} & \\
    \ch{H}\ldots\ch{H} & \ch{H-C-H} & $-(\num{10}$ & $\ldots \num{15})$ \\
    & \ch{H-C-C-H} & \num{5} & \ldots \num{8} \\
    \bottomrule
  \end{tabular}
  \caption{Spin-Spin-Kopplungskonstanten unterschiedlicher Bindungen.}
  \label{tab:21.1}
\end{table}

In Abbildung~\ref{fig:20.5} wurde gezeigt, dass zwei Kerne $A$ und $B$
in zwei Dubletts mit gleicher Intensität aufspalten.  Der
Linienabstand in den Dubletts ist durch die
Spin-Spin-Kopplungskonstante $J$ gegeben.  Die Mitten der Dubletts
sind durch die chemische Verschiebung voneinander getrennt.  Ist nun
die chemische Verschiebung nicht mehr viel größer als $J$, so kommt es
zur Überlagerung.  Die Linienhöhen sind dann nicht mehr gleich.
Überlagern sich die Linien gerade so, dass nur eine Linie zurück
bleibt nennt man diese Kerne äquivalent.

\acct{Chemische Äquivalenz} liegt vor, wenn die Überlagerung der
Linien auf Grund derselben chemischen Verschiebung zustande kommt.
Von \acct[Magnetische Äquivalenz]{magnetischer Äquivalenz} spricht
man, wenn die beiden Kerne im Magnetfeld dieselbe Winkelabhängigkeit
der Wechselwirkung haben.

Die indirekte Kernspin-Kernspin-Wechselwirkung zwischen mehreren
Kernen führt zur charakteristischen Aufspaltung und Multiplizitäten.

In \ch{CH2}- oder \ch{CH3}-Gruppen liegen mit den Protonen des
Wasserstoffs nur äquivalente Kerne vor, weshalb man bei
NMR-Spektroskopie der isolierten Gruppen nur eine Resonanzlinie
findet.  Anders sieht es jedoch aus, wenn \ch{CH2}- und
\ch{CH3}-Gruppen innerhalb eines Moleküls wechselwirken können.
Dieses Szenario ist in Abbildung~\ref{fig:21.1} dargestellt.

\begin{figure}[tb]
  \centering
  \chemfig{
    Br-\textcolor{DarkOrange3}{C}(
    (-[2,,,,DarkOrange3]\textcolor{DarkOrange3}{H})
    (-[6,,,,DarkOrange3]\textcolor{DarkOrange3}{H})
    )
    -\textcolor{MidnightBlue}{C}(
    (-[0,,,,MidnightBlue]\textcolor{MidnightBlue}{H})
    (-[2,,,,MidnightBlue]\textcolor{MidnightBlue}{H})
    (-[6,,,,MidnightBlue]\textcolor{MidnightBlue}{H})
    )
  }

  \begin{tikzpicture}[gfx]
    \draw[->] (0,0) -- (5,0) node[right] {$f$};

    \node[DarkOrange3] at (0,3) {\ch{CH2}};
    \draw[DarkOrange3] (1-.6,0) coordinate (n1l) -- (1-.6,1);
    \draw[DarkOrange3] (1-.2,0) coordinate (n3l) -- (1-.2,3);
    \draw[DarkOrange3] (1+.2,0) coordinate (n3r) -- (1+.2,3);
    \draw[DarkOrange3] (1+.6,0) coordinate (n1r) -- (1+.6,1);
    \node[DarkOrange3,below,pin={below left:$\dn\dn\dn$}] at (n1l) {$1$};
    \node[DarkOrange3,below,pin={[text width=2em,align=center]-95:{
        $\dn\dn\up$ \\ $\dn\up\dn$ \\ $\dn\dn\up$}}] at (n3l) {$3$};
    \node[DarkOrange3,below,pin={[text width=2em,align=center]-80:{
        $\dn\up\up$ \\ $\up\dn\up$ \\ $\up\up\dn$}}] at (n3r) {$3$};
    \node[DarkOrange3,below,pin={below right:$\up\up\up$}] at (n1r) {$1$};

    \node[MidnightBlue] at (3.5,2) {\ch{CH3}};
    \draw[MidnightBlue] (4-.3,0) coordinate (n1l) -- (4-.3,1);
    \draw[MidnightBlue] (4   ,0) coordinate (n2c) -- (4   ,2);
    \draw[MidnightBlue] (4+.3,0) coordinate (n1r) -- (4+.3,1);
    \node[MidnightBlue,below,pin={-100:$\dn\dn$}] at (n1l) {$1$};
    \node[MidnightBlue,below,pin={[text width=2em,align=center]below:{
        $\dn\up$ \\ $\dn\up$}}] at (n2c) {$2$};
    \node[MidnightBlue,below,pin={-80:$\up\up$}] at (n1r) {$1$};
  \end{tikzpicture}
  \caption{Die beiden isolierten Gruppen \ch{CH2} und \ch{CH3} würden
    jeweils nur eine Linie hervorrufen.  Wenn sie jedoch innerhalb
    eines Moleküls (oben) verbaut sind können sie miteinander
    wechselwirken.  Dabei wechselwirkt die \ch{CH2}-Gruppe mit den
    drei Protonen der \ch{CH3}-Gruppe.  Mit jedem weiteren Proton, das
    in der Wechselwirkung berücksichtigt wird kommt eine weitere
    Aufspaltung hinzu, d.h.\ im Falle der Wechselwirkung mit drei
    Protonen $1 \to 2 \to 3 \to 4$ (unten).  Die Höhe der Linien bestimmt sich
    aus der Anzahl der verschiedenen Einstellungsmöglichkeiten für die
    jeweilige Konfiguration und entspricht der jeweiligen Zeile des
    Pascalschen Dreiecks.  Für vier Linien nehmen wir die dritte Zeile
    des Pascalschen Dreiecks $1\;3\;3\;1$.  Analog für die
    \ch{CH3}-Gruppe.}
  \label{fig:21.1}
\end{figure}

\subsection{Bewegungsgleichungen}

Im Magnetfeld wirkt auf den Drehimpuls $\hbar\bm I$ ein Drehmoment $\bm\mu\times\bm
B_0$. Nach dem Drehimpulssatz gilt
\begin{equation}
  \label{eq:12.16}
  \hbar \frac{\diff\bm I}{\diff t} = \bm\mu\times\bm B_0 \; ,
\end{equation}
bzw.\ für das damit verbundene magnetische Moment $\mu$ nach \eqref{eq:12.1}
\begin{equation}
  \label{eq:12.17}
  \frac{\diff \bm\mu}{\diff t} = \gamma \bm\mu\times\bm B_0 \; .
\end{equation}
Mit $\bm M$ als magnetisches Moment pro Volumen $\bm M = n \bm \mu$.
\begin{equation}
  \label{eq:12.18}
  \frac{\diff \bm M}{\diff t} = \gamma \bm M \times \bm B_0 \; .
\end{equation}

Betrachten wir nun Kerne im statischen Feld in $z$-Richtung $\bm B_0 =
B_0 \hat{\bm z}$.  Im thermischen Gleichgewicht ist die Magnetisierung
entlang dieses Feldes ausgerichtet und es ist
\begin{equation}
  \label{eq:12.19}
  \implies M_x = M_y = 0 \; .
\end{equation}
Die Magnetisierung eines Spinsystems mit $I = 1/2$ ist durch den
Besetzungsunterschied $n_1 - n_2$ des oberen und unteren Niveaus
gegeben
\begin{equation}
  \label{eq:12.20}
  M_z = (n_1 - n_2) \mu \; .
\end{equation}
Das Verhältnis der Besetzungszahlen ist durch den Boltzmannfaktor
\begin{equation}
  \label{eq:12.21}
  \frac{n_2}{n_1} = \ee^{-2 \frac{\mu B_0}{\kB T}}
\end{equation}
gegeben.

Die Gleichgewichtsmagnetisierung ist gegeben durch
\begin{equation}
  \label{eq:12.22}
  M_0 = n \mu \tanh\left(\frac{\mu B_0}{\kB T}\right) \; .
\end{equation}
Wir nehmen an, dass falls die Magnetisierungskomponente $M_z$ sich
nicht im thermischen Gleichgewicht befindet sie mit der Relaxationszeit $T_1$
zurück ins Gleichgewicht relaxiert.
\begin{equation}
  \label{eq:12.23}
  \frac{\diff M_z}{\diff t} = \frac{M_0 - M_z}{T_1}
\end{equation}
Man nennt $T_1$ die longitudinale Relaxationszeit oder
Spin-Gitter-Relaxationszeit.  Die Differentialgleichung
\eqref{eq:12.23} lässt sich mit einem Separationsansatz lösen.
\begin{align}
  \notag
  \int_0^{M_z} \frac{\diff M_z'}{M_0 - M_z'} &= \frac{1}{T_1} \int_0^t \diff t' \\
  \notag
  \ln \frac{M_0}{M_0 - M_z} &= \frac{t}{T_1} \\
  \label{eq:12.24}
  M_z(t) &= M_0 [1-\ee^{-t/T_1}]
\end{align}
Die dominante Spin-Gitter-Wechselwirkung paramagnetischer Ionen in
Kristallen erfolgt über die von Phononen bewirkte Modulation des
elektrischen Kristallfeldes.  Setzt man \eqref{eq:12.23} in die
Bewegungsgleichungen \eqref{eq:12.18} ein, so findet man
\begin{equation}
  \label{eq:12.25}
  \frac{\diff M_z}{\diff t} = \gamma (\bm M \times \bm B_0)_z + \frac{M_0 - M_z}{T_1}
\end{equation}
Falls in einem statischen Feld $B_0 \hat{\bm z}$ die transversalen
Magnetisierungskomponenten $M_x$, $M_y$ anfangs nicht Null sind,
werden sie auf null abnehmen, da sie im thermischen Gleichgewicht null
sein müssen.  Die transversale Relaxation ist gegeben durch
\begin{align}
  \label{eq:12.26}
  \frac{\diff M_x}{\diff t} &= \gamma (\bm M \times \bm B_0)_x - \frac{M_x}{T_2} \\
  \label{eq:12.27}
  \frac{\diff M_y}{\diff t} &= \gamma (\bm M \times \bm B_0)_y - \frac{M_y}{T_2} 
\end{align}
mit der transversalen Relaxationszeit $T_2$.  Die Gleichungen
\eqref{eq:12.25}--\eqref{eq:12.27} sind die Blochgleichungen.

\minisec{Diskussion einfacher Spezialfälle}

Betrachten wir zunächst die Bewegung für ein homogenes Magnetfeld in
$z$-Richtung $\bm B=(0,0,B_0)$ und vernachlässigen die
Relaxationsprozesse.  Die Blochgleichungen lauten dann
\begin{equation}
  \label{eq:12.28}
  \begin{aligned}
    \frac{\diff M_x}{\diff t} &= \gamma B_0 M_y \\
    \frac{\diff M_y}{\diff t} &= -\gamma B_0 M_x \\
    \frac{\diff M_z}{\diff t} &= 0
  \end{aligned}
\end{equation}
Diese gekoppelten Differentialgleichungen kann man leicht lösen.
\begin{equation}
  \begin{aligned}
    M_x(t) &= M_\perp \cos\omega_0 t \\
    M_y(t) &= - M_\perp \sin\omega_0 t \\
    M_z(t) &= M_z = \const
  \end{aligned}
  \quad\text{mit }\omega_0 = \gamma B_0
\end{equation}
Die Magnetisierung $\bm M(t)$ präzediert also mit der Larmorfrequenz
$\omega_0$ um die Richtung von $\bm B_0$. Der Betrag der
\enquote{Quermagnetisierung} $M_\perp$ ist konstant.  Die Bewegung der
Magnetisierung in der $x,y$-Ebene ist in Abbildung~\ref{fig:21.2}
links dargestellt.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node[below] at (0,-1.5) {ohne transversale Relaxation};
      \draw[->] (-2,0) -- (2,0) node[right] {$x$};
      \draw[->] (0,-1.5) -- (0,1.5) node[left] {$y$};
      \draw[dotted] (0,0) circle (1);
      \draw[MidnightBlue,->] (0,0) -- (-45:1) node[below right] {$M_\perp$};
      \draw[DarkOrange3,->] (0,-1) arc (-90:-150:1);
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \node[below] at (0,-1.5) {mit transversaler Relaxation};
      \draw[->] (-2,0) -- (2,0) node[right] {$x$};
      \draw[->] (0,-1.5) -- (0,1.5) node[left] {$y$};
      \draw[dotted] plot[domain=0:10] ({exp(-0.2*\x)*cos(deg(-\x-pi/4))},{exp(-0.2*\x)*sin(deg(-\x-pi/4))});
      \draw[MidnightBlue,->] (0,0) -- (-45:1) node[below right] {$M_\perp$};
      \draw[DarkOrange3,->] plot[domain=pi/4:pi/2] ({exp(-0.2*\x)*cos(deg(-\x-pi/4))},{exp(-0.2*\x)*sin(deg(-\x-pi/4))});
    \end{scope}
  \end{tikzpicture}
  \caption{Präzession der Magnetisierung in der $x,y$-Ebene bei
    einem homogenen Magnetfeld in $z$-Richtung.  Links ist der im
    Text besprochene Fall ohne transversale Relaxation gezeigt.
    Berücksichtigt man die transversale Relaxation, so nimmt die
    Magnetisierung in Querrichtung ab.}
  \label{fig:21.2}
\end{figure}

Wir haben bisher nur besprochen, wie die Magnetisierung aus einer Lage
quer zum Feld wieder in die Gleichgewichtslage relaxiert, jedoch haben
wir den Auslenkungsprozess aus dem Gleichgewicht bisher
vernachlässigt.  Wie bringt man also das magnetische Moment $M(t)$ aus
der Ruhelage entlang des Feldes $\bm B_0$ \enquote{quer} zum Feld $\bm
B_0$?  Man macht dies indem man über eine Spule ein kleines Magnetfeld
$B_1(t)$ in $x$-Richtung anlegt, welches mit der Frequenz $\omega_0$
oszilliert.
\begin{equation*}
  B_{1,x} = 2 B_1 \cos\omega_0 t \; .
\end{equation*}
Das gesamte Feld ist damit eine Überlagerung aus dem Wechselfeld und
dem homogenen Feld
\begin{equation*}
  \bm B(t) =
  \begin{pmatrix}
    2 B_1 \cos\omega_0 t \\
    0 \\
    B_0 \\
  \end{pmatrix}
  \; .
\end{equation*}
Die Blochgleichungen in Vektorschreibweise lauten damit
\begin{equation}
  \label{eq:12.30}
  \dot{\bm M}(t) = \gamma \bm M \times \bm B
  = \gamma
  \begin{pmatrix}
    M_x \\ M_y \\ M_z \\
  \end{pmatrix}
  \times
  \begin{pmatrix}
    2 B_1 \cos\omega_0 t \\
    0 \\
    B_0 \\
  \end{pmatrix}
  = \gamma
  \begin{pmatrix}
    M_y B_0 \\
    2 B_1 M_z \cos\omega_0 t - M_x B_0 \\
    -2 B_1 M_y \cos\omega_0 t \\
  \end{pmatrix}
  \; .
\end{equation}
Die Lösung dieser Differentialgleichung ist eine Spiralbewegung der
Magnetisierung auf der Blochkugel vom Nordpol hinunter zum Äquator und
weiter zum Südpol.  Die formale Behandlung erfolgt am besten im
rotierenden Koordinatensystem, welches mit der Larmorfrequenz
$\omega_0$ um die $z$-Achse rotiert.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$z$} |- (3,0) node[right] {$\perp$};
    \draw[MidnightBlue,->] (0,0) -- (30:2) node[above right] {$\bm M$};
    \draw[MidnightBlue] (0,1) arc (90:30:1);
    \node[MidnightBlue] at (60:1.3) {$\omega_1 t$};
  \end{tikzpicture}
  \caption{Nach Transformation ins mit $\omega_0$ rotierende
    Koordinatensystem, rotiert $\bm M$ mit $\omega_1 t$.}
  \label{fig:21.3}
\end{figure}

Wir benötigen nur die Lösung aus der Skizze in
Abbildung~\ref{fig:21.3}.  Unter dem Einfluss von $\bm B_1$ rotiert
die Magnetisierung mit der Rabi-Frequenz $\omega_1 = \gamma B_1$ ($f_1
= \omega_1/(2\pi)$) von der $z$-Achse in der Querrichtung
\begin{equation}
  \label{eq:12.40}
  \begin{aligned}
    M_\perp(t) &= M_0 \sin\omega_1 t \\
    M_z(t) &= M_0 \cos\omega_1 t
  \end{aligned}
\end{equation}
Nach welcher Zeit $\tau_{\pi/2}$ ist die Magnetisierung quergestellt?
\begin{equation}
  \omega_1 \tau_{\pi/2} \stackrel!= \pi/2
  \implies
  \tau_{\pi/2} = \frac{\pi}{2\omega_1} = \frac{\pi}{4 \pi f_1} = \frac{1}{4 f_1}
\end{equation}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: