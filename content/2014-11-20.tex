\minisec{Stromfluss im Supraleiter}

Für die Suprastromdichte gilt nach \eqref{eq:10.8}
\begin{equation*}
  \bm j_S = -n_S e \bm v \; .
\end{equation*}
Der Quasi-Impuls ist mit dem realen Impuls verknüpft über
\begin{equation*}
  m \bm v = \hbar \bm k' \; .
\end{equation*}
Setzen wir für $\bm v$ die Suprastromdichte ein, so finden wir, dass jedes einzelne Elektron in einem Cooper-Paar damit bei
Stromfluss eine Änderung seines $\bm k$-Vektors um
\begin{equation}
  \label{eq:10.23}
  \frac12 \bm k' = - \frac{m}{n_S e \hbar} \bm j_S
\end{equation}
erfährt.  Für ein Cooper-Paar im Stromfluss bedeutet das
\begin{equation*}
  (\bm k\up,-\bm k\dn)
  \to
  \left(\bm k + \frac12 \bm k' \up, -\bm k + \frac12 \bm k' \dn\right) \; .
\end{equation*}
Der Strom wird im Supraleiter durch die Schwerpunktsbewegung des
Cooper-Paars hervorgerufen. Für die Wellenfunktion gilt dann
\begin{equation*}
  \psi_{\text{CP}}(\bm k') = \ee^{\ii \bm k' \bm R} \, \psi_{\text{CP}}(0)
\end{equation*}
mit der Schwerpunktskoordinate $\bm R$.  Bei Stromfluss ändert sich
die Wellenfunktion des Cooper-Paars nur um einen Phasenfaktor.  Alle
Cooper-Paare erhalten denselben Impulszuwachs.  Wir haben also ein
kohärentes quantenmechanisches System, denn alle Cooper-Paare besitzen
die gleiche Phase und wir haben \emph{eine} Wellenfunktion für die
Beschreibung.

Eine Änderung des Zustands durch inelastische Elektronenstreuung
(z.B. mit Phononen) kann nur durch Aufbrechen mindestens eines
Cooper-Paares zustande kommen.  Was inelastische Elektronen-Stöße
angeht, so sind diese als Ursache für Ladungsträgerrelaxation nicht
mit einer Energieaufnahme verknüpft ist, die Anregungen über $2\Delta$
hinaus ermöglicht.

Betrachte nun ein Elektron des Cooper-Paares bei Stromfluss.  Dann
gilt für seine Bewegung
\begin{equation*}
  \bm k \to \bm k + \frac12 \bm k'
\end{equation*}
Damit ändert sich auch seine Energie, die sich aufspalten lässt in
einen Teil ohne Stromfluss $E_0$ und einen Teil, der durch Stromfluss
verursacht wird.
\begin{equation*}
  E = \frac{\hbar^2}{2m} \left( \bm k + \frac{\bm k'}{2} \right)^2
  = \frac{\hbar^2}{2m} \biggl(
  \underbrace{\bm k^2}_{\sim E_0}
  + \bm k \bm k'
  + \underbrace{\frac{1}{4}\bm k'^2}_{\text{klein}}
  \biggr)
\end{equation*}
Wir nehmen an, dass $|\bm k'| \ll k_F$, damit wir den letzten Summand
auf Grund seines geringen Beitrags vernachlässigen können.  Die
Energiezunahme wird also nur durch den zweiten Summand bestimmt und
beträgt bei $k = k_F$
\begin{equation}
  \label{eq:10.24}
  \delta E' = \frac12 \frac{\hbar^2 k_F k'}{m}
\end{equation}
Damit der supraleitende Zustand zusammenbricht muss gelten $2\, \delta
E' > 2 \Delta$, also
\begin{equation*}
  2\, \delta E' = \frac{\hbar^2 k_F k'}{m}
  = \frac{2 \hbar k_F}{e n_S} j_S \ge 2 \Delta \; .
\end{equation*}
Hier haben wir für $k'$ wieder den Suprastrom eingesetzt.  Stellen
wir nach diesem um, erhalten wir den kritischen Strom, bei dem die
Supraleitung zusammenbricht
\begin{equation}
  \label{eq:10.25}
  \boxed{ j_C \simeq \frac{e n_S \Delta}{\hbar k_F} } \; .
\end{equation}
Da der Strom mit dem Magnetfeld verknüpft ist über \eqref{eq:10.11}
muss es auch ein kritisches Magnetfeld geben bei dem die Supraleitung
zusammenbricht.  Eine Herleitung für dessen Form findet sich bei
\textcite{hunklinger}
\begin{equation}
  \label{eq:10.26}
  \boxed{ B_C = \mu_0 \lambda_L j_C } \; .
\end{equation}
Der kritischer Strom, die kritische Magnetfeldstärke und die
Energielücke sind direkt miteinander verknüpft, unabhängig davon ob
die Stromdichte durch Abschirm- oder Transportströme hervorgerufen
wird.

Bei endlicher Temperatur $T < T_C$ werden Cooper-Paare aufgebrochen.
Beim Aufbrechen werden Phononen ausgesendet, wodurch wiederum
Cooper-Paare gebildet werden können.  Im Gleichgewicht heben sich die
beiden Prozesse auf.  Warum verursachen thermisch angeregte
Quasi-Teilchen aber keine Verluste?  Im statischen Zustand sind die
elektrischen Felder kurzgeschlossen.  Die Quasiteilchen werden daher
nicht beschleunigt und tragen nicht zum Stromtransport bei.

\begin{notice}
  Diese Argumentation gilt nicht beim Anlegen von Wechselspannung, da
  in diesem Fall entsprechend den 1. Londonschen Gleichungen ein
  elektrisches Feld existiert. Die Quasiteilchen werden beschleunigt,
  wechselwirken mit dem Gitter und rufen Verluste hervor.
  \begin{center}
    \fbox{Verlustfreie Leitung nur bei Gleichstrom}
  \end{center}
\end{notice}

\section{Messung der Energielücke}

Wenden wir uns nun dem wichtigsten experimentellen Resultat zu, das
die vorgestellte Theorie belegt.  Die Theorie sagt die Existenz einer
Energielücke zwischen supraleitendem Zustand und normalleitendem
Zustand voraus.  Diese Energielücke wollen wir nun messen.

Intuitiv würde man zur Messung einer solchen Energielücke Mikrowellen-
und Infrarotexperimente vorschlagen.  Eingestrahlte Wellen werden
abhängig von ihrer Energie dann entweder absorbiert oder transmittiert
entsprechend der folgenden Regeln
\begin{itemize}
\item $\hbar\omega < 2 \Delta$ Strahlung wird nicht absorbiert,
\item $\hbar\omega > 2 \Delta$ Strahlung wird absorbiert.
\end{itemize}
Diese Messungen sind sehr schwer durchzuführen, da die Energielücke
nicht der einzige Absorptionsprozess im Material ist.  Die Ergebnisse
reproduzieren die BCS-Theorie jedoch sehr gut.

Im Folgenden gehen wir deshalb auf die Tunnel-Kontakt-Spektroskopie
bei $T=\SI{0}{\K}$ ein.  Dabei werden zwei Metalle zusammengebracht,
sind jedoch durch eine dünne Isolatorschicht getrennt.  Durch die
Ausdehnung der Elektronenwellenfunktion vom einen Metall bis ins
andere findet ein Tunnelprozess statt.  Diesen kann man verstärken
indem man eine Spannung anlegt.  Der prinzipielle Aufbau ist in
Figur~\ref{fig:12.1} skizziert.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \fill[MidnightBlue!20] (-2.5,-1) rectangle (-.5,1);
    \fill[MidnightBlue!40] (-.5,-1) rectangle (.5,1);
    \fill[MidnightBlue!20] (.5,-1) rectangle (2.5,1);
    \draw (-2.5,1) -- (2.5,1) (-2.5,-1) -- node[below,pos=.2] {Metall 1}
    coordinate[pin={below:Isolator}] (A) node[below,pos=.8] {Metall 2} (2.5,-1);
    \draw (-.5,1) -- (-.5,-1) (.5,1) -- (.5,-1);
    \draw[DarkOrange3,->] (-2,0) -- node[above] {$I$} (-1,0);
    \draw[DarkOrange3,->] (1,0) -- (2,0);
    \node[draw,circle] (U) at (0,1.5) {$U$};
    \draw (-1,1) |- (U) -| (1,1);
  \end{tikzpicture}
  \caption{Tunnelkontakt.  Zwei Metallschichten sind durch einen
    Isolator getrennt.  Der Spannungsabfall bei gegebenem Strom $I$
    zwischen den beiden Metallen kann gemessen werden.}
  \label{fig:12.1}
\end{figure}

Sind Metall 1 und 2 in Abbildung~\ref{fig:12.1} Normalleiter, so
verschiebt sich die Fermi-Energie linear mit der angelegten Spannung.
Die Tunnelwahrscheinlichkeit (im \si{\mV}-Bereich) ist unabhängig von
der Spannung.  Eine Skizze des Tunnelprozesses und der
Strom-Spannungs-Charakerstik ist in Abbildung~\ref{fig:12.2} zu sehen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \fill[MidnightBlue!20] (-1.5,1.5) rectangle (-.5,2);
      \fill[MidnightBlue!20] (.5,1) rectangle (1.5,2);
      \draw[fill=MidnightBlue!40] (-1.5,1.5) rectangle (-.5,.5);
      \draw[fill=MidnightBlue!40] (.5,1) rectangle (1.5,0);
      \draw (-1.5,2) -- (-1.5,1.5) node[left] {$E_F$} (-.5,1.5) -- (-.5,2);
      \draw (.5,2) -- (.5,1) (1.5,1) node[right] {$E_F$} -- (1.5,2);
      \draw[->] (-1.5,.5) -- (-2,.5) node[left] {$D(E)$};
      \draw[dashed] (-2,0) -- (2,0);
      \draw[<->] (-1,0) -- node[right] {$eU$} (-1,.5);
      \draw[MidnightBlue,->] (-.8,1.25) -- (.8,1.25);
    \end{scope}
    \begin{scope}[shift={(3,0)}]
      \draw[<->] (0,2) node[right] {$I$} |- (3,0) node[right] {$U$};
      \draw[DarkOrange3] (0,0) -- node[sloped,above] {Ohmsches Gesetz} (3,2);
    \end{scope}
  \end{tikzpicture}
  \caption{Handelt es sich bei den beteiligten Metallen um
    Normalleiter findet ein bekannter ohmscher Kontakt statt.}
  \label{fig:12.2}
\end{figure}

Ist das Metall 1 ein Supraleiter und Metall 2 ein Normalleiter müssen
wir beachten, dass die besetzten von den freien Zuständen durch eine
Energielücke $\Delta$ getrennt sind.  Im supraleitenden Zustand
befinden sich die Elektronen in gebundenen Cooper-Paar-Zuständen.
Damit Elektronen vom Normalleiter in den Supraleiter tunneln können
muss die Spannung über die Energielücke angehoben werden, also $e U >
\Delta$, denn für $U < U_0$ gibt es im Supraleiter keine freien
Zustände bei $T=\SI{0}{\K}$ und der Strom verbleibt $I=0$.  Werden
Cooper-Paare bei endlicher Temperatur $T<T_C$ aufgebrochen so können
die so entstandenen Quasi-Teilchen tunneln.  Abbildung~\ref{fig:12.3}
zeigt das Energieschema und den Tunnelstrom.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,yscale=1.5]
    \begin{scope}
      \fill[MidnightBlue!20] (.5,.8) rectangle (1.5,2);
      \draw[fill=MidnightBlue!40] (.5,.8) rectangle (1.5,0);
      \draw (.5,2) -- (.5,.8) (1.5,.8) node[right] {$E_F$} -- (1.5,2);
      \draw[dashed,<-] (-2.5,.5) node[below] {$D(E)$} --
      node[MidnightBlue,pos=.3] {$\circ\circ$}
      node[MidnightBlue,pos=.6] {$\circ\circ$}
      node[MidnightBlue,pos=.9] {$\circ\circ$} (0,.5);
      \fill[MidnightBlue!20] (-.3,2) to[out=-100,in=0] (-2,1.2) -- (-2,1) -| (0,2);
      \draw (0,2) |- (-2,1);
      \draw (-.3,2) to[out=-100,in=0] (-2,1.2);
      \draw[<->] (-1.7,1) -- node[left] {$\Delta$} (-1.7,.5);
      \draw[DarkOrange3,->] (.8,.7) node[dot] {} -- (-.2,.7);
      \draw[dashed] (2.25,.8) -- (2.75,.8) (2.25,.5) -- (2.75,.5);
      \draw[<->] (2.5,.8) -- node[right] {$eU$} (2.5,.5);
    \end{scope}
    \begin{scope}[shift={(3.5,0)}]
      \draw[<->] (0,2) node[right] {$I$} |- (3,0) node[right] {$U$};
      \draw[dashed] (0,0) -- (3,2);
      \draw[DarkOrange3,dashed] (0,0) node[below] {$T>\SI{0}{\K}$}
      ..controls (1.5,0) and (1.5,1) .. (3,2);
      \draw[DarkOrange3] (1.5,0) node[below right] {$U_0 = \frac\Delta e$}
      .. controls (1.5,.6) .. node[right] {$T=\SI{0}{\K}$} (3,2);
    \end{scope}
  \end{tikzpicture}
  \caption{Kontakt eines Supraleiters und eines Normalleiters.  Die
    offenen Kreise stehen für Cooper-Paare.  Ein Stromfluss kann nur
    stattfinden, wenn $eU$ so groß ist, dass Elektronen in den
    Supraleiter tunneln können.}
  \label{fig:12.3}
\end{figure}


Sind die beiden Metalle 1 und 2 Supraleiter ergeben sich Phänomene,
die als Josephson-Effekte\footnote{Josephson-Effekte vorhergesagt
  1962, Nobelpreis 1973} bekannt sind.  Eine Skizze zum Aufbau eines
Josephson-Kontakts ist in Abbildung~\ref{fig:12.4} gezeigt.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,circuit ee IEC]
    \fill[MidnightBlue!20] (-2,-1) rectangle (-.5,1);
    \fill[MidnightBlue!40] (-.5,-1) rectangle (.5,1);
    \fill[MidnightBlue!20] (.5,-1) rectangle (2,1);
    \draw (2,1) -- (-2,1) -- (-2,-1) -- node[below] {Isolator} (2,-1) -- cycle;
    \draw (-.5,1) -- (-.5,-1) (.5,1) -- (.5,-1);
    \node at (-1.25,0) {SL1};
    \node at (1.25,0) {SL2};
    \node[draw,circle] (U) at (0,1.5) {$U$};
    \draw (-1,1) |- (U) -| (1,1);
    \draw (-2,0) -| (-2.5,-2)
    to[resistor={DarkOrange3,info'=\textcolor{DarkOrange3}{$R$}}] (0,-2)
    to[battery={DarkOrange3,info'=\textcolor{DarkOrange3}{$U_{\text{ext}}$}}]
    (2.5,-2) |- (2,0);
  \end{tikzpicture}
  \caption{Beschaltung eines Jospehson-Kontakts von zwei
    Supraleitern.}
  \label{fig:12.4}
\end{figure}

Falls die Isolatorschicht sehr dünn ist ($\sim\SI{1}{\nm}$), reicht
die Wellenfunktion des einen Supraleiters merklich in den Bereich des
anderen Supraleiters.  Dies führt zu einer Kopplung der
Supraleiter.  Die makroskopische Wellenfunktion des BCS-Zustands
lautet
\begin{equation}
  \label{eq:10.27}
  \psi = \psi_0 \ee^{\ii \varphi(r)} = \sqrt{n_S} \, \ee^{\ii \varphi(r)} \; ,
\end{equation}
wobei
\begin{equation}
  \label{eq:10.28}
  \psi\psi^* = |\psi_0|^2 = n_S
\end{equation}
Die Funktion $\varphi(r)$ beschreibt eine Phase und besitzt über
makroskopische Entfernungen einen wohldefinierten Wert.  Sind die
Supraleiter voneinander getrennt erfüllen die Wellenfunktionen der
Supraleiter separate Schrödingergleichungen
\begin{align*}
  \ii\hbar\dot\psi_1 &= H_1 \psi_1 \\
  \ii\hbar\dot\psi_2 &= H_2 \psi_2
\end{align*}
mit den Eigenwerten $E_1$ und $E_2$.  Für gekoppelte Supraleiter
betreiben wir Störungstheorie
\begin{equation}
  \label{eq:10.29}
  \begin{aligned}
    \ii\hbar\dot\psi_1 &= E_1 \psi_1 + \kappa \psi_2 \\
    \ii\hbar\dot\psi_2 &= E_2 \psi_2 + \kappa \psi_1
  \end{aligned}
\end{equation}
mit dem Kopplungsparameter $\kappa$.  Falls die Supraleiter aus
dem gleichen Material sind gilt $n_{S1} = n_{S2} = n_S$ und $E_1
= E_2$. Falls die Spannung an der Isolationsschicht abfällt gilt
\begin{equation}
  \label{eq:10.30}
  E_2 - E_1 = - 2 e U \; .
\end{equation}
Man setzt \eqref{eq:10.27} in \eqref{eq:10.29} ein und man lässt
eine zeitliche Entwicklung der Dichte $n_S$ und der Phase
$\varphi$ zu. Trennen von Real- und Imaginärteil liefert
\begin{align}
  \label{eq:10.31}
  \dot n_{S1} &= \frac{2\kappa}{\hbar} \sqrt{n_{S1} n_{S2}}
  \sin(\varphi_2 - \varphi_1) \; ,
  &
  \dot n_{S2} &= -\frac{2\kappa}{\hbar} \sqrt{n_{S1} n_{S2}}
  \sin(\varphi_2 - \varphi_1) \; ,
  \\
  \label{eq:10.32}
  \dot\varphi_1 &= \frac{\kappa}{\hbar} \sqrt{\frac{n_{S1}}{n_{S2}}}
  \cos(\varphi_2 - \varphi_1) - \frac{E_1}{\hbar} \; ,
  &
  \dot\varphi_2 &= \frac{\kappa}{\hbar} \sqrt{\frac{n_{S1}}{n_{S2}}}
  \cos(\varphi_2 - \varphi_1) + \frac{E_2}{\hbar} \; .
\end{align}
Die Differenz der beiden letzten Gleichungen ist
\begin{equation}
  \label{eq:10.33}
  \hbar(\dot\varphi_2 - \dot\varphi_1) = - (E_2 - E_1) = 2 e U
\end{equation}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: