In Dipolnäherung erhalten wir durch Fermis Goldene Regel die
Übergangswahrscheinlichkeit für die Absorption
\begin{equation}
  \label{eq:14.2}
  W_{i\to j} = \frac{2\pi}{\hbar} |\braket{j|e \bm r\cdot\bm E|i}|^2
  \delta(E_j - E_i - \hbar\omega)
\end{equation}
und für die Emission
\begin{equation}
  \label{eq:14.3}
  W_{j\to i} = \underbrace{
    \frac{2\pi}{\hbar} |\braket{j|e \bm r\cdot\bm E|i}|^2
    \delta(E_i - E_j + \hbar\omega)
  }_{\text{stimulierte Emission}}
  + \underbrace{
    \frac{4\alpha\omega_{ji}^3}{c^2} |\braket{j|\bm r|i}|^2
  }_{\text{spontane Emission}}
\end{equation}
mit der Feinstrukturkonstante $\alpha$ und der Bohrschen Frequenz
$\omega_{ji} = (E_j - E_i)/\hbar$.

\minisec{Beispiele zu Rastersondentechniken}

Bei Rastersondentechniken wird eine Sonde nahe an die Oberfläche
gebracht und an ihr entlang gezogen.  Die Wechselwirkung der Sonde mit
der Oberfläche lässt Rückschlüsse auf die Topographie zu.

Beim Rastertunnelmikroskop (STM) wird eine Spitze bis auf ca.\
\SI{1}{\nm} in die Nähe der Probe gebracht.  Piezoelektrische Elemente
bestimmen die Position der Spitze bis auf wenige Picometer genau,
siehe dazu auch Figur~\ref{fig:29.1}.  Die Probe wird auf die Spannung
$V$ gelegt und der Tunnelstrom $I$ der zwischen Spitze und Probe
fließt wird gemessen.  Der Strom ist proportional zur
Tunnelwahrscheinlichkeit
\begin{equation}
  \label{eq:14.4}
  T \sim \exp\left( -2z\sqrt{2m\phi/\hbar^2} \right)
\end{equation}
dabei ist $z$ der Abstand zwischen Spitze und Porbe (\SI{1}{\nm}),
$\phi$ ist die effektive Barrierenhöhe des Tunnelprozesses und $m$ die
Masse des Elektrons.  Im Rückkopplungsmodus wird der Strom $I$ durch
die Änderung der Spitzenhöhe $z$ konstant gehalten ($\leq
\SI{1}{\nm}$).  Man erhält so Informationen über die Topologie der
Oberfläche.  Mit dem STM können außerdem Atome manipuliert werden.
Der STM-Tunnelstrom $I$ liefert als Funktion der Vorpsannung $V$
räumliche und spektroskopische Informationen. Bei $T=\SI{0}{\K}$ ist
die Ableitung des Tunnelstroms nach der Vorspannung
\begin{equation}
  \label{eq:14.5}
  \frac{\diff I}{\diff t} \sim T \sum_j |\psi_j(\bm r_t)|^2 \delta(E_F+eV-E_j) \; .
\end{equation}
Die Ableitung ist proportional zur Zustandsdichte der tunnelnden
Elektronen bei der Energie $E_F + eV$, gewichtet mit der Dichte ihrer
Aufenthaltswahrscheinlichkeit am Ort $\bm r_t$ der STM-Spitze.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    % probe
    \shadedraw[left color=DimGray!50,right color=DimGray!50,middle color=white,looseness=.5]
    (-.5,1) to[bend left] (-.1,0) arc(-180:0:.1 and .05) to[bend left] (.5,1);
    \shadedraw[left color=DimGray!50,right color=DimGray!50,middle color=white]
    (-1,3) -- (-.5,1) arc(-180:0:.5 and .1) -- (1,3);
    % x-y-piezo
    \shade[left color=white,right color=white,middle color=DimGray!50]
    (-2,3) -- (.5,3) -- (2,4) -- (1,4) -- (.25,3.5) -- (-1.25,3.5) -- cycle;
    \draw (1,4) -- (.25,3.5) -- (-1.25,3.5);
    \shade[left color=white,right color=DimGray!50] (-2,2.5) rectangle (.5,3);
    \shade[left color=DimGray!50,right color=white] (.5,2.5) -- (2,3.5) -- (2,4) -- (.5,3) -- cycle;
    \draw (-2,2.5) -- (.5,2.5) -- (2,3.5);
    \draw (-2,3) -- (.5,3) -- (2,4);
    \draw (.5,2.5) -- (.5,3);
    % z-piezo
    \shade[top color=white,bottom color=DimGray!50] (-.6,3.1) rectangle (.4,5);
    \shade[top color=white,bottom color=DimGray!50] (.4,3.1) -- (.8,3.4) -- (.8,5.3) -- (.4,5) -- cycle;
    \draw (-.6,5) -- (-.6,3.1) -- (.4,3.1) -- (.4,5);
    \draw (.4,3.1) -- (.8,3.4) -- (.8,5.3);
    % Nodes
    \node[below] at (-1,2.5) {$x$-piezo};
    \path (.5,2.5) -- node[sloped,below] {$y$-piezo} (2,3.5);
    \path (-.6,3.1) -- node[sloped,above] {$z$-piezo} (-.6,5);
    % sample
    \shade[ball color=DimGray!50] (-1.8,-1) circle (.2);
    \shade[ball color=DimGray!50] (-.6,-1) circle (.2);
    \shade[ball color=DimGray!50] (.6,-1) circle (.2);
    \shade[ball color=DimGray!50] (1.8,-1) circle (.2);
    \shade[ball color=DimGray!50] (-1.2,-1.5) circle (.2);
    \shade[ball color=DimGray!50] (0,-1.5) circle (.2);
    \shade[ball color=DimGray!50] (1.2,-1.5) circle (.2);
    \draw[dashed,looseness=1.5]
    (-2.1,-.5) to[out=60,in=120] (-1.5,-.5) to[out=-60,in=-120] (-.9,-.5) to[out=60,in=120] (-.3,-.5)
    to[out=-60,in=-120] (.3,-.5) to[out=60,in=120] (.9,-.5) to[out=-60,in=-120] (1.5,-.5)
    to[out=60,in=120] (2.1,-.5);
  \end{tikzpicture}
  \caption{Schemazeichnung eines Rastertunnelmikroskops (STM).}
  \label{fig:29.1}
\end{figure}

Bei der Rasterkraftmikroskopie (AFM) wird die Kraft zwischen Spitze
und Probe gemessen und ist damit geeignet für leitende und isolierende
Proben.  Die Spitze ist auf einem elastischem Cantilever angebracht,
dessen Auslenkung $\Delta z$ mit einem Laser gemessen wird.  Im
Kontaktmodus wird die Spitze in Kontakt mit der Oberfläche gebracht
und an der Oberfläche entlang gezogen.  Die Auslenkung ist
proportional zur Kraft
\begin{equation*}
  F = C\Delta z
\end{equation*}
und liefert so Informationen über die Topographie.  Es gibt auch einen
kontaktfreien Modus (tapping mode) bei dem Informationen über
langreichweitige Kräfte zwischen Probe und Spitze gesammelt werden.
Der Cantilever oszilliert nahe seiner Resonanzfrequenz $\omega_0$
kanpp über der Probe, angetrieben durch eine Kraft mit der Amplitude
$F_\omega$.  Dies entspricht einem getriebenen harmonischen Oszillator
\begin{equation}
  \label{eq:14.6}
  |z_\omega| = \frac{F_\omega}{C} \frac{\omega_0^2}{\left[
      (\omega^2 - \omega_0^2) + \left( \frac{\omega \omega_0}{Q} \right)
    \right]^{1/2}}
\end{equation}
mit der Güte $Q$ der Resonators und der Kraftkonstante $C$ des
Oszillators.

Bei der Resonanzfrequenz $\omega=\omega_0$ ist die Auslenkung
$z_\omega$ um $Q$-mal größer als bei niedrigen Frequenzen.  Damit ist
die Detektion von sehr kleinen Kräften möglich.  Mögliche Kräfte
zwischen Spitze und Probe sind van-der-Waals-Kräfte, elektrostatische
Kräfte oder auch magnetische Kräfte.  Die Wechselwirkung verschiebt
die Resonanzfrequenz $\omega_0$ und/oder modifiziert $Q$.  Diese
Änderung wird aufgezeichnet.

\section{Zustandsdichten}

Die Zustandsdichte im reziproken Raum ist gegeben durch das Volumen im
Realraum geteilt durch das Volumen im $k$-Raum.  Für die
unterschiedlich dimensionalen Fälle ergibt sich damit
\begin{align}
  \label{eq:14.7}
  D_k^{\text{0D}} &= \delta(k-k_0) \\
  \label{eq:14.8}
  D_k^{\text{1D}} &= \frac{L}{2\pi} \\
  \label{eq:14.9}
  D_k^{\text{2D}} &= \frac{A}{4\pi^2} = \frac{L^2}{4\pi^2} \\
  \label{eq:14.10}
  D_k^{\text{3D}} &= \frac{V}{(2\pi)^3} = \frac{L^3}{8\pi^3}
\end{align}
Generell ist die Zustandsdichte definiert als
\begin{equation*}
  D(\omega) = \sum_{\bm k} \delta(\omega - \omega(\bm k)) \; .
\end{equation*}
Diese Summe kann man in ein Integral überführen nach der Regel
\begin{equation*}
  \sum_{\bm k} \to \frac{L^d}{(2\pi)^d} \int\diff^d k \; .
\end{equation*}
In drei Dimensionen ergibt sich damit
\begin{equation*}
  D^{\text{3D}}(\omega) = \frac{V}{(2\pi)^3} \int \delta(\omega - \omega(\bm k)) \diff^3 k \; .
\end{equation*}
Eine Eigenschaft der Delta-Distribution ist
\begin{equation*}
  \int \delta(g(x)) \diff x = \sum_n \int \frac{\delta(x - x_n)}{|g'(x_n)|} \diff x = \sum_n \frac{1}{|g'(x_n)|} \; .
\end{equation*}
Sind die Nullstellen kontinuierlich verteilt, so kann man diese Summe
in ein Integral überführen
\begin{equation*}
  \int \delta(g(\bm k)) \diff^3 k = \int_{S_n} \frac{1}{|\nabla_{\bm k_n} g(\bm k_n)|} \diff\bm k_n
  \quad\text{mit}\quad
  S_n = \{ \bm k_n : g(\bm k_n) = 0 \} \; .
\end{equation*}
Wenden wir dies auf unsere Zustandsdichten an und nehmen an, dass
\begin{equation*}
  \omega(\bm k) = v_g |k| \; ,
\end{equation*}
sodass $|\nabla_{\bm k_n} g(\bm k_n)| = v_g$ ist, so erhalten wir
\begin{equation}
  \label{eq:14.11}
  D^{\text{3D}}(\omega) \diff\omega
  = \frac{V}{(2\pi)^3} \diff\omega \int \delta(\omega-\omega(\bm k)) \diff^3 k
  \stackrel{\text{(6.2)}}{=}
  \frac{V}{(2\pi)^3} \diff\omega \int \frac{\diff S_\omega}{v_g}
\end{equation}
wobei $S_\omega$ die Fläche ist auf der $\omega = \omega(\bm k)$.

In isotropen Festkörpern ist die Oberfläche konstanter Frequenz eine
Kugelfläche auf der die Gruppengeschwindigkeit konstant ist.
\begin{align}
  \label{eq:14.12}
  D^{\text{3D}}(\omega)\diff\omega
  &= \frac{V}{(2\pi)^3}\diff\omega\frac{4\pi k^2}{v_g}
  = \frac{V}{2\pi^2}\frac{k^2}{v_g}\diff\omega \sim k^2 \\
  \label{eq:14.13}
  D^{\text{2D}}(\omega)\diff\omega
  &= \frac{A}{(2\pi)^2}\diff\omega\frac{2\pi k}{v_g}
  = \frac{A}{2\pi}\frac{k}{v_g}\diff\omega \sim k \\
  \label{eq:14.14}
  D^{\text{1D}}(\omega)\diff\omega
  &= \frac{L}{2\pi}\diff\omega\frac{2}{v_g}
  = \frac{L}{\pi v_g}\diff\omega \sim 1
\end{align}

\minisec{Spezifische Wärme in Nanostrukturen}

In drei Dimensionen gilt das Debyesche $T^3$-Gesetz mit der
Debye-Temperatur $\Theta$
\begin{equation*}
  C_V \sim
  \begin{cases}
    T^3 & T<\Theta \\
    C_V = \const & T\gg\Theta   
  \end{cases} \; .
\end{equation*}

Typische Energien der quantisierten Schwingungsmoden sind bei
Zimmertemperatur kleiner als $\kB T$, die Ausnahme sind allerkleinste
Strukturen.  Schwingungen sind auch in Richtung eingeschränkter
Probendimension angeregt.  Die thermischen Eigenschaften von
Nanostrukturen und großen Proben sind ähnlich.

Bei tiefen Temperaturen, also $\kB T < \hbar \omega_{\text{Phonon}}$
sind die Schwingungsanregungen in Richtung der eingeschränkten
Geometrie eingefroren (für die Rechnung mit der 1D und 2D
Zustandsdichte siehe Kapitel 6).  Die Wärmekapazität ergibt sich dann
zu
\begin{align*}
  \text{2D:}\quad C_V &\sim T^2 \\
  \text{1D:}\quad C_V &\sim T
\end{align*}

\minisec{Wärmetransport in eindimensionalen Strukturen}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[draw,rectangle,minimum size=1cm,pin={below left:Warmes Reservoir}] (TW) at (-2,0) {$T_W$};
    \node[draw,rectangle,minimum size=1cm,pin={below right:Kaltes Reservoir}] (TK) at (2,0) {$T_K$};
    \draw[double,double distance=.2cm] (TW) -- node[above=.5cm] {1D-Kanal} (TK);
    \draw[<->] (TW.30) -- node[fill=white,inner ysep=0pt] {$L$} (TK.150);
    \draw[MidnightBlue,->] (-1.5,-1) -- node[below] {$k$} (-1,-1);
    \draw[MidnightBlue,->] (1.5,-1) -- node[below] {$-k$} (1,-1);
  \end{tikzpicture}
  \caption{Schematischer Verlauf eines eindimensionalen Leiters der
    Länge $L$.  Wärme wird über den eindimensionalen Kanal vom warmen
    Reservoir ins kalte transportiert.  Die Pfeile mit $k$ und $-k$
    stellen gegenläufig propagierende Phononen dar.}
  \label{fig:29.2}
\end{figure}

Der Energiefluss von der warmen zur kalten Seite durch die Probe in
Abbildung~\ref{fig:29.2} ist gegeben durch
\begin{equation}
  \label{eq:14.15}
  J = \frac{1}{L} \sum_k \hbar\omega_k v_k \; .
\end{equation}
Dabei ist $v_k$ die Gruppengeschwindigkeit der Phononen.  Die Summe
läuft über alle thermisch angeregten Phononen.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: