\subsection{Quantentheorie des Quanten-Hall-Effekts}

Beim Quanten-Hall-Effekt handelt es sich um einen topologischen
Quanten-Effekt.  Daher benötigen wir etwas topologischen Jargon bevor
wir mit der Herleitung starten können.  Diese ist aus \textcite{TIM}
übernommen und an einigen Stellen etwas ausgeführt.

\subsubsection{Berry-Phase}

Sei $\bm R(t)$ die Vektorrepräsentation eines Satzes zeitabhängiger
Parameter im Parameterraum.  Der Hamiltonoperator des System hängt von
diesen Parametern ab und wird deshalb $H[\bm R(t)]$ geschrieben,
gleiches gilt für die Eigenzustände $\ket{n,\bm R(t)}$.  Das System
erfüllt eine stationäre Schrödingergleichung
\begin{equation*}
  H[\bm R(t)] \ket{n,\bm R(t)} = E_n[\bm R(t)] \ket{n,\bm R(t)} \; .
\end{equation*}
Für eine adiabatische Zeitentwicklung ab einem Zeitpunkt $t=0$ gilt
\begin{equation*}
  H[\bm R(t)] \ket{n,t} = \ii \hbar \frac{\diff}{\diff t} \ket{n,t} \; ,
\end{equation*}
wobei die zeitabhängigen Zustände $\ket{n,t}$ durch
\begin{equation*}
  \ket{n,t} = \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L[\bm R(t')] \right)
  \ket{n,\bm R(t)}
\end{equation*}
mit
\begin{equation*}
  L[\bm R(t)] = \ii\hbar\dot{\bm R}(t) \braket{n,\bm R(t)|\nabla_R|n,\bm R(t)}
  - E_n[\bm R(t)]
\end{equation*}
ausgedrückt werden.  Die Wahl von $L_n[\bm R(t)]$ mag eventuell etwas
seltsam erscheinen wird aber klar, sobald wir die Zustände $\ket{n,t}$
in die zeitabhängige Schrödingergleichung einsetzen.
\begin{align*}
  H[\bm R(t)] \ket{n,t}
  &= \ii \hbar \frac{\diff}{\diff t} \ket{n,t} \\
  &= \ii \hbar \frac{\diff}{\diff t} \left[
    \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L_n[\bm R(t')] \right)
    \ket{n,\bm R(t)} \right]
\end{align*}
Hier muss offensichtlich die Produktregel angewendet werden.
\begin{align*}
  \MoveEqLeft
  H[\bm R(t)] \ket{n,t} \\
  &=
  \begin{multlined}[t]
    \ii \hbar \left[ \frac{\diff}{\diff t}
      \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L_n[\bm R(t')] \right) \right]
    \ket{n,\bm R(t)} \\
    + \ii \hbar \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L_n[\bm R(t')] \right)
    \frac{\diff}{\diff t} \ket{n,\bm R(t)}
  \end{multlined}
  \intertext{Im zweiten Term fügen wir eine Eins in der Form $1 =
    \ket{n,\bm R(t)}\bra{n,\bm R(t)}$ ein.}
  &= \!
  \begin{multlined}[t]
    {-} L_n[\bm R(t)] \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L_n[\bm R(t')] \right) \ket{n,\bm R(t)} \\
    + \ii \hbar \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L_n[\bm R(t')] \right)
    \ii\hbar \dot{\bm R}(t) \nabla_R \ket{n,\bm R(t)}
  \end{multlined}
  \intertext{Wir identifizieren $\ket{n,t} = \exp(\ldots)\ket{n,\bm R(t)}$.}
  &= \!
  \begin{multlined}[t]
    {-} \ii\hbar\dot{\bm R}(t) \braket{n,\bm R(t)|\nabla_R|n,\bm R(t)}
    \ket{n,t} + E_n[\bm R(t) \ket{n,t} \\
    + \ii \hbar \exp\left( \frac\ii\hbar \int_0^t \diff t'\; L_n[\bm R(t')] \right)
    \ket{n,\bm R(t)} \ii\hbar \dot{\bm R}(t) \braket{n,\bm R(t)|\nabla_R|n,\bm R(t)}    
  \end{multlined} \\
  &=
  \begin{multlined}[t]
    \cancel{- \ii\hbar\dot{\bm R}(t) \braket{n,\bm R(t)|\nabla_R|n,\bm R(t)} \ket{n,t}}
    + E_n[\bm R(t)] \ket{n,t} \\
    + \cancel{\ii \hbar\ket{n,t}\dot{\bm R}(t) \braket{n,\bm R(t)|\nabla_R|n,\bm R(t)}}
  \end{multlined} \\
  &= E_n[\bm R(t)] \ket{n,t}
\end{align*}
Die zeitabhängigen Zustände erfüllen also eine stationäre
Schrödingergleichung.  Mit der Form von $L_n[\bm R(t)]$ kann man
$\ket{n,t}$ schreiben als
\begin{equation*}
  \ket{n,t} = \exp\left(
    - \int_0^t \diff t'\; \dot{\bm R}(t') \braket{n,\bm R(t')|\nabla_R|n,\bm R(t')}
  \right)
  \exp\left(
    -\frac\ii\hbar \int_0^t \diff t'\; E_n[\bm R(t')]
  \right)
  \ket{n,\bm R(t)} \; ,
\end{equation*}
wobei die erste Exponentialfunktion die wegabhängige Phase beschreibt,
die zweite hingegen eine konstante Phase.  Widmen wir uns dem ersten
Term.  Wir fahren $\bm R$ auf einer \emph{geschlossenen} Kurve
$\mathcal C$ im Parameterraum von $t=0$ bis $t=T$ durch, sodass $\bm
R(0) = \bm R(T)$.  Die \acct{Berry-Phase} $\gamma_n[\mathcal C]$
dieser Kurve ist definiert als
\begin{equation*}
  \gamma_n[\mathcal C] =
  \ii \int_0^T \diff t'\; \dot{\bm R}(t') \cdot \braket{n,\bm R(t')|\nabla_R|n,\bm R(t')} \; .
\end{equation*}
Dies lässt sich in ein Kurvenintegral überführen.
\begin{equation*}
  \gamma_n[\mathcal C] =
  \ii \oint_{\mathcal C} \diff \bm R\cdot \braket{n,\bm R|\nabla_R|n,\bm R} \; .
\end{equation*}
Mit der Ersetzung $\bm A_n(\bm R) = - \ii \braket{n,\bm R|
  \nabla_R|n,\bm R}$ folgt
\begin{equation*}
  \gamma_n[\mathcal C] =
  - \oint_{\mathcal C} \diff \bm R\cdot \bm A_n(\bm R) \; .
\end{equation*}
Nun wenden wir noch den Satz von Stokes an, führen also das
Kurvenintegral in ein Integral über die von der Kurve eingeschlossene
Fläche über
\begin{align*}
  \gamma_n[\mathcal C]
  &= - \int_S \diff \bm S\cdot (\nabla_R \times \bm A_n(\bm R)) \\
  &= - \int_S \diff \bm S\cdot \bm B_n(\bm R) \; .
\end{align*}

Im vorliegenden Abschnitt haben wir also einige wichtige Größen
definiert.  Zum einen den Berry-Zusammenhang
\begin{equation*}
  \bm A_n(\bm R) = - \ii \braket{n,\bm R|\nabla_R|n,\bm R}
\end{equation*}
und die damit über Rotation verbundene Berry-Krümmung
\begin{equation*}
  \bm B_n(\bm R) = \nabla_R \times \bm A_n(\bm R) \; .
\end{equation*}
Damit ist die Berry-Phase definiert als
\begin{equation*}
  \gamma_n[\mathcal C] = - \int_S \diff \bm S\cdot \bm B_n(\bm R) \; .
\end{equation*}

\subsubsection{Topologische Invariante}

Die topologische Invariante des Quanten-Hall-Effekts wird
TKNN-Invariante genannt, nach den Autoren Thouless, Kohmoto,
Nightingale, und den Nijs \cite{TKNN}.  Um diese abzuleiten berechnen
wir die Leitfähigkeit eines $L\times L$ großen zweidimensionalen
Elektronensystems.  Das elektrische Feld liege in $y$-Richtung, das
magnetische Feld in $z$-Richtung an.  Wir behandeln das elektrische
Feld störungstheoretisch.  Es ist gegeben durch das externe Potential
$V = -eEy$.  Die Zustandskorrektur in erster Ordnung stationärer
Störungstheorie lautet
\begin{equation*}
  \ket{n}_E = \ket{n} + \sum_{m\neq n} \frac{\braket{m|-eEy|n}}{E_n - E_m} \ket{m} \; .
\end{equation*}
Wir berechnen nun den Erwartungswert der Stromdichte entlang der
$x$-Achse unter dem gestörten Zustand.
\begin{align*}
  \braket{j_x}_E
  &= \sum_n f(E_n) \left\langle n \middle|_E \frac{e v_x}{L^2} \middle| n \right\rangle_E \\
  &= \sum_n f(E_n)
  \biggl(\bra{n}+\sum_{m\neq n}\frac{\braket{n|-eEy|m}}{E_n-E_m}\bra{m}\biggr)
  \frac{e v_x}{L^2}
  \biggl(\ket{n}+\sum_{m\neq n}\frac{\braket{m|-eEy|n}}{E_n-E_m}\ket{m}\biggr) \\
  &= \!
  \begin{multlined}[t]
    \sum_n f(E_n) \biggl[
    \underbrace{\Braket{n|\frac{e v_x}{L^2}|n}}_{\sim\braket{j_x}_{E=0}}
    +\sum_{m\neq n}\frac{\braket{m|-eEy|n}}{E_n-E_m}\Braket{n|\frac{e v_x}{L^2}|m} \\
    +\sum_{m\neq n}\frac{\braket{n|-eEy|m}}{E_n-E_m}\Braket{m|\frac{e v_x}{L^2}|n}
    +\mathcal{O}(E_n^{-2})
    \biggr]
  \end{multlined}
\end{align*}
Wir vernachlässigen die Terme zweiter Ordnung und erhalten
\begin{equation*}
  = \braket{j_x}_{E=0}
  + \frac{1}{L^2} \sum_n f(E_n) \biggl[
  \sum_{m\neq n} \frac{\braket{m|-eEy|n}\Braket{n|e v_x|m}}{E_n-E_m}
  + \frac{\braket{n|-eEy|m}\Braket{m|e v_x|n}}{E_n-E_m}
  \biggr] \; .
\end{equation*}
Dabei ist $v_x$ die Geschwindigkeit der Elektronen entlang der
$x$-Achse und $f(E_n)$ die Fermi-Verteilungsfunktion.  In der
Geschwindigkeit $v_x$ ist die Wirkung des Magnetfeldes enthalten,
welches die Elektronen in $x$-Richtung ablenkt.  Wir bedienen uns noch
eines Tricks um $y$ durch $v_y$ auszudrücken.  Dazu sehen wir uns die
Heisenberg-Bewegungsgleichung von $y$ an.
\begin{equation*}
  \frac{\diff}{\diff t} y = v_y = -\frac\ii\hbar [y,H] \; .
\end{equation*}
Betrachten wir davon das Matrixelement der rechten Seite
\begin{align*}
  \Braket{m|\frac{1}{\ii\hbar} [y,H]|n}
  = \frac{1}{\ii\hbar} \braket{m|yH-Hy|n} \\
  = \frac{1}{\ii\hbar} \bigl( \braket{m|y E_n|n} - \braket{m|E_m y|n} \bigr) \\
  = \frac{1}{\ii\hbar} (E_n - E_m) \braket{m|y|n} \; .
\end{align*}
Dies ist äußerst nützlich, weil damit
\begin{equation*}
  \braket{m|v_y|n} = \frac{1}{\ii\hbar} (E_n - E_m) \braket{m|y|n} \; .
\end{equation*}
Dann können wir im Erwartungswert der Stromdichte die Matrixelemente
von $y$ ersetzen durch Ausdrücke mit $v_y$.
\begin{multline*}
  \frac{1}{L^2} \sum_n f(E_n) \biggl[
  \sum_{m\neq n} \frac{\braket{m|-eEy|n}\Braket{n|e v_x|m}}{E_n-E_m}
  + \frac{\braket{n|-eEy|m}\Braket{m|e v_x|n}}{E_n-E_m}
  \biggr] \\
  =
  -\frac{\ii\hbar e^2 E}{L^2} \sum_n \sum_{m\neq n} f(E_n)
  \frac{\Braket{n|v_x|m}\braket{m|v_y|n}
    -\braket{n|v_y|m}\Braket{m|v_x|n}}{(E_n-E_m)^2} \; .
\end{multline*}
Ohne ein elektrisches Feld in $y$-Richtung bewegen sich die Elektronen
nicht fort.  Es kann also auch keine Ablenkung in $x$-Richtung
stattfinden und wir können ohne Beschränkung der Allgemeinheit
$\braket{j_x}_{E=0}=0$ setzen.  Damit erhalten wir für die
Leitfähigkeit in $x$-Richtung
\begin{equation*}
  \sigma_{xy} = \frac{\braket{j_x}_E}{E} =
  -\frac{\ii\hbar e^2}{L^2} \sum_n \sum_{m\neq n} f(E_n)
  \frac{\Braket{n|v_x|m}\braket{m|v_y|n}
    -\braket{n|v_y|m}\Braket{m|v_x|n}}{(E_n-E_m)^2} \; .
\end{equation*}
Nehmen wir den Leiter als einfachen Festkörper an, so wissen wir, dass
die Elektronenwellenfunktionen in diesem periodischen Potential durch
Blockwellen gegeben sind.  Von weiter oben kennen wir die Identität
\begin{equation*}
  \braket{m|v_y|n} = \frac{1}{\ii\hbar} (E_n - E_m) \braket{m|y|n} \; .
\end{equation*}
Außerdem wissen wir, dass Blochwellen in Ortsdarstellung wie folgt aussehen
\begin{equation*}
  \braket{\bm r|u_{n,\bm k}} = u_{n,\bm k}(\bm r) = \phi_{n,\bm k}(r) \, \ee^{-\ii \bm k \cdot \bm r} \; .
\end{equation*}
Daraus lässt sich leicht erkennen, dass
\begin{equation*}
  r_\mu u_{n,\bm k}(\bm r) = \ii \frac{\partial}{\partial k_\mu} u_{n,\bm k}(\bm r)
  \quad\text{für}\quad
  \mu \in \{x,y,z\}
  \quad\text{also}\quad
  r_\mu \hateq \{x,y,z\} \; .
\end{equation*}
Setzen wir dies in unsere Identität von oben ein so gilt
\begin{equation*}
  \braket{u_{m,\bm k'}|v_\mu|u_{n,\bm k}}
  = \frac{1}{\hbar} (E_{n,\bm k} - E_{m,\bm k'})
  \Braket{u_{m,\bm k'}|\frac{\partial}{\partial k_\mu}|u_{n,\bm k}} \; .
\end{equation*}
Damit können wir die Leitfähigkeit umschreiben, wobei die Zustände
$\ket{m}$ und $\ket{n}$ in Blochwellen $\ket{u_{m,\bm k'}}$ und
$\ket{u_{n,\bm k}}$ entwickelt werden.
\begin{multline*}
  \sum_n \sum_{m\neq n} f(E_n)
  \frac{\Braket{n|v_x|m}\braket{m|v_y|n}
    -\braket{n|v_y|m}\Braket{m|v_x|n}}{(E_n-E_m)^2} \\
  =
  -\frac{1}{\hbar^2}
  \sum_n \sum_{m\neq n} \sum_{\bm k} \sum_{\bm k'}
  f(E_{n,\bm k})
  \Bigl[
  \braket{u_{n,\bm k}|\partial_{k_x}|u_{m,\bm k'}}
  \braket{u_{m,\bm k'}|\partial_{k_y}|u_{n,\bm k}} \\
  -
  \braket{u_{n,\bm k}|\partial_{k_y}|u_{m,\bm k'}}
  \braket{u_{m,\bm k'}|\partial_{k_x}|u_{n,\bm k}}
  \Bigr]
\end{multline*}
Wir können die Spektralzerlegung der Eins identifizieren.  Außerdem
möchten wir den Differentialoperator, der davor steht nach links
wirken lassen.  Dazu müssen wir uns kurz ansehen, was passiert wenn
wir diesen adjungieren.
\begin{equation*}
  \partial_x \hateq \frac\ii\hbar p_x
  \implies
  \partial_x^\dagger = - \partial_x \; .
\end{equation*}
Damit
\begin{multline*}
  -\frac{1}{\hbar^2}
  \sum_{n\neq m} \sum_{\bm k}
  f(E_{n,\bm k})
  \biggl[
  \bra{u_{n,\bm k}}
  \partial_{k_x} \sum_m \sum_{\bm k'} \ket{u_{m,\bm k'}}\bra{u_{m,\bm k'}}
  \partial_{k_y} \ket{u_{n,\bm k}} \\
  -
  \bra{u_{n,\bm k}}
  \partial_{k_y} \sum_m \sum_{\bm k'} \ket{u_{m,\bm k'}}\bra{u_{m,\bm k'}}
  \partial_{k_x} \ket{u_{n,\bm k}}
  \biggr]
  \\
  =
  \frac{1}{\hbar^2}
  \sum_n \sum_{\bm k}
  f(E_{n,\bm k})
  \Bigl[
  \bigl(\partial_{k_x} \bra{u_{n,\bm k}}\bigr)
  \bigl(\partial_{k_y} \ket{u_{n,\bm k}}\bigr)
  -
  \bigl(\partial_{k_y} \bra{u_{n,\bm k}}\bigr)
  \bigl(\partial_{k_x} \ket{u_{n,\bm k}}\bigr)
  \Bigr] \; .
\end{multline*}
Wenden wir nun noch die Produktregel rückwärts an, so ergibt sich
schließlich die Leitfähigkeit zu
\begin{equation*}
  \sigma_{xy} =
  -\frac{\ii e^2}{\hbar L^2} \sum_n \sum_{\bm k}
  f(E_{n,\bm k}) \bigl[
  \partial_{k_x} \bra{u_{n,\bm k}} \partial_{k_y} \ket{u_{n,\bm k}}
  - \partial_{k_y} \bra{u_{n,\bm k}} \partial_{k_x} \ket{u_{n,\bm k}}
  \bigr] \; .
\end{equation*}
Mit der vorigen Definition des Berry-Zusammenhangs schreiben wir
diesen für Blochwellen
\begin{equation*}
  \bm a_n(\bm k) = -\ii \braket{u_{n,\bm k}|\nabla_{\bm k}|u_{n,\bm k}}
\end{equation*}
Damit reduziert sich die Leitfähigkeit zu
\begin{equation*}
  \sigma_{xy} =
  \frac{e^2}{\hbar L^2} \sum_n \sum_{\bm k}
  f(E_{n,\bm k}) \left[
  \frac{\partial a_{n,y}(\bm k)}{\partial_{k_x}}
  - \frac{\partial a_{n,x}(\bm k)}{\partial_{k_y}}
  \right] \; .
\end{equation*}
Nun führen wir die Summe über $\bm k$ in ein Integral gemäß der Ersetzung
\begin{equation*}
  \sum_{\bm k} f(E_{n,\bm k}) \to
  \left(\frac{L}{2\pi}\right)^2\int_{\mathrm{BZ}} \diff k_x \diff k_y
\end{equation*}
wobei wir uns auf die erste Brillouin-Zone beschränken.  Zur
Erinnerung, das Integral geht nur über $k_x$ und $k_y$ weil die
Geometrie in $z$-Richtung eingeschränkt ist.
\begin{equation*}
  \sigma_{xy} =
  \frac{e^2}{h}
  \underbrace{
    \sum_n \int_{\mathrm{BZ}} \frac{\diff k_x \diff k_y}{2\pi}
    \left[
      \frac{\partial a_{n,y}(\bm k)}{\partial_{k_x}}
      - \frac{\partial a_{n,x}(\bm k)}{\partial_{k_y}}
    \right]}_{\nu}
  = \nu \frac{e^2}{h} \; .
\end{equation*}
Das $\nu$, das wir hier definiert haben kann offensichtlich zerlegt
werden in $\nu = \sum_n \nu_n$, wobei jedes $\nu_n$ den Beitrag des
$n$-ten Bandes angibt.  In $\nu_n$ identifizieren wir die Berry-Phase
\begin{align*}
  \nu_n &= \int_{\mathrm{BZ}} \frac{\diff k_x \diff k_y}{2\pi}
  \left[
    \frac{\partial a_{n,y}(\bm k)}{\partial_{k_x}}
    - \frac{\partial a_{n,x}(\bm k)}{\partial_{k_y}}
  \right] \\
  &= \frac{1}{2\pi} \int_{\mathrm{BZ}} \diff^2 k\; \nabla_{\bm k} \times \bm a_n(\bm k) \\
  &= \frac{1}{2\pi} \oint_{\partial\mathrm{BZ}} \diff\bm k \cdot \bm b_n(\bm k) \\
  &= -\frac{1}{2\pi} \gamma_n[\partial\mathrm{BZ}] \; .
\end{align*}
Die Brillouin"=Zone kann mit periodischen Randbedingungen als Torus aufgefasst
werden, $\mathrm{BZ} = \mathbb T^2$.  Da der Torus aber keinen Rand hat wäre
das Integral über $\partial\mathrm{BZ}$ immer null.  Dies setzt allerdings
voraus, dass $\bm a_n(\bm k)$ überall in der Brillouin"=Zone wohldefiniert ist.
Hat $\bm a_n(\bm k)$ also Singularitäten in der Brillouin"=Zone, so wird $\nu$
endlich.  Dies ist immer der Fall, da wir keine glatte Eichung der
Berry-Vektorpotentials über die gesamte Brillouin"=Zone finden können
\cite{Bernevig2013}.

Der Phasenfaktor beim Umrunden der Brillouin-Zone muss daher ein Vielfaches
von $2\pi$ sein, daher
\begin{equation*}
  \gamma_n[\partial\mathrm{BZ}] = 2 \pi m
  \quad\text{mit}\quad
  m \in \mathbb{Z} \; .
\end{equation*}
Daraus folgt also, dass $\nu_n \in \mathbb{Z}$, folglich muss auch
$\nu \in \mathbb{Z}$ und damit gilt
\begin{equation*}
  \boxed{
    \sigma_{xy} = \nu \frac{e^2}{h}
    \quad\text{mit}\quad
    \nu \in \mathbb{Z}
  }
\end{equation*}
Die Leitfähigkeit in Querrichtung nimmt also nur ganzzahlige Vielfache
von $e^2/h$ an.

Die ganze Zahl $\nu$ ist die topologische Invariante und wird
TKNN-Invariante genannt.  Das Quanten-Hall-System ist ein
topologischer Isolator, welches die Zeitumkehrsymmetrie bricht.


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End:
