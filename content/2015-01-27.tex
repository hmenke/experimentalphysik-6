\subsection{Orientierungspolarisation}

\minisec{Statische Polarisation}

Bisher hatten wir nur induzierte Dipolmomente betrachtet und den Fall,
dass ein permanentes Dipolmoment vorhanden ist vernachlässigt.  Ein
statisches Feld bewirkt eine Vorzugsorientierung, da die potentielle
Energie im Feld gegeben ist durch
\begin{equation*}
  U = -\bm p\cdot\bm E = - p E \cos\theta
\end{equation*}
und immer minimiert wird.

Eine typische experimentelle Bedingung ist dabei $\bm p\cdot\bm
E\ll\kB T$, sodass nur eine partielle Ausrichtung
stattfindet.  Wir berechnen den thermischen Mittelwert
\begin{equation*}
  \braket{\cos\theta} = \frac{p E}{3 \kB T} \; .
\end{equation*}
Für $p E \ll \kB T$ folgt damit die Langevin-Debye-Gleichung
\begin{equation}
  \label{eq:13.48}
  \boxed{
    P_0 = n p \braket{\cos\theta} = np \frac{p E}{3 \kB T}
  } \; .
\end{equation}
Der Temperaturverlauf der mit dieser Polarisation verbundenen
Suszeptibilität entspricht dem Curie-Gesetz bei der Magnetisierung.
Im Festkörper können die Moleküle manchmal auch in der festen Phase
noch umorientiert werden.  Voraussetzung dafür ist, dass es keine
kovalenten Bindungen gibt und die Moleküle näherungsweise kugelförmig
sind.

In Festkörpern können die Bausteine im Allgemeinen nicht wirklich frei
rotieren, sondern nehmen bevorzugte diskrete Orientierungen ein.
Falls es zwei diskrete Einstellmöglichkeiten zum Feld gibt (parallel
und antiparallel), so zeigt sich in der Rechnung (siehe Magnetismus):
\begin{equation}
  \label{eq:13.49}
  P_0 = n p \tanh \frac{p E}{\kB T} \approx \boxed{\frac{n p^2 E}{\kB T}}
\end{equation}

\minisec{Dynamische Polarisation}

Wir haben die Wechselwirkungen mit einem statischen Feld betrachtet.
Nur legen wir ein Wechselfeld, d.h.\ eine elektromagnetischen Welle
an.  Die Dynamik der Orientierungsprozesse bestimmt die Reaktion des
Systems.  Ähnlich wie bei der Magnetisierung nehmen wir eine
Relaxation ins Gleichgewicht an
\begin{equation}
  \label{eq:13.50}
  \frac{\diff P(t)}{\diff t} = - \frac{P(t) - P_0(t)}{\tau} \; .
\end{equation}
Dabei ist $\tau$ die Relaxationszeit und $P_0(t)$ der Wert, den $P$
bei instantaner Relaxation ($\tau\to0$) in einem Wechselfeld einnehmen
würde.  Bei sprunghaftem Einschalten des Feldes relaxiert die
Polarisation gemäß
\begin{equation}
  \label{eq:13.51}
  P(t) = P_0 \left[ 1 - \ee^{-t/\tau} \right] \; .
\end{equation}
Die Relaxationszeit, d.h.\ die Reorientierungszeit der Dipole ist
thermisch aktiviert (Sprung über die Potentialbarriere).  Der
einfachste Ansatz für ein periodisches Wechselfeld ist
\begin{equation*}
  E(t) = E_0 \ee^{-\ii\omega t} \; .
\end{equation*}
Die Polarisation wird dieser Anregung folgen
\begin{equation*}
  P(t) = P(\omega) \ee^{-\ii\omega t} \; ,
\end{equation*}
gleiches gilt für den Gleichgewichtswert
\begin{equation*}
  P_0(t) = P_0(0) \ee^{-\ii\omega t} \; .
\end{equation*}
Dabei bezeichnet man $P_0(0)$ als statische dipolare Suszeptibilität
($\omega=0$).

Setze diese Ansätze nun in \eqref{eq:13.50} ein und verwende
$P(\omega) = \varepsilon_0 \chi(\omega) E(t)$, dann erhält man die
dipolare Suszeptibilität
\begin{equation}
  \label{eq:13.52}
  \boxed{
    \chi_d(\omega) = \frac{\chi_d(0)}{1-\ii\omega\tau}
  }
\end{equation}
Daraus können wir die dielektrische Funktion erhalten.  Dazu müssen
wir aber noch die Beträge der Ionen und Elektronen addieren.
\begin{equation}
  \label{eq:13.53}
  \varepsilon(\omega) = 1 + \chi_d(\omega)
  + \chi_{\text{ion}}(\omega) + \chi_e(\omega) \; .
\end{equation}
Der Beitrag $\chi_d(\omega)$ der permanenten Dipole ist nur im
Mikrowellenbereich von Bedeutung.  Wir können daher $\chi_e$ und
$\chi_{\text{ion}}$ als konstant annehmen und erhalten für
Mikrowellenfrequenzen
\begin{equation}
  \label{eq:13.54}
  \varepsilon(\omega) = 1 + \chi_{\text{ion}}(\omega) + \chi_e(\omega)
  + \frac{\chi_d(0)}{1-\ii\omega\tau}
  = \varepsilon_\infty
  + \frac{\varepsilon_{\text{stat}} - \varepsilon_\infty}{1-\ii\omega\tau}
\end{equation}
Die Größen $\varepsilon_\infty$ und $\varepsilon_{\text{stat}}$ sind
wie im vorherigen Kapitel definiert.  Wir teilen die komplexe Größe
$\varepsilon(\omega)$ noch in Real- und Imaginärteil und erhalten
damit die Debye-Gleichungen
\begin{equation}
  \label{eq:13.55}
  \boxed{
    \begin{aligned}
      \varepsilon'(\omega) &= \varepsilon_\infty
      + \frac{\varepsilon_{\text{stat}} - \varepsilon_\infty}{1+\omega^2\tau^2} \\
      \varepsilon''(\omega) &=
      \frac{(\varepsilon_{\text{stat}} - \varepsilon_\infty) \omega \tau}{1+\omega^2\tau^2}
    \end{aligned}
  }
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{semilogxaxis}[axis on top,domain=1e-2:1e2,enlargelimits=false,ymin=0,ymax=1.1,width=6cm,
      xlabel={Frequenz $\omega$},ylabel={Amplitude},
      ytick={1},yticklabels={$\varepsilon_{\text{stat}}$},
      extra y ticks={.2},extra y tick labels={$\varepsilon_\infty$},
      every extra y tick/.style={yticklabel pos=right},
      samples=50,smooth]
      \draw[dashed] (axis cs:1,0) -- (axis cs:1,1.1);
      \node[draw,circle,pin={160:$\frac{\varepsilon_{\text{stat}} - \varepsilon_\infty}{2}$}] at (axis cs:1,.4) {};
      \addplot[MidnightBlue] {(1-.2)*x/(1+x^2)};
      \addplot[DarkOrange3] {.2 + (1-.2)/(1+x^2)};
      \legend{$\varepsilon'(\omega)$,$\varepsilon''(\omega)$}
    \end{semilogxaxis}
  \end{tikzpicture}
  \caption{Schematischer Verlauf der dielektrischen Funktion gegeben
    durch die Debye-Gleichungen.}
  \label{fig:26.1}
\end{figure}

\subsection{Ferroelektrizität}

Ähnlich wie sich in den ferromagnetischen Materialien eine spontane
Magnetisierung einstellt, stellt sich in pyroelektrischen Kristallen
eine spontane elektrische Polarisation ein.  Als
\acct[Ferroelektrizität]{ferroelektrisch} bezeichnet man Kristalle,
wenn die spontane Polarisation durch ein genügend starkes, der
Polarisation entgegen gerichtetes Feld umgeklappt werden kann.  Eine
spontane Polarisation tritt nur auf, wenn der Kristall eine
Vorzugsachse besitzt.  Besitzt der Kristall mehrere solcher Achsen
müssen diese durch mechanische Deformation gleichgerichtet werden.
Kristalle, die bei Deformation eine elektrische Polarisation erhalten
nennt man \acct[Piezoelektrizität]{piezoelektrisch}.  Oberhalb einer
kritischen Temperatur $T_c$ verschwindet die Polarisation und die
Kristalle werden \acct[Paraelektrizität]{paraelektrisch}.  Für $T>T_C$
gilt das Curie-Weiss-Gesetz \refstepcounter{equation}%
\begin{equation}
  \label{eq:13.57}
  \varepsilon_{\text{stat}} = \frac{C}{T-\Theta}
  \;,\quad T>T_C
\end{equation}
mit der materialspezifischen Konstante $C$ und der paraelektrischen
Curie-Temperatur $\Theta$.  Ferromagnetismus und Ferroelektrizität
haben sehr große phänomenologische Ähnlichkeit aber unterschiedliche
mikroskopische Ursachen.  Ferromagnetismus ist ein Phasenübergang
2. Ordnung, Ferroelektrizität eine Phasenübergang 1. \emph{oder} 2. Ordnung.

Man unterscheidet zwei Mechanismen.  Zum einen gibt es Übergänge, die
mit einem Ordnungs-Unordnungs-Übergang verbunden sind, d.h.\ die
Ausrichtung von vorhandenen Dipolen verschwindet, die Dipole selbst
bleiben aber erhalten.  Man findet dieses Verhalten bei
Hydrogenphosphaten wie \ch{KH2PO4} (KDP-Typ).

Bei der anderen Klasse werden Dipole am Phasenübergang durch
Verschiebung von Ionen erst erzeugt.  Wenn die auslenkende Kraft, die
auf ein Ion auf Grund des lokalen elektrischen Feldes wirkt, schneller
mit der Verschiebung ansteigt als die lineare elektrische
Rückstellkraft kommt es zur \acct{Polarisationskatastrophe}.  Die
Auslenkung bleibt aber trotzdem endlich wegen zusätzlicher
nichtlinearer Kräfte.  Materialien dieser Klasse sind Perowskite wie
\ch{BaTiO3}, \ch{SrTiO3} oder \ch{LiNbO3}.

Ferroelektrika finden Anwendungen in der Technik.  Da die
Dielektrizitätskonstante nach \eqref{eq:13.57} temperaturabhängig ist
finden sich verschiedene Anwendungsgebiete bei verschiedenen
Temperaturen.  Für Temperaturen überhalb der kritischen Temperatur
$T>T_c$ ist $\varepsilon_{\text{stat}}$ sehr groß.  Ferroelektrika
eignen sich dann als Dielektrika in Kondensatoren.  Unterhalb der
kritischen Temperatur $T<T_c$ findet man große piezoelektrische
Konstanten.  Diese Materialien eignene sich als piezoelektrische
Aktuatoren für große elektro-optische Effekte, wie
Frequenzverdopplung.

\subsection{Exzitonen}

Strahlt man Photonen mit einer Energie etwas kleiner als die Bandlücke
ein, kann es zu einer speziellen elektrisch neutralen Anregung, bei
der Elektron und Loch nicht vollständig voneinander getrennt werden,
kommen.  Auf Grund der Coulomb-Wechselwirkung ziehen sich Elektron und
Loch an und bilden eine Einheit, ein \acct{Exziton}.

Voraussetzung ist, dass beide Ladungsträger sich mit der gleichen
Gruppengeschwindigkeit bewegen.  Dies ist an den kritischen Punkten
der Elektronenbänder der Fall.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: