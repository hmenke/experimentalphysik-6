\section{Ferromagnetismus}

Ferromagnetische Materialien zeigen eine spontane Magnetisierung
$M_S$, d.h.\ die magnetischen Momente sind auch ohne äußeres Feld
ausgerichtet.  Man unterscheidet drei Grundformen:
\begin{itemize}
\item Ferromagnetismus
\item Antiferromagnetismus
\item Ferrimagnetismus
\end{itemize}
Eine Skizze der Anordnung der magnetischen Moment ist in
Abbildung~\ref{fig:16.1} zu sehen.  Dies ist jedoch nur eine
zweidimensionale Projektion.  Im Dreidimensionalen können die
Strukturen viel komplizierter sein durch Verkippungen oder Spiralen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,
    yscale=.5,xscale=.33,every path/.append style={MidnightBlue,>={Triangle[scale=.5]}},
    dot/.append style={inner sep=.5pt}]
    \begin{scope}
    \foreach \x in {1,...,6} {
      \foreach \y in {1,...,4} {
        \draw[->] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
      }
    }
    \end{scope}
    \begin{scope}[shift={(7,0)}]
    \foreach \x in {1,...,6} {
      \foreach \y in {1,...,4} {
        \ifodd\y
          \ifodd\x
            \draw[<-] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
          \else
            \draw[->] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
          \fi
        \else
          \ifodd\x
            \draw[->] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
          \else
            \draw[<-] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
          \fi
        \fi
      }
    }
    \end{scope}
    \begin{scope}[shift={(14,0)}]
    \foreach \x in {1,...,6} {
      \foreach \y in {1,...,4} {
        \ifodd\y
          \ifodd\x
            \draw[<-] (\x,\y-.25) -- node[dot] {} (\x,\y+.25);
          \else
            \draw[->] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
          \fi
        \else
          \ifodd\x
            \draw[->] (\x,\y-.4) -- node[dot] {} (\x,\y+.4);
          \else
            \draw[<-] (\x,\y-.25) -- node[dot] {} (\x,\y+.25);
          \fi
        \fi
      }
    }
    \end{scope}
  \end{tikzpicture}
  \caption{Skizze der drei Grundformen des Ferromagnetismus.  Ganz
    links der Ferromagnetismus, in der Mitte der Antiferromagnetismus
    und rechts der Ferrimagnetismus.  Für eine Erklärung siehe Text.
    Nach \textcite{hunklinger}.}
  \label{fig:16.1}
\end{figure}

Bei $T=\SI{0}{\K}$ bilden sich im Ferromagneten Domänen von Momenten
gleicher Ausrichtung aus.  Diese Domänen nennt man Weißsche Bezirke.
Von einem Bezirk zum nächsten wechselt die Ausrichtung der Momente.
Die Grenzflächen dazwischen heißen Blochwände.  Durch ein externes
Magnetfeld kann man die Änderung der Polung eines Bezirks erzwingen,
was als Barkhausen-Sprung bezeichnet wird.  Innerhalb eines Bezirks
stellt sich die Sättigungsmagnetisierung $M_S$ ein.  Schwache
Magnetfelder können eine hohe Magnetisierung hervorrufen durch
Drehung, bzw.\ Ausrichtung und Vergrößerung der
Magnetisierungsrichtung der Domänenen.

Ferromagnetismus ist eine Kristalleigenschaft und beruht auf
kollektiven Phänomenen, im Gegensatz zu Para- und Diamagnetismus wo
die magnetischen Momente völlig unabhängig voneinander sind.

Jedoch können die mikroskopischen Ursachen für das gleiche
phänomenologische Verhalten unterschiedlich sein.  Die korrekte
mikroskopische Beschreibung gestaltet sich schwierig da Einelektronen-
und Mehrelektronenaspekte berücksichtigt werden müssen, wenn auch in
allen Fällen die Austauschwechselwirkung die Ursache für die
Ausrichtung der magnetischen Momente ist.

\subsection{Klassische Molekularfeldtheorie}

In der Molekularfeldtheorie werden wechselwirkende Teilchen als freie
Teilchen im effektiven Potential eines externen Feldes behandelt.  Das
externe Feld wird dabei als konstant angesehen, womit eine lokale
Veränderung des Feldes durch die Teilchen vernachlässigt wird.  Damit
reduzieren wird das Vielteilchenproblem auf eine Einteilchenproblem im
externen Feld, in unserem Fall das Aufatom im Molekularfeld $\bm B_M$.

Wir nehmen an, dass das Molekularfeld $\bm B_M$ proportional zur
Magnetisierung ist
\begin{equation}
  \label{eq:11.28}
  \bm B_{\text{eff}} = \bm B_a + \bm B_M = \bm B_a + \lambda \mu_0 \bm M \; .
\end{equation}
Das Molekularfeld $\bm B_M$ ist kein reales Magnetfeld, sondern nur
ein formales Hilfsfeld, das die nicht-magnetischen Wechselwirkungen
des Aufatoms mit allen anderen Atomen beschreibt.  Wie bereits erwähnt
wird so ein komplexes Vielteilchenproblem auf ein Einteilchenproblem
zurückgeführt.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[xmin=0,xmax=1.5,ymin=0,ymax=1.5,width=6cm,
      xlabel={Normierte Temperatur $T/T_C$},ylabel={Normierte Magnetisierung\\ $M_S(T)/M_S(0)$},
      ylabel style={align=center}]
      \draw[MidnightBlue] (axis cs:0,1) to[out=0,in=90] (axis cs:1,0);
    \end{axis}
  \end{tikzpicture}
  \caption{Schematischer Verlauf der sponatenen Magnetisierung in
    Abhängigkeit der Temperatur.}
  \label{fig:16.2}
\end{figure}

Mit diesem Formalismus wollen wir die spontane Magnetisierung $M_S(T)$
berechnen, die ohne ein äußeres Feld auftritt.  Die Magnetisierung ist
bei tiefen Temperaturen nahezu temperaturunabhängig und erst bei $T =
T_C$ kommt es zu einem steilen Abfall.  Oberhalb von $T_C$
verschwindet die spontane Magnetisierung es liegt die paramagnetische
Phase vor.  Die Gleichung \eqref{eq:11.20} beschreibt die
Magnetisierung im paramagnetischen Fall.  Liegt die ferromagnetische
Phase vor, so ist das Molekularfeld wirksam und die magnetischen
Momente werden ausgerichtet.  Daher ersetzen wir das ursprünglich
äußere Feld in der Brillouin-Funktion in \eqref{eq:11.20} durch das
Molekularfeld $B_M = \lambda\mu_0 M_S$.  Wir erhalten also
\begin{equation}
  \label{eq:11.29}
  M_S = n g \mu_B J \mathcal B(x)
  \quad\text{mit}\quad
  x = \frac{g\mu_B J \lambda \mu_0 M_S}{\kB T} \; .
\end{equation}
Die implizite Form dieser Gleichung berücksichtigt sämtliche
Rückkopplungseffekte.  Die Gleichung besitzt nur Lösungen für
Temperaturen, die kleiner als die kritische Temperatur $T_C$ sind und
liefert den Ausdruck für die ferromagnetische Curie-Temperatur $T_C$
\begin{equation}
  \label{eq:11.30}
  T_C = \frac{n g^2 J(J+1) \mu_B^2 \mu_0 \lambda}{3 \kB} = C\lambda
\end{equation}
mit der \acct{Curie-Konstante} $C$.

Am Beispiel von Eisen werden wir nun das Molekularfeld und die
Molekularfeldkonstante berechnen.  Bei Metallen mit $3d$-Elektronen
dominieren die Spinbeiträge und die Bahndrehimpulse können
vernachlässigt werden.  Für die effektive Magnetonenzahl gilt also
\begin{equation*}
  p^2 = g^2 S(S+1) \; .
\end{equation*}
Für Eisen ist $S=1$ und die Teilchenzahldichte
$n=\SI{8.5e28}{\per\cubic\m}$.  Die Molekularfeldkonstante $\lambda$
ergibt sich damit zu $\lambda\approx\num{1000}$.  Mit einer spontanen
Magnetisierung von $M_S = \SI{1.75e6}{\A\per\m}$ folgt für das
Molekularfeld $B_M = \lambda\mu_0 M_S \approx \SI{2000}{\tesla}$.
Dabei wird nochmals deutlich, dass es sich um ein theoretisches
Hilfsfeld handelt.  Echte Magnetfelder von dieser Größenordnung sind
nicht realisierbar.  Das Dipolfeld, das am Ort des Bezugsatoms von den
benachbarten Spins hervorgerufen wird kann ebenfalls berechnet werden
\begin{equation*}
  \frac{\mu_0\mu_B}{a^3} \approx \SI{0.1}{\tesla} \; .
\end{equation*}
Man sieht, dass das Molekularfeld mit seinen \SI{2000}{\tesla} um
einiges größer ist als das Feld der benachbarten Spins mit
\SI{0.1}{\tesla}.  Die magnetischen Momente werden also durch Kräfte
ausgerichtet, die ihren Ursprung nicht in der magnetischen
Wechselwirkung zwischen den Spins haben.

Wenden wir uns nun dem Übergang in die paramagnetische Phase zu.  Wie
bereits erwähnt geht der Ferromagnet für $T>T_C$ in einen Paramagneten
über.  Allerdings verschwindet die Wechselwirkung, die in der
ferromagnetischen Phase zur Ausrichtung der Spins führt nicht
plötzlich, sondern trägt auch in der paramagnetischen Phase noch zur
Verstärkung des äußeren Feldes bei.  Für eine Beschreibung
modifizieren wir das Curie-Gesetz, indem wir einen Rückkopplungsterm
hinzufügen.  Damit geht $\mu_0 M = \chi B_a = C B_a/T$ über in
\begin{equation}
  \label{eq:11.31}
  \mu_0 M = \frac{C}{T} (B_a + B_M) = \frac{C}{T} (B_a + \lambda \mu_0 M) \; .
\end{equation}
Daraus folgt unmittelbar das für ferromagnetische Materialien in der
paramagnetischen Phase gültige \acct{Curie-Weiss-Gesetz}
\begin{equation}
  \label{eq:11.32}
  \chi_p = \frac{\mu_0 M}{B_a} = \frac{C}{T-\lambda C} = \frac{C}{T-\Theta}
\end{equation}
mit der paramagnetischen Curie-Temperatur $\Theta$.  Aus dieser Form
erwarten wir einen steilen Anstieg der Suszeptibilität, der bei
$T=\Theta$ in einer Singularität endet.  Nach \eqref{eq:11.30} sollte
$T_C = \Theta$ sein, was aber für reale Systeme nicht beobachtet
werden kann.  Für Nickel findet man zum Beispiel $T_C = \SI{630}{\K}$
und $\Theta = \SI{649}{\K}$.  Diese Abweichungen resultieren aus
Fluktuationen bei einem Phasenübergang zweiter Ordnung.

\subsection{Austauschwechselwirkung zwischen lokalisierten Elektronen}

Eine Austauschwechselwirkung zwischen lokalisierten Elektronen ist vor
allem bei ferro- und antiferromagnetischen Isolatoren wie \ch{CrBr3},
\ch{EuO} oder \ch{EuS} vorzufinden.  Wir wollen damit klären warum es
zu paralleler bzw.\ antiparalleler Ausrichtung der Spins kommt.

Wir konstruieren dazu ein einfaches Modell zweier Ionen $a$ und $b$
mit zwei Elektronen mit dem Ortsvektor $\bm r_i$ und dem Spin $S_i$.
Die Wellenfunktion der Elektronen lässt sich als Produkt aus einer
Ortswellenfuktion und einer Spinfunktion darstellen.  Abhängig von der
Symmetrie der Ortswellenfunktion sind die beiden Spins entweder
parallel oder antiparallel orientiert.  Die Beschreibung der
Ortswellenfunktion geschieht durch den Ansatz von Heitler und London
\begin{align*}
  \Psi_S &= N_S [\psi_a(\bm r_1) \psi_b(\bm r_2) + \psi_b(\bm r_1) \psi_a(\bm r_2)] \; ,\\
  \Psi_A &= N_A [\psi_a(\bm r_1) \psi_b(\bm r_2) - \psi_b(\bm r_1) \psi_a(\bm r_2)] \; .
\end{align*}
Die Indizes $S$ und $A$ stehen für symmetrisch und antisymmetrisch.
Der symmetrischen Ortswellenfunktion ordnet man den einzigen
antisymmetrischen Spinzustand, das Singulett zu, der antisymmetrischen
Ortswellenfunktion die drei Triplett-Zustände.  Seien die
Normierungskonstanten
\begin{equation*}
  N_S \approx N_A \equiv N \; ,
\end{equation*}
sodass
\begin{equation*}
  \int \Psi^* \Psi \diff V_1 \diff V_2 = 1 \; .
\end{equation*}

Wir berechnen mit diesem Ansatz die potentielle Energie des
Grundzustandes.  Voraussetzung ist, dass das Wechselwirkungspotential
$\tilde V(\bm r_1,\bm r_2)$ symmetrisch unter Austausch der Elektronen
ist, also $\tilde V(\bm r_1,\bm r_2) = \tilde V(\bm r_2,\bm r_1)$.
Damit ist die potentielle Energie für den symmetrischen und den
antisymmetrischen Zustand ähnlich
\begin{multline*}
  U_{S,A}
  = 2 N^2 \int \psi_a^*(\bm r_1) \psi_b^*(\bm r_2) \tilde V(\bm r_1,\bm r_2) \psi_a(\bm r_1) \psi_b(\bm r_2) \diff V_1 \diff V_2 \\
  \pm 2 N^2 \int \psi_a^*(\bm r_1) \psi_b^*(\bm r_2) \tilde V(\bm r_1,\bm r_2) \psi_b(\bm r_1) \psi_a(\bm r_2) \diff V_1 \diff V_2 \; .
\end{multline*}
Das positive Vorzeichen gilt für den symmetrischen Zustand, das
negative für den antisymmetrischen.  Der Beitrag zur kinetischen
Energie ist bei den beiden Zuständen kaum zu unterscheiden.  Somit ist
die Differenz der Energieeigenwerte wesentlich durch die Differenz
$U_S - U_A$ gegeben.  Wir definieren damit die Austauschkonstante $J$
\begin{equation}
  \label{eq:11.33}
  J = E_S - E_A \approx 4 N^2 \int \psi_a^*(\bm r_1) \psi_b^*(\bm r_2) \tilde V(\bm r_1,\bm r_2) \psi_b(\bm r_1) \psi_a(\bm r_2) \diff V_1 \diff V_2 \; .
\end{equation}
Das Vorzeichen von $J$ hängt von der Form der Wellenfunktion und des
Potentials ab.  Für $J > 0$ findet eine parallele Ausrichtung der
Spins statt, für $J < 0$ eine antiparallele.  Das Potential $\tilde
V(\bm r_1,\bm r_2)$ kann in drei Teile zerlegt werden
\begin{equation*}
  \tilde V(\bm r_1,\bm r_2) = \tilde V_{\text{i}}(\bm r_1) + \tilde V_{\text{i}}(\bm r_2)
  + \tilde V_{\text{ee}}(\bm r_1,\bm r_1)
\end{equation*}
wobei
\begin{equation*}
  \tilde V_{\text{ee}}(\bm r_1,\bm r_1) = \frac{e^2}{4\pi\varepsilon_0 |\bm r_1 - \bm r_2|} \; .
\end{equation*}
Die ersten zwei Summanden beschreiben die Wechselwirkung der
Elektronen mit den Ionen, der dritte Summand die Wechselwirkung der
Elektronen untereinander.  Der Coulomb-Term $\tilde V_{\text{ee}}$
liefert einen positiven Beitrag zur Austauschkonstante $J$, d.h.\ die
Coulomb-Wechselwirkung zwischen den Elektronen versucht die Spins
parallel auszurichten.  Die Wechselwirkung der Elektronen mit den
Ionen ist attraktiv und bewirkt eine negativen Beitrag zur
Austauschkonstante $J$.  Das Vorzeichen von $J$ hängt also von der
relativen Größe aller Beiträge ab.  Auf Grund des Pauli-Verbots sind
die Ortswellenfunktionen in eindeutiger Weise mit dem
Spinwellenfunktionen verknüpft.  Die auftretenden Energien können also
auch mit Spinzuständen ausgedrückt werden.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: