\chapter{Magnetische Resonanz}

\section{Einführung}

Die magnetische Resonanzspektroskopie benützt die Spins und
magnetischen Momente von Elektronen und Kernen als Sonden zum Studium
von elektronischen Strukturen, Defekten in Festkörpern, dynamischen
Prozessen und chemischen Strukturen von Molekülen.

In diesem Kapitel werden wir verschiedene Techniken zur
Resonanzspektroskopie kennen lernen.  Grob gesagt werden wir die
Folgenden betrachten:
\begin{itemize}
\item Kernspinresonanz (NMR)
\item Kernquadrupolresonanz (NQR)
\item Elektronenspinresonanz (ESR)
\item Ferromagnetische Resonanz (FMR)
\end{itemize}
Die Grundideen sind sehr einfach, moderne NMR-Methoden sind jedoch in
der Zwischenzeit sehr fortgeschritten und sehr speziell.  Für die
beiden einfacheren Verfahren, ESR und NMR, können wir hier in wenigen
Sätzen die Grundprinzipien erläutern.

Damit Elektronenspinresonanz möglich ist benötigen wir mindestens
einen ungepaarter Spin, bzw.\ ein ungepaartes magnetisches Moment in
der Probe.  Allgemeiner könnte man sagen, wir brauchen $S\neq0$.
Radikale haben beispielsweise $S=1/2$, Triplettzustände haben $S=1$,
Übergangsmetallionen haben $S=1/2\ldots5/2$ und seltene Erden haben
$S=1/2\ldots7/2$.

Die Kernspinresonanz benötigt einen Kernspin $I\neq0$, wie man ihn
beispielsweise im Wasserstoff-Isotop \ch{^1_1H} mit $I=1/2$ findet.
Es hat eine natürliche Häufigkeit von \SI{99.89}{\percent}.  Das
Kohlenstoff-Isotop \ch{^{13}_{6}C} mit $I=1/2$ hat eine Häufigkeit von
\SI{1.1}{\percent}.  Der Gesamtdrehimpuls eines Atomkerns ist die
Summe der Eigendrehimpulse der Nukleonen und ihrer Bahndrehimpulse.
Diesen Gesamtdrehimpuls nennt man Kernspin $I$.  Die meisten Atomkerne
besitzen ein oder mehrere stabile Isotope mit $I\neq0$.  Der Kernspin
kann halb- oder ganzzahlig sein, je nachdem ist der Kern ein Fermion
oder ein Boson.

\minisec{Fundamentale Eigenschaften des Drehimpulses}

Bevor wir mit den Resonanzphänomenen beginnen können müssen wir
nochmals einige Eigenschaften des Drehimpulses rekapitulieren.

Für ein Elektron mit Spin $\bm S$, Bahndrehimpuls $\bm L$ und
Gesamtdrehimpuls $\bm J = \bm L + \bm S$ gilt
\begin{equation*}
    |\hbar\bm S| = \sqrt{S(S+1)} \hbar \; , \quad
    |\hbar\bm J| = \sqrt{J(J+1)} \hbar \; .
\end{equation*}
Ähnliches gilt für den Kernspin $\bm I$
\begin{equation*}
  |\hbar\bm I| = \sqrt{I(I+1)} \hbar \; .
\end{equation*}

Mit dem Spin ist immer ein magnetisches Dipolmoment $\mu$ verknüpft.
Bei Elektronen ist dieses Moment antiparallel zum Drehimpuls wie durch
das Minus-Vorzeichen deutlich gemacht wird
\begin{align*}
  \bm\mu_S &= - g_S \mu_B \bm S \; , \\
  \bm\mu_J &= - g_J \mu_B \bm J \; .
\end{align*}
Dabei ist $\mu_B$ das Bohrsche Magneton
\begin{equation*}
  \mu_B = \frac{e\hbar}{2 m_e} \; .
\end{equation*}
Das magnetische Moment des Kerns ist parallel zum Kernspin
ausgerichtet, daher das positive Vorzeichen
\begin{equation*}
  \bm\mu_I = g_I \mu_K \bm I \; .
\end{equation*}
Hier ist $\mu_K$ das Kernmagneton
\begin{equation*}
  \mu_K = \frac{e\hbar}{2 m_{\text{prot}}} = \frac{\mu_B m_e}{m_{\text{prot}}} \; .
\end{equation*}

Wir sind noch nicht auf die Vorfaktoren $g$ eingegangen.  Diese
bezeichnet man als Landé-Faktor.  Dieser berechnet sich allgemein nach
\begin{equation*}
    g_J = 1 + \frac{J(J+1)+S(S+1)-L(L+1)}{2J(J+1)} \; .
\end{equation*}
Die relativistische Quantentheorie sieht für das Elektron einen
Landé-Faktor von $g_S = 2$ vor.  Messungen haben jedoch $g_S =
\num{2.002}$ ergeben.  Die Kern-$g$-Faktoren liegen im Bereich
$\num{-4.255} \leq g_I \leq \num{5.95}$.

Wählt man die $z$-Richtung als Vorzugsrichtung, so gilt für die
Drehimpulse in diese Richtung
\begin{equation*}
  J_z \hbar = m_J \hbar
  \quad\text{mit}\quad
  m_J = -J,-J+1,\ldots,J \; .
\end{equation*}

\section{Kernspinresonanz}

Atomkerne haben ein magnetisches Moment
\begin{equation}
  \label{eq:12.1}
  \bm\mu = g_I \mu_K \bm I = \gamma \hbar \bm I
\end{equation}
mit dem gyromagnetischen Verhältnis
\begin{equation}
  \label{eq:12.2}
  \gamma = \frac{g_I \mu_K}{\hbar} \; .
\end{equation}
Das Kernmagneton hat den Wert $\mu_K = \SI{0.505e-26}{\A\square\m}$.
Im homogenen Magnetfeld $\bm B_0 = (0,0,B_0)$ spalten die
Energieniveaus eines Systems mit Kernspin in einzelne quantisierte
Niveaus auf.  Die Wechselwirkungsenergie mit einem Magnetfeld ist
gegeben durch
\begin{equation*}
  U = - \bm\mu \cdot \bm B_0 \; .
\end{equation*}
Wählt man $\bm B_0 = B_0 \hat{\bm z}$, so gilt
\begin{equation*}
  U = - \mu_z B_0 = - \gamma \hbar B_0 I_z \; .
\end{equation*}
Wir wissen, dass $I_z$ die Quantenzahlen $m_I$ annehmen kann.  Setzen
wir dies noch in die Wechselwirkungsenergie ein, so gilt
\begin{equation}
  \label{eq:12.3}
  \boxed{ U_{m_I} = - m_I g_I \mu_K B_0 } \; .
\end{equation}
In Abbildung~\ref{fig:19.1} findet sich ein Beispiel für die
Aufspaltung der Energieniveaus von \ch{^1H}-Kernen mit $I = 1/2$ im
Magnetfeld.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-1.5) -- (0,1.5) node[left] {$E$};
    \draw[->] (0,0) -- (3,0) node[right] {$B_0$};
    \draw (0,0) -- (2,1) coordinate (m1) node[right] {$m_I = -1/2$};
    \draw (0,0) -- (2,-1) coordinate (m2) node[right] {$m_I = +1/2$};
    \draw[<->,DarkOrange3] (m1) -- (m2);
  \end{tikzpicture}
  \caption{Aufspaltung der Energie für $I=1/2$ von z.B.\
    \ch{^1H}-Kernen im Magnetfeld.}
  \label{fig:19.1}
\end{figure}

Wir können die Energie, die wir für einen Übergang zwischen den zwei
Aufspaltungen benötigen mit Hilfe der Einstein-Planck-Beziehung durch
eine Frequenz $\omega_0$ ausdrücken.
\begin{align}
  \hbar \omega_0 &= \gamma \hbar B_0 = g_I \mu_K B_0 \notag \\
  \label{eq:12.4}
  \omega_0 &= \gamma B_0
\end{align}
Daraus ergibt sich für Protonen in einem Feld von $B_0 = \SI{1}{\tesla}$
\begin{equation*}
  \nu = \SI{42.58}{\MHz} \; .
\end{equation*}

Die NMR benützt magnetische Dipolübergänge zwischen den in einem
Magnetfeld aufgespalteten Energieniveaus. NMR wird vorallen an
Protonen, das heißt Wasserstoff-Kernen (\ch{^1H}) durchgeführt.
Wasserstoff \ch{^1H} kommt in praktisch allen organischen Molekülen
vor und ist in chemisch relevanten Gruppen wie \ch{CH3}, \ch{CH2} oder
\ch{COOH} anwesend.  Die \ch{OH}-Gruppe ist Strukturbestandteil vieler
Moleküle.

Für die NMR benötigt man hauptsächlich ein starkes, homogenes
Magnetfeld $B_0$ (bis \SI{22.3}{\tesla}) zur Ausrichtung der
magnetischen Momente und eine sehr monochromatische elektromagnetische
Strahlung bei der Frequenz $\hbar\omega_0 = g_I \mu_K B_0$.  Deshalb
gibt man üblicherweise die Probe in ein Reagenzglas was in eine Spule
eingeführt wird.  Spule und Reagenzglas befinden sich im homogenen
Feld $B_0$.  Durch die Spule wird nun das Wechselfeld $\bm B_1(t)$
angelegt, siehe Abbildung~\ref{fig:19.2}.
\begin{equation}
  \label{eq:12.5}
  \bm B_1(t) = \bm e_x 2 B_1 \cos\omega_0t \; .
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[,x={(1cm,0cm)},y={(-.5cm,-.5cm)},z={(0cm,1cm)}]
      \draw[->] (0,0) -- (1,0,0) node[below] {$\bm e_x$};
      \draw[->] (0,0) -- (0,1,0) node[below] {$\bm e_y$};
      \draw[->] (0,0) -- (0,0,1) node[above] {$\bm e_z$};
    \end{scope}
    \begin{scope}[shift={(4,0)}]
      \draw (-.5,1.1) -- (-.5,0) arc(180:360:.5) -- (.5,1.1);
      \foreach \i in {.8,.6,...,0} {
        \draw[DarkOrange3] (-.5,\i) arc (180-35:360+35:.6 and .1);
      }
      \draw[DarkOrange3] (-.5,1) arc (180-35:270:.6 and .1) -| ++(1,-.3) node[dot] {};
      \draw[DarkOrange3] (-.5,-.2) arc (180-35:270:.6 and .1) -| ++(1,.3) node[dot] {};
      \draw[DarkOrange3,<->] (-1,0) -- node[left] {$\bm B_1(t)$} (-1,.8);
      \draw[->] (-.5,-1) -- (1,-1) node[right] {$B_0$};
    \end{scope}
  \end{tikzpicture}
  \caption{Prinzip-Schema zur Messung der Kernspin-Resonanz.}
  \label{fig:19.2}
\end{figure}

Die Spule dient gleichzeitig als Sender und Empfänger.  Zunächst wird der Übergang bei
\begin{equation}
  \label{eq:12.6}
  f_0 = \frac{g_I \mu_K}{h} B_0
\end{equation}
angeregt.  Für Protonen ist $f_0 = \SI{42.576}{\MHz\per\tesla}$. Für
ein Magnetfeld mit $B_0 = \SI{22.3}{\tesla}$ erwarten wir als Resonanz
bei $f_0 = \SI{950}{\MHz}$.  Um die Messgenauigkeit zu erhöhen
benötigen wir einen Leistungsverstärker bei $f_0$ (\SI{10}{\watt} bis
\SI{1}{\kilo\watt}) und ein hochempfindliches Nachweissystem für sehr
kleine in der Spule induzierte Spannungen
\begin{equation*}
  U_{\text{ind}}(t) = U_{\text{ind}} \cos(2\pi f_0 t+\varphi) \; .
\end{equation*}
Die für genaue Messungen erforderliche Homogenität von $B_0$ sollte
bei $\Delta B_0/B_0 \leq \num{e-9}$ liegen für ein Volumen von
\SI{35}{\micro\litre} ($\varnothing = \SI{1}{\mm}$).

\subsection{Eindimensionale FFT-NMR}

In der NMR messen wir nicht die Präzession eines einzelnen Kernspin,
sondern an einer Probe mit einer großen Zahl von (identischen)
Molekülen.  Was wir also aufnehmen ist des Ensemble-Mittel des
Kernspins.  Ein typisches Probenvolumen ist von der Größenordnung
\SI{35}{\micro\l} (\SI{3.5e-8}{\cubic\m}).  Bei einer Konzentration
von z.B.\ \SI{1}{\milli\mol\per\l} hat man bei diesem Volumen eine
Stoffmenge von \SI{35}{\nano\mol} vorliegen, d.h.\ \num{2.1e16}
Moleküle in der Probe.

% \begin{figure}[tb]
%   \centering
%   \begin{tikzpicture}[gfx]
%     \begin{scope}
%       \draw (-.5,1.1) -- (-.5,0) arc(180:360:.5) -- (.5,1.1) arc(0:180:.05) -- (.4,0) arc(0:-180:.4) -- (-.4,1.1) arc(0:180:.05);
%       \draw[dashed] (-.4,.8) -- (.4,.8);
%       \draw[<->] (-.4,-.7) -- node[below] {\SI{3}{\mm}} (.4,-.7);
%       \draw[<->] (.7,-.4) -- node[right] {\SI{5}{\mm}} (.7,.8);
%     \end{scope}
%   \end{tikzpicture}
%   \caption{Skizze eines typischen Probenvolumens.}
%   \label{fig:19.3}
% \end{figure}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \draw[->] (0,0) -- (0,3) node[right] {$\bm B_0$};
      \draw[MidnightBlue,->] (0,1) -- (0,2) node[right] {$\bm M = n \bm \mu$};
    \end{scope}
    \draw[->] (2,1.5) --
    node[above] {$\pi/2$-Puls}
    node[below,text width=3cm,align=center] {$\bm M$ klappt in $x,y$-Ebene und präzediert}
    (5,1.5);
    \begin{scope}[shift={(8,1.5)},x={(1cm,0cm)},y={(0cm,1cm)},z={(-.5cm,-.5cm)}]
      \draw[MidnightBlue,->] (0,0) -- (1.5,0) node[right] {$M$};
      \draw[MidnightBlue,dotted] plot[samples=100,smooth,domain=-2:2] ({1.5*exp(-.5*(\x+2))*cos(2*pi*\x r)},0,{1.5*exp(-.5*(\x+2))*sin(2*pi*\x r)});
      \draw[DarkOrange3] plot[samples=100,smooth,domain=-2:2] (\x,{cos(2*pi*\x r)},{sin(2*pi*\x r)});
      \node[DarkOrange3] at (1.2,1.5) {Empfängerspule};
    \end{scope}
  \end{tikzpicture}
  \caption{Durch einen $\pi/2$-Puls wird $\bm M$ in die $x,y$-Ebene
    umgeklappt und präzediert um die $\bm B_0$-Achse, bzw.\ $z$-Achse.
    Die rotierende Magnetisierung induziert einen Strom in der
    Empfängerspule.  Die Magnetisierung relaxiert mit der
    Relaxationszeit $T_2$ zurück auf die $\bm B_0$-Achse.}
  \label{fig:19.4}
\end{figure}

Nach der Anregung des Kernspinsystems durch einen kurzen
Hochfrequenzimpuls hat man in der Probe ein präzedierendes
magnetisches Moment
\begin{equation*}
  M_x(t) = M_0 \cos\omega_0 t \; \ee^{-t/T_2} \; .
\end{equation*}
Die Präzessionsfrequenz $\omega_0$ ist durch die Betriebsfrequenz des
Spektrometers gegeben
\begin{equation*}
  f_0 = \omega_0/(2\pi) \; .
\end{equation*}
Die Betriebsfrequenz ist in der Regel ca.\ $f_0=\SI{950}{\MHz}$.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-1.5) -- (0,1.5) node[left] {$M_x(t)$};
    \draw[->] (0,0) -- (3,0) node[right] {$t$};
    \draw[MidnightBlue] plot[samples=51,smooth,domain=0:2.8] (\x,{cos(2*pi*\x r)*exp(-\x)});
    \draw[MidnightBlue,dotted] plot[samples=51,smooth,domain=0:2.8] (\x,{exp(-\x)});
  \end{tikzpicture}
  \caption{Die Magnetisierung präzediert in der $x,y$-Ebene, relaxiert
    jedoch wieder zurück in ihre Ausgangsstellung.  Daher ist die
    Oszillation mit einem exponentiellen Abfall eingehüllt.}
  \label{fig:19.5}
\end{figure}

In der Empfängerspule wird durch das präzedierende magnetische Moment
eine Spannung $U(t)$ induziert, wie auch in Abbildung~\ref{fig:19.4}
angedeutet
\begin{equation}
  \label{eq:12.7}
  \boxed{ U(t) = U_0 \cos\omega_0 t\;\ee^{-t/T_2} }
\end{equation}
Mit der Magnetisierung relaxiert natürlich auch die
Induktionsspannung.  Dieses Signal wird als FID (Free Induction Decay)
bezeichnet.  Das Frequenzspektrum erhält man durch eine
Fourier-Transformation von $U(t)$.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,0) -- (0,1.5) node[left] {FFT};
    \draw[->] (0,0) -- (3,0) node[right] {$t$};
    \draw (2,2pt) -- (2,-2pt) node[below] {$f_0$};
    \draw[MidnightBlue] plot[samples=51,smooth,domain=0:2.8] (\x,{exp(-20*(\x-2)^2)});
    \node[above right] at (2,1) {$\Delta f = 1/(2\pi T_2)$};
  \end{tikzpicture}
  \caption{Die Fourier-Transformation des FID-Signals macht die
    Resonanzfrequenz $f_0$ eindeutig sichtbar.  Das Signal ist um die
    Resonanzfrequenz herum verbreitert mit der Linienbreite $\Delta
    f$.}
  \label{fig:19.6}
\end{figure}

Für kleine Moleküle sind die $T_2$-Zeiten sehr lange, die
Spektrallinien sind daher sehr schmal.  Für eine Relaxationszeit von
$T_2\sim\SI{1}{\s}$ ist zum Beispiel $\Delta f \sim 1/(2 \pi T_2) \sim
\SI{160}{\MHz}$.  Bei einer Spektrometerfrequenz von $f_0 =
\SI{400}{\MHz}$ hat man dann eine Auflösung von $\Delta f/f_0 \approx
\num{4e-10}$.  Die NMR ist also eine hochauflösende Spektroskopie.
Kleinste Unterschiede der Kerne können quantitativ bestimmt werden.
Die NMR ist für die chemische Analyse von Molekülen von großer
Bedeutung.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: