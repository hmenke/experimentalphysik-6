\chapter{Nanostrukturen}

\section{Einleitung}

In niederdimensionalen Systemen ist die Ausdehnung des Systems
entweder in einer, in zwei oder in drei zueinander orthogonalen
Richtungen eingeschränkt.  Man unterscheidet daher zwischen drei
verschiedenen Gruppen von Strukturen.  Ist das System nur in einer
Richtung eingeschränkt, so spricht man von Quantenfilmen
(2D-Struktur).  Wir haben bereits einige Vertreter dieser Gruppe in
Form von Halbleiter-Heterostrukturen, wie z.B.\
\ch{AlGaAs}/\ch{GaAs}/\ch{AlGaAs}, kennen gelernt.  Ein weiterer
Vertreter dieser Gruppe sind die Graphene.  Ein in zwei Dimensionen
eingschränktes System nennt man Quantendraht (1D-Struktur).
Quantendrähte sind z.B.\ Halbleiter-Heterostrukturen, Nanoröhrchen aus
Kohlenstoff oder leitfähige Polymere.  Ist ein System in drei
Dimensionen eingschränkt bleibt nur noch ein eniziger Punkt im Raum
übrig an dem es sich befinden kann.  Diese nulldimensionalen Systeme
nennt man Quantenpunkte.  In Halbleiter-Heterostrukturen entstehen
\enquote{Quantum dots} entweder selbstorganisiert oder chemisch
synthetisiert.  Quantenpunkte können auch in der Form von metallischen
Nanoteilchen auftreten.

Im Folgenden beschränken wir uns auf Nanostrukturen, die aus
eingeschränkten periodischen Festkörpern erzeugt werden.

\minisec{Herstellungstechniken}

Die Herstellung von Nanostrukturen gliedert sich in zwei Kategorien.
Beim Top-down-approach dienen große Strukturen als Ausgangspunkt.  Mit
Hilfe von lithographischen Techniken und Ätztechniken werden im
Nanomaßstab Strukturen erzeugt.  Damit sind Strukturgrößen im Bereich
von \SI{30}{\nm} bis \SI{50}{\nm} erzeilbar.  Der Bottom-up-approach
nutzt Wachstum und Selbstorganisation aus, um Nanostrukturen aus
atomaren und molekularen Vorläuferprodukten aufzubauen.  Die
Strukturgrößen liegen im Bereich von \SI{50}{\nm}.

Eine Herausforderung ist die Verbindung beider Techniken.  Dies
erlaubt die Herstellung von komplexen Systemen auf allen Längenskalen.

Eine Reduktion der Dimensionalität führt natürlich zu einer Änderung
der physikalischen Eigenschaften der Systeme.  Dies beinhaltet
optische, magnetische, elektrische und thermische Eigenschaften.

\minisec{Oberflächeneffekte}

Wir bilden das Verhältnis der Zahl der Atome an der Oberfläche zu der
im Volumen, z.B.\ für eine Kugel mit Radius $r$ und Atomabstand $a$
\begin{equation}
  \label{eq:14.1}
  \frac{N_0}{N_V} = \frac{4\pi r^2}{a^2} {3 a^3}{4\pi r^3} = \frac{3 a}{r}
\end{equation}
Bei einem Radius von $r = 6 a \sim \SI{1}{\nm}$ ist bereits die Hälfte
aller Atome an der Oberfläche.  Eine mögliche Anwendung ist die
Gasspeicherung, dabei wird ein Gasmolekül an der Oberfläche
absorbiert.  Eine andere Anwendung ist die Katalyse.  Die Oberfläche
hat einen drastischen Einfluss auf die Stabilität der Nanoteilchen da
dort die Bindungsenergie reduziert ist.  Die Schmelztemperatur der
Nanoteilchen ist geringer.

Auf Langenskalen von Nanometern sind Quantisierungseffekte relevant.
Fundamentale elektronische und vibronische Anregungen sind quantisiert
und prägen damit die wichtigsten Eigenschaften von nanostrukturierten
Materialien.  Ein typischer Größenbereich ist \SI{1}{\nm} bis
\SI{100}{\nm}.

\minisec{Quantenbeschränkung}

Nach der Heisenbergschen Unschärferelation gilt
\begin{equation*}
  \Delta p_x \approx \frac{\hbar}{\Delta x} \; .
\end{equation*}
Die Beschränkung in $x$-Richtung verleiht dem Teilchen eine
zusätzliche Energie
\begin{equation*}
  E_{\text{Confinement}} = \frac{(\Delta p_x)^2}{2 m}
  \approx \frac{\hbar^2}{2 m (\Delta x)^2} \; .
\end{equation*}
Diese Confinement-Energie wird signifikant, wenn sie vergleichbar mit
der kinetischen Energie des Teilchens auf Grund seiner thermischen
Bewegung in $x$-Richtung wird.
\begin{equation*}
  E_{\text{Confinement}} \approx \frac{\hbar^2}{2 m (\Delta x)^2}
  > \frac12 \kB T \; .
\end{equation*}
Folglich sind Quantengrößeneffekte wichtig
\begin{align*}
  \Delta x &\leq \sqrt{\frac{\hbar^2}{m\kB t}} \; , \\
  \Delta x &\leq \frac{h}{p_x} = \lambda_{\text{de Broglie}} \; .
\end{align*}

\section{Abbildungstechnik}

Es gibt zwei Gruppen von Abbildungstechniken.  Zum einen gibt es die
\acct{Fokaltechniken} bei denen Sondenteilchen mit Linsen auf die
Probe fokussiert werden.  Die optische Auflösung ist entweder durch
die Heisenbergsche Unschärferelation oder äquivalent durch die Beugung
begrenzt.  Es gilt für die Auflösung $d$
\begin{equation*}
  d = \frac{\lambda}{2\sin\theta}
\end{equation*}
mit der numerischen Apertur $\sin\theta$ und der Wellenlänge $\lambda$
des verwendeten Teilchens.

Zur anderen Gruppe gehören die \acct{Rastertechniken}. Bei der
Rastersondentechnik wird eine winzige Sonde ganz nahe an die Probe
gebracht und damit die Oberfläche abgetastet.  Die Auflösung wird
durch die effektive Reichweite der Wechselwirkung zwischen der Sonde
und der zu untersuchenden Struktur bestimmt.  Zusätzlich zur Abbildung
liefern Raster- und Fokaltechniken Informationen über elektrische,
vibronische, magnetische und optische Eigenschaften.

Eine wichtige Rastertechnik ist die Elektronenmikroskopie, von der es
verschiedene Realisierungen gibt.  Bei der
Transmissionselektronenmikroskopie (TEM) durchdringt ein
Elektronenstrahl die Probe und wird dahinter auf eine Detektorplatte
fokussiert.  Um eine Näherung für die Auflösungsgrenze zu ermitteln
beginnen wir mit Abbes Gesetz
\begin{equation*}
  d = \frac{\lambda}{2 \sin\theta} \;.
\end{equation*}
Für die Wellenlänge setzen wir die de-Broglie-Wellenlänge $\lambda = h /
(m_e v)$ des Elektrons ein.  Die Geschwindigkeit $v$ des Elektrons ist als
Funktion der Beschleunigungsspannung $V$ der Elektronenquelle durch den
Energieerhaltungssatz $q_e V = m_e v^2 / 2$ gegeben.  Damit erhalten wir
$\lambda = h/\sqrt{2 m_e q_e V} \approx \SI{1.23}{\nm}/\sqrt{V}$.  Somit
gilt
\begin{equation*}
    d \approx \frac{\SI{0.6}{\nm}}{\sin\theta\sqrt{V}} \;.
\end{equation*}
Linsenfehler begrenzen die Auflösung praktisch zu
$d\sim\SI{0.1}{\nm}$.

Die Rasterelektronenmikroskopie (SEM) unterscheidet sich von der TEM
dahingehend, dass der Elektronenstrahl die Probe nicht durchdringen
muss.  Die Energie der Elektronenstrahlen sind im Bereich von
\SI{100}{\V} bis \SI{100}{\kV}.  Es erfolgt die Detektion der
rückgestreuten Elektronen und der Sekundärelektronen.  Die Auflösung
ist dabei \SI{1}{\nm}.

Für die optische Spektroskopie verwendet man ein herkömmliches
Mikroskop.  Dieses arbeitet im Bereich sichtbaren Lichts weshalb die
Auflösung bei \SI{200}{\nm} bis \SI{400}{\nm} liegt.  Einzelne
Nanostrukturen können anhand verschiedener Effekte untersucht werden,
wie Lichtstreuung, Absorption oder Lumineszenz.  Voraussetzung ist,
dass man ein einzelnes Objekt im Gesichtsfeld des Mikroskops hat.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End:
