% Gleichungsnummerierung korrigieren
\refstepcounter{equation}

\section{Elektronen im Magnetfeld}

Die Fermi"=Flächen sind Flächen konstanter Energie im $k$-Raum und
trennen für $T=0$ die besetzten von den unbesetzten Zuständen.  Die
experimentelle Bestimmung der Fermi"=Flächen (also des Elektronengases)
wird vor allem mit Messungen in Magnetfeldern durchgeführt. Hier
werden verschiedene Effekte ausgenutzt, zum Beispiel die
Zyklotronresonanz, der De-Haas"=van-Alphén"=Effekt, der
Shubmikov"=de-Haas"=Effekt oder der Magnetowiderstand.

\subsection{Zyklotronresonanz}

Die Probe wird im statischen Magnetfeld mit Mikrowellen bestrahlt. Die
Frequenz der Mikrowelle wird durchgestimmt und es tritt ein
Absorptionsmaximum bei der Zyklotronfrequenz in Erscheinung.

Für Elektronen im Festkörper unter Einfluss eines externen Feldes
gelten die Gleichungen \eqref{eq:8.15} und \eqref{eq:8.17}.
\begin{align*}
  \dot{\bm r} &= \bm v_m(\bm k) = \frac{1}{\hbar} \nabla_{\bm k} E_m(\bm k)
  = \frac{1}{\hbar} \frac{\partial E_m(\bm k)}{\partial\bm k}, \tag{8.15} \\
  \hbar \dot{\bm k} &= \bm F = - e [\bm E(\bm r,t) + \bm v_m(k) \times \bm B(\bm r,t)]. \tag{8.17}
\end{align*}
Setzt man \eqref{eq:8.15} in \eqref{eq:8.17} ein erhält man
\begin{equation}
  \label{eq:8.37}
  \diff\bm k = - \frac{e}{\hbar^2} [\nabla_k E(k) \times \bm B ] \diff t.
\end{equation}
Die Bedeutung dieser Gleichungen ist, dass sich Elektronen im
Magnetfeld auf Flächen konstanter Energie bewegen, denn $\diff\bm k$
steht auf Grund des Kreuzprodukts senkrecht auf $\nabla_k E$ und somit
ist $\diff\bm k \cdot \nabla_{\bm k} E = 0$.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \coordinate (O) at (0,0);
    \coordinate (k) at ({1*cos(220)},{2*sin(220)});
    \coordinate (kc) at (k -| O);
    \shadedraw[ball color=white] (O) ellipse (1 and 2);
    \draw[->] (0,0) -- (200:2) node[left] {$x$};
    \draw[->] (0,0) -- (1.5,0) node[right] {$y$};
    \draw[->] (0,0) -- (0,2.5) node[right] {$z$};
    \draw[densely dotted] (O) ellipse (1 and .3);
    \draw[fill=MidnightBlue!50,opacity=.3] let \p1 = (kc), \p2 = (k), \n{r} = {veclen(\x1,\x2)} in
    (\p1) ellipse (1.5*\n{r} and .4*\n{r});
    \draw[fill=MidnightBlue,opacity=.3] let \p1 = (kc), \p2 = (k), \n{r} = {veclen(\x1,\x2)} in
    (\p1) ellipse (\n{r} and .3*\n{r})
    (kc) +({\n{r}*cos(240)},{.3*\n{r}*sin(240)}) coordinate (kn);
    \draw[DarkOrange3,->] (O) -- node[right] {$\bm B$} ++(90:1.5);
    \draw[DarkOrange3,->] (O) -- node[right] {$\bm k$} (kn);
    \draw[DarkOrange3,->] (kn) -- +(210:.7) node[below] {$(\nabla E)_\perp$};
    \begin{scope}[shift={(5,0)},x={(-17:1)},y={(215:1)},
      mypin/.style={inner sep=0pt,pin={#1}}
      ]
      \draw[fill=MidnightBlue!50,opacity=.3] (0,0) circle (2);
      \draw[fill=MidnightBlue,opacity=.3] (0,0) circle (1);
      \draw[dotted] (0,0) -- (0,2.5);
      \draw[->] (0,1) -- (0,2) node[below] {$\diff\bm k_\perp$};
      \draw[->] (0,1) -- node[below] {$\diff\bm k$} (1,1);
      \node[mypin={below right:$\diff S=\oint \diff k_\perp\ |\diff\bm k|$}] at (1.2,.5) {};
      \node[mypin={[pin distance=1cm]above right:$E$}] at (0,-1) {};
      \node[mypin={[pin distance=1cm]above right:$E + \diff E$}] at (0,-2) {};
      \node[mypin={[pin distance=1.2cm]above left:$S$}] at (-.5,.5) {};
    \end{scope}
  \end{tikzpicture}
  \caption{Im Magnetfeld bewegen sich Elektronen auf den Fermi"=Flächen.
    Links: Umlaufbahnen im Falle, dass die Fermi"=Fläche ein
    Rotationsellipsoid ist.  Rechts: Bestimmung von $\diff\bm k$ und
    $\diff\bm k_\perp$ in der Bahnebene $S$.  Nach \textcite{hunklinger}.}
  \label{fig:3.1}
\end{figure}

Die Trajektorien der Elektronen mit der Fermi"=Energie $E_F$ liegen auf
der Fermi"=Fläche. In vielen Fällen ist dies auf Grund der Symmetrie
der Fermi"=Flächen längs einer geschlossenen Bahn.  In
Abbildung~\ref{fig:3.1} verläuft die Bahn orthogonal zum Magnetfeld.
Das Kreuzprodukt in \eqref{eq:8.37} führt dazu, dass nur die
Komponente von $\nabla_{\bm k} E$ senkrecht zum Magnetfeld einen
Beitrag liefert.  Es gilt also
\begin{equation*}
  |\nabla_{\bm k} E(k) \times \bm B|
  = B \bigl(\nabla_{\bm k} E(\bm k)\bigr)_{\perp}
  \equiv B \frac{\diff E}{\diff k_\perp}.
\end{equation*}
Durch Integration in \eqref{eq:8.37} erhält man die Umlaufzeit $\tilde
T$, die ein Elektron für einen Bahnumlauf benötigt.
\begin{equation*}
  \tilde T
  = \oint \diff t
  = \frac{\hbar^2}{e B} \oint \frac{\lvert\diff k\rvert}{\lvert\diff E/\diff k_\perp\rvert}
  = \frac{\hbar^2}{e B} \oint \frac{\diff k_\perp}{\diff E} \lvert\diff k\rvert .
\end{equation*}
Für die Querschnittsfläche $S$ im $k$-Raum gilt
\begin{equation*}
  \diff S = \oint \diff k_\perp \; \lvert\diff k\rvert.
\end{equation*}
Einsetzen liefert die Umlaufzeit mit
\begin{equation}
  \label{eq:8.38}
  \tilde T = \frac{\hbar^2}{e B} \frac{\diff S}{\diff E}.
\end{equation}
Wie man sieht wird die Umlaufzeit durch die Energieabhängigkeit der im
$k$-Raum von der Bahn eingeschlossenen Schnittfläche $S$ bestimmt.

Für das freie Elektronengas findet man unabhängig vom Wellenvektor
\begin{equation}
  \label{eq:8.39}
  \frac{\diff S}{\diff E} = \frac{2\pi m}{\hbar^2}.
\end{equation}
Also besitzen alle Elektronen die gleiche Umlaufzeit mit der
Umlauffrequenz $\omega_c$, wobei man diese einfach aus der Überlegung
$\bm F_{\text{Lorentz}} = \bm F_{\text{Zentripetal}}$ erhält.  Die
\acct{Zyklotronfrequenz} ist gegeben durch
\begin{equation}
  \label{eq:8.40}
  \omega_c = \frac{e B}{m}.
\end{equation}
Für Elektronen im Festkörper gilt
\begin{equation*}
  m \to m^* .
\end{equation*}

\begin{theorem}[Achtung]
  Die Zyklotronmasse $m_c^*$ ist nicht mit der dynamischen Masse $m^*$
  aus \eqref{eq:8.20} identisch. $m_c^*$ wird durch die Lage der Bahn
  auf der Fermi"=Fläche bestimmt und nicht durch einen elektrischen
  Zustand.
  \begin{equation}
    \label{eq:8.41}
    \omega_c = \frac{e B}{m^*}.
  \end{equation}
\end{theorem}

\begin{notice}[Ergänzungen:]
  \begin{itemize}
  \item Die Elektronenbahnen im realen Raum sind Spiralen, da auch die
    $z$-Komponente des Wellenvektors berücksichtigt werden muss.
  \item Stimmen Zyklotron- und Mikrowellenfrequenz überein, so werden
    die Elektronen beschleunigt und nehmen aus dem elektrischen Feld
    Energie auf. Man erhält ein Absorptionsmaximum.
  \item In Metallen treten Komplikationen durch den Skin"=Effekt
    auf. Elektromagnetische Wellen können nur bis zur Eindringtiefe
    $\delta$ in das Material eindringen.  Die Eindringtiefe ist
    gegeben durch
    \begin{equation*}
      \delta = \sqrt{\frac{2}{\mu_0 \omega \sigma}}.
    \end{equation*}
    Daraus ergibt sich, dass das umlaufende Elektron nur an der
    Probenoberfläche mit dem $\bm E$-Feld in Berührung kommt.
    Der Energieeintrag oder -austrag bestimmt sich je nach Phasenlage.
    Nicht nur bei $\omega_c$ erscheint ein Maximum, sondern auch wenn
    zwischen zwei Durchläufen eine ganzzahlige Anzahl von Perioden der
    elektromagnetischen Schwingung verstreicht.  Es ergibt sich eine
    Serie von Maxima.
  \item Scharfe Resonanzen treten nur dann auf, wenn ein Elektron
    mehrere Umläufe ungestört vollenden kann, d.h.\ es muss die
    Bedingung erfüllt sein
    \begin{equation*}
      \omega_c T \gg 1 \quad (\text{$T$: mittlere Stoßzeit}).
    \end{equation*}
    Dazu sind hohe Frequenzen, hohe $\bm B$-Felder, tiefe Temperaturen
    und reine Proben (keine Defekte) notwendig.
  \item Sind die effektiven Massen der Elektronen nicht gleich ergibt
    sich ein Unterschied in den Umlauffrequenzen $\omega_c$. Dieser
    äußert sich in der Absorption von verschiedenen Frequenzen. Man
    erwartet folglich keine scharfe Absorption. Diese wird dennoch
    beobachtet, wofür es zwei Gründe gibt.
    \begin{enumerate}
    \item Nur Elektronen an der Fermi"=Fläche können Energie aufnehmen
      und tragen zur Absorption bei durch Anregung in unbesetzte
      Zustände.
    \item Das Signal kommt von Elektronen, die mit annähernd der
      gleichen Frequenz umlaufen. Dies ist dann der Fall, wenn die
      Umlaufzeiten stationär sind, bezüglich kleiner Änderungen der
      Komponente des Wellenvektors in Richtung des Magnetfeldes.  Das
      heißt, die Querschnittsfläche $S$ hat ein Extremum.  Diese
      Bahnen nennt man \acct{Extremalbahnen}.
    \end{enumerate}
  \end{itemize}
\end{notice}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \shadedraw[ball color=white] (0,0) ellipse (2 and 1);
    \filldraw[MidnightBlue!100] (70:2 and 1) arc(70:80:2 and 1)
    to[bend right] (260:2 and 1) arc(260:270:2 and 1) to[bend left] node[right,pos=.8] {$A$} (70:2 and 1) -- cycle;
    \filldraw[MidnightBlue!90] (130:2 and 1) arc(130:140:2 and 1)
    to[bend right] (220:2 and 1) arc(220:230:2 and 1) to[bend left] node[right,pos=.3] {$B$} (130:2 and 1) -- cycle;
    \filldraw[MidnightBlue!80] (90:2 and 1) arc(90:100:2 and 1)
    to[bend right] node[left,pos=.2] {$A'$} (280:2 and 1) arc(280:290:2 and 1) to[bend left] (90:2 and 1) -- cycle;
    \coordinate (A1) at (1.3,0);
    \coordinate (A2) at (2.7,0);
    \coordinate (B1) at (1.1,.3);
    \coordinate (B2) at (2,1);
    \path[name path=A] (A1) -- (A2);
    \path[name path=B] (B1) -- (B2);
    \path[name path=E] (0,0) ellipse (2 and 1);
    \path[name intersections={of=A and E,name=a}];
    \path[name intersections={of=B and E,name=b}];
    \draw[DarkOrange3,dashed] (A1) -- (a-1);
    \draw[DarkOrange3,->] (a-1) -- (A2) node[below] {$B$};
    \draw[DarkOrange3,dashed] (B1) -- (b-1);
    \draw[DarkOrange3,->] (b-1) -- (B2) node[right] {$B'$};
  \end{tikzpicture}
  \caption{Die beiden Bahnen $A$ und $A'$ sind Extremalbahnen.  Zu
    ihnen gehören die Magnetfelder $B$, bzw.\ $B'$.  Die Bahn $B$
    erfüllt die Bedingung für eine Extremalbahn nicht.  Nach
    \textcite{hunklinger}.}
  \label{fig:3.3}
\end{figure}

\subsection{Landau"=Niveaus}

Die Elektronenbahnen in einem äußeren Magnetfeld sind quantisiert.  Um
dies zu sehen betrachten wir quasifreie Elektronen mit der effektiven
Masse $m^*$. Der Spin wird vernachlässigt, da er lediglich einen
Zusatzterm in der Energie darstellt.

Die stationäre Schrödingergleichung lautet
\begin{equation*}
  H \psi = E \psi.
\end{equation*}
In der quantenmechanischen Beschreibung wird des Magnetfeld $\bm B$
über das Vektorpotential $\bm A$ nach dem Prinzip der minimalen
Kopplung im Hamiltonoperator berücksichtigt.  Die stationäre
Schrödingergleichung lautet dann
\begin{equation}
  \label{eq:8.42}
  \frac{1}{2 m^*} (-\ii \hbar \nabla + e \bm A)^2 \psi = E \psi .
\end{equation}
Zeige das Magnetfeld nun in $z$-Richtung, also $\bm B = (0,0,B)$,
wobei das Vektorpotential $\bm A = (0,x B,0)$ gewählt wurde.  Der von
Landau gewählte Ansatz ist eine in $y$- und $z$-Richtung ebene Welle
\begin{equation*}
  \psi = \tilde\psi(x) \ee^{-\ii (k_y y + k_z z)}.
\end{equation*}
Die Energieniveaus ergeben sich damit zu
\begin{equation}
  \label{eq:8.43}
  E = E_\ell + E(k_z) = \left( \ell + \frac12 \right) \hbar \omega_c + \frac{\hbar^2 k_z^2}{2 m^*} .
\end{equation}
Der Spin"=Beitrag wäre zusätzlich $\pm\frac12 g_S \mu_B B$, wird aber,
wie oben erwähnt, vernachlässigt.  Mit $\ell \geq 0$ ergeben sich die
Landau"=Niveaus.  Für die ausführliche Rechnung siehe
\textcite{hunklinger}.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[domain=-2:2,ymin=0,ymax=6,enlargelimits=false,no marks,
      extra y ticks={4.6},extra y tick labels={$E_F$},
      xlabel={$k_z$ (bel.\ Einheit)},ylabel={Energie (in Einheiten von $\hbar\omega_c$)}]
      \draw[gfx] (axis cs: 0,0) -- (axis cs:0,6);
      \draw[gfx] (axis cs:-2,4.6) -- (axis cs:2,4.6);
      \draw[<->,gfx] (axis cs:.5,.75) -- node[left] {$\hbar\omega_c$} (axis cs: .5,1.75);
      \addplot[dashed] {x^2};
      \pgfplotsinvokeforeach{0.5,1.5,...,5}{
        \addplot[gfx,smooth,MidnightBlue!20] {x^2+#1};
      }
      \path[clip] (axis cs:-2,4.6) rectangle (axis cs:2,0);
      \pgfplotsinvokeforeach{0.5,1.5,...,5}{
        \addplot[gfx,smooth,MidnightBlue] {x^2+#1};
      }
    \end{axis}
  \end{tikzpicture}
  \caption{Die Elektronenenergie im Magnetfeld ist um
    $\hbar\omega_c(\ell + 1/2)$ nach oben verschoben.  Die
    gestrichelte Referenzkurve gibt die Energie freier Elektronen an.
    Die Zustände der verschobenen Bänder sind bis zur Fermi"=Energie
    $E_F$ besetzt. Nach \textcite{hunklinger}.}
  \label{fig:3.2}
\end{figure}

Die parabelförmigen Bänder freier Elektronen spalten im $B$-Feld in
Sublevel auf, die als Landau"=Niveaus bezeichnet werden. Die
Energieaufspaltung zwischen den Subleveln ist gerade $\delta E = \hbar
\omega_c$.  Bei einer typischen Fermi"=Temperatur von $T_F =
\SI{5e4}{\K}$ ist eine große Zahl von Landau"=Niveaus besetzt.

\minisec{Bohrsches Korrespondenzprinzip}

Im Grenzfall großer Quantenzahlen (bei $E_F$) gehen die klassische und
die quantenmechanische Beschreibung ineinander über. Die Elektronen
bewegen sich in der $x,y$-Ebene auf Kreisbahnen mit klassischem
Bahnradius $r_\ell$, der durch die Amplitude des linearen Oszillators
gegeben ist
\begin{align}
  \label{eq:8.44}
  \frac12 m^* \omega_c^2 r_\ell^2 &= \left( \ell + \frac12 \right) \hbar \omega_c \; , \\
  \label{eq:8.45}
  r_\ell^2 &= \left( \ell + \frac12 \right) \frac{2 \hbar}{m^* \omega_c} \; .
\end{align}
Da offensichtlich die Bahnradien mit $\ell$ quantisiert sind muss auch
der von der Bahn umschlossene magnetische Fluss $\Phi$ quantisiert
sein.
\begin{equation}
  \label{eq:8.46}
  \Phi_\ell = \pi r_\ell^2 B = \left( \ell + \frac12 \right) \frac{\hbar}{e} \; .
\end{equation}
Im $k$-Raum durchlaufen die Elektronen Kreisbahnen mit quantisiertenm
$k_\ell$ und schließen definierte Flächen $S_\ell$ ein.  Die
klassische Umlaufgeschwindigkeit ist gegeben durch $v_\ell = \omega_c
r_\ell$, die semiklassische durch $v_\ell = \hbar k_\ell$.
Gleichsetzen und Einsetzen des Bahnradius liefert
\begin{equation}
  \label{eq:8.47}
  k_\ell^2 = \left( \ell + \frac12 \right) \frac{2 m^* \omega_c}{\hbar} \; .
\end{equation}
Die von der Kreisbahn im $k$-Raum eingeschlossene Fläche berechnet man
leicht mit $S_\ell = \pi k_\ell^2$.  Setzt man wiederum $k_\ell$ ein
erhält man
\begin{equation}
  \label{eq:8.48}
  S_\ell = \left( \ell + \frac12 \right) \frac{2\pi e B}{\hbar} \; .
\end{equation}
Für die Fläche im Ortsraum gilt $A_\ell = \pi r_\ell^2 = (2 \ell +1 )
\pi \hbar / e B$ oder in Abhängigkeit von $S_\ell$ ausgedrückt
\begin{equation}
  \label{eq:8.49}
  A_\ell = \left( \frac{\hbar}{e B} \right)^2 S_\ell \; .
\end{equation}
Mit steigendem Magnetfeld verengen sich im Ortsraum die Kreise
($A_\ell \sim 1/B$), während sie sich im Impulsraum vergrößern
($S_\ell \sim B$).

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End:
