\section{Heisenberg-Modell}

Die Austauschenergie zweier Elektronen $i$ und $j$ kann in der Form
\begin{equation}
  \label{eq:11.34}
  U = - J \bm S_i \bm S_j
\end{equation}
geschrieben werden, gerade so, als ob es eine direkte Kopplung
zwischen den Richtungen der Spins gäbe.  Der allgemeinste
Hamiltonoperator lautet
\begin{equation}
  \label{eq:11.35}
  H_{\text{Spin}} = - \sum_i \sum_{j\ne i} J_{ij} \hat{\bm S}_i \hat{\bm S}_j \; .
\end{equation}
Die Summation erfolgt über alle Atome $i$ und alle Nachbarn $j$. Die
$\hat{\bm S}_i$ und $\hat{\bm S}_j$ sind Spin-Operatoren und die
$J_{ij}$ sind Austauschkoeffizienten.

Um den Zusammenhang von $J$ mit $\lambda$, bzw.\ $C$ in der
Molekularfeldtheorie besser zu verstehen berechnen wir die potentielle
Energie, die ein Spin in Molekularfeldtheorie besitzt und vergleichen
das Ergebnis mit der Austauschenergie.

Mit den Ergebnissen des vorherigen Abschnitts folgt für die
potentielle Energie
\begin{equation}
  \label{eq:11.36}
  U = - \bm\mu_B \bm B_{\text{eff}}
  = g \mu_B \bm S_i \cdot \bm B_{\text{eff}}
  \stackrel{\eqref{eq:11.28}}{\approx} \mu_0 g \mu_B \lambda \bm S_i \cdot \bm M \; .
\end{equation}
Dabei haben wir für $\bm B_{\text{eff}}$ aus \eqref{eq:11.28} das
äußere Feld $\bm B_a$ vernachlässigt und nur das Molekularfeld $\bm
B_M = \lambda\mu_0\bm M$ eingesetzt.

Um die Austauschenergie zur berechnen ersetzen wir den Operator $\bm
S_j$ durch seinen Erwartungswert $\braket{\bm S_j}$.  Der Zusammenhang
zwischen $\bm M$ und dem Erwartungswert $\braket{\bm S_j}$ ist gegeben
durch
\begin{equation*}
  \bm M = - n g \mu_B \braket{\bm S_j} \; .
\end{equation*}
Wir beschränken außerdem die Wechselwirkung auf $z$ nächste Nachbarn.
Wir ersetzen $\bm S_j$ in \eqref{eq:11.34} durch den Erwartungswert
und setzen den Zusammenhang mit der Magnetisierung ein
\begin{equation}
  \label{eq:11.37}
  U = - z J \bm S_i \cdot \braket{\bm S_j}
  = \frac{z J}{n g \mu_B} \bm S_i \cdot \bm M \; .
\end{equation}
Aus dem Vergleich von \eqref{eq:11.36} mit \eqref{eq:11.37} erkennt
man
\begin{equation}
  \label{eq:11.38}
  \lambda = \frac{z J}{n \mu_0 g^2 \mu_B^2} \; .
\end{equation}
Aus dem Curie-Weiß-Gesetz \eqref{eq:11.32} erhalten wir einen
Zusammenhang der Molekularfeldkonstante mit der Curie-Temperatur und
mit der Definition der Curie-Konstante \eqref{eq:11.21} folgt
\begin{equation}
  \label{eq:11.39}
  J = \frac{3 \kB T_C}{z S(S+1)}
\end{equation}
wobei der Gesamtdrehimpuls $J$ durch den Spin $S$ ersetzt wurde, da
bei Ferromagneten der Bahnbeitrag unterdrückt ist.

Die Austauschenergie ist also vergleichbar mit der thermischen Energie
am Phasenübergang.

Nicht in allen Fällen wechselwirken die Ionen direkt miteinander,
d.h.\ es tritt nicht immer ein Überlapp der Elektronenhüllen der
Spin-tragenden Ionen auf. Im Beispiel von \ch{MnO} in
Abbildung~\ref{fig:17.1} wird die Austauschwechselwirkung von den
zwischen den Mangan-Ionen liegenden \ch{O2^-} vermittelt. Dies wird
als \acct{Superaustausch} bzeichnet..

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[DarkOrange3,draw,circle] at (-1.5,0) {$\up\up\up\up\up$};
    \node[DarkOrange3,draw,circle] at (1.5,0) {$\dn\dn\dn\dn\dn$};
    \draw[MidnightBlue] (0,0) to[out=30,in=90] (1.1,0) to[out=-90,in=-30] (0,0);
    \draw[MidnightBlue] (0,0) to[out=180-30,in=90] (-1.1,0) to[out=-90,in=180+30] (0,0);
    \node[MidnightBlue] at (-.7,0) {$\dn$};
    \node[MidnightBlue] at (.7,0) {$\up$};
    \node[DarkOrange3,below] at (-1.5,-.7) {\ch{Mn^{2+}}};
    \node[DarkOrange3,below] at (1.5,-.7) {\ch{Mn^{2+}}};
    \node[MidnightBlue,below] at (0,-.7) {\ch{O^{2-}}};
  \end{tikzpicture}
  \caption{Der Spinaustausch findet nicht direkt statt, sondern
    wird von einem Mangan-Ion zum anderen über den Sauerstoff
    übertragen.}
  \label{fig:17.1}
\end{figure}

Bei seltenen Erden beobachtet man einen indirekten Austausch.  Die
$4f$-Elektronen tragen die magnetischen Momente wobei der Überlapp
ihrer Wellenfunktionen gering ist.  Die Koppling erfolgt über die
Leitungslektronen.  Das magnetische Moment des Aufatoms richtet die
Spins der Leitungslektronen in der Umgebung aus und diese orientieren
die Spins der benachbarten Ionen.  Das Potential dieser
\acct{RKKY-Wechselwirkung} (Rudermann, Kittel, Kasuya, Yosida) lautet
\begin{equation*}
  U_{\text{RKKY}} \sim \frac{1}{r^3} \cos^2 k_F r
\end{equation*}
und hat eine große Reichweite.  Es bewirkt abhängig vom Abstand eine
parallele oder antiparallele Ausrichtung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[draw,circle,text=MidnightBlue] at (-1.5,0) {$\up$};
    \node[draw,circle,text=MidnightBlue] at (0,0) {$\up$};
    \node[draw,circle,text=MidnightBlue,pin={[MidnightBlue]below right:$4f$-Ionen}] at (1.5,0) {$\up$};
    \foreach \p in {(-1,.5),(-.7,.3),(-.5,.4),(-1.1,-.4),(-.8,-.2),(-.5,-.4)}
    \node[DarkOrange3] at \p {$\up$};
    \foreach \p in {(1,.5),(.7,.3),(.5,.4),(1.1,-.4),(.8,-.2),(.5,-.4)}
    \node[DarkOrange3] at \p {$\up$};
    \node[pin={[DarkOrange3]below left:freie $e^-$}] at (1.1,-.4) {};
  \end{tikzpicture}
  \caption{Die Leitungselektronen (orange) vermitteln den
    Spinaustausch zwischen den $4f$-Elektronen der Ionen
    (dunkelblau).}
  \label{fig:17.2}
\end{figure}

\minisec{Austausch-Wechselwirkung im freien Elektronengas}

Für Eisen, Kobalt und Nickel kann das oben diskutierte Konzept
zwischen lokalisierten magnetischen Momenten nicht verwendet werden.
Kollektive Eigenschaften der dreidimensionalen Elektronen spielen eine
wichtige Rolle.  Dazu kombinieren wir das Bändermodell und die
Austausch-Wechselwirkung.  Betrachte dazu zwei Elektronen mit
parallelem Spin.  Als Ansatz wählen wir eine antisymmetrische
Überlagerung von ebenen Wellen, da sich ebene Wellen zur Beschreibung
des Elektronengases anbieten und die Tatsache, dass Elektronen
vorliegen das Pauli-Prinzip impliziert, weshalb wir antisymmetrisieren
müssen.
\begin{align}
  \notag
  \psi(\bm r_1,\bm r_2) &= N \left( \ee^{\ii\bm k_1 \bm r_1} \ee^{\ii\bm k_2 \bm r_2}
    - \ee^{\ii\bm k_1 \bm r_2} \ee^{\ii\bm k_2 \bm r_1} \right) \\
  \label{eq:11.40}
  &= N \ee^{\ii(\bm k_1\bm r_1 + \bm k_2\bm r_2)} \left[ 1 -
    \ee^{-\ii(\bm k_1-\bm k_2)(\bm r_1-\bm r_2)} \right]
\end{align}
Die Wahrscheinlichkeit Elektron $1$ in $\diff V_1$ und Elektron $2$ in
$\diff V_2$ zu finden ist gegeben durch
\begin{equation}
  \label{eq:11.41}
  |\psi(\bm r_1,\bm r_2)|^2 \diff V_1 \diff V_2
  = |N|^2 \{ 1 - \cos[(\bm k_1-\bm k_2)(\bm r_1-\bm r_2)] \} \diff V_1 \diff V_2
\end{equation}
Die Wahrscheinlichkeit zwei Elektronen mit gleichem Spin am gleichen
Ort zu finden verschwindet also für beliebige Wellenvektoren, ohne
dass die abstoßende Coulomb"=Wechselwirkung berücksichtigt wurde.

Greift man ein Elektron mit vorgegebener Spinrichtung heraus so ist
die Aufenthaltswahrscheinlichkeit von Elektronen mit der gleichen
Spinrichtung in der Nachbarschaft reduziert.  Dies bewirkt ein
Austauschloch mit einem Radius von ca.\ \num{1} bis \SI{2}{\angstrom}.
Die Abschirmung der Atomrümpfe durch freie Elektronen ist nicht mehr
so effizient.  Es findet eine Reduktion der Energie des Aufelektrons
statt, die Bindungsenergie wird erhöht.  Eine Parallelstellung von
Spins führt daher zu einem Energiegewinn.  Der Korrelationseffekt
wirkt wie eine kollektive Austauschwechselwirkung mit positivem $J$.

Der \enquote{Gegenspieler} der abgesenkten Austauschenergie ist die
Zunahme in kinetischer Energie der Elektronen.  Die quantitative
Beschreibung des Band-Ferromagnetismus erfolgte durch E.\,C.\ Stoner
und E.\,P.\ Wohlfahrt, siehe \textcite[703]{gross} und
\textcite[532]{hunklinger}.

\minisec{Spinwellen, Magnonen}

Aus dem Hamiltonoperatur \eqref{eq:11.35} kann man sich leicht klar
machen, dass im Grundzustand eines Ferromagneten mit $J>0$ alle Spins
parallel ausgerichtet sind.  Gleichung \eqref{eq:11.37} gibt uns eine
Abschätzung für die Energie, die zum Umklappen eines Spins nötig ist.
\begin{equation*}
  \tag{\ref{eq:11.37}}
  U = - z J \bm S_i \braket{\bm S_j} \; .
\end{equation*}
Gehen wir von einer linearen Kette von Spins aus, also einer
Koordinationszahl von $z=2$.  Außerdem betrachen wir die beiden Konfigurationen
\begin{align*}
  U_1 &\hateq \up\up\up\up\up \; , \\
  U_2 &\hateq \up\up\dn\up\up \; ,
\end{align*}
die sich durch einen umgeklappten Spin unterscheiden.  Nach \eqref{eq:11.37} gilt für die Energien
\begin{align*}
  U_1 &= -z J \bm S_1 \bm S_2 = - z J S^2 = - 2 J S^2 \; , \\
  U_2 &= -z J \bm S_1 (-\bm S_1) = z J S^2 = 2 J S^2 \; .
\end{align*}
Die Energiedifferenz der beiden Konfiguration ist also
\begin{equation*}
  \delta E = U_1 - U_2 = 2 z J S^2 = 4 J S^2
\end{equation*}
Sei außerdem $J \approx \kB T_C$, dann ist
\begin{equation}
  \label{eq:11.42}
  \delta E = 2 z \kB T_C S^2 \; .
\end{equation}
Derartige Einspinanregungen werden weit unterhalb von $T_C$ kaum
erzeugt.  Energetisch günstiger ist die Anregung einer kollektiven
Präzessionsbewegung wie in Abbildung~\ref{fig:17.3}. Die Anregung von
Spinwellen ist möglich.  Dabei präzedieren die Spins mit gleichem
$\omega$ und festem Winkel zwischen $\bm S_m$ und $\bm S_{m+1}$.  Wir
führen im Folgenden eine klassische Behandlung des Problems durch.

\begin{figure}[tb]
  \centering
  \def\kegel(#1,#2)#3{
    \begin{scope}[shift={(#1,#2)}]
      \draw (.3,.5) -- (0,0) -- (-.3,.5) (0,.5) ellipse (.3 and .05);
      \path (.3,.5) arc (0:#3:.3 and .05) coordinate (A) {};
      \draw[DarkOrange3,->] (0,0) -- (A);
    \end{scope}
  }
  \def\kreis(#1,#2)#3{
    \begin{scope}[shift={(#1,#2)}]
      \draw (0,.3) circle (.3);
      \draw[DarkOrange3,->] (0,.3) -- ++(#3:.3);
    \end{scope}
  }
  \begin{tikzpicture}[gfx]
    \draw (-1,0) -- (6,0);
    \kegel(0,0){0};
    \kegel(1,0){-45};
    \kegel(2,0){-90};
    \kegel(3,0){-135};
    \kegel(4,0){-180};
    \kegel(5,0){135};
    \node[below] at (1,0) {$S_{m-1}$};
    \node[below] at (2,0) {$S_m$};
    \node[below] at (3,0) {$S_{m+1}$};

    \draw (-1,-2) -- (6,-2);
    \kreis(0,-2){0};
    \kreis(1,-2){-45};
    \kreis(2,-2){-90};
    \kreis(3,-2){-135};
    \kreis(4,-2){-180};
    \kreis(5,-2){135};
  \end{tikzpicture}
  \caption{Schematische Darstellung eines Magnons, das entlang einer
    Kette propagiert.  Oben ist die perspektivische Darstellung der
    Präzession gezeigt, unten eine Draufsicht auf die Kegel.}
  \label{fig:17.3}
\end{figure}

Wie bei den Phononen ist auch hier die Amplitude der Auslenkung
quantisiert, sodass jedem Magnon die Energie $\hbar\omega$
zugeschrieben werden kann.  Die Anregung eines Magnons entspricht dem
Umklappen eines Spin $1/2$.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: