Falls die Probendicke sehr viel kleiner als die Eindringtiefe ist $d
\ll \lambda_L$ so durchdringt das Feld den Supraleiter. Der
Meißnereffekt ist nicht vollständig (Supraleiter zweiter Art).

Aus den Londonschen Gleichungen lassen sich sogleich
Schlussfolgerungen für eine mikroskopische Theorie ziehen.  Wir
wissen, dass wir aus einem Vektorpotential $\bm A$ eine elektrisches
und ein magnetisches Feld erhalten können
\begin{equation*}
  \bm B = \rot \bm A
  \; , \quad
  \bm E = - \frac{\partial\bm A}{\partial t} \; .
\end{equation*}
Zusätzlich gilt in Coulomb-Eichung
\begin{equation*}
  \div \bm A = 0 \; .
\end{equation*}
Wir setzen dies in die Materialgleichungen \eqref{eq:10.9} und
\eqref{eq:10.10} ein, wobei wir $\Lambda = m_S / (n_S e_S^2)$
abkürzen.
\begin{align*}
  \Lambda \frac{\partial\bm j_S}{\partial t} &= - \frac{\partial\bm A}{\partial t} \\
  \Lambda \rot\bm j_S &= - \rot\bm A
\end{align*}
Aus beiden Gleichungen folgt durch Vergleich (oder aufintegrieren)
\begin{equation*}
  \Lambda\bm j_S = -\bm A \; .
\end{equation*}
Um einen besseren Einblick in die Bedeutung dieser Beziehung zu
erhalten sehen wir uns die Definition der
Wahrscheinlichkeitsstromdichte der Quantentheorie an.  Mit minimaler
Kopplung an das Magnetfeld gilt
\begin{align*}
  \bm j(\bm x) &= \underbrace{\frac{e\hbar}{2 m\ii} \Bigl[
    \psi^*(\bm x) \nabla \psi(\bm x) - \psi(\bm x) \nabla \psi^*(\bm x) \Bigr]}_{\bm j_1}
  \underbrace{- \frac{e^2}{m} \bm A \psi^*(\bm x) \psi(\bm x)}_{\bm j_2} \; .
\end{align*}
Im Rahmen der Feldquantisierung sind natürlich $\psi^*(\bm x)$ und
$\psi(\bm x)$ durch die entsprechenden Feldoperatoren
$\hat\psi^\dagger(\bm x)$ und $\hat\psi(\bm x)$ zu ersetzen.  Sodann
wird auch $\bm j(\bm x)$ zu einem Operator $\hat{\bm j}(\bm x)$, der
einen Erwartungswert besitzt.
\begin{equation*}
  \braket{\hat{\bm j}(\bm x)} = \braket{\Phi|\hat{\bm j}(\bm x)|\Phi} \; .
\end{equation*}
Ist kein Vektorpotential $\bm A$, also kein Magnetfeld, vorhanden, so
verschwindet die Stromdichte im Grundzustand, da der Ausdruck $\bm j_1$ in
eckigen Klammern Null ergibt.  Wäre der Klammerausdruck $\bm j_1$ auch
mit Magnetfeld weiterhin Null, so wäre die Bedingung $\Lambda\bm j_S =
-\bm A$ identisch erfüllt.  Die Stromdichte würde sich nämlich dann
reduzieren auf
\begin{equation*}
  \braket{\hat{\bm j}(\bm x)}
  = - \frac{e^2}{m} \bm A \braket{\Phi|\hat\psi^\dagger(\bm x) \hat\psi(\bm x)|\Phi}
  = - \frac{e^2}{m} \bm A \braket{\hat n} \; .
\end{equation*}
wobei die Stromdichte proportional zum Vektorpotential $\bm A$ und dem
Produkt $\hat\psi^\dagger(\bm x) \hat\psi(\bm x)$, das die
Teilchendichte wiedergibt, ist.  Diese Forderung ist jedoch unsinnig,
weil sie jeden Leiter zum Supraleiter machen würde.  In der Realität
beeinflusst ein Vektorpotential die Elektronenwellenfunktionen und der
Ausdruck $\bm j_1$ in Klammern liefert einen Beitrag.  Damit $\bm j_1$
auch bei einem eingeschalteten Magnetfeld Null bleibt müssen die
Elektronenwellenfunktionen eine Starrheit besitzen, also bei Anlegen
eines Magnetfeldes unverändert bleiben.  Diese Starrheit wäre
gewährtleistet, wenn der Grundzustand durch eine \acct{Energielücke}
vom angeregten Zustand getrennt wäre, die durch die Stärke des
Magnetfeldes erst überwunden werden müsste.  Die Theorie der
Energielücke hat sich als sehr erfolgreich herausgestellt und wurde
experimentell verifiziert.

\section{Cooper-Paare und BCS-Theorie}

Es folgt eine kurze Zusammenfassung der bisherigen Erkenntnisse, die
in die Theorie einfließen müssen.
\begin{enumerate}
\item Es gibt eine kritische Temperatur $T_C$, einen kritischen Strom
  $I_C$ und ein kritisches Feld $B_C$ bei dem die Supraleitung
  verschwindet.
\item Der eingefrorene Fluss ist mit $h/2e$ quantisiert.
\item $T_C \sim 1/\sqrt{m}$
\item $c_V$ zeigt einen Sprung bei $T_C$ und exponentielles Verhalten
  für $T < T_C$.
\end{enumerate}
Die kurze Antwort für all diese Forderungen wäre
\begin{enumerate}[{aus} 1.]
  \refstepcounter{enumi}
\item Elektronen-Paare sind für die Supraleitung verantwortlich.
\item Die Wechselwirkung der Elektronen untereinander ist phononischer
  Natur.
\item Supraleitung ist mit einer Energielücke verbunden.
\end{enumerate}

Bei einer attraktiven Wechselwirkung zwischen zwei Elektronen ist der
Grundzustand des Fermi-Gases nicht mehr stabil und die Energie dieser
zwei Elektronen wird abgesenkt. Wie kann es zur einer attraktiven
Wechselwirkung kommen?

Diese Frage wurde von Fröhlich 1950 beantwortet, indem er eine
Wechselwirkung zwischen Elektronen und Phononen quantenfeldtheoretisch
begründete.  Der Hamiltonoperator des Gesamtsystems aus Elektronen und
Phononen kann zerlegt werden in die unabhängigen Anteile der
jeweiligen Dynamik und einen Wechselwirkungsterm
\begin{equation*}
  H = H_0 + H_{\text{WW}} \; .
\end{equation*}
Die beiden Bestandteile lauten
\begin{align*}
  H_0 &= \sum_{\bm k,\sigma} \hbar\varepsilon_{\bm k} a_{\bm k,\sigma}^\dagger a_{\bm k,\sigma}
  + \sum_{\bm w,\sigma} \hbar\omega_{\bm w} b_{\bm w}^\dagger b_{\bm w} \; , \\
  H_{\text{WW}} &= \hbar \sum_{\bm k,\bm w,\sigma} \Bigl(
    g_{\bm w} b_{\bm w} a_{\bm k+\bm w,\sigma}^\dagger a_{\bm k,\sigma}
    + g_{\bm w}^* b_{\bm w}^\dagger a_{\bm k,\sigma}^\dagger a_{\bm k+\bm w,\sigma} \Bigr) \; .
\end{align*}
mit den Elektronenoperatoren $a_{\bm k,\sigma}$, den
Phononenoperatoren $b_{\bm w}$ und den Kopplungskonstanten
$g_{\bm w}$.  Wir haben dabei angenommen, dass durch die
Wechselwirkung der Elektronen mit den Phononen der Spin nicht geändert
wird.  Nun wird der Hamiltonoperator ins Heisenbergbild transformiert
wobei alle zuvor zeitunabhängigen Operatoren durch ihre zeitabhängigen
Partner ersetzt werden, was durch eine Tilde über dem Operator
symbolisiert wird.  Stellt man die Heisenberg-Bewegungsgleichungen für
den Phononen-Erzeugungsoperator auf so ergibt sich
\begin{equation*}
  \dot{\tilde b}_{\bm w}^\dagger = \frac{\ii}{\hbar} [\tilde H,\tilde b_{\bm w}^\dagger]
  = \ii \sum_{\bm k,\sigma} g_{\bm w} \ee^{\ii(\varepsilon_{\bm k+\bm w}-\varepsilon_{\bm k}-\omega_{\bm w})t}
  \tilde a_{\bm k+\bm w,\sigma}^\dagger \tilde a_{\bm k,\sigma} \; .
\end{equation*}
Wären die Operatoren klassische Amplituden, so würde dies bedeuten,
dass sich die Phononenamplitude in Abhängigkeit der Elektronenbewegung
ändert.  Anschaulich könnte man sagen, dass ein durch das Gitter
laufende Elektron dieses polarisiert indem es die positiven Ionen aus
ihrer Ruhelage auslenkt.  Diese Ionenverschiebung wirkt natürlich
zurück auf die Elektronenbewegung.  Dazu betrachtet man einen
allgemeinen Operator $\tilde A$, der aus Elektronenoperatoren besteht
und stellt auch dessen Heisenberg-Bewegungsgleichung auf
\begin{equation*}
  \dot{\tilde A} = \ii \sum_{\bm k,\bm w,\sigma} \Bigl(
    g_{\bm w} [\tilde a_{\bm k+\bm w,\sigma}^\dagger \tilde a_{\bm k,\sigma},\tilde A] \tilde b_{\bm w}
    \ee^{\ii(\varepsilon_{\bm k+\bm w}-\varepsilon_{\bm k}-\omega_{\bm w})t}
    + g_{\bm w}^* \tilde b_{\bm w}^\dagger [\tilde a_{\bm k,\sigma}^\dagger \tilde a_{\bm k+\bm w,\sigma},\tilde A]
    \ee^{-\ii(\varepsilon_{\bm k+\bm w}-\varepsilon_{\bm k}-\omega_{\bm w})t}
  \Bigr) \; .
\end{equation*}
Wir integrieren nun die Bewegungsgleichung für $\tilde b_{\bm
  w}^\dagger$, wobei wir annehmen, dass die Wechselwirkung von
Elektronen mit Phononen schwach ist und wir deshalb $\tilde a_{\bm
  k+\bm w,\sigma}^\dagger \tilde a_{\bm k,\sigma}$ für die
Zeitintegration als konstant ansehen können.  Das Resultat für $\tilde
b_{\bm w}^\dagger$ setzen wir in die Bewegungsgleichung für $\tilde A$
ein.  Nach langer Rechnung und Rücktransformation ins Schrödingerbild,
siehe \textcite[275--281]{haken-qft}, kann man in dieser
Bewegungsgleichung
\begin{equation*}
  \dot{\tilde A} = \frac\ii\hbar [H_0,A] + \frac\ii\hbar [H_{\text{WW}}^{\text{eff}},A]
\end{equation*}
identifizieren.  Dabei gilt für die effektive Wechselwirkung
\begin{multline*}
  H_{\text{WW}}^{\text{eff}} =
  \hbar \sum_{\substack{\bm k,\bm k',\bm w\\ \sigma,\sigma'}}
  \lvert g_{\bm w}\rvert^2 \frac{\omega_{\bm w}}{(\varepsilon_{\bm k'+\bm w}-\varepsilon_{\bm k'})^2-\omega_{\bm w}^2}
  a_{\bm k+\bm w,\sigma}^\dagger a_{\bm k',\sigma'}^\dagger a_{\bm k'+\bm w,\sigma'} a_{\bm k,\sigma} \\
  + \hbar \sum_{\bm k,\sigma} a_{\bm k,\sigma'}^\dagger a_{\bm k,\sigma} \biggl[ \sum_{\bm w}
    \lvert g_{\bm w}\rvert^2 \frac{1}{\varepsilon_{\bm k}-\varepsilon_{\bm k-\bm w}-\omega_{\bm w}}
  \biggr] \; .
\end{multline*}
Der zweite Summand stellt die Selbstenergie des Elektrons im Gitter
dar, was sich in einer Energieverschiebung ausrückt, die durch eine
effektive Masse berücksichtigt werden kann.  Der erste Summand
beinhaltet jedoch eine Elektron-Elektron-Wechselwirkung.  Diese können
wir auf die Form
\begin{equation*}
  H_{\text{El-El}} = -\frac12 \sum_{\substack{\bm k,\bm k',\bm w\\ \sigma,\sigma'}}
  v_{\bm k,\bm k',\bm w} a_{\bm k+\bm w,\sigma}^\dagger a_{\bm k',\sigma'}^\dagger a_{\bm k'+\bm w,\sigma'} a_{\bm k,\sigma}
\end{equation*}
bringen.  Damit können wir den Hamiltonoperator der Supraleitung
hinschreiben
\begin{equation*}
  H = \sum_{\bm k,\sigma} E_{\bm k} a_{\bm k,\sigma}^\dagger a_{\bm k,\sigma}
  -\frac12 \sum_{\substack{\bm k,\bm k',\bm w\\ \sigma,\sigma'}}
  v_{\bm k,\bm k',\bm w} a_{\bm k+\bm w,\sigma}^\dagger a_{\bm k',\sigma'}^\dagger a_{\bm k'+\bm w,\sigma'} a_{\bm k,\sigma} \; .
\end{equation*}
Hier ist nicht mehr ersichtlich, dass die
Elektron-Elektron-Wechselwirkung tatsächlich durch ein Phonon
stattfindet.  Cooper fand jedoch 1956, dass auch für diesen
Hamiltonoperator eine anziehende Kraft zwischen zwei Elektronen mit
antiparallelen Spins möglich ist.  Wir bauen daher die Zustände durch
Erzeugung von Paaren mit entgegengesetztem Spin und Wellenvektor aus
dem Vakuumzustand auf
\begin{equation*}
  \Phi = \prod_{\bm k} (u_{\bm k} + v_{\bm k} a_{\bm k,\up}^\dagger a_{-\bm k,\dn}^\dagger) \Phi_0 \; .
\end{equation*}
Die Herleitung dieses Zustandes ist nicht ganz trivial und kann bei
\textcite[281--289]{haken-qft} nachvollzogen werden.  Dort ist auch
dargelegt wie man den Energieerwartungswert des Zustandes $\Phi$
berechnet.  Dieser lautet
\begin{equation*}
  E = 2 \sum_{\bm k} E'_{\bm k} v_{\bm k}^2
  - \sum_{\bm k,\bm k'} V_{\bm k,\bm k'} u_{\bm k} v_{\bm k} u_{\bm k'} v_{\bm k'}
\end{equation*}
mit der Abkürzung $2V_{\bm k,\bm k'} = (v_{\bm k,-\bm k',\bm k'-\bm k}
+ v_{-\bm k,\bm k',\bm k-\bm k'})$.  Eine Minimierung des
Energieerwartungswertes führt auf die Energielückengleichung, deren
Lösung näherungsweise durch
\begin{equation*}
  \Delta \approx 2 \hbar\omega \ee^{-2/(D(E_F) V_0)}
\end{equation*}
gegeben ist mit der Zustandsdichte $D(E)$ der Elektronen und der
konstanten Näherung $V_{\bm k,\bm k'} = V_0$ des Matrixelements der
Wechselwirkung.

\minisec{Vereinfachte Betrachtung}

Die quantenfeldtheoretische Behandlung der
Elektron-Gitter-Elektron-Wechselwirkung ist aufwändig.  Daher wenden
wir uns der anschaulichen Interpretation zu.  Ein Elektron polarisiert
das Gitter und es entsteht eine positive Ladungswolke, die ein anderes
Elektron anziehen kann.  Erst nach einem Viertel der
Ionen-Schwingungszeit bildet sich die höchste positive Ladungsdichte
aus.  Das erste Elektron ist nach dieser Zeit schon ca.\
\SI{100}{\nano\m} weiter entfernt.  Die
Elektron-Elektron-Coulomb-Wechselwirkung mit einem zweiten Elektron
ist klein und die attraktive Wechselwirkung überwiegt.

Die Elektronen tauschen im Gitter virtuelle Phononen aus.  Man kann
dafür eine Impulsbetrachtung vornehmen.  Die Elektronen haben vor dem
Austausch die Impulse $\bm k_1$ und $\bm k_2$.  Für die Impulse nach
dem Stoß gilt folglich
\begin{equation*}
  \bm k_1'=\bm k_1 + \bm q
  \; , \quad
  \bm k_2'=\bm k_2 - \bm q
  \; .
\end{equation*}
Da die Impulserhaltung weiterhin gelten muss folgt aus der
Gesamtimpulserhaltung
\begin{equation}
  \label{eq:10.16}
  \bm k_1 + \bm k_2 = \bm k_1' + \bm k_2' = \bm K
\end{equation}
Die folgenden Betrachtungen gelten für $T = \SI{0}{\kelvin}$. Für die
beiden Elektronen sind nur Zustände oberhalb von $E_F$ zugänglich, da
alle Zustände unterhalb der Fermi-Energie besetzt sind, also der
Energiebereich $E_F$ bis $E_F + \hbar \omega_D$ wobei $\omega_D$ die
Debye-Frequenz ist. Im $k$-Raum bedeutet das
\begin{align*}
  \frac{\hbar^2 (k_F + \delta k)^2}{2 m} &= E_F + \hbar \omega_D \\
  \implies \delta k &= \frac{m \omega_D}{\hbar k_F}
\end{align*}
Die Zustände halten sich also in einer Kugelschale der Dicke
$\delta\bm k$ auf.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,phonon/.style={
      decorate,decoration={
        snake,amplitude=1pt,segment length=5pt,post length=5pt}}]
    \begin{scope}
      \begin{scope}
        \clip (-1,0) circle ({sqrt(2)+.1});
        \clip (1,0) circle ({sqrt(2)+.1});
        \fill[DimGray!30] (-.5,-2) rectangle (.5,2);
        \fill[white] (-1,0) circle ({sqrt(2)-.1});
        \fill[white] (1,0) circle ({sqrt(2)-.1});
      \end{scope}
      \draw (-2.6,0) -- (2.6,0);
      \draw[->,DarkOrange3] (-1,0) -- node[below] {$\bm K$} (1,0);
      \draw[->,MidnightBlue] (-1,0) -- node[above left] {$\bm k_1$} +(45:{sqrt(2)});
      \draw[<-,MidnightBlue] (1,0) -- node[above right] {$\bm k_2$} +(90+45:{sqrt(2)});
      \draw (-1,0) circle ({sqrt(2)+.1});
      \draw (-1,0) circle ({sqrt(2)-.1});
      \draw (1,0) circle ({sqrt(2)+.1});
      \draw (1,0) circle ({sqrt(2)-.1});
      \draw[<-] (1,0) ++(45:{sqrt(2)-.1}) -- ++(45:-.5);
      \draw[<-] (1,0) ++(45:{sqrt(2)+.1}) -- node[above left] {$\delta\bm k$} ++(45:.5);
    \end{scope}
    \begin{scope}[shift={(6,0)}]
      \begin{scope}
        \clip (0,0) circle ({sqrt(2)+.1});
        \fill[DimGray!30] (-2,-2) rectangle (2,2);
        \fill[white] (0,0) circle ({sqrt(2)-.1});
      \end{scope}
      \draw (-2.6,0) -- (2.6,0);
      \draw (0,0) circle ({sqrt(2)+.1});
      \draw (0,0) circle ({sqrt(2)-.1});
      \draw[<->,MidnightBlue] (40:{sqrt(2)})
      -- node[above,pos=.25] {$\bm k$} node[below,pos=.75] {$-\bm k$}
      (180+40:{sqrt(2)});
      \draw[<->,MidnightBlue] (10:{sqrt(2)})
      -- node[below,pos=.25] {$\bm k'$} node[above,pos=.75] {$-\bm k'$}
      (180+10:{sqrt(2)});
      \draw[phonon,->] (40:{sqrt(2)}) arc(40:10:{sqrt(2)});
      \draw[phonon,->] (180+40:{sqrt(2)}) arc(180+40:180+10:{sqrt(2)});
    \end{scope}
  \end{tikzpicture}
  \caption{Die beiden Elektronen im $k$-Raum (zweidimensionale
    Projektion).  Links ist eine Skizze zur Illustration der
    Impulserhaltung abgebildet.  Rechts sieht man einen typischen
    Streuprozess zweier Elektronen in einem Cooper-Paar.}
  \label{fig:11.1}
\end{figure}

Bei vorgegebenem $\bm K$ können nur Elektronenpaare in den in
Abbildung~\ref{fig:11.1} schraffierten Bereichen die Impulserhaltung
erfüllen. Am wahrscheinlichsten ist dies für $\bm K = 0$. Für ein
Cooper-Paar gilt
\begin{equation}
  \label{eq:10.17}
  \bm k_1 = - \bm k_2
\end{equation}

Betrachten wir die Zwei-Teilchen-Wellenfunktion $\psi(\bm r_1,\bm
r_2)$ des Cooper-Paars. Wir wählen einen Ansatz ebener Wellen
\begin{equation*}
  \psi
  = A \ee^{i \bm k_1 \bm r_1} \ee^{\ii \bm k_2 \bm r_2}
  = A \ee^{\ii \bm k_1 (\bm r_1 - \bm r_2)}
  = A \ee^{\ii \bm k \bm r}
\end{equation*}
mit der Relativkoordinate $\bm r = \bm r_1 - \bm r_2$. Die Lösung
der Schrödingergleichung ist eine Superposition solcher
Paarzustände.
\begin{equation}
  \label{eq:10.18}
  \psi(\bm r) = \sum_{k = k_F}^{k_F+\delta k} A_{\bm k} \ee^{\ii \bm k \bm r}
\end{equation}
dabei ist $|A_{\bm k}|^2$ ein Maß für die Wahrscheinlichkeit ein
spezielles Paar im Zustand $(\bm k,-\bm k)$ zu finden. Die
Schrödingergleichung lautet
\begin{equation}
  \label{eq:10.19}
  \left[ -\frac{\hbar^2}{2m} (\nabla_1^2 + \nabla_2^2)
    + \tilde V(\bm r_1, \bm r_2) \right] \psi(\bm r_1,\bm r_2)
  = E \psi(\bm r_1,\bm r_2)
\end{equation}
Hier hat $\tilde V(\bm r_1,\bm r_2)$ zwei Anteile. Zum einen die
attraktive Wechselwirkung durch Phononenaustausch und zum anderen die
Coulomb-Abstoßung.  Für eine ausführliche Herleitung der
Energieabsenkung siehe \textcite{hunklinger,gross,ibach-lueth}.  Nach
\textcite{hunklinger} ergibt sich
\begin{equation}
  \label{eq:10.20}
  \Delta E = E - 2 E_F
  = \frac{2 \hbar \omega_D}{1 - \ee^{4/(\tilde V_0 D(E_F))}}
  \approx - 2 \hbar \omega_D \ee^{-4/(\tilde V_0 D(E_F))}
\end{equation}
wobei $\tilde V_0$ das konstantes Matrixelement der Wechselwirkung
ist. Die Energieänderung ist negativ, d.h.\ die Energie der
Cooper-Paare wird reduziert. An der Oberfläche des Fermi-Sees bilden
sich Zwei-Elektronenzustände, deren Energie um den Wert $\delta E$
gegenüber der Energie der freien Elektronen bei $T = \SI{0}{\K}$
abgesenkt ist.

\begin{notice}
  \begin{enumerate}
  \item Die Gleichung \eqref{eq:10.20} erlaubt eine Erklärung der
    scheinbar paradoxen Beobachtung, dass gute Metalle wie Silber oder
    Kupfer \emph{nicht} supraleitend sind. Die Elektronen koppeln nur
    schwach an die Phononen.
  \item Elektronen sind Fermionen, die Gesamtwellenfunktion muss
    also antisymmetrisch sein. Die Wellenfunktion \eqref{eq:10.18}
    ist symmetrisch bezüglich Elektronenaustausch. Wir wählen also
    den Spinanteil antisymmetrisch.
    \begin{equation*}
      (\bm k \up,- \bm k \dn) \quad\text{(Singulett-Paar, $S=0$)}
    \end{equation*}
    Dieses verhält sich nach außen hin wie ein Boson. Es ist also ein
    gemeinsamer quantenmechanischer Grundzustand möglich.
  \item Falls die Wechselwirkungen zwischen den Elektronen nicht
    isotrop ist, können auch andere Fälle auftreten, z.B.\
    \begin{itemize}
    \item Suprafluides $^3$He: Triplett $S=1$ Ortswellenfunktion
      $p$-artig und antisymmetrisch.
    \item Hochtemperatur-Supraleiter: Ortswellenfunktion $d$-artig.
    \end{itemize}
  \item Die Ausdehnung eines Cooper-Paares beträgt \SI{100}{\nm} bis
    \SI{1000}{\nm}. Das bedeutet, dass sich zwischen den beiden
    Elektronen eines Cooper-Paares Millionen andere aufhalten.
  \end{enumerate}
\end{notice}

\paragraph{BCS-Grundzustand:}
Die theoretische Beschreibung des Gesamtzustandes ist mathematisch
sehr aufwändig \cite{hunklinger,gross,ibach-lueth}. Im Energiespektrum
des Supraleiters tritt eine Energielücke $\Delta$ auf. Diese ist eng
mit der Bindungsenergie der Cooper-Paare verbunden.
\begin{equation*}
  \Delta = 2 \hbar \omega_D \exp\left[ -\frac{2}{\tilde V_0 D(E_F)} \right]
\end{equation*}
Die minimale Energie, die nötig ist um ein Cooper-Paar aufzubrechen beträgt
\begin{equation}
  \label{eq:10.21}
  \delta E_{\text{min}} = 2 \Delta \; ,
\end{equation}
da beim Aufbrechen immer zwei ungepaarte Elektronen erzeugt werden
müssen.  Angeregte Elektronen werden im Allgemeinen als Quasi-Teilchen
(teils Elektron-, teils Loch-Charakter) bezeichnet. Der gemeinsame
Grundzustand der Cooper-Paare ist von den Zuständen der Quasiteilchen
durch die Energielücke getrennt.

Bei endlicher Temperatur sind nicht alle Elektronen an der
Fermi-Fläche gepaart, denn durch thermische Anregung werden
Cooper-Paare aufgebrochen und damit Quasiteilchen erzeugt. Für $T \to
T_C$ geht $\Delta \to 0$. Aus der Theorie folgt
\begin{equation}
  \label{eq:10.22}
  \Delta(0) = \num{1.764}\,\kB T_C \; .
\end{equation}
% Fortsetzung: 20.11.2014
Cooper-Paare sind der quantenmechanische Grundzustand.  Sie beteiligen
sich daher nicht an der spezifischen Wärme und am Wärmetransport. Die
Quasiteilchen sind dafür zuständig.

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: