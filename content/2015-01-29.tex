Man unterscheidet zwei Exzitonen-Grundtypen.  Zum einen die
\acct{Frenkel-Exzitonen}, welche in Alkalihalogeniden wie \ch{NaCl}
oder \ch{KCl}, Molekülkristallen oder Edelgaskristallen auftreten.
Zum anderen die \acct{Mott-Wannier-Exzitonen}, die in Halbleitern wie
\ch{GaAs}, \ch{Si} oder \ch{Ge} angeregt werden können.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,eh/.style={fill=white,circle,inner sep=0pt}]
    \begin{scope}
      \node at (0,2.5) {Frenkel};
      \draw (-2,-2) rectangle (2,2);
      \foreach \x in {-1.5,-1,...,1.5} {
        \foreach \y in {-1.5,-1,...,1.5} {
          \node[dot] at (\x,\y) {};
        }
      }
      \draw[decorate,decoration=brace] (1.6,0) -- node[right] {$a$} (1.6,-.5);
      \draw[MidnightBlue] (-.5,0) ellipse (.2 and .3);
      \node[MidnightBlue,eh] at (-.5,.3) {$+$};
      \node[MidnightBlue,eh] at (-.5,-.3) {$-$};
      \node[below,text width=4.5cm] at (0,-2) {
        \begin{itemize}
        \item $r\approx a$
        \item $e$, $h$ am selben Ort
        \item $e$-$h$-Paar stark gebunden
        \item $E_B \approx \SI{1}{\eV}$
        \end{itemize}
      };
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \node at (0,2.5) {Mott-Wannier};
      \draw (-2,-2) rectangle (2,2);
      \foreach \x in {-1.5,-1,...,1.5} {
        \foreach \y in {-1.5,-1,...,1.5} {
          \node[dot] at (\x,\y) {};
        }
      }
      \draw[decorate,decoration=brace] (1.6,1.5) -- node[right] {$a$} (1.6,1);
      \draw[MidnightBlue] (0,0) circle (1.6);
      \node[MidnightBlue,eh] (plus) at (50:1.6) {$+$};
      \node[MidnightBlue,eh] at (50:-1.6) {$-$};
      \draw[MidnightBlue,->] (0,0) -- node[below right] {$r$} (plus);
      \node[below,text width=4.5cm] at (0,-2) {
        \begin{itemize}
        \item $r \gg a$
        \item $E_B \propto \SI{1}{\milli\eV}\ldots\SI{50}{\milli\eV}$
        \end{itemize}
      };
    \end{scope}
  \end{tikzpicture}
  \caption{Vergleich von Frenkel- und Mott-Wannier-Exzitonen.}
  \label{fig:27.1}
\end{figure}

\paragraph{Frenkel-Exzitonen}
Auf Grund der Coulomb- und Austausch-Wechselwirkung $J$ findet eine
Energieübertrag zwischen den Ionen (z.B.\ \ch{Cl^-} bei \ch{NaCl})
statt.  Dies ermöglicht den Transport von Anregungsenergie durch den
Festkörper.  Frenkel-Exzitonen besitzen in der Regel starke
Wechselwirkung mit Licht.

\paragraph{Mott-Wannier-Exzitonen}
Aus der Schrödingergleichung lässt sich für parabolische,
nicht-entartete, isotrope Leitungs- und Valenzbänder eine
effektive-Masse-Gleichung für die Wellenfunktion der
Mott-Wannier-Exzitonen ableiten
\begin{equation}
  \label{eq:13.58}
  \left( - \frac{\hbar^2}{2 m_e^*} \nabla_e^2 - \frac{\hbar^2}{2 m_h^*} \nabla_h^2
    - \frac{e^2}{4 \pi \varepsilon_0 \varepsilon} \frac{1}{|r_e - r_h|} \right)
  \psi(\bm r_e,\bm r_h) = (E_{\text{Ges}} - E_g) \psi(\bm r_e,\bm r_h) \; .
\end{equation}
Die Einführung von Relativ- und Schwerpunktskoordinaten führt zu einer
Faktorisierung in zwei Gleichungen.  Die Energieeigenwerte spalten
ebenfalls in zwei Anteile auf
\begin{equation}
  \label{eq:13.59}
  E_{\text{Ges}} = E_g
  + \overbrace{\frac{\hbar^2 K^2}{2 M}}^{\mathclap{\text{Schwerpunktsbewegung}}}
  - \underbrace{\frac{1}{\varepsilon^2} \left( \frac{m_{\text{red}}}{m_0} \right)
      \frac{R_y}{n^2}}_{\text{Relativbewegung}}
\end{equation}
mit $K = k_e + k_h$ und $M = m_e + m_h$. Die reduzierte Masse $m_{\text{red}}$ ist gegeben durch
\begin{equation*}
  \frac{1}{m_{\text{red}}} = \frac{1}{m_e} + \frac{1}{m_h} \; .
\end{equation*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[axis on top,enlargelimits=false,width=6cm,
      xlabel={Wellenzahl $k$},ylabel={Energie $E$},
      xmin=-1.5,xmax=1.5,ymin=-2,ymax=.5,
      xtick=\empty,ytick=\empty,smooth]
      \pgfplotsinvokeforeach{1,2,3}{
        \addplot[MidnightBlue!20,domain=-2:-sqrt(1/#1)] {x^2-1/#1};
        \addplot[MidnightBlue,domain=-sqrt(1/#1):sqrt(1/#1)] {x^2-1/#1};
        \addplot[MidnightBlue!20,domain=sqrt(1/#1):2] {x^2-1/#1};
      }
      \draw (axis cs:-2,0) -- (axis cs:2,0);
      \draw (axis cs:0,-2) -- (axis cs:0,.5);
      \draw[DarkOrange3,<->] (axis cs:0,-2) -- node[right] {$h\nu$} (axis cs:0,-1);
      \draw[dashed] (axis cs:-1,-1) -- (axis cs:0,-1);
      \draw[<->] (axis cs:-.9,-1) -- node[left] {$E_x$} (axis cs:-.9,0);
    \end{axis}
  \end{tikzpicture}
  \caption{Energieschema der Exzitonen.  Im Grundzustand des Kristalls
    (unterer Rand des Graphen) sind keine Exzitonen vorhanden.  Wurde
    ein Exziton angeregt, so zerlegt sich dessen Bewegung in Relativ-
    und Schwerpunktsbewegung.}
  \label{fig:27.2}
\end{figure}

\subsection{Quantentheorie der Exzitonen}

Im Rahmen einer Quantenfeldtheorie des Festkörpers kann man die
Eigenschaften der Exzitonen auf intuitive Weise ableiten.  Im
Folgenden wollen wir dies für die beiden bereits vorgestellten
Exzitonen-Typen tun.  Die Ausführungen sind dem Buch von
\textcite{haken-qft} entnommen.

\subsubsection*{Exzitonen mit großem Bahnradius (Wannier-Exzitonen)}

Gehen wir aus von einem gefüllten Valenzband.  Außerdem befindet sich
im Valenzband eine Anzahl von Defektelektronen, im Leitungsband eine
Zahl von Elektronen.  Elektronen und Defektelektronen wechselwirken
miteinander gemäß dem Hamiltonoperator
\newcommand\ttop[2]{\vcenter{\hbox{$\genfrac{}{}{0pt}{2}{#1}{#2}$}}}
\begin{align*}
  H_{\text{tot}}
  &= W_{\text{voll}}
  + \sum_{\bm k}\left(\frac{\hbar^2 k^2}{2 m_L}+E_{0,L}\right)
  a_{\bm k}^\dagger a_{\bm k}
  + \sum_{\bm k}\left(\frac{\hbar^2 k^2}{2 m_V}-E_{0,V}\right)
  d_{\bm k}^\dagger d_{\bm k} \\
  &-\sum_{\bm k_1,\bm k_2,\bm k_3,\bm k_4}
  a_{\bm k_1}^\dagger a_{\bm k_4} d_{\bm k_3}^\dagger d_{\bm k_2}
  \bigl\{
    W(\ttop{\bm k_1}{L}\ttop{\bm k_2}{V}|\ttop{\bm k_3}{V}\ttop{\bm k_4}{L})
    -
    W(\ttop{\bm k_2}{V}\ttop{\bm k_1}{L}|\ttop{\bm k_3}{V}\ttop{\bm k_4}{L})
  \bigr\} \\
  &+ \frac12 \sum_{\bm k_1,\bm k_2,\bm k_3,\bm k_4}
  a_{\bm k_1}^\dagger a_{\bm k_2}^\dagger a_{\bm k_3} a_{\bm k_4}
  W(\ttop{\bm k_1}{L}\ttop{\bm k_2}{L}|\ttop{\bm k_3}{L}\ttop{\bm k_4}{L}) \\
  &+ \frac12 \sum_{\bm k_1,\bm k_2,\bm k_3,\bm k_4}
  d_{\bm k_3}^\dagger d_{\bm k_4}^\dagger d_{\bm k_1} d_{\bm k_2}
  W(\ttop{\bm k_1}{V}\ttop{\bm k_2}{V}|\ttop{\bm k_3}{V}\ttop{\bm k_4}{V}) \; .
\end{align*}
Der erste Term ist die Energie des gefüllten Valenzbandes.  Der zweite
und dritte Term beschreibt die kinetische Energie der Elektronen im
Leitungsband, bzw.\ Defektelektronen im Valenzband.  Als Nächstes
folgen die Wechselwirkungsterme.  Dazu sollten wir zunächst
klarstellen, wie die Matrixelemente definiert sind.
\begin{equation*}
  W(\ttop{\bm k_1}{j_1}\ttop{\bm k_2}{j_2}|\ttop{\bm k_3}{j_3}\ttop{\bm k_4}{j_4})
  = \int\diff^3 x\int\diff^3 x'\
  \varphi_{\bm k_1,j_1}^*(\bm x) \varphi_{\bm k_2,j_2}^*(\bm x')
  \frac{e^2}{\lvert \bm x-\bm x' \rvert}
  \varphi_{\bm k_3,j_3}(\bm x') \varphi_{\bm k_4,j_4}(\bm x) \; .
\end{equation*}
Die obere Reihe in $W$ bezieht sich also auf die $\bm k$-Vektoren,
während die untere Reihe den Bandindex angibt, also $j_i \in \{L,V\}$
für Leitungs- und Valenzband.  In der zweiten Zeile ist also die
Wechselwirkung eines Elektrons mit einem Defektelektron angegeben,
wobei in der geschweiften Klammer zwei Matrixelemente auftreten.  Das
erste beschreibt die Coulombsche Wechselwirkung, das zweite die
Coulombsche Austauschwechselwirkung.  Die beiden letzten Zeilen
beschreiben die Wechselwirkung der Elektronen im Leitungsband, bzw.\
Defektelektronen im Valenzband untereinander.  Für eine detaillierte
Herleitung des Hamiltonoperators siehe \textcite{haken-qft}.

Für diesen Hamiltonoperator gilt es die Schrödingergleichung zu lösen.
\begin{equation*}
  H_{\text{tot}} \Phi = E \Phi \; .
\end{equation*}
Dabei machen wir für $\Phi$ den Ansatz, dass sich nur ein Elektron im
Leitungsband und ein Defektelektronen im Valenzband befindet.  Diesen
Zustand erhalten wir durch Anwenden der entsprechenden Operatoren auf
die Wellenfunktion des vollen Valenzbandes $\Phi_V$.
\begin{equation*}
  a_{\bm k_1}^\dagger d_{\bm k_2}^\dagger \Phi_V \; .
\end{equation*}
Da Elektronen und Defektelektronen, wie bereits im Hamiltonoperator
notiert, miteinander wechselwirken werden Streuprozesse dazu führen,
dass Elektron und Defektelektron ihre Zustände $\bm k_1$ und $\bm k_2$
nicht beibehalten.  Daher summieren wir über alle möglichen $\bm k_1$
und $\bm k_2$ und gewichten die Terme mit Koeffizienten $c_{\bm
  k_1,\bm k_2}$.
\begin{equation*}
  \Phi = \sum_{\bm k_1,\bm k_2} c_{\bm k_1,\bm k_2}
  a_{\bm k_1}^\dagger d_{\bm k_2}^\dagger \Phi_V \; .
\end{equation*}
Dieser Zustand, mit einem Elektron im Leitungsband und einem
Defektelektronen im Valenzband wird \acct{Exziton} genannt.

Da in diesem Zustand nur ein Elektron im Leitungsband und ein
Defektelektron im Valenzband ist fallen die beiden letzten Zeilen des
Hamiltonoperators direkt weg.  Mathematisch gesehen werden einfach
zwei Vernichtungsoperatoren auf einen Zustand mit nur einem Erzeuger
angewendet, was Null ergibt.  Wir verschieben die Energie $E$ in der
Schrödingergleichung um $W_{\text{voll}}$, damit wir diesen Term nicht
weiter berücksichtigen müssen.  Wir können nun der Hamiltonoperator in
einen kinetischen und einen Wechselwirkungsterm zerlegen.
\begin{equation*}
  H_{\text{tot}} = H_{\text{kin}} + H_{\text{El-D}}
\end{equation*}
Der kinetische Anteil der daraus resultierenden Schrödingergleichung
ist offensichtlich
\begin{equation*}
  H_{\text{kin}} \Phi = \sum_{\bm k_1,\bm k_2} c_{\bm k_1,\bm k_2}
  \left(\frac{\hbar^2 k_1^2}{2 m_L}+\frac{\hbar^2 k_2^2}{2 m_V}+\const\right)
  a_{\bm k_1}^\dagger d_{\bm k_2}^\dagger \Phi_V
  \quad\text{mit}\quad
  \const = E_{0,L}-E_{0,V}\; .
\end{equation*}
Der Wechselwirkungsanteil kann umgeschrieben werden und es ergibt sich
\begin{equation*}
  H_{\text{El-D}} \Phi = - \sum_{\bm k_1,\bm k_2,\bm k_3,\bm k_4} c_{\bm k_3,\bm k_4}
  a_{\bm k_1}^\dagger d_{\bm k_2}^\dagger \Phi_V
  \bigl\{
    W(\ttop{\bm k_1}{L}\ttop{\bm k_4}{V}|\ttop{\bm k_2}{V}\ttop{\bm k_3}{L})
    -
    W(\ttop{\bm k_4}{V}\ttop{\bm k_1}{L}|\ttop{\bm k_2}{V}\ttop{\bm k_3}{L})
  \bigr\} \; .
\end{equation*}
Nehmen wir uns das erste Matrixelement vor, so gilt, wie oben beschrieben:
\begin{equation*}
  W(\ttop{\bm k_1}{L}\ttop{\bm k_4}{V}|\ttop{\bm k_2}{V}\ttop{\bm k_3}{L})
  = \int\diff^3 x\int\diff^3 x'\
  \varphi_{\bm k_1,L}^*(\bm x) \varphi_{\bm k_4,V}^*(\bm x')
  \frac{e^2}{\lvert \bm x-\bm x' \rvert}
  \varphi_{\bm k_2,V}(\bm x') \varphi_{\bm k_3,L}(\bm x) \; .
\end{equation*}
Für die Bandfunktionen $\varphi_{\bm k,j}(\bm x)$ setzen wir die
entsprechenden Blochwellenfunktionen
\begin{equation*}
  \varphi_{\bm k,j}(\bm x) = u_{\bm k,j}(\bm x) \ee^{\ii\bm k\cdot\bm x}
\end{equation*}
ein.  Wir entwickeln die gitterperiodische Funktion $u_{\bm k,j}(\bm
x)$ in $\bm k$ um Null herum.  Da wir Exzitonen mit großen Bahnradius
betrachten werden nur kleine Werte von $\bm k$ relevant sein.  Wir
behalten deshalb nur die führende Ordnung $\bm k=0$ der Entwicklung
und erhalten so
\begin{equation*}
  W(\ttop{\bm k_1}{L}\ttop{\bm k_4}{V}|\ttop{\bm k_2}{V}\ttop{\bm k_3}{L})
  = \int\diff^3 x\int\diff^3 x'\
  \ee^{-\ii\bm k_1\cdot\bm x} \ee^{-\ii\bm k_4\cdot\bm x'}
  \frac{e^2}{\lvert \bm x-\bm x' \rvert}
  \ee^{\ii\bm k_2\cdot\bm x'} \ee^{\ii\bm k_3\cdot\bm x}
  \lvert u_{0,L}(\bm x)\rvert^2 \lvert u_{0,V}(\bm x')\rvert^2 \; .
\end{equation*}
Da, wie bereits erwähnt, nur kleine Werte von $\bm k$ eine Rolle
spielen können wir die Exponentialfunktionen über eine Gitterzelle als
konstant ansehen.  Dies erlaubt uns das Integral über die Gitterzellen
zu mitteln.  Die Blochfunktionen sind über $V$ auf Eins normiert und
liefern daher den Faktor $1/V^2$.
\begin{equation*}
  W(\ttop{\bm k_1}{L}\ttop{\bm k_4}{V}|\ttop{\bm k_2}{V}\ttop{\bm k_3}{L})
  \approx \frac{1}{V^2} \int\diff^3 x\int\diff^3 x'\
  \ee^{-\ii\bm k_1\cdot\bm x} \ee^{-\ii\bm k_4\cdot\bm x'}
  \frac{e^2}{\lvert \bm x-\bm x' \rvert}
  \ee^{\ii\bm k_2\cdot\bm x'} \ee^{\ii\bm k_3\cdot\bm x} \; .
\end{equation*}
Es bleibt noch das zweite Matrixelement im Wechselwirkungsterm:
$W(\ttop{\bm k_4}{V}\ttop{\bm k_1}{L}|\ttop{\bm k_2}{V}\ttop{\bm
  k_3}{L})$.  Dieses stellt eine Austauschwechselwirkung dar, die auf
Grund des großen Abstandes jedoch komplett vernachlässigt werden darf.

Wir nehmen für die Wellenfunktion des Zwei-Teilchen-Systems
\begin{equation*}
  \psi(\bm x_1,\bm x_2) = \sum_{\bm k_1,\bm k_2} c_{\bm k_1,\bm k_2}
  \frac{1}{V} \ee^{\ii\bm k_1\cdot\bm x_1 - \ii\bm k_2\cdot\bm x_2}
\end{equation*}
an.  Multiplizieren wir die beiden Terme des Hamiltonoperators mit
\begin{equation*}
  \frac{1}{V} \ee^{\ii\bm k_1\cdot\bm x_1 - \ii\bm k_2\cdot\bm x_2}
\end{equation*}
und nutzen die Identitäten der Exponentialfunktion
\begin{equation*}
  k^2 \ee^{\ii\bm k\cdot \bm x} = - \nabla^2 \ee^{\ii\bm k\cdot \bm x}
\end{equation*}
und der Delta-Distribution
\begin{equation*}
  \frac{1}{V} \sum_{\bm k} \ee^{\ii\bm k\cdot (\bm x-\bm x')} = \delta(\bm x-\bm x')
\end{equation*}
aus, so erhalten wir die Gleichung
\begin{equation*}
  \left(
    \const - \frac{\hbar^2}{2 m_L} \nabla^2_1
    - \frac{\hbar^2}{2 m_V} \nabla^2_2
    - \frac{e^2}{\lvert \bm x_1-\bm x_2 \rvert}
  \right) \psi(\bm x_1,\bm x_2) = E \psi(\bm x_1,\bm x_2)
\end{equation*}
welche gerade die Form einer Zwei-Teilchen-Schördingergleichung mit
Coulombwechselwirkung hat.  Dieses Problem ist uns bekannt und wir
können es in Schwerpunkt- und Relativkoordinaten zerlegen.  Die
weitere Diskussion des Ergebnisses erfolgte bereits oben.

\subsubsection*{Frenkel-Exzitonen}

Nehmen wir an, dass Elektron und Defektelektron nicht weit voneinander
entfernt, sondern stark am gleichen Gitterplatz lokalisiert sind, so
bietet es sich an die Teilchen statt durch Blochfunktionen durch
Wannierfunktionen zu beschreiben.  Diese sind gegeben durch
\begin{equation*}
  w_{\bm\ell,j}(\bm x) \equiv w_j(\bm x-\bm \ell) \equiv
  \frac{1}{\sqrt{N}} \sum_{\bm k} \ee^{-\ii\bm k\cdot\bm\ell}
  \underbrace{\ee^{\ii\bm k\cdot\bm x} u_{\bm k,j}(\bm x)}_{\text{Blochfunktion}}
\end{equation*}
wobei $\bm\ell$ der Lokalisierungsort ist.  Der Index $j\in\{L,V\}$
bezeichnet wie im vorigen Abschnitt das Band.  Wir entwickeln die
Feldoperatoren nach den Wannierfunktionen:
\begin{align*}
  \psi(\bm x) &= \sum_{\bm\ell} a_{\bm\ell,V} w_V(\bm x-\bm\ell)
  + \sum_{\bm\ell} a_{\bm\ell,L} w_L(\bm x-\bm\ell) \; , \\
  \psi^\dagger(\bm x) &= \sum_{\bm\ell} a^\dagger_{\bm\ell,V} w^*_V(\bm x-\bm\ell)
  + \sum_{\bm\ell} a^\dagger_{\bm\ell,L} w^*_L(\bm x-\bm\ell) \; .
\end{align*}

Der Hamiltonoperator kann in einen kinetischen Term und einen
Wechselwirkungsterm zerlegt werden, also
\begin{equation*}
  H = H_0 + H_{\text{WW}} \; .
\end{equation*}
Durch Einsetzen der nach den Wannierfunktionen entwickelten
Feldoperatoren können die beiden Teile in zweiter Quantisierung
geschrieben werden.
\begin{align*}
  H_0 &= \sum_{\bm\ell,\bm m} a^\dagger_{\bm\ell,L} a_{\bm m,L} H_{\bm\ell,\bm m,L}
  + \sum_{\bm\ell,\bm m} a^\dagger_{\bm\ell,V} a_{\bm m,V} H_{\bm\ell,\bm m,V}  \; , \\
  H_{\text{WW}} &= \frac{1}{2} \sum_{\substack{
      \bm\ell_1,\bm\ell_2,\bm\ell_3,\bm\ell_4 \\
      j_1,j_2,j_3,j_4
    }} a^\dagger_{\bm\ell_1,j_1} a^\dagger_{\bm\ell_2,j_2} a_{\bm\ell_3,j_3} a_{\bm\ell_4,j_4}
  \hat W(\ttop{\bm\ell_1}{j_1}\ttop{\bm\ell_2}{j_2}\ttop{\bm\ell_3}{j_3}\ttop{\bm\ell_4}{j_4})
\end{align*}
mit den Matrixelementen
\begin{align*}
  H_{\bm\ell,\bm m,j} &= \int\diff^3x\ w_j^*(\bm x-\bm\ell)
  \left\{ -\frac{\hbar^2}{2m}\nabla^2 + V(x) \right\}
  w_j(\bm x-\bm m) \; , \\
  \hat W(\ttop{\bm\ell_1}{j_1}\ttop{\bm\ell_2}{j_2}\ttop{\bm\ell_3}{j_3}\ttop{\bm\ell_4}{j_4})
  &= \int\diff^3 x\int\diff^3 x'\
  w_{j_1}^*(\bm x-\bm\ell_1) w_{j_2}^*(\bm x'-\bm\ell_2)
  \frac{e^2}{\lvert \bm x-\bm x' \rvert}
  w_{j_3}(\bm x'-\bm\ell_3) w_{j_4}(\bm x-\bm\ell_4) \; .
\end{align*}
Wir benötigen noch zwei weitere Abkürzungen.  Die Energie der Elektronen im Leitungsband ist gegeben durch ihre kinetische Energie und außerdem durch ihre Coulombwechselwirkung mit dem vollen Valenzband.
\begin{equation*}
  H^{\text{eff}}_{\bm\ell,\bm m,L} = H_{\bm\ell,\bm m,L}
  + \delta_{\bm\ell,\bm m} \bigg\{ \sum_{\bm\ell'}
  \hat W(\ttop{\bm\ell'}{V}\ttop{\bm\ell}{L}\ttop{\bm\ell}{L}\ttop{\bm\ell'}{V})
  -
  \hat W(\ttop{\bm\ell}{V}\ttop{\bm\ell}{L}\ttop{\bm\ell}{V}\ttop{\bm\ell}{L})
  \bigg\} \; .
\end{equation*}
Da Defektelektronen im Leitungsband gleichbedeutend sind mit fehlenden
Elektronen im Valenzband ist ihre Energie durch den negativen Beitrag
der fehlenden Elektronen im Valenzband gegeben.
\begin{equation*}
  H^{\text{eff}}_{\bm\ell,\bm m,D} = - H_{\bm\ell,\bm m,V}
\end{equation*}
Mit diesen Umformungen lautet der gesamte Hamiltonoperator
\begin{align*}
  H &= \sum_{\bm\ell,\bm m} H^{\text{eff}}_{\bm\ell,\bm m,L} a_{\bm\ell}^\dagger a_{\bm m}
  + \sum_{\bm\ell,\bm m} H^{\text{eff}}_{\bm\ell,\bm m,D} d_{\bm\ell}^\dagger d_{\bm m} \\
  &- \sum_{\bm l,\bm l'} a_{\bm\ell'}^\dagger a_{\bm\ell'} d_{\bm\ell}^\dagger d_{\bm\ell}
  \hat W(\ttop{\bm\ell}{V}\ttop{\bm\ell'}{L}\ttop{\bm\ell'}{L}\ttop{\bm\ell}{V}) \\
  &+ \sum_{\bm l,\bm l'} a_{\bm\ell'}^\dagger a_{\bm\ell} d_{\bm\ell'}^\dagger d_{\bm\ell}
  \hat W(\ttop{\bm\ell}{V}\ttop{\bm\ell'}{L}\ttop{\bm\ell'}{V}\ttop{\bm\ell}{L})
  + \sum_{\bm\ell} H_{\bm\ell,\bm\ell,V}
\end{align*}
wobei sich alle Erzeugungs- und Vernichtungsoperatoren auf das
Leitungsband beziehen, d.h.\ $a^\dagger_{\bm\ell}$ erzeugt ein
Elektron am Ort $\ell$ im Leitungsband.  In der ersten Zeile sind die
unabhängigen Bewegungen der Elektronen und Defektelektronen
dargestellt.  In der zweiten Zeile findet sich einfach die
Coulombwechselwirkung zwischen Elektronen und Defektelektronen.  Die
erste Summe in der letzten Zeile beinhaltet den Term
\begin{equation*}
  a_{\bm\ell'}^\dagger a_{\bm\ell} d_{\bm\ell'}^\dagger d_{\bm\ell}
  = a_{\bm\ell'}^\dagger d_{\bm\ell'}^\dagger a_{\bm\ell} d_{\bm\ell} \; .
\end{equation*}
Dieser Term beschreibt die Vernichtung eines Elektron-Loch-Paares am
Ort $\bm\ell$ und dessen erneute Erzeugung am Ort $\bm\ell'$.  Dieser
Term ist also so etwas wie eine Bewegung des gebundenen Exzitons.
Beim allerletzten Term handelt es sich um die konstante Energie aller
anderen wechselwirkungsfreien Elektronen des Valenzbandes.

Beim Frenkel-Exziton sollen Elektron und Defektelektron stark
aneinander gebunden sein.  Nicht alle Terme des Hamiltonoperators sind
dieser Forderung zuträglich.  Die beiden unabhängigen Bewegungsterme
der Elektronen und Defektelektronen können das Exziton
auseinanderreißen.  Sie sollten also klein sein.  Der dritte Term
hingegen fördert die Anziehung der beiden Ladungen und verstärkt die
Bindung.  Der vierte Term schließlich ist, wie bereits erwähnt, der
Paartransport von Elektron und Defektelektron.

Ein Elektron, bzw.\ Defektelektron, bewegt sich nach der ersten Zeile
des Hamiltonoperators genau dann wenn $\bm\ell\neq\bm m$.  Dies ist
dann gegeben, wenn der Überlapp der Wannierfunktionen an Ort $\bm\ell$
und $\bm m$ klein ist.  Auch wenn keine Transport der einzelnen
Ladungsträger mehr möglich ist, so bewegt sich das Exziton doch als
ganzes.  Dieser Transport ist einzig und allein durch die Größe des
Matrixelements $\hat W(\ttop{\bm\ell}{V} \ttop{\bm\ell'}{L}
\ttop{\bm\ell'}{V} \ttop{\bm\ell}{L})$ gegeben.

\section{Optische Eigenschaften freier Ladungsträger}

In diesem Abschnitt beschäftigen wir uns mit Wechselwirkungen von
elektromagnetischen Wellen mit den freien Ladungsträgern in Metallen
und stark dotierten Halbleitern.  In teilbesetzten Bändern sind
Intrabandübergänge möglich.

Wir wollen die dielektrische Funktion herleiten.  Dazu betrachten wir
die eindimensionale Bewegung eines Elektrons im periodischen Feld.
Die Differentialgleichung dafür lautet
\begin{equation}
  \label{eq:13.60}
  m^* \ddot{u} + \frac{m^* \dot{u}}{\tau} = -e E(t) \; .
\end{equation}
Es existiert keine rücktreibende Kraft, daher ist $\omega_0 = 0$.
Weiterhin ist $\tau$ die Stoßzeit und $\sigma = n e^2 \tau/m^*$ die
Leitfähigkeit.  Das periodische Feld folgt der Dynamik $E(t) = E_0
\ee^{-\ii(\omega t - k x)}$.  Die Rechnung verläuft wie bei
\eqref{eq:13.22} und für die dielektrische Funktion folgt
\begin{equation}
  \label{eq:13.61}
  \varepsilon(\omega) = 1
  + \underbrace{\chi_{\text{el}}}_{\text{geb.\ $e^-$}}
  - \underbrace{\frac{ne^2}{\varepsilon_0 m^*}
    \frac{1}{\omega^2 + \ii\omega/\tau}}_{\text{freie $e^-$ aus \eqref{eq:13.60}}} \; .
\end{equation}
Die mittlere Stoßzeit der Elektronen in Metallen mittlerer Reinheit
liegt bei Zimmertemperatur in der Größenordnung von \SI{e-14}{\s}.
Die Dämpfung kann bei optischen Frequenzen vernachlässigt werden (für
eine Rechnung mit Dämpfung siehe \textcite{gross}).  Für optische
Frequenzen können wir \eqref{eq:13.61} vereinfachen
\begin{equation}
  \label{eq:13.62}
  \varepsilon(\omega)
  = \underbrace{\varepsilon_{\infty}}_{\mathclap{\text{Beitrag der geb.\ $e^-$}}}
  - \frac{ne^2}{\varepsilon_0 m^*} \frac{1}{\omega^2}
  = \varepsilon_{\infty}\left(1 - \frac{\omega_p^2}{\omega^2}\right)
\end{equation}
mit der \acct{Plasmafrequenz}
\begin{equation}
  \label{eq:13.63}
  \boxed{
    \omega_p^2 = \frac{n e^2}{\varepsilon_0 \varepsilon_\infty m^*}
  } \; .
\end{equation}

\subsection{Ausbreitung elektromagnetischer Wellen in Metallen}

Für elektromagnetische Wellen gilt die Wellengleichung
\eqref{eq:13.45}.  Setzen wir eine ebene Welle der Form $E(t) = E_0
\ee^{-\ii(\omega t-kx)}$ an, erhalten wir die Dispersionsbeziehung
\begin{equation}
  \label{eq:13.64}
  \varepsilon(\omega) \omega^2 = c^2 k^2 \; .
\end{equation}
Setzen wir nun die in \eqref{eq:13.62} gefundene dielektrische
Funktion für optische Frequenzen in \eqref{eq:13.64} ein, so folgt
\begin{equation}
  \label{eq:13.65}
  \varepsilon_\infty \omega^2 \left(1 - \frac{\omega_p^2}{\omega^2}\right) = c^2 k^2 \; .
\end{equation}
In dieser Funktion treten zwei Bereiche mit unterschiedlichen
Eigenschaften auf.  Im Bereich $\omega < \omega_p$ ist offensichtlich
$k^2 < 0$, also die Wellenzahl komplex.  Es ist keine Ausbreitung von
Wellen möglich.  Für $\varepsilon(\omega) < 0$ findet also
Totalreflexion statt.  Im Bereich $\omega > \omega_p$ ist hingegen
immer $\varepsilon(\omega) > 0$.  Wenn wir \eqref{eq:13.65} dann nach
$\omega$ umstellen finden wir
\begin{equation}
  \label{eq:13.66}
  \omega^2 = \omega_p^2 + \frac{c^2 k^2}{\varepsilon_\infty} \; .
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[axis on top,enlargelimits=false,width=6cm,
      domain=0:3,legend pos=outer north east,
      xlabel={Wellenzahl $ck/\omega_p$},ylabel={Frequenz $\omega/\omega_p$}]
      \fill[MidnightBlue!20] (axis cs:0,0) rectangle (axis cs:3,1);
      \draw (axis cs:0,1) -- (axis cs:3,1);
      \addplot[MidnightBlue] {sqrt(1+x^2)};
      \addplot[DarkOrange3] {x};
      \legend{$\sqrt{\smash[b]{\omega_p^2}+c^2 k^2}$,$c k$};
    \end{axis}
  \end{tikzpicture}
  \caption{Dispersionsrelation von elektromagnetischen Wellen in
    Metallen.  Der verbotene Bereich, in dem keine Propagation von
    Wellen möglich ist, ist hellblau hinterlegt.}
  \label{fig:27.3}
\end{figure}

\subsection{Longitudinale Schwingungen des Elektronengases: Plasmonen}

Im Gas freier Elektronen sind longitudinale Schwingungen möglich, die
allerdings nicht an elektromagnetische Wellen ankoppeln.  Das liegt
daran, dass die Felder senkrecht aufeinander stehen.  Ein
Durchstrahlen von Metallfilmen mit Elektronen führt zur Anregung von
Plasmaschwingungen.  Im Grenzfall $k\to 0$ schwingen alle Elektronen
und Ionen gegeneinander und es baut sich eine Auslenkung $u$ wie in
Abbildung~\ref{fig:27.4} auf.  An den Rändern der Probe baut sich
durch den Überhang der durch die Auslenkung entsteht eine
Flächenladung auf.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \fill[fill=MidnightBlue!20] (-2,-1.5) rectangle (2,-1);
    \fill[fill=MidnightBlue!40] (-2,1) rectangle (2,1.5);
    \draw (-2,-1.5) rectangle (2,1.5);
    \draw (-2,1) -- (2,1);
    \foreach \i in {-1.5,-1,...,1.5} {
      \node[MidnightBlue] at (\i,1.25) {$-$};
      \node[MidnightBlue] at (\i,-1.25) {$+$};
    }
    \draw[MidnightBlue,->] (0,-.5) -- node[right] {$E$} (0,.5);
    \node[right] at (2,1.25) {nur Elektronen};
    \node[right] at (2,-1.25) {nur Ionen};
    \draw (-2,-1) -- (2,-1);
    \draw[<->] (-2.2,-1.5) -- node[left] {$u$} (-2.2,-1);
  \end{tikzpicture}
  \caption{Im Grenzfall $k\to0$ schwingen alle Elektronen und Ionen
    gegeneinander.  Die Auslenkung $u$ der beiden gegeneinander
    bewirkt eine Flächenladung des Überhangs und damit ein
    elektrisches Feld.}
  \label{fig:27.4}
\end{figure}

Die Flächenladung verursacht ein elektrisches Feld
\begin{equation}
  \label{eq:13.67}
  E = \frac{neu}{\varepsilon_0 \varepsilon_\infty}
\end{equation}
$\varepsilon_\infty$ berücksichtigt auch die Polarisierbarkeit der
Rumpf-Elektronen.  Die Bewegungsgleichung der freien Elektronen (ohne
Dämpfung) lautet
\begin{equation}
  \label{eq:13.68}
  nm\ddot u(t) = -neE(t) = - \frac{n^2 e^2 u(t)}{\varepsilon_0\varepsilon_\infty}
\end{equation}
oder
\begin{equation*}
  \ddot u + \omega_p^2 u = 0 \; .
\end{equation*}
Dies entspricht einem harmonischer Oszillator mit der Plasmafrequenz
$\omega_p$.  Die gleiche Schwingungsfrequenz erhält man auch aus
\eqref{eq:13.62} mit $\varepsilon(\omega_\ell = \omega_p) = 0$.  Wie
bei den Phonon-Polaritonen stellt auch hier die Frequenz der
longitudinalen Schwingung eine untere Grenze für die Ausbreitung von
elektromagnetischen Wellen dar.

Die Plasmaschwingungen sind kohärente, kollektive Anregungen aller
Elektronen des Fermigases.  Die Anregungen sind quantisiert mit
$\hbar\omega_p$ und werden Plasmonen genannt.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: