\minisec{Meißner-Ochsenfeld-Effekt}

Ein Supraleiter hat wie ein idealer Leiter einen Widerstand von $R=0$.
Nun stellt sich die Frage ob der Supraleiter mehr kann als ein idealer
Leiter.  Dazu betrachten wir das Verhalten im Magnetfeld.  Wir denken
uns dazu einen idealen Leiter, der unterhalb der Sprungtemperatur
$T_C$ den Widerstand $R=0$ hat.  Sein Verhalten vergleichen wir mit
dem, was man für einen Supraleiter beobachtet.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node[above] at (0,1.5) {$T>T_C$};
      \draw (0,0) ellipse (.5 and 1);
      \foreach \i in {-.6,-.4,...,.7} {
        \draw[MidnightBlue,->] (\i,-1.5) -- (\i,1.5);
      }
      \node[MidnightBlue,left] at (-.7,0) {$\bm B_a$};
      \node[MidnightBlue,right] at (.7,0) {$\bm B_i = \bm B_a$};
    \end{scope}
    \begin{scope}[shift={(5,0)},curved/.style={MidnightBlue,->,out=90,in=-90,looseness=.8}]
      \node[above] at (0,1.5) {$T<T_C$};
      \draw[name path=A] (0,0) ellipse (.5 and 1);
      \foreach \y in {-.5,0,.5} {
      \path[name path=B] (0,\y) coordinate (A) -- (1,\y);
      \draw[DarkOrange3,name intersections={of=A and B,name=i}]
      let \p1=(A), \p2=(i-1), \n1={veclen(\x1,\x2)} in (i-1) arc(0:-180:{\n1} and {.2*\n1});
      \draw[DarkOrange3,dashed,name intersections={of=A and B,name=i}]
      let \p1=(A), \p2=(i-1), \n1={veclen(\x1,\x2)} in (i-1) arc(0:180:{\n1} and {.2*\n1});
      }
      \draw[curved] (-.4,-1.5) to (-.7,0) to (-.4,1.5);
      \draw[curved] (-.2,-1.5) to (-.6,0) to (-.2,1.5);
      \draw[curved] (.4,-1.5) to (.7,0) to (.4,1.5);
      \draw[curved] (.2,-1.5) to (.6,0) to (.2,1.5);
      \node[MidnightBlue,left] at (-.7,0) {$\bm B_a$};
      \node[MidnightBlue,right] at (.7,0) {$\bm B_i = 0$};
      \coordinate[pin={[DarkOrange3]above right:Oberflächenströme}] (O) at (i-1);
    \end{scope}
  \end{tikzpicture}
  \caption{Durchflutung eines Leiters nach dem Meißner-Ochsenfeld-Effekt}
  \label{fig:10.1}
\end{figure}

Werden der ideale Leiter und der Supraleiter erst abgekühlt und dann
ein äußeres Magnetfeld angelegt, so verdrängen beide das Feld aus dem
Inneren, da an der Probenoberfläche Ströme induziert werden, die das
Feld abschirmen.  Die Ströme fallen mangels Widerstand nie ab und das
Magnetfeld kann nicht eindringen.  Schaltet man das Feld wieder ab, so
stellt sich der Anfangszustand wieder ein.

Nun schalten wir zuerst das Magnetfeld an und kühlen den idealen
Leiter und den Supraleiter erst danach unter die Sprungtemperatur.
Bei hoher Temperatur kann das Feld beide Leiter durchfluten, da die
Abschrimströme abfallen.  Das Feld bleibt konstant und die Temperatur
wird abgesenkt.  Da sich das Feld nicht ändert werden keine weiteren
Abschirmströme induziert und das Feld durchflutet den idealen Leiter
weiterhin.  Schalten wir das Feld nun ab, so werden Ströme induziert
(gemäß der Lenzschen Regel), die ein entgegengesetztes Magnetfeld
aufbauen.  Beim Supraleiter sieht es anders aus.  Wird dieser bei
aktivem Feld unter die Sprungtemperatur gekühlt, so wird das Feld
verdrängt.  Schalten wir das Feld beim Supraleiter wieder ab, so
stellt sich der Anfangszustand wieder ein.

Es zeigt sich also, dass der Endzustand des idealen Leiters nach dem
Zyklus \enquote{Feld anlegen} und \enquote{Abkühlen} von der
Reihenfolge dieser Operationen abhängt, während es beim Supraleiter
keine Rolle spielt.

Die Magnetfeldverdrängung bei der Sprungtemperatur wird
\acct{Meißner-Ochsenfeld-Effekt} genannt.

Ein Supraleiter verdrängt angelegte Magnetfelder, da die induzierten
Oberflächenströme nicht abfallen und ein Gegenfeld aufbauen, das dem
äußeren Feld
\begin{equation*}
  \bm B_a = \mu_0 \bm H
\end{equation*}
entgegenwirkt.  Im Inneren verschwindet das Feld durch die
Magnetsierung $\bm M$ des Supraleiters und es gilt
\begin{equation*}
  \bm B_i = \mu_0 (\bm H + \bm M) = \mu_0 (\bm H + \chi\bm H) = \mu_0 \bm H (1+\chi) = 0
\end{equation*}
Damit dies erfüllt ist muss
\begin{equation}
  \label{eq:10.2}
  \chi = -1
\end{equation}
sein.  Ein Supraleiter ist also ein idealer Diamagnet.  Wird das
äußere Feld erhöht, so bricht die Abschirmung bei einem kritischen
Feld $\bm B_C$ zusammen und es folgt ein Übergang in den Normalzustand
(Figur~\ref{fig:10.2}).

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$B_i$} |- (3,0) node[right] {$B_a$};
    \draw[dashed] (0,0) -- (1.5,1);
    \draw[MidnightBlue] (0,0) -- (1.5,0) node[below] {$B_C$} -- (1.5,1) -- (3,2);
  \end{tikzpicture}
  \qquad
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$-M$} |- (3,0) node[right] {$B_a$};
    \draw[MidnightBlue,fill=MidnightBlue!40] (0,0) -- (1.5,0) node[below] {$B_C$} -- (1.5,1) -- cycle;
  \end{tikzpicture}
  \caption{Verlauf des inneren Magnetfeldes $\bm B_i$ (links) und der
    Magnetisierung (rechts) in Abhängigkeit des äußeren Feldes $\bm
    B_a$.  Überhalb des kritischen Feldes $\bm B_C$ bricht die
    Verdrängung zusammen und der Normalzustand tritt wieder ein.}
  \label{fig:10.2}
\end{figure}

Empirisch ergibt sich für das kritische Feld in Abhängigkeit von der
Temperatur
\begin{equation}
  \label{eq:10.3}
  B_C(T) = B_C(0) \left[ 1 - \left( \frac{T}{T_C} \right)^2 \right] \; .
\end{equation}
$B_C$ umfasst hierbei externe Felder $\bm B_a$ und interne Felder $\bm
B_i$.  Das innere Feld $B_i$ wird aufgebaut wenn der Supraleiter von
einem Strom durchflossen wird.  Bei einem Draht mit Radius $R_0$ ist
$B_i$ an der Oberfläche
\begin{equation*}
  B_i = \frac{\mu_0}{2 \pi} \frac{I_0}{R_0}
\end{equation*}
Es muss gelten $B_i + B_a \le B_C(T)$, das heißt die supraleitende
Phase stellt sich nur im Bereich unter der Kurve in
Abbildung~\ref{fig:10.3} ein.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$B_C$} |- (3,0) node[right] {$T$};
    \draw[MidnightBlue] (0,1.5) arc (90:0:1.5);
    \node[pin={above right:$B_C(T)$}] at (30:1.5) {};
    \node at (1,.5) {SL};
    \node at (1,1.7) {NL};
  \end{tikzpicture}
  \caption{Verlauf des kritischen Feldes in Abhängigkeit der
    Temperatur.  Unterhalb der Kurve befindet sich der Supraleiter im
    supraleitenden Zustand, überhalb im normalleitenden.}
  \label{fig:10.3}
\end{figure}

Supraleitung kann durch zu hohen Strom unterbrochen werden. Es gibt
einen Grenzstrom $I > I_C$ bei dem die Supraleitung
verschwindet. ($I_C = \num{5}\ldots\SI{200}{\A}$ für $T =
\SI{0}{\kelvin}$).

\section{Grundkenntnisse über Supraleitung}

Hier werden nun einige Fakten zusammengestellt, die teilweise später
noch eine weitergehende Erklärung erhalten.  Elementsupraleiter sind
vor allem Nichtübergangsmetalle sowie einige Übergangsmetalle mit
nicht gefüllten inneren Schalen.  Ferromagneten sind keine
Supraleiter.  Verbindungs-Supraleiter haben eine höhere
Sprungtemperatur.  Die Sprungtemperatur von Element-Supraleitern
befindet sich im Bereich von $T_C \le \SI{10}{\kelvin}$ für
Verbindungs-Supraleiter hingegen $T_C \ge \SI{20}{\kelvin}$.
Hochtemperatur-Supraleiter kommen bis $T_C \approx \SI{135}{\kelvin}$.
$T_C$ ist mit der Masse der Gitterbausteine verknüpft.
\begin{equation}
  \label{eq:10.4}
  T_C \sim \frac{\const}{\sqrt{m}} \; .
\end{equation}
Diese Tatsache bezeichnet man als \acct{Isotopieeffekt}.  Die
Wechselwirkung, die bei der Supraleitung auftritt muss etwas mit der
Masse der Atomrümpfe zu tun haben. Phononen spielen also eine Rolle.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (-1,0) -- (1,0);
    \draw (0,0) -- (0,-3);
    \draw (-.2,-1.5) rectangle (.2,-1.2);
    \draw[arrow inside] (2,-.5) node[right] {Lichtzeiger} -- (0,-1.35);
    \draw[arrow inside] (0,-1.35) -- (2,-1.85);
    \node[pin={left:Torsionsfaden}] at (0,-.3) {};
    \draw[->,DarkOrange3] (-1,-.8) -- ++(-60:1);
    \draw[->,DarkOrange3] (-1.2,-.8) -- ++(-60:1);
    \draw[->,DarkOrange3] (-1.4,-.8) -- node[left] {$\bm B_M$} ++(-60:1);
    \begin{scope}[shift={(0,-3)}]
      \draw (-2,-.5) -- (2,.5);
      \draw (-2,-1) -- (2,0);
      \draw (-2,-.75) circle (.125 and .25);
      \draw (2,0) arc (-90:90:.125 and .25);
      % 
      \draw[MidnightBlue] (-1,-.1) -- (1,.4);
      \draw[MidnightBlue] (-1,-.9) -- (1,-.4);
      \draw[MidnightBlue] (-1,-.1) arc(90:-90:.2 and .4);
      \draw[MidnightBlue,dashed] (-1,-.1) arc(90:270:.2 and .4);
      \draw[MidnightBlue] (1,-.4) arc (-90:90:.2 and .4);
      % 
      \node[below right] at (2,0) {SiO$_2$ Quarzröhrchen};
      \node[MidnightBlue,below right] at (1,-.4) {Pb};
      % 
      \draw[<->] (2.2,.5) -- node[right] {\SI{10}{\micro\m}} (2.2,0);
      % 
      \draw[->,DarkOrange3] (-4,-1.1) -- node[below] {$\bm B_e$} (-3,-.85);
    \end{scope}
  \end{tikzpicture}
  \caption{Versuchsaufbau von Doll und Nähbauer zur Feldquantisierung}
  \label{fig:10.4}
\end{figure}

Bei der Messung sehr kleiner Abschirmströme stellte sich heraus, dass
sie gequantelt sind. Siehe dazu das Experiment von Doll und Nähbauer
in Abbildung~\ref{fig:10.4}.  Dabei wird eine Quarzröhrchen in einen
supraleitenden Hohlzylinder aus Blei gesetzt.  Bei $T > T_C$ wird
$B_e$ eingeschaltet, wobei $B_e$ auch durch die Röhrchenmitte geht.
Dann wird abgekühlt auf $T < T_C$, sodass die Feldverdrängung aus dem
supraleitenden Bleizylinder stattfindet, nicht jedoch aber aus dem
Quartzröhrchen.  Wird nun $B_e$ ausgeschaltet wird das Feld im
Quarzröhrchen eingefroren, da die Feldlinien nicht aus dem
Quartzröhrchen austreten können.  Der magnetische Fluss ist $\Phi_e =
B_e A$ mit der Querschnittsfläche $A$ des Röhrchens ($\approx
\SI{7.5}{\angstrom}$).  Der Bleizylinder mit dem Quarzröhrchen wirkt
nun als magnetischer Dipol $\mu$ im Feld $B_M$ und erfährt ein
Drehmoment $\bm D = \bm \mu \times \bm B$.  Im Prinzip ist eine
statische Magnetisierung möglich, die es erlaubt $\Phi_e$ zu
bestimmen.

Die dynamische Methode ist jedoch sehr viel empfindlicher.  Hier wird
ein Wechselfeld $B_M = B_{M,z} \sin\omega t$ angelegt und die
Resonanzamplitude der so erzwungenen Torsionsschwingung bestimmt.
Daraus lässt sich dann berechnen $\Phi_e$.

Es zeigt sich dass der Fluss quantisiert ist und zwar mit
\begin{equation}
  \label{eq:10.5}
  \Phi_e = n \Phi_0 = n \frac{h}{2e}
\end{equation}
Das elementare Flussquant ist $h/e$.  Hier ist der Fluss jedoch
proportional zu $h/2e$.  Daraus schließt man, dass die Abschrimströme
nicht von Einzelelektronen, sondern von Elektronen-Paaren mit der
Ladung $2e$, den sogenannten Cooper-Paaren, getragen werden.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$c_V$} |- (3,0) node[right] {$T$};
    \draw[dashed] (0,0) -- (2,2*2/3);
    \draw[dotted] (2,2*2/3) -- (2,0) node[below] {$T_C$};
    \draw[MidnightBlue] (0,0) to[bend right] (2,1.8) -- (2,2*2/3) -- (3,2);
  \end{tikzpicture}
  \caption{Die spezifische Wärme im Supraleiter folgt einer
    exponentiellen Abhängigkeit.  Überhalb der Sprungtemperatur geht
    sie wieder in den normalleitenden Zustand über, der gestrichelt
    eingezeichnet ist.  Da bei tiefen Temperaturen der Beitrag der
    Phononen zur spezifischen Wärme vernachlässigbar ist, ist hier nur
    der Beitrag der Elektronen $\sim\gamma T$ eingezeichnet.}
  \label{fig:10.5}
\end{figure}

Auch die spezifische Wärme hat im Supraleiter einen anderen Verlauf
als im Normalleiter.  Für Normalleiter gilt $C_e(\text{NL}) \sim T$
mit $C = \gamma T + \alpha T^3$, wobei der Term $\gamma T$ der Beitrag
der Elektronen und $\alpha T^3$ der Beitrag der Phononen ist.  Beim
Supraleiter beobachtet man eine exponentielle Abhängigkeit
\begin{equation}
  \label{eq:10.6}
  C_e(\text{SL}) \sim \ee^{-\Delta/\kB T} \; .
\end{equation}
Dies lässt vermuten, dass die Anregung der Elektronen über eine
Energielücke erreicht wird.  Bei der Sprungtemperatur springt die
spezifische Wärme vom exponetiellen Verlauf auf den Verlauf der
normalleitenden Phase (siehe Figur~\ref{fig:10.5}).  Es handelt sich
also um einen Phasenübergang zweiter Ordnung.  Dabei tritt keine
latente Wärme auf aber ein Sprung in $c_V$.  Der Sprung folgt daraus,
dass die Entropie der Elektronen im supraleitenden Zustand kleiner als
im normalleitenden ist, $S(\text{SL}) < S(\text{NL})$.  Der
supraleitende ist also ein geordneter Zustand.

\minisec{Theoretischer Überblick}

Ein theoretisches Verständnis der Phänomene, die mit der Supraleitung
verbunden sind, wird auf verschiedene Arten gewonnen.  Es gibt den
Ansatz über phänomenologische Gleichungen zu denen die Londonschen
Gleichungen (Ergänzungen zu den Maxwellgleichungen) (1930) und die
Landau-Ginzburg-Gleichungen (1950) gehören.  Vollständig ist jedoch
nur die Quantentheorie des Supraleiters nach Bardeen, Cooper und
Schriefer.

Bevor wir jedoch in die mathematische Beschreibung der Supraleitung
starten erinnern wir uns kurz was wir überhaupt von einer Theorie
erwarten.  Sie muss in jedem Fall die folgenden experimentellen
Tatsachen erklären:
\begin{enumerate}
\item Unterhalb der Sprungtemperatur $T_C$ gehen gewisse Stoffe vom
  normalleitenden in den supraleitenden Zustand über.
\item Im supraleitenden Zustand ist die Leitfähigkeit unendlich und
  damit der elektrische Widerstand null.
\item Die magnetische Induktion im Inneren ist im supraleitenden
  Zustand, unabhängig von der Vorgeschichte, null.
\end{enumerate}
Es ist außerdem wichtig, dass der verschwindende Widerstand und die
Feldverdrängung unabhängige Eigenschaften sind.

\section{Londonsche Gleichungen}

Zu den Maxwellschen Gleichungen fügten die Gebrüder London zwei
Materialgleichungen hinzu mit denen wir uns nun beschäftigen.

Die Elektronen des Fermi-Gases in einem Leiter bewegen sich gemäß der
Bewegungsgleichung des Drude-Modells
\begin{equation*}
  m \frac{\diff\bm v}{\diff t} = - e \bm E - m \frac{\bm v_d}{\tau} \; .
\end{equation*}
Für den idealen Leiter mit spezifischem Widerstand $\varrho = 0$ wird
der Stoßterm/Reibungsterm $m \bm v_d/\tau$ in den klassischen
Bewegungsgleichungen vernachlässigt.
\begin{equation}
  \label{eq:10.7}
  m \dot{\bm v} = -e \bm E \; .
\end{equation}
Für die Stromdichte gilt allgemein
\begin{equation}
  \label{eq:10.8}
  \bm j = -e n \bm v \; .
\end{equation}
Durch differenzieren und einsetzen von \eqref{eq:10.7} folgt die
1. Londonsche Gleichung
\begin{equation}
  \label{eq:10.9}
  \boxed{
    \frac{\diff\bm j_S}{\diff t} = \frac{n_S e_S^2}{m_S} \bm E
  } \; .
\end{equation}
Der Index $S$ steht für Suprastrom.  Wichtig ist, dass im Gegensatz
zum ohmschen Gesetz ($\bm j = \sigma \bm E$) nicht die Stromdichte,
sondern ihre zeitliche Ableitung proportional zur elektrischen
Feldstärke ist.  Für $E = 0$ ist $\partial_t j = 0$ und damit $j =
\const$, d.h.\ ein einmal angeworfener Strom fließt auch ohne
angelegtes Feld.  Einsetzen von \eqref{eq:10.9} in die Maxwellsche
Gleichung $\rot\bm E = - \dot{\bm B}$ liefert
\begin{equation}
  \label{eq:10.10}
  \frac{\partial}{\partial t} \left(
    \frac{m_S}{n_S e_S^2} \rot \bm j_S + \bm B \right) = 0
\end{equation}
Die Gleichung besagt, dass der Magnetfluss durch eine beliebige Fläche
innerhalb der Probe zeitlich unveränderlich ist. Da nach dem
Meißner-Effekt in einem Supraleiter aber das Magnetfeld und nicht nur
seine zeitliche Ableitung verschwindet, muss der Klammerausdruck
selbst verschwinden.
\begin{equation}
  \label{eq:10.11}
  \boxed{
    \rot \bm j_S = - \frac{n_S e_S^2}{m_S} \bm B
  }
\end{equation}
Dies ist die 2. Londonsche Gleichung.

Da Abschrimströme auch die Präsenz von Magnetfeldern erfordern muss das
Magnetfeld etwas eindringen. Betrachte dazu
\begin{equation*}
  \rot \bm B = \mu_0 \bm j_S \; .
\end{equation*}
Wir bilden auf beiden Seiten die Rotation und ersetzen $(\rot\rot = -
\nabla^2)$.
\begin{equation*}
  \rot\rot \bm B = - \nabla^2 \bm B = \mu_0 \rot \bm j_S \; .
\end{equation*}
Auf der rechten Seite setzen wir die 2. Londonsche Gleichung
\eqref{eq:10.11} ein und es ergibt sich
\begin{equation}
  \label{eq:10.12}
  \nabla^2 \bm B - \frac{\mu_0 n_S e_S^2}{m_S} \bm B = 0 \; .
\end{equation}
Für einen Supraleiter im Halbraum $x >0$ und das Magnetfeld in
$z$-Richtung lässt sich die Gleichung lösen mit
\begin{align}
  \label{eq:10.13}
  B_z(x) &= B_0 \, \ee^{-x/\lambda_L} \\
  \label{eq:10.14}
  j_{S,y} &= j_{S,0} \, \ee^{-x/\lambda_L} \\
  \intertext{mit der Londonschen Eindringtiefe}
  \label{eq:10.15}
  \lambda_L &= \sqrt{\frac{m_S}{\mu_0 n_S e_S^2}} \; .
\end{align}
Die Londonsche Eindringtiefe (Abbildung~\ref{fig:10.6}) ist für reale
Supraleiter in der Größenordnung $\lambda \approx \SI{15}{\nano\m}$.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[domain=-1:2,no marks,axis on top,width=6cm,
      enlargelimits=false,ymin=0,ymax=1.4,
      xlabel={Ortskoordinate $x$},ylabel={Magnetfeld $B_z$},
      xtick=\empty,ytick=\empty,
      extra x ticks={0,0.3678},extra x tick labels={$0$,$\lambda_L$},
      extra y ticks={0,1},extra y tick labels={$0$,$B_0$}
      ]
      \fill[MidnightBlue!20] (axis cs:0,0) rectangle (axis cs:2,1.4);
      \draw (axis cs:0,0) -- (axis cs:0,1.4);
      \node at (axis cs:-.5,1.3) {Vakuum};
      \node at (axis cs:1,1.3) {Supraleiter};
      \node[MidnightBlue] at (axis cs:1,.8) {$\sim \ee^{-x/\lambda_L}$};
      \addplot[MidnightBlue] {ifthenelse(x<0,1,exp(-x))};
    \end{axis}
  \end{tikzpicture}
  \caption{Das Magnetfeld wird nicht komplett abgeschirmt, sondern
    fällt im Inneren des Supraleiters ab.  Der exponentielle Abfall
    wird durch $\lambda_L$ bestimmt.}
  \label{fig:10.6}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: