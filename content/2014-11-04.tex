Zusätzlich können in piezoelektrischen Halbleitern (z.B.\ III--V- und
II--VI-Halbleitern) noch Beiträge von Streuung an Phononen herrühren,
die mit einer Polarisation behaftet sind, was sich in
piezoelektrischer Streuung und Streuung an optischen Phononen
ausdrückt.

In modernen Halbleiter-Bauelementen werden Felder $>
\SI{e5}{\V\per\m}$ erreicht. Das Ohmsche Gesetz gilt dann nicht mehr
und $v_D$ ist nicht mehr proportional zur Feldstärke
\begin{align*}
  \bm v_D \sim \bm E \quad\text{bis ca.\ \SI{2e3}{\V\per\m} (\ch{GaAs}, \ch{Si}, \ch{Ge})}
\end{align*}
Ladungsträger werden im äußeren Feld entlang der Energiebänder $E(\bm
k)$ beschleunigt, bis sie die Energie (bezogen auf $E_F$) optischer
Phononen mit hoher Zustandsdichte (etwa \SI{60}{\milli\eV} bei
\ch{Si}, \SI{36}{\milli\eV} bis \ch{GaAs}) erreichen. Dies führt zur
Anregung optischer Phononen und die Driftgeschwindigkeit $v_D$
sättigt.

Eine weitere Besonderheit bei direkten Halbleitern (\ch{GaAs},
\ch{InP}, \ch{GaN}) ist, dass ab einer kritischen Feldstärke
Elektronen in Seitentäler (L, X) gestreut werden. Es resultiert ein
negativer differentieller Widerstand, das bedeutet in den Seitentälern
ist die effektive Masse größer.

Für noch größere Felder führt dies zum sogenannten Lawinendurchbruch.
Das heißt, die beschleunigten Elektronen gewinnen so viel Energie,
dass sie weitere Elektronen aus dem Valenzband ins Leitungsband
anregen können.

\section{Inhomogene Halbleiter}

Das Verständnis von inhomogenen Halbleitern ist Voraussetzung zum
Verständnis der technischen Anwendung von Halbleitern.  Inhomogen
bedeutet hierbei, dass die chemische Zusammensetzung des Halbleiters
über seine Ausdehnung variiert.

\subsection{p-n-Übergang}

Im Folgenden betrachten wir abrupte Übergänge, also sprunghafte
Änderungen in der Dotierung.  Die Dotieratome werden dabei künstlich
eingebracht durch Diffusion oder Lithographie.

In Abbildung~\ref{fig:7.1} ist die Ausgangssituation für die
Behandlung des p-n-Übergangs dargestellt.  Dabei bezeichnen
$E_L^{\text{p}}$ und $E_L^{\text{n}}$ die Energie der
Leitungsbandkanten und $E_V^{\text{p}}$ und $E_V^{\text{n}}$ die Lage
der Valenzbandkanten des p- bzw.\ n-Halbleiters in genügend großer
Entfernung zum Übergang.  $E_F^{\text{p}}$ und $E_F^{\text{n}}$ steht
für die Lage der Fermi-Niveaus.  Diese liegt abhängig von der
Dotierung bei Zimmertemperatur entweder knapp überhalb des
Akzeptorniveaus oder knapp unterhalb des Donatorniveaus.  Bringt man
p- und n-Halbleiter in Kontakt so gleichen sich die Fermi-Niveaus an.
Dies geschieht durch Ladungsträgerausgleich.  Ladungsträger
diffundieren aus Gebiet höherer Konzentration in das niedrigerer
Konzentration.  Es fließen also Löcher vom p- in den n-Halbleiter und
Elektronen vom n- in den p-Halbleiter.  Unten in
Abbildung~\ref{fig:7.1} ist zu erkennen, dass sich dadurch eine
Spannung aufbaut.  Der Prozess provoziert eine Bandverbiegung, die
Anlass zu Strömen gibt, die der Diffusion entgegen wirken.  Aus der
Verbiegung von $E_L$ folgt ein Strom von Elektronen nach links, aus
der Verbiegung von $E_V$ folgt ein Strom von Löchern nach rechts.  Die
Verbiegung kann durch das ortsabhängige Potential $\tilde V(x)$
beschrieben werden.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \draw[<->] (0,3) node[above] {$E$} |- (1,0) node[below] {$x$};
    \end{scope}
    \begin{scope}[shift={(2,0)}]
      \node[above] at (1,3) {p-Halbleiter};
      \draw (0,0) rectangle (2,3);
      \draw[fill=MidnightBlue!50] (0,0) rectangle (2,.5);
      \draw[fill=MidnightBlue!20] (0,2.5) rectangle (2,3);
      \node[left] at (0,.5) {$E_V^{\text{p}}$};
      \node[left] at (0,2.5) {$E_L^{\text{n}}$};
      \draw[MidnightBlue,dashed] (0,.7) -- (2,.7) node[right] {$E_A$};
      \draw[DarkOrange3,dashed] (0,.9) node[left] {$E_F^{\text{p}}$} -- (2,.9);
    \end{scope}
    \begin{scope}[shift={(6,0)}]
      \node[above] at (1,3) {n-Halbleiter};
      \draw (0,0) rectangle (2,3);
      \draw[fill=MidnightBlue!50] (0,0) rectangle (2,.5);
      \draw[fill=MidnightBlue!20] (0,2.5) rectangle (2,3);
      \node[right] at (2,.5) {$E_V^{\text{n}}$};
      \node[right] at (2,2.5) {$E_L^{\text{n}}$};
      \draw[MidnightBlue,dashed] (0,2.3) node[left] {$E_D$} -- (2,2.3);
      \draw[DarkOrange3,dashed] (0,2.1) -- (2,2.1) node[right] {$E_F^{\text{n}}$};
    \end{scope}
    \begin{scope}[shift={(2,-4)}]
      \draw[fill=MidnightBlue!20] (0,3) -- (2,3) to[out=0,in=180] (4,2) -- (6,2) -- (6,3.5) -- (0,3.5) -- cycle;
      \foreach \i in {.2,.4,...,.8} {
	\path[Purple] (0,3) -- node[above,pos=\i] {$-$} (2,3) to[out=0,in=180] node[above,pos=\i] {$-$} (4,2) -- (6,2);
      }
      \draw[fill=MidnightBlue!50] (0,1) -- (2,1) to[out=0,in=180] (4,0) -- (6,0) -- (6,-.5) -- (0,-.5) -- cycle;
      \foreach \i in {.2,.4,...,.8} {
	\path[MidnightBlue] (0,1) -- node[below,pos=\i] {$\circ$} (2,1) to[out=0,in=180] node[below,pos=\i] {$\circ$} (4,0) -- node[below,pos=\i] {$\circ$} (6,0);
      }
      \draw (0,3) -- (0,1);
      \draw (6,2) -- (6,0);
      \draw[dotted] (0,3) -- (6,3);
      \draw[dotted] (3,2.5) -- (6,2.5);
      \draw[dotted] (0,2) -- (6,2);
      \draw[dotted] (3,3) -- (3,0);
      \draw[DarkOrange3,dashed] (0,1.5) -- (6,1.5) node[right] {$E_F$};
      \draw[<->] (1,3) -- node[right] {$-eV_D$} (1,2);
      \draw[<->] (4,2.5) -- node[right] {$-e\tilde V(x)$} (4,2);
    \end{scope}
  \end{tikzpicture}
  \caption{Oben ist die Ausgangssituation für unseren p-n-Übergang
    aufgezeigt.  Eingezeichnet sind ein p- und ein n-Halbleiter mit
    den entsprechenden Störstellenniveaus.  }
  \label{fig:7.1}
\end{figure}

Wir sind interessiert an der \acct{Diffusionsspannung} $V_D$, die sich
zwischen den Halbleitern aufbaut.  Die Diffusionsspannung ist durch
die Differenz $E_F^{\text{n}}-E_F^{\text{p}}$ der beiden Fermi-Niveaus der dotierten
Kristalle gegeben, also
\begin{equation*}
  e V_D = E_F^{\text{n}} - E_F^{\text{p}} \; .
\end{equation*}
Aus dem Abschnitt über undotierte Halbleiter wissen wir nach
\eqref{eq:9.7} und \eqref{eq:9.8}, dass
\begin{align*}
  N_D &= N_{\text{eff}}^L \ee^{-(E_L-E_F^{\text{n}})/\kB T} \; , \\
  N_A &= N_{\text{eff}}^V \ee^{(E_V-E_F^{\text{p}})/\kB T} \; .
\end{align*}
Stellen wir beides jeweils nach $E_F$ um und setzen dies ein in $e V_D
= E_F^{\text{n}} - E_F^{\text{p}}$ so erhalten wir
\begin{equation*}
  e V_D = E_L - \kB T \ln\left( \frac{N_{\text{eff}}^L}{N_D} \right)
  - E_V - \kB T \ln\left( \frac{N_{\text{eff}}^V}{N_A} \right) \; .
\end{equation*}
Im Beispiel der Störstellenerschöpfung gilt
\begin{equation*}
  \tag{\ref{eq:9.22}}
  n \approx N_D
\end{equation*}
und damit
\begin{equation*}
  e V_D = E_g - \kB T \ln\left( \frac{N_{\text{eff}}^L N_{\text{eff}}^V}{N_D N_A} \right) \; .
\end{equation*}
Da die Bandlücke $E_g$ weitaus größer ist als die thermische Energie
bei Raumtemperatur können wir in erster Näherung
\begin{equation*}
  e V_D = E_g
\end{equation*}
setzen.

Die Ladungsträger, die sich im durch ihre Dotierung vorgegebenen
Gebiet aufhalten heißen \acct{Majoritätsladungsträger}.  Also sind
Elektronen im n-Gebiet und Löcher im p-Gebiet Majoritätsladungsträger.
Diffundieren sie in das jeweils andere Gebiet heißen sie
\acct{Minoritätsladungsträger}.  Im Folgenden erhalten alle
Ladungsträger deshalb einen Index, der anzeigt in welchen Gebiet sie
sich befinden, also z.B.\ $n_{\text{n}}$ für Elektronen im n-Gebiet.

In großer Entfernung vom Übergang gilt
\begin{align*}
  n_{\text{n}} \approx N_D^+ \approx N_D \quad\text{bzw.}\quad p_{\text{p}} \approx N_A^- \approx N_A \; .
\end{align*}
Nach dem Massenwirkungsgesetz gilt
\begin{equation*}
  n_{\text{p}} \cdot p_{\text{p}} = n_{\text{n}} \cdot p_{\text{n}} = n_i \cdot p_i \; .
\end{equation*}
Bei üblichen Dotierungen ist die Majoritätsladungsträgerdichte sehr
viel größer, die Minoritätsladungsträger sehr viel kleiner als die
intrinsische Ladungsträgerdichte im Übergangsbereich.  Wie bereits
erwähnt wird der Potentialverlauf durch $-e\tilde V(x)$ beschrieben.
Wir können die Bandverbiegung daher schreiben als
\begin{align*}
  E_L^{\text{n}}(x) &= E_L - e \tilde V(x) \; , \\
  E_V^{\text{p}}(x) &= E_V - e \tilde V(x) \; .
\end{align*}
Eingesetzt in \eqref{eq:9.7} und \eqref{eq:9.8} erhalten wir
\begin{subequations}
  \begin{align}
    \label{eq:9.31a}
    n(x) &= N_{\text{eff}}^L \exp\left(-\frac{E_L-e\tilde V(x)-E_F}{\kB T}\right) \; , \\
    \label{eq:9.31b}
    p(x) &= N_{\text{eff}}^V \exp\left(-\frac{E_F-E_V+e\tilde V(x)}{\kB T}\right) \; .
  \end{align}
\end{subequations}
Dieser Verlauf ist in Abbildung~\ref{fig:7.2} schematisch dargestellt.
Für die Skizze wurde angenommen, dass $N_D > N_A$.  Die Störstellen
sind alle völlig ionisiert und die Dichte der Ladungsträger folgt dem
Massenwirkungsgestz
\begin{equation*}
  n(x) p(x) = \const \; .
\end{equation*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,3) node[above] {$\log n,p$} |- (6,0) node[below] {$x$};
    \foreach \i in {2,4} {
      \draw[dotted] (\i,3) -- (\i,0);
    }
    \node at (1,3) {p-HL};
    \node at (5,3) {n-HL};
    \draw[MidnightBlue,name path=A] (0,1.8) node[left] {$p_{\text{p}}$} -- (2.4,1.8) to[out=0,in=180] (4,.3) -- (6,.3) node[right] {$p_{\text{n}}$};
    \draw[DarkOrange3,name path=B] (0,.5) node[left] {$n_{\text{p}}$} -- (2.4,.5) to[out=0,in=180] (4,2.3) -- (6,2.3) node[right] {$n_{\text{n}}$};
    \begin{scope}[name intersections={of=A and B,name=i}]
      \begin{scope}[on background layer]
        \fill[MidnightBlue!40] (0,0) rectangle (2,2);
        \fill[MidnightBlue!20] (2,0) rectangle (i-1 |- 2,2);
        \fill[Purple!40] (4,0) rectangle (6,2.5);
        \fill[Purple!20] (i-1 |- 0,0) rectangle (4,2.5);
      \end{scope}
      \draw[dotted] (i-1 -| 0,0) -- (i-1) -- (i-1 -| 6,0) node[right] {$n_i,p_i$};
      \draw[dotted] (i-1 |- 0,0) node[below] {$x=0$} -- (i-1) -- (i-1 |- 0,3);
      \draw (0,2) -- (i-1 |- 0,2) node[above left] {$N_A^-$};
      \draw (i-1 |- 6,2.5) node[above right] {$N_D^+$} -- (6,2.5);
    \end{scope}
    \draw[decorate,decoration=brace] (4,-.5) -- node[below,text width=3cm,align=center] {Verarmungszone} (2,-.5);
  \end{tikzpicture}
  \caption{Bringt man einen p- und einen n-Halbleiter zusammen, so
    diffundieren die freien Ladungsträger in das Gebiet niedriger
    Konzentration.  Am Kontakt treffen aber Elektronen auf Löcher und
    rekombinieren.  Die Ladung der ionisierten Dotieratome kann dann
    mangels freier Ladungsträger lokal nicht mehr kompensiert werden
    und es baut sich eine Raumladung auf.}
  \label{fig:7.2}
\end{figure}

In der \acct{Verarmungszone} wird die Ladung der ionisierten Donatoren
und Akzeptoren nicht mehr durch die freien Ladungsträger
kompensiert. Es resultiert der Aufbau einer \acct{Raumladung}.  Für
deren Raumladungsdichte gilt
\begin{alignat}{4}
  \label{eq:9.32}
  \varrho(x) &{}={}&   & e [N_D^+ - n_{\text{n}}(x) + p_{\text{n}}(x) ] &\; , \quad x>0 \hateq \text{n-Gebiet} \; , \\
  \label{eq:9.33}
  \varrho(x) &{}={}& - & e [N_A^- + n_{\text{p}}(x) - p_{\text{p}}(x) ] &\; , \quad x<0 \hateq \text{p-Gebiet} \; .
\end{alignat}
Aus der Elektrodynamik wissen wir, dass wir das Potential $\tilde
V(x)$ des elektrischen Feldes über die Poisson-Gleichung aus der
Ladungsdichte erhalten können
\begin{equation}
  \label{eq:9.34}
  \frac{\partial^2}{\partial x^2} \tilde V(x) = - \frac{\varrho}{\varepsilon_0 \varepsilon_r} \; .
\end{equation}
Die Lösung muss über ein selbstkonsistentes Verfahren erfolgen, weil
die Raumladungsdichte selbst vom Potentialverlauf abhängt.  In der
Raumladungszone ist die Konzentration freier Ladungsträger gering und
wird daher vernachlässigt.  Wir machen die Näherung, dass der Verlauf
von $\varrho(x)$ durch einen rechteckigen Verlauf ersetzt wird. Dies
nennt man \acct{Schottky-Modell}.  Damit gehen die Gleichungen
\eqref{eq:9.32} und \eqref{eq:9.33} über in
\begin{align}
  \label{eq:9.35}
  \varrho(x) &=
  \begin{cases*}
    0 & für $x < -d_{\text{p}}$ \\
    -e N_A & für $-d_{\text{p}}<x<0$ \\
    e N_D & für $0 < x < d_{\text{n}}$ \\
    0 & für $d_{\text{n}} < x$ \\
  \end{cases*}
\end{align}
Dabei sind $d_{\text{n}}$ und $d_{\text{p}}$ die Dicken der
Raumladungszone. Für den n-leitenden Teil der Raumladungszone ($0 <
x < d_{\text{n}}$) lösen wir die Poisson-Gleichung.
\begin{align}
  \label{eq:9.36}
  \frac{\partial^2}{\partial x^2} \tilde V(x) &= - \frac{e N_D}{\varepsilon_0 \varepsilon_r} \\
  \label{eq:9.37}
  E_x = - \frac{\partial U(x)}{\partial x} &= - \frac{e N_D}{\varepsilon_0 \varepsilon_r} (d_{\text{n}}-x) \\
  \label{eq:9.38}
  \tilde V(x) &= \tilde V_{\text{n}}(\infty) - \frac{e N_D}{2 \varepsilon_0 \varepsilon_r} (d_{\text{n}}-x)^2
\end{align}
Der schematische Verlauf all dieser Größen, also $\varrho(x)$, $E(x)$
und $\tilde V(x)$, ist in Abbildung~\ref{fig:7.3} dargestellt.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[dotted] (2,6) -- (2,0) node[below] {$-d_{\text{p}}$};
    \draw[dotted] (4,6) -- (4,0) node[below] {$d_{\text{n}}$};
    \draw[<->] (0,1.9) node[left] {$-\tilde V(x)$} |- (6,0) node[below] {$x$};
    \draw[<->] (0,3.9) node[left] {$E(x)$} |- (6,2) node[below] {$x$};;
    \draw[<->] (0,5.9) node[left] {$\varrho(x)$} |- (6,4) node[below] {$x$};
    \draw[dashed] (0,3) -- (6,3) node[right] {$0$};
    \draw[dashed] (0,5) -- (6,5);
    \draw[DarkOrange3,name path=A] (0,1.5) node[left] {$\tilde V_{\text{p}}(-\infty)$}
    -- (2,1.5) to[out=0,in=180] (4,.3) -- (6,.3) node[right] {$\tilde V_{\text{n}}(+\infty)$};
    \path[name path=B] (0,.7) -- (6,.7);
    \begin{scope}[name intersections={of=A and B,name=i}]
      \draw[dashed] (i-1 -| 0,0) -- (i-1) -- (i-1 -| 6,0) node[right] {$0$};
      \draw (i-1 |- 0,0) node[below] {$x=0$} -- (i-1) -- (i-1 |- 0,6);
      \draw[DarkOrange3] (2,3) -- (i-1 |- 0,2.5) -- (4,3);
      \draw[MidnightBlue,fill=MidnightBlue!40] (2,5) -- (2,4.6) -- node[above] {$-$} (i-1 |- 0,4.6) --  (i-1 |- 0,5);
      \draw[MidnightBlue,fill=MidnightBlue!20] (i-1 |- 0,5) -- (i-1 |- 0,5.8) -- node[below] {$+$} (4,5.8) -- (4,5);
      \node[MidnightBlue,right] at (4,5.8) {$eN_D$};
      \node[MidnightBlue,right] at (4,4.6) {$eN_A$};
      \draw[Purple] (1.8,5) to[out=0,in=180] (2.2,4.6) -- (i-1 |- 0,4.6)-- (i-1 |- 0,5.8) -- (3.8,5.8) to[out=0,in=180] (4.2,5);
    \end{scope}
  \end{tikzpicture}
  \caption{Im Schottky-Modell werden die Ladungsdichten in der
    Raumladungszone durch Rechtecke genähert (realistischer Verlauf in
    violett).  Unterhalb sind der Verlauf des elektrischen Feldes und
    der Potentialverlauf aufgetragen.}
  \label{fig:7.3}
\end{figure}

Damit der p-n-Kontakt nach außen hin neutral ist sollten sich die
positive und die negative Raumladung wegheben.  Wir multiplizieren
daher die Raumladungsdichte mit der Dicke der Raumladungszone und
erhalten für die Neutralitätsbedingung
\begin{equation}
  \label{eq:9.39}
  N_D d_{\text{n}} = N_A d_{\text{p}} \; .
\end{equation}
Aus \eqref{eq:9.39} und der Stetigkeit von $\tilde V(x)$ bei $x = 0$
erhält man
\begin{align*}
  \frac{e}{2 \varepsilon_0 \varepsilon_r} (N_D d_{\text{n}}^2 + N_A d_{\text{p}}^2) =
  \tilde V_{\text{n}}(\infty) - \tilde V_{\text{p}}(\infty) = V_D \; .
\end{align*}
Damit lassen sich die Dicken $d_{\text{p}}$ und $d_{\text{n}}$ berechnen
\begin{align}
  \label{eq:9.40}
  d_{\text{n}} &= \sqrt{\frac{2 \varepsilon_0 \varepsilon_r V_D}{e} \frac{N_A/N_D}{N_A+N_D}} \; , \\
  \label{eq:9.41}
  d_{\text{p}} &= \sqrt{\frac{2 \varepsilon_0 \varepsilon_r V_D}{e} \frac{N_D/N_A}{N_A+N_D}} \; .
\end{align}

\begin{example}
  Typische Werte für die Bandlücke und die Störstellenkotentrationen
  sind $e V_D = E_g = \SI{1}{\eV}$ und $N_A = N_D =
  \num{e20}\ldots\SI{e24}{\per\cubic\m}$.  Damit ergeben sich für die
  Dicke der Raumladungszonen und die daraus resultierenden Feldstärken
  $d_{\text{n}} = d_{\text{p}} = \SI{1}{\micro\m}\ldots\SI{10}{\nano\m}$ und $E =
  \SI{e6}{\V\per\m}\ldots\SI{e8}{\V\per\m}$
\end{example}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: