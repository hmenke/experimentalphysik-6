Herrscht zwischen den Tunnelkontakten keine Spannung, so ist
offensichtlich
\begin{equation*}
  \hbar (\dot\varphi_1 - \dot\varphi_2) = 0 \; .
\end{equation*}
Daraus folgt natürlich sofort
\begin{equation*}
  \varphi_1 - \varphi_2 = \const \; .
\end{equation*}
Dies bedeutet konstante Argumente in den Winkelfunktionen in
\eqref{eq:10.31} und damit
\begin{equation*}
  \dot n_{S1} = - \dot n_{S2} \; .
\end{equation*}
Der Strom sollte zwischen den beiden Supraleitern fließen wobei
$n_{S1}$ und $n_{S2}$ konstant sind, sonst würde eine Aufladung der
Supraleiter erfolgen.
\begin{equation}
  \label{eq:10.34}
  \boxed{ I_S = I_j \sin(\varphi_2 - \varphi_1) }
\end{equation}
Es fließt ein Gleichstrom durch den Tunnelkontakt und an der
Isolatorschicht fällt keine Spannung ab. Dies ist der
\acct{Josephson-Gleichstrom-Effekt}. Der kritische Strom $I_j$ hängt
von der Dichte $n_S$ der Cooper-Paare, der Kontaktfläche $A$ (typisch
\SI{0.1}{\square\cm}) und von $\kappa$ ab.  Normalerweise liegt der
kritische Strom in der Gegend von $I_j \approx \SI{1}{\milli\A}$.

Erhöht man $U_{\text{ext}}$ so springt bei $I_j$ die Spannung am
Kontakt auf einen endlichen Wert
\begin{equation*}
  E_2 - E_1 = - 2 e U \; .
\end{equation*}
Integration von \eqref{eq:10.33} liefert
\begin{equation}
  \label{eq:10.35}
  \varphi_2 - \varphi_1 = \frac{2eU}{\hbar} t + \varphi_0 = \omega_j t + \varphi_0 \; .
\end{equation}
Einsetzen von \eqref{eq:10.35} in \eqref{eq:10.34} liefert
\begin{align}
  \label{eq:10.36}
  I_{\text{CP}}^\approx &= I_j \sin(\omega_j t + \varphi_0)
  \intertext{mit Kreisfrequenz}
  \label{eq:10.37}
  \omega_j &= \frac{2eU}{\hbar}
\end{align}
Dies ist der \acct{Josephson-Wechselstrom-Effekt}.

\begin{example}
  Bei einer Spannung von $U = \SI{100}{\micro\V}$ liegt die Frequenz
  bei $\nu = \SI{48}{\GHz}$.  Nach \eqref{eq:10.37} ist die Beziehung
  nur von $e/h$ abhängig.  Wenn $e/h$ bekannt ist, kann man ein
  Spannungsnormal mit hoher Präzision erzeugen.
\end{example}


Bringt man Josephson-Kontakte ins Magnetfeld kommt es zu einer
makroskopischen Quanteninterferenz.  Ein durch einen supraleitenden
Kreis mit zwei Kontakten hindurchtretendes Magnetfeld bewirkt, dass
der Suprastrom Interferenzeffekte als Funktion der
Magnetfeldintensität zeigt. Dieser Effekt wird genutzt für
empfindliche Magnetometer, sogenannte SQUIDs\footnote{Superconductive
  Quantum Interference Device}, mit denen Magnetfelder bis
\SI{e-14}{\tesla} zuverlässig detektiert werden können.

\section{Supraleiter zweiter Art}

Bisher haben wir angenommen, dass Supraleiter bis zu einer kritischen
Feldstärke $B_{C1}$ supraleitend sind und darüber in den
normalleitenden Zustand übergehen.  Dieses Verhalten wird so für
Supraleiter erster Art gefunden.  Es gibt allerdings auch Supraleiter
zweiter Art.  Für geringe Magnetfelder verhalten sie sich genau wie
Supraleiter erster Art.  Wird die Feldstärke jedoch über $B_{C1}$
erhöht bricht die Supraleitung nicht sofort zusammen, das Feld kann
aber trotzdem teilweise eindringen.  Dieses teilweise Eindringen
bezeichnet man als \acct{Shubnikov-Phase}.  Erst überhalb der zweiten
kritischen Fedlstärke $B_{C2}$ wird ein Supraleiter zweiter Art wieder
normalleitend.  Dabei kann $B_{C2}$ bis zu $100 \cdot B_{C1}$
sein.  Der Rekord liegt bei $B_{C2} = \SI{60}{\tesla}$.  Dies ist
nützlich für die technischen Nutzung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,scale=1.5]
    \begin{scope}
      \draw[<->] (0,2) node[left] {$-M$} |- (3,0) node[right] {$B$};
      \node[Purple] at (1.5,2.5) {Supraleiter 1. Art};
      \draw[MidnightBlue] (0,0) -- (1,1);
      \draw[MidnightBlue,dashed] (1,1) -- (1,0) node[below] {$B_C$};
      \node[below,text width=3cm] at (1.5,-.5) {idealer Diamagnet \[ \bm M = - \bm B \]};
    \end{scope}
    \begin{scope}[shift={(4,0)}]
      \fill[MidnightBlue!20] (1.5,0) -- (1.5,1.5) -- (1,1) to[out=-45,in=180] (2.8,0) -- cycle;
      \draw[<->] (0,2) node[left] {$-M$} |- (3,0) node[right] {$B$};
      \node[Purple] at (1.5,2.5) {Supraleiter 2. Art};
      \draw[MidnightBlue] (0,0) -- (1,1) to[out=-45,in=180] (2.8,0) node[below] {$\strut B_{C2}$};
      \draw[MidnightBlue,dotted] (1,1) -- (2,2);
      \draw[MidnightBlue,dashed] (1,1) -- (1,0) node[below] {$\strut B_{C1}$};
      \draw[MidnightBlue,dashed] (1.5,1.5) -- (1.5,0) node[below] {$\strut B_{C,\text{th}}$};
      \draw[DarkOrange3,|<->|] (0,-.5)
      -- node[below,text width=1.5cm,align=center] {Meissner-Phase $B = 0$} (1,-.5);
      \draw[DarkOrange3,<->|] (1,-.5)
      -- node[below,text width=2.5cm,align=center] {Shubnikov-Phase $B\ne0$} (3,-.5);
      \draw[DarkOrange3,|<->|] (0,-1.5)
      -- node[below,text width=5cm,align=center] {supraleitend\\(bei Flusswirbel Verankerung)} (3,-1.5);
    \end{scope}
  \end{tikzpicture}
  \caption{Vergleich von Supraleitern erster und zweiter Art.}
  \label{fig:13.1}
\end{figure}

Warum gibt es Supraleiter zweiter Art?  Die Erklärung hierfür gibt die
Betrachtung der Grenzfläche zwischen Normalleiter und Supraleiter im
Rahmen der Ginzburg-Landau-Theorie (thermodynamische
Betrachtung). Essenziell gibt es zwei \enquote{Gegenspieler}.  Zum
einen verkleinern Grenzflächen die \acct{Kondensationsenergie}, da
dort die Cooper-Paar-Dichte reduziert ist.  Zum anderen leistet der
Supraleiter bei Anlegen eines Magnetfeldes \acct{Verdrängungarbeit}.
Grenzflächen reduzieren die Verdrängungarbeit, da dort diese Energie
nicht aufgebracht werden muss.  Aus der Ginzburg-Landau-Theorie erhält
man
\begin{equation}
  \label{eq:10.38}
  \Delta E_{\text{Grenz}} = \Delta E_{\text{Kon}} - \Delta E_{\text{Verdräng}}
  = (\xi_{GL} - \lambda_L) A \frac{B_{C,+k}^2}{2 \mu_B} \; .
\end{equation}
Die Kohärenzlänge $\xi_{GL}$, spiegelt die charakteristische Länge
wieder, über die sich die Wellenfunktion ändern kann.  Ist $\xi_{GL} >
\lambda$, so ist $\Delta E_G$ immer positiv und die Ausbildung von
Grenzflächen wird unterdrückt. Es handelt sich um einen Supraleiter
erster Art. Ist hingegen $\xi_{GL} < \lambda$ so wird die Ausbildung
von Grenzflächen günstiger und es liegt ein Supraleiter zweiter Art
vor.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,0) rectangle (4,3);
    \draw (1,0) -- node[pos=.9,left] {NL} node[pos=.9,right] {SL} (1,3);
    \draw[MidnightBlue] (0,2) node[left] {$B_a$} -- (1,2) to[out=0,in=180] coordinate[pos=.3] (A) (2.5,0);
    \draw[DarkOrange3] (4,1.5) node[right] {$n_S$} -- (2.5,1.5) to[out=180,in=0] coordinate[pos=.3] (B) (1,0);
    \draw[MidnightBlue,dotted] (A) -- (A |- 0,0);
    \draw[DarkOrange3,dotted] (B) -- (B |- 0,0);
    \coordinate[pin={[MidnightBlue]below left:$\lambda_L$}] (C) at (A |- 0,0);
    \coordinate[pin={[DarkOrange3]below right:$\xi_{GL}$}] (D) at (B |- 0,0);
  \end{tikzpicture}
  \caption{Verlauf des Magnetfeldes und der Cooper-Paar-Dichte in der
    Nähe der Grenzfläche von Normalleiter und Supraleiter.}
  \label{fig:13.2}
\end{figure}

Aus einer genaueren Rechnung kann man den Ginzburg-Landau-Parameter
$\kappa$ erhalten für den gilt
\begin{equation*}
  \kappa = \frac{\lambda_L}{\xi_{GL}} \; .
\end{equation*}
Mit seiner Hilfe kann man exakt zwischen Supraleitern erster und
zweiter Art unterscheiden.
\begin{align*}
  \kappa &< \frac{1}{\sqrt{2}} \to \text{Supraleiter 1. Art} \; , \\
  \kappa &> \frac{1}{\sqrt{2}} \to \text{Supraleiter 2. Art} \; .
\end{align*}
Das Eindringen von $B$ erfolgt durch dünne Flussschläuche. Jeder
Flussschlauch trägt ein Flussquant $\phi = h/2e$.  In perfekten
Kristallen ordnen sich die Flussschläuche regelmäßig an.  Man nennt
das entstehende Muster \acct{Abrikosov-Struktur}.

\section{Hochtemperatur-Supraleiter}

Der wichtigste Fortschritt in der Erforschung der
Hochtemperatur-Supraleitung wurde von J.\,G.\ Beduouz und K.\,A.\
Müller in Form von \ch{BaLuCuO} mit $T_C = \SI{30}{\K}$ geliefert.
Ihre Entdeckung wurde 1986 mit dem Nobelpreis geehrt.

Die \ch{CuO2}-Ebenen der Materialien in Tabelle~\ref{tab:13.1} stehen
senkrecht zur $c$-Achse und sind für die Ausbildung der Supraleitung
verantwortlich.  Die bisher gefunden Materialien sind spröde.  Die
Kohärenzlänge $\xi_{GL}$ ist mit $\approx \SI{1}{\nano\m}$ sehr klein.
Die Ursache für Hochtemperatur-Supraleitung ist noch nicht geklärt, es
werden magnetische Wechselwirkungen vermutet.

\begin{table}[tb]
  \centering
  \begin{tabular}{rll}
    \toprule
    \ch{BaLuCuO} & $T_C = \SI{30}{\K}$ \\
    \ch{La_{1.85}Ba_{0.15}CuO4} & $T_C = \SI{36}{\K}$ & (LBCO)\\
    \ch{YBa2Cu3O7} & $T_C = \SI{90}{\K}$ & (YBCO) \\
    \ch{Tl2Ba2Ca2Cu3O10} & $T_C = \SI{120}{\K}$ & (TBCO) \\
    \ch{Tl2Ba2Ca2Cu3O_{8.33}} & $T_C = \SI{138}{\K}$ \\
    \bottomrule
  \end{tabular}
  \caption{Einige Hochtemperatur-Supraleiter und ihre Sprungtemperaturen.}
  \label{tab:13.1}
\end{table}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: