#!/usr/bin/env perl

@default_files = ('ExPhys6', 'ExPhys6-booklet');

$pdf_mode = 1;
$postscript_mode = $dvi_mode = 0;
$clean_full_ext = 'bbl run.xml thm'
