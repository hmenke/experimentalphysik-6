# COMPILING

If you already have a *full* installation you can simply change to the source
directory and run

    latexmk

For detailed instructions on the Ubuntu or the Windows operating system see
below.

## Ubuntu

Open a terminal and type

    sudo apt-get install texlive-full

This will install the full TeXlive distribution.  You can then compile the
source simply using

    latexmk

## Windows

If you want to save yourself some hours of inconvenience switch to GNU/Linux.

If you still want to build the script on Windows go to http://www.miktex.org/
and obtain the latest release of MikTeX.

No matter if you already had MikTeX installed or just installed it, start the
"Package Manager" and go press "Repository->Synchronize".  Then start the
"Update" utility of MikTeX and update all components.  Then start again the
"Package Manager" and install *all* of the following packages.

    babel-german
    beamer
    biblatex
    booktabs
    braket
    chemfig
    chemformula
    csquotes
    currfile
    eso-pic
    etoolbox
    extsizes
    filehook
    fpl
    hobby
    imakeidx
    koma-script
    l3kernel
    l3packages
    logreq
    marvosym
    mathpazo
    mathtools
    microtype
    mptopdf
    ms
    ntheorem
    pdfpages
    pgf
    pgfplots
    siunitx
    todonotes
    units
    url
    xcolor
    xpatch

Now press the Windows key and "R" to get start the "Run" dialog.  Enter "cmd"
to start the command line.  Use "cd" to change directories to the path where
you put the source.  In that directory run the following statements in that
order

     "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin\pdflatex.exe" ExPhys6.tex
     "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin\biber.exe" ExPhys6
     "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin\pdflatex.exe" ExPhys6.tex
     "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin\pdflatex.exe" ExPhys6.tex
     "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin\pdflatex.exe" ExPhys6-booklet.tex

## Remarks

The official release version of the script uses the Lucida fonts in terms of
the lucimatx package.  All spacing was optimised for that font.  If you don't
use the Lucida font, expect overfull and underfull boxes.

Building in dvi-mode will fail, due to the microtype package.

Building was tested with TeXlive 2014.